﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.SessionState;
using System.Web.Services;
using DrCitas.Servicios;
using System.Text.RegularExpressions;
using DrCitas.Controlador;

namespace Cuidamet.DrCitas
{
    /// <summary>
    /// Summary description for DrCitas
    /// </summary>
    [WebService(Namespace = "Cuidamet.DrCitas")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class DrCitas : System.Web.Services.WebService
    {

        #region Atributos
            private string session = "";
        #endregion
        
        #region GET

            [WebMethod]
            public void GetCiudadesByIdEstado(string obj)
            {
                FuncionesGlobales.GetJsonp(new Controlador().GetCiudadesXEstado(obj), false);
            }
    
            /*
            [WebMethod(EnableSession = true)]
            public void GetClientes_(int pIdEspecialidad, int pIdCiudad, string pFechaInicio, int pInicio, int pIdUsuarioX, int pag, string sexo, string tipoConsulta, string nombre, string idCliente) {
                checkSession(pIdEspecialidad);

                string res = new Controlador().GetListDr(pIdEspecialidad, pIdCiudad, pFechaInicio, pInicio, sexo, tipoConsulta, nombre, idCliente, pIdUsuarioX, pag, this.session);

                HttpContext.Current.Session["SESSION"] = Regex.Match(res, @"(\w*\-(\w*){5}\-(\w*){5}\-(\w*){5}\-\w*)");
                HttpContext.Current.Session["ESPECIALIDAD"] = pIdEspecialidad;
                HttpContext.Current.Session["OBJMEDICOS"] = res;

                FuncionesGlobales.GetJsonp(res, false);
            }
            */

            [WebMethod]
            public void GetListaMedicos(string obj) {
                FuncionesGlobales.GetJsonp(new Controlador().GetListDr(obj), false);
            }

            [WebMethod(EnableSession = true)]
            public void GetConsultorios(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorMedico().ListarConsultorios(obj), false);
            }

            [WebMethod]
            public void GetEspecialidades() {
                FuncionesGlobales.GetJsonp(new Controlador().GetEspecialidades(), false);
            }
            
            [WebMethod]
            public void GetEspecialidadesXCliente(string obj)
            {
                FuncionesGlobales.GetJsonp(new Controlador().GetEspecialidadesXCliente(obj), false);
            }
            
            [WebMethod]
            public void GetPaises()
            {
                FuncionesGlobales.GetJsonp(new Controlador().GetPaises(), false);
            }

            [WebMethod]
            public void GetEstados() {
                FuncionesGlobales.GetJsonp(new Controlador().GetEstados(), false);
            }

            [WebMethod]
            public void GetEstadosXPais(string obj) {
                FuncionesGlobales.GetJsonp(new Controlador().GetEstadosXPais(obj), false);
            }

            [WebMethod]
            public void GetCiudadesXEstado(string obj) {
                FuncionesGlobales.GetJsonp(new Controlador().GetCiudadesXEstado(obj), false);
            }

            [WebMethod]
            public void GetNombreEstadoCiudad(string obj) {
                FuncionesGlobales.GetJsonp(new Controlador().GetNombreEstadoCiudad(obj), false);
            }

            [WebMethod]
            public void GetLenguajes() {
                FuncionesGlobales.GetJsonp(new Controlador().GetLenguajes(), false);
            }

            [WebMethod]
            public void GetUniversidades() {
                FuncionesGlobales.GetJsonp(new Controlador().GetUniversidades(), false);
            }

            [WebMethod]
            public void GetClinicas(string obj) {
                FuncionesGlobales.GetJsonp(new Controlador().GetClinicasXCiudad(obj), false);
            }

            [WebMethod]
            public void GetInfUsuarioOlvidarContrasena(string obj) {
                FuncionesGlobales.GetJsonp(new Controlador().GetInfUsuarioOlvidarContrasena(obj).ToString(), false);
            }

            [WebMethod]
            public void GetListHorarios(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorMedico().GetHorarios(obj), false);
            }

            [WebMethod(EnableSession = true)]
            public void ExisteUsuario(string obj) {
                FuncionesGlobales.GetJsonp(new Controlador().ValidarUsuario(obj).ToString(), false);
            }

            [WebMethod(EnableSession = true)]
            public void ExisteCorreo(string obj) {
                FuncionesGlobales.GetJsonp(new Controlador().ValidarCorreo(obj).ToString(), false);
            }

            [WebMethod(EnableSession = true)]
            public void DatosAdicionalesPacienteTitular(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorPaciente().DatosAdicionalesPacienteTitular(obj), false);
            }

            [WebMethod(EnableSession = true)]
            public void DatosAdicionalesMedico(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorMedico().DatosAdicionalesMedico(obj), false);
            }

            [WebMethod]
            public void GetImagenPerfil(string obj) {
                FuncionesGlobales.GetJsonp(new Controlador().GetImagenPerfil(obj), false);
            }

            #region Sesion

                [WebMethod(EnableSession = true)]
                public void ValidarSesionUsuario(string obj) {
                    string resp = new ControladorSesiones().ValidarSesionUsuario(obj);
                    HttpContext.Current.Session["USUARIOACTUAL"] = resp;
                    FuncionesGlobales.GetJsonp(resp, false);
                }

                [WebMethod(EnableSession = true)]
                public void GetSession() {
                    FuncionesGlobales.GetJsonp( ((HttpContext.Current.Session["USUARIOACTUAL"] == null) ? "null" : HttpContext.Current.Session["USUARIOACTUAL"].ToString()), false);
                }

                [WebMethod(EnableSession = true)]
                public void DesconectarUsuario(string obj) {
                    FuncionesGlobales.GetJsonp("{\"SesionOut\":\"" + new ControladorSesiones().DesconectarSesion(obj).ToString() + "\"}", false);
                }

                [WebMethod(EnableSession = true)]
                public void GetSessionMedicos() {
                    if (HttpContext.Current.Session["OBJMEDICOS"] != null)
                        FuncionesGlobales.GetJsonp(HttpContext.Current.Session["OBJMEDICOS"].ToString(), false);
                }

                [WebMethod(EnableSession = true)]
                public void checkSession(int idEspecialidad) {
                    if (HttpContext.Current.Session["SESSION"] != null)
                        this.session = ((Int32.Parse(HttpContext.Current.Session["ESPECIALIDAD"].ToString()) == idEspecialidad) ? Session["SESSION"].ToString() : null);
                }

            #endregion Sesion

            #region Cita

                [WebMethod]
                public void CodigoConfirmacion(string obj) {
                    FuncionesGlobales.GetJsonp(new ControladorCitas().CodigoConfirmacion(obj).ToString(), false);
                }
                
                [WebMethod]
                public void GetDatosDeLaAgenda(int idAgenda, int pIdUsuarioX) {
                    FuncionesGlobales.GetJsonp(new ControladorCitas().GetDatosDeLaAgenda(idAgenda, pIdUsuarioX), false);
                }
    
                [WebMethod]
                public void GetListaDeRazones(int pIdEspecialidad) {
                    FuncionesGlobales.GetJsonp(new ControladorCitas().ListaDeRazones(pIdEspecialidad), false);
                }

                [WebMethod]
                public void GetInterrogatorioMedico(string obj) {
                    FuncionesGlobales.GetJsonp(new ControladorCitas().GetInterrogatorioMedico(obj), false);
                }

                [WebMethod]
                public void GetAsociados(string obj) {
                    FuncionesGlobales.GetJsonp(new ControladorCitas().GetAsociados(obj), false);
                }

                [WebMethod]
                public void GetMisAsociados(string obj) {
                    FuncionesGlobales.GetJsonp(new ControladorPaciente().GetMisAsociados(obj), false);
                }

                [WebMethod]
                public void GetListaCitasPaciente(string obj) {
                    FuncionesGlobales.GetJsonp(new ControladorCitas().GetListCitasPaciente(obj), false);
                }

                [WebMethod]
                public void GetListaCitasMedico(string obj) {
                    FuncionesGlobales.GetJsonp(new ControladorCitas().GetListCitasMedico(obj), false);
                }

                [WebMethod]
                public void GetEmpresasAsistentes(string obj) {
                    FuncionesGlobales.GetJsonp(new ControladorAsistente().GetMisEmpresas(obj), false);
                }

                [WebMethod]
                public void GetClinicasAsistentes(string obj) {
                    FuncionesGlobales.GetJsonp(new ControladorAsistente().GetMisClinicas(obj), false);
                }

                [WebMethod]
                public void GetMedicosAsistentes(string obj) {
                    FuncionesGlobales.GetJsonp(new ControladorAsistente().GetMisMedicos(obj), false);
                }

                [WebMethod]
                public void GetMedicosAsistente(string obj)
                {
                    FuncionesGlobales.GetJsonp(new ControladorAsistente().GetMedicosAsistentes(obj), false);
                }

                [WebMethod]
                public void GetMisMedicos(string obj) {
                    FuncionesGlobales.GetJsonp(new ControladorPaciente().GetMisMedicos(obj), false);
                }

                [WebMethod]
                public void GetMisPacientes(string obj) {
                    FuncionesGlobales.GetJsonp(new ControladorMedico().GetMisPacientes(obj), false);
                }

                [WebMethod]
                public void HorarioConsultorio(string obj)
                {
                    FuncionesGlobales.GetJsonp(new ControladorMedico().HorarioConsultorio(obj), false);
                }

                [WebMethod]
                public void GetDiagnostico(string obj) {
                    FuncionesGlobales.GetJsonp(new ControladorCitas().getDiagnostico(obj), false);
                }
            #endregion Cita

            [WebMethod]
            public void GetTC()
            {
                FuncionesGlobales.GetJsonp(new Controlador().GetTiposCliente(), false);
            } 

            [WebMethod]
            public void BuscarEmpresas(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().buscarEmpresas(obj), false);
            }

            [WebMethod]
            public void BuscarClinicas(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().buscarClinicas(obj), false);
            }

            [WebMethod]
            public void BuscarEspecialidades(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().buscarEspecialidades(obj), false);
            }

            [WebMethod]
            public void BuscarEspecialidadClinica(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().buscarEspecialidadClinica(obj), false);
            }

            [WebMethod]
            public void BuscarAsistenteEmpresa(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().buscarAsistenteEmpresa(obj), false);
            }

            [WebMethod]
            public void BuscarMedicoEmpresa(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().buscarMedicoEmpresa(obj), false);
            }

            [WebMethod]
            public void BuscarPacienteEmpresa(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().buscarPacienteEmpresa(obj), false);
            }

            [WebMethod]
            public void BuscarAsistenteClinica(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().buscarAsistenteClinica(obj), false);
            }

            [WebMethod]
            public void BuscarAsistenteConsultorio(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().buscarAsistenteConsultorio(obj), false);
            }

            [WebMethod]
            public void BuscarMedicoAsistente(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().buscarMedicoAsistente(obj), false);
            }

            [WebMethod]
            public void BuscarMedicosRevision(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().buscarMedicosRevision(obj), false);
            }

            [WebMethod]
            public void BuscarMedicosAprobacion(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().buscarMedicosAprobacion(obj), false);
            }

            [WebMethod]
            public void BuscarMedicos(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().buscarMedicos(obj), false);
            }

            [WebMethod]
            public void BuscarClientes(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().buscarClientes(obj), false);
            }

            [WebMethod]
            public void getEmpresas(string obj) {
                FuncionesGlobales.GetJsonp(new Controlador().getEmpresas(obj), false);
            }
            
            [WebMethod]
            public void _getEmpresas() {
                FuncionesGlobales.GetJsonp(new Controlador()._getEmpresa(), false);
            }

            [WebMethod]
            public void getClinicas(string obj) {
                FuncionesGlobales.GetJsonp(new Controlador().getClinicas(obj), false);
            }

            [WebMethod]
            public void getEspecialidades(string obj) {
                FuncionesGlobales.GetJsonp(new Controlador().getEspecialidades(obj), false);
            }

            [WebMethod]
            public void getConsultorios(string obj) {
                FuncionesGlobales.GetJsonp(new Controlador().getConsultorios(obj), false);
            }

            [WebMethod]
            public void getMedicos(string obj) {
                FuncionesGlobales.GetJsonp(new Controlador().getMedicos(obj), false);
            }

            [WebMethod]
            public void getDiagnosticoXEspecialidad(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorCitas().GetDiagnosticoXEspecialidad(obj), false);
            }
            
            #region Reportes

            [WebMethod]
            public void BuscarReporteGeneralCitas(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorReportes().getReporteGeneralCitas(obj), false);
            }

            [WebMethod]
            public void BuscarReporteCitas(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorReportes().getReporteCitas(obj), false);
            }

            [WebMethod]
            public void BuscarReporteDetalladoCitas(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorReportes().getReporteDetalladoCitas(obj), false);
            }

            [WebMethod]
            public void ExportarReporte(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorReportes().exportarReporte(obj), false);
            }

            #endregion Reportes

            [WebMethod]
            public void sendEmail(string obj) {
                FuncionesGlobales.GetJsonp(new Controlador().sendEmail(obj).ToString(), false);
            }
        #endregion GET

        #region POST

            [WebMethod(EnableSession = true)]
            public void ConectarUsuario(string obj) {
                string objSesion = new ControladorSesiones().ConectarUsuario(obj);
                HttpContext.Current.Session["USUARIOACTUAL"] = objSesion;
                FuncionesGlobales.GetJsonp(objSesion, false);
            }

            [WebMethod]
            public void EnviarSMSAlClienteXCita(int IdAgenda, string pCelular, string pMensaje, int pIdUsuarioX) {
                new Controlador().EnviarSMSAlClienteXCita(IdAgenda, pCelular, pMensaje, pIdUsuarioX);
            }

            [WebMethod(EnableSession = true)]
            public void RegistrarAdministrador(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().RegistrarAdministrador(obj), false);
            }

            [WebMethod(EnableSession = true)]
            public void RegistrarPaciente(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorPaciente().RegistrarPaciente(obj), false);
            }

            [WebMethod]
            public void RegistrarAsociado(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorCitas().RegistrarAsociado(obj).ToString(), false);
            }
      
            [WebMethod(EnableSession = true)]
            public void RegistrarMedico(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorMedico().RegistrarMedico(obj), false);
            }

            [WebMethod(EnableSession = true)]
            public void RegistrarAsistente(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAsistente().RegistrarAsistente(obj), false);
            }

            
            [WebMethod(EnableSession = true)]
            public void RegistrarUsuario(string obj)
            {
                FuncionesGlobales.GetJsonp(new ControladorAsistente().RegistrarAsistente(obj), false);
            }

            [WebMethod]
            public void ReservarCita(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorCitas().ReservarCita(obj).ToString(), false);
            }

            [WebMethod]
            public void CrearDiagnostico(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorCitas().crearDiagnostico(obj).ToString(), false);
            }

            [WebMethod]
            public void RegistararConsultorio(string obj)
            {
                FuncionesGlobales.GetJsonp(new ControladorMedico().RegistrarConsultorio(obj), false);
            }

            [WebMethod]
            public void RegistrarHorarios(string obj)
            {
                FuncionesGlobales.GetJsonp(new ControladorMedico().RegistrarHorarios(obj), false);
            }

            [WebMethod]
            public void GuardarImagenPerfil(string obj) {              
                FuncionesGlobales.GetJsonp(new Controlador().GuardarImagenPerfil(obj), false);
            }

            [WebMethod]
            public void AgregarEmpresa(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().agregarEmpresa(obj), false);
            }

            [WebMethod]
            public void AgregarClinica(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().agregarClinica(obj), false);
            }

            [WebMethod]
            public void AgregarEspecialidad(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().agregarEspecialidad(obj), false);
            }

            [WebMethod]
            public void AsociarEspecialidadClinica(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().asociarEspecialidadClinica(obj), false);
            }

            [WebMethod]
            public void AsociarAsistenteEmpresa(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().asociarAsistenteEmpresa(obj), false);
            }

            [WebMethod]
            public void AsociarMedicoEmpresa(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().asociarMedicoEmpresa(obj), false);
            }

            [WebMethod]
            public void AsociarPacienteEmpresa(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().asociarPacienteEmpresa(obj), false);
            }

            [WebMethod]
            public void AsociarAsistenteClinica(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().asociarAsistenteClinica(obj), false);
            }

            [WebMethod]
            public void PermitirAccesoConsultorio(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().permitirAccesoConsultorio(obj), false);
            }

            [WebMethod]
            public void AsociarMedicoAsistente(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().asociarMedicoAsistente(obj), false);
            }
            
        #endregion POST

        #region PUT

            [WebMethod]
            public void CambioUsuario(string obj) {
                FuncionesGlobales.GetJsonp(new Controlador().ActualizarNombreUsuario(obj).ToString(), false);
            }

            [WebMethod]
            public void CambioContrasena(string obj) {
                FuncionesGlobales.GetJsonp(new Controlador().ActualizarContrasenaUsuario(obj).ToString(), false);
            }

            [WebMethod]
            public void ActualizarDatosBasicosPaciente(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorPaciente().UpdateDatosBasicosPaciente(obj).ToString(), false);
            }

            [WebMethod]
            public void ActualizarDatosBasicosMedico(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorMedico().UpdateDatosBasicosMedico(obj).ToString(), false);
            }

            [WebMethod]
            public void ActualizarOtrosDatosMedico(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorMedico().UpdateOtrosDatosMedico(obj).ToString(), false);
            }

            [WebMethod]
            public void ActualizarConsultorio(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorMedico().ActualizarConsultorio(obj).ToString(), false);
            }

            [WebMethod]
            public void DesactivarHorarios(string obj) 
            {
                FuncionesGlobales.GetJsonp(new ControladorMedico().DesactivarHorarios(obj), false);
            }

            [WebMethod]
            public void DesactivarHoras(string obj)
            {
                FuncionesGlobales.GetJsonp(new ControladorMedico().DesactivarHoras(obj), false);
            }

            [WebMethod]
            public void ActualizarEstadoCita(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorCitas().actualizarEstadoCita(obj).ToString(), false);
            }

            [WebMethod]
            public void ActualizarDiagnostico(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorCitas().actualizarDiagnostico(obj).ToString(), false);
            }

            [WebMethod]
            public void ActualizarEmpresa(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().actualizarEmpresa(obj), false);
            }

            [WebMethod]
            public void EliminarEmpresa(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().eliminarEmpresa(obj), false);
            }

            [WebMethod]
            public void ActualizarClinica(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().actualizarClinica(obj), false);
            }

            [WebMethod]
            public void EliminarClinica(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().eliminarClinica(obj), false);
            }

            [WebMethod]
            public void ActualizarEspecialidad(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().actualizarEspecialidad(obj), false);
            }

            [WebMethod]
            public void EliminarEspecialidad(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().eliminarEspecialidad(obj), false);
            }

            [WebMethod]
            public void AprobarRevisionMedico(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().aprobarRevisionMedico(obj), false);
            }

            [WebMethod]
            public void AprobarMedico(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().aprobarMedico(obj), false);
            }

            [WebMethod]
            public void NegarMedico(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().negarMedico(obj), false);
            }

            [WebMethod]
            public void SolicitarRevisionRegistro(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorMedico().SolicitarRevisionRegistro(obj), false);
            }

        #endregion PUT

        #region DELETE

            [WebMethod]
            public void EliminarEspecialidadMedico(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorMedico().EliminarEspecialidadMedico(obj), false);
            }

            [WebMethod]
            public void EliminarAtributoMedico(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorMedico().EliminarAtributoMedico(obj), false);
            }

            [WebMethod]
            public void CancelarCita(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorCitas().CancelarCita(obj), false);
            }

            [WebMethod]
            public void DesasociarEspecialidadClinica(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().desasociarEspecialidadClinica(obj), false);
            }

            [WebMethod]
            public void DesasociarAsistenteEmpresa(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().desasociarAsistenteEmpresa(obj), false);
            }

            [WebMethod]
            public void DesasociarMedicoEmpresa(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().desasociarMedicoEmpresa(obj), false);
            }

            [WebMethod]
            public void DesasociarPacienteEmpresa(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().desasociarPacienteEmpresa(obj), false);
            }

            [WebMethod]
            public void DesasociarAsistenteClinica(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().desasociarAsistenteClinica(obj), false);
            }

            [WebMethod]
            public void RetirarAccesoConsultorio(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().retirarAccesoConsultorio(obj), false);
            }

            [WebMethod]
            public void DesasociarMedicoAsistente(string obj) {
                FuncionesGlobales.GetJsonp(new ControladorAdministrador().desasociarMedicoAsistente(obj), false);
            }

        #endregion DELETE

        [WebMethod]
        public void cargarClientes(string obj) {
            FuncionesGlobales.GetJsonp(new ControladorAdministrador().cargarClientes(obj), false);
        }

        [WebMethod]
        public string test(string t, string cadena) {

            if (Int32.Parse(t) == 0)
                return FuncionesGlobales.Decrypt(cadena);
            else
                return FuncionesGlobales.Encrypt(cadena);
        }

        [WebMethod]
        public string test2(string t, string cadena) {

            if (Int32.Parse(t) == 0)
                return FuncionesGlobales.Decry64Decry(cadena);
            else
                return FuncionesGlobales.Encry64Encry(cadena);
        }

        [WebMethod]
        public void RegistrarClientesTitularesCuidamed(string obj) {
            FuncionesGlobales.GetJsonp(new ControladorAdministrador().RegistrarClientesTitularesCuidamed(), false);
        }

        [WebMethod]
        public void RegistrarCompaniaClientesAsociadosCuidamed(string obj) {
            FuncionesGlobales.GetJsonp(new ControladorAdministrador().RegistrarCompaniaClientesAsociadosCuidamed(), false);
        }

        /*Methodos para cuidanet*/


        [WebMethod]
        public void obtenerClienteCuidanet(string obj) 
        {
            FuncionesGlobales.GetJsonp(new ControladorCuidanet().obtenerClienteCuidanet(obj));
        }
    }
}