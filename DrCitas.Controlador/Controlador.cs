﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrCitas.Servicios;
using DrCitas.Dtos;
using System.Data;
using System.Text.RegularExpressions;


namespace DrCitas.Servicios
{
    public class Controlador
    {

        #region Atributos y constructor

        private string StringConnDrCitas { get; set; }

        public Controlador()
        {
            switch (System.Net.Dns.GetHostName().ToUpper()) {
                case "PC-DESARROLLO":
                case "CMEDC0000040-PC":
                case "CMEDC000050":
                    StringConnDrCitas = "C3CqH6HFwPWPTcGcEbmpX4cFEqpWRq8y+k5hNGV2tqhgLmYgnPJrYW7qz+6JMfJ9F+VggSAODZ03/2otJ5kgMlmYcSe9rYjh7l52pMvL6kN6M7bN+JBA+DwFwn7JsV8/";
                    break;
                default:
                    StringConnDrCitas = "bLaTQuRW9H3+d1sT6jsrwVsPYMIqUyAEbH0XboWU6uwmeliOa9KCXqBPuSWSl3KA/jCZrNAiQ/H0osgdldTIL7jFjwLv8fOa3Cn1fhLIjJCJ8QH86lNjSzIB3Le/StLh";
                    break;
            }
        } 

        #endregion

        #region Especialidades, Estados, paises, idiomas

        public string GetEspecialidades() 
            {
                try
                {
                    return FuncionesGlobales.JsonSerializer<List<dtoListaDeValores>>( new ServicioListaDeValores().ListaDeEspecialidades("M") ); 
                }
                catch (Exception ex)
                {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            public string _getEmpresa()
            {
                try
                {
                    return FuncionesGlobales.JsonSerializer<List<dtoListaDeValores>>(new ServicioListaDeValores().ListaDeEmpresas());
                }
                catch (Exception ex)
                {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace, innerException = ex.InnerException.ToString() });
                }
            }
    
            public string GetUniversidades() 
            {
                 try
                {
                    return FuncionesGlobales.JsonSerializer<List<dtoListaDeValores>>(new ServicioListaDeValores().ListaUniversidades());
                }
                catch (Exception ex)
                {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            /// <summary>
            /// Obtiene los pacientes del usuario medico conectado
            /// </summary>
            public string GetPaises() {
                try {
                    return FuncionesGlobales.JsonSerializer<List<dtoListaDeValores>>(new ServicioListaDeValores().ListaPaises());
                }
                catch (Exception ex) {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            public string GetEstados() {
                try {
                    return FuncionesGlobales.JsonSerializer<List<dtoListaDeValores>>(new ServicioListaDeValores().ListaDeEstados());
                }
                catch (Exception ex) {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            public string GetEstadosXPais(string idPais)
            {
                try
                {
                    return FuncionesGlobales.JsonSerializer<List<dtoListaDeValores>>(new ServicioListaDeValores().ListaDeEstados(Convert.ToInt32(idPais)));
                }
                catch (Exception ex)
                {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            public string GetCiudadesXEstado(string idEstado)
            {
                try
                {
                    return FuncionesGlobales.JsonSerializer<List<dtoListaDeValores>>(new ServicioListaDeValores().ListaDeCiudades(Convert.ToInt32(idEstado)));
                }
                catch (Exception ex)
                {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            public string GetNombreEstadoCiudad(string obj) 
            {
                try
                {
                    EstadoCiudad ec = FuncionesGlobales.ChangeObjTo<EstadoCiudad>(obj);
                    return FuncionesGlobales.JsonSerializer<EstadoCiudad>(new ServicioListaDeValores().GetEstadoCiudad(ec));
                }
                catch (Exception ex)
                {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }           

            public string GetClinicasXCiudad(string ciudad) 
            {
                try
                {
                    return FuncionesGlobales.JsonSerializer<List<dtoListaDeValores>>(new ServicioListaDeValores().ListaCLinicasXciudad(Convert.ToInt32(FuncionesGlobales.DecryptFromBase64(ciudad))));
                }
                catch (Exception ex)
                {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            public string GetLenguajes() 
            {
                try
                {
                    return FuncionesGlobales.JsonSerializer<List<dtoListaDeValores>>(new ServicioListaDeValores().ListaDeLenguajes());
                }
                catch (Exception ex)
                {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            public string GetEmpresas() {
                try {
                    return FuncionesGlobales.JsonSerializer<List<dtoListaDeValores>>(new ServicioListaDeValores().ListaDeEmpresas());
                }
                catch (Exception ex) {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            public string GetTiposCliente() {
                try {
                    return FuncionesGlobales.JsonSerializer<List<dtoListaDeValores>>(new ServicioListaDeValores().ListaTiposCliente());
                }
                catch (Exception ex) {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            public string GetEstatusCita() {
                try {
                    return FuncionesGlobales.JsonSerializer<List<dtoListaDeValores>>(new ServicioListaDeValores().ListaEstatusCita());
                }
                catch (Exception ex) {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            public string GetEspecialidadesXCliente(string obj) {

                string param = "";
                int idCliente = 0;

                try {

                    param = FuncionesGlobales.DecryptFromBase64(obj);

                    idCliente =
                        (
                            (!Regex.IsMatch(param, @"(\w{8}\-\w{4}\-\w{4}\-\w{4}\-\w{12})"))
                            ? Convert.ToInt32(FuncionesGlobales.Decry64Decry(param))
                            : Convert.ToInt32(FuncionesGlobales.Decry64Decry((new ServicioCliente().GetDatosCliente(param).idCliente)))
                       );

                    return FuncionesGlobales.JsonSerializer<List<dtoListaDeValores>>(new ServicioListaDeValores().ListaDeEspecialidadesDelCliente(idCliente));
                }
                catch (Exception ex) {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

        #endregion

        #region Datos Dr

            public string GetListDr(string obj) {
                dtoBusqueda datos = null;

                try {
                    datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                    return FuncionesGlobales.ChangeObjFrom<dtoPagMedicos>(new ServicioCliente().GetListDr(datos, FuncionesGlobales.Decrypt(this.StringConnDrCitas)));
                }
                catch (Exception ex) {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            } 
        
        #endregion

        #region olvido contrseña

            public bool GetInfUsuarioOlvidarContrasena(string obj)
            {
                ServicioCliente servicio = null;
                dtoUsuarioBody lUsuarioB = null;
                dtoEnviarInfUsuario infUser = null;
                string param = "";

                try
                {
                    servicio = new ServicioCliente();

                    lUsuarioB = FuncionesGlobales.ChangeObjTo<dtoUsuarioBody>(obj);

                    if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(lUsuarioB.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                        lUsuarioB.param = servicio.GetUsuario(param);
                    else
                        lUsuarioB.param = FuncionesGlobales.Enc(lUsuarioB.param);

                    lUsuarioB.Body = FuncionesGlobales.DecryptFromBase64(lUsuarioB.Body);

                    infUser = servicio.GetInfUsuario(lUsuarioB.param);
                    lUsuarioB.Body = lUsuarioB.Body.Replace("@pNombreCliente", infUser.Nombre);
                    lUsuarioB.Body = lUsuarioB.Body.Replace("@pKey", infUser.param);

                    return ServicioUtilidad.NotificarAlCliente("Recuperar su Constraseña DrCitas.com", lUsuarioB.Body, infUser.Correo);

                    // return FuncionesGlobales.JsonSerializer<dtoEnviarInfUsuario>(infUser);
                }
                catch (Exception)
                {
                    //return "{\"Mensage\": \"" + ex.Message + "\", \"InnerException\":\"" + ex.InnerException + "\"}";
                    return false;
                }
            }


            public string ActualizarNombreUsuario(string obj) {
                ServicioCliente servicio = null;
                dtoCambioUsuario datos = null;
                string param = "";

                try {
                    servicio = new ServicioCliente();
                    datos = FuncionesGlobales.ChangeObjTo<dtoCambioUsuario>(obj);

                    datos.newUser = FuncionesGlobales.Enc(datos.newUser);

                    if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                        param = servicio.GetDatosCliente(param).idCliente;

                    return servicio.ActualizarNombreUsuario(datos, Convert.ToInt32(FuncionesGlobales.Decry64Decry(param))).ToString();
                }
                catch (Exception ex) {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace, innerException = ex.InnerException.ToString() });
                }
            }

            public string ActualizarContrasenaUsuario(string obj)
            {
                ServicioCliente servicio = null;
                dtoCambioUsuario datos = null;
                string param = "";

                try
                {
                    servicio = new ServicioCliente();
                    datos = FuncionesGlobales.ChangeObjTo<dtoCambioUsuario>(obj);

                    if (!string.IsNullOrEmpty(datos.oldPassword))
                        datos.oldPassword = FuncionesGlobales.Enc(datos.oldPassword);
                    datos.newPassword = FuncionesGlobales.Enc(datos.newPassword);

                    if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                        param = servicio.GetDatosCliente(param).idCliente;

                    return servicio.ActualizarContrasenaUsuario(datos, Convert.ToInt32(FuncionesGlobales.Decry64Decry(param))).ToString();
                }
                catch (Exception ex)
                {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace, innerException = ex.InnerException.ToString() });
                }
            }

        #endregion

        #region Datos Extras, Existencia P & D

            public bool ValidarUsuario(string user) {
                return new ServicioCliente().ExistUser(FuncionesGlobales.Enc(user));
            }

            public bool ValidarCorreo(string email) {
                return new ServicioCliente().ExistEmail(FuncionesGlobales.Enc(email));
            }

        #endregion

        public void EnviarSMSAlClienteXCita(int IdAgenda, string pCelular, string pMensaje, int pIdUsuarioX) {
            try {
                ServicioUtilidad.EnviarSMSAlCliente(IdAgenda, pCelular, pMensaje, pIdUsuarioX);
            }
            catch (Exception) {

            }
        }

        public string GuardarImagenPerfil(string obj) {
            string param = "";
            dtoImagen img = null;
            
            try {
                img = FuncionesGlobales.ChangeObjTo<dtoImagen>(obj);

                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(img.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioCliente().guardarImagenPerfil(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)), img.imagen, img.extension).ToString();
                else
                    return new ServicioCliente().guardarImagenPerfil(param, img.imagen, img.extension).ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace});
            }
        }

        public string GetImagenPerfil(string obj) {
            string param = "";
            
            try {
                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(obj)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioCliente().getImagenPerfil(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)));
                else
                    return new ServicioCliente().getImagenPerfil(param);
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace, innerException = ex.InnerException.ToString() });
            }
        }
        
        public string getEmpresas(string obj) {
            string param = "";
            List<dtoEmpresa> datos = null;

            try {
                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(obj)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    datos = new ServicioCliente().getEmpresas(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)));
                else
                    datos = new ServicioCliente().getEmpresas(param);

                return FuncionesGlobales.ChangeObjFrom<List<dtoEmpresa>>(datos);
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace, innerException = ex.InnerException.ToString() });
            }
        }

        public string getClinicas(string obj) {
            string param = "";
            dtoBusqueda datosBusq = null;
            List<dtoClinica> datos = null;

            try {
                datosBusq = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datosBusq.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    datos = new ServicioCliente().getClinicas(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)), datosBusq.idEmpresa);
                else
                    datos = new ServicioCliente().getClinicas(param, datosBusq.idEmpresa);

                return FuncionesGlobales.ChangeObjFrom<List<dtoClinica>>(datos);
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace, innerException = ex.InnerException.ToString() });
            }
        }

        public string getEspecialidades(string obj) {
            string param = "";
            dtoBusqueda datosBusq = null;
            List<dtoEspecialidad> datos = null;

            try {
                datosBusq = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datosBusq.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    datos = new ServicioCliente().getEspecialidades(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)), datosBusq.idEmpresa, datosBusq.idClinica);
                else
                    datos = new ServicioCliente().getEspecialidades(param, datosBusq.idEmpresa, datosBusq.idClinica);

                return FuncionesGlobales.ChangeObjFrom<List<dtoEspecialidad>>(datos);
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace, innerException = ex.InnerException.ToString() });
            }
        }

        public string getConsultorios(string obj) {
            string param = "";
            dtoBusqueda datosBusq = null;
            List<dtoConsultorio> datos = null;

            try {
                datosBusq = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datosBusq.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    datos = new ServicioCliente().getConsultorios(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)), datosBusq.idEmpresa, datosBusq.idClinica);
                else
                    datos = new ServicioCliente().getConsultorios(param, datosBusq.idEmpresa, datosBusq.idClinica);

                return FuncionesGlobales.ChangeObjFrom<List<dtoConsultorio>>(datos);
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace, innerException = ex.InnerException.ToString() });
            }
        }

        public string getMedicos(string obj) {
            string param = "";
            dtoBusqueda datosBusq = null;
            List<dtoClienteResume> datos = null;

            try {
                datosBusq = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datosBusq.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    datos = new ServicioCliente().getMedicos(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)), datosBusq.idEmpresa, datosBusq.idClinica, datosBusq.idEspecialidad, datosBusq.idConsultorio);
                else
                    datos = new ServicioCliente().getMedicos(param, datosBusq.idEmpresa, datosBusq.idClinica, datosBusq.idEspecialidad, datosBusq.idConsultorio);

                return FuncionesGlobales.ChangeObjFrom<List<dtoClienteResume>>(datos);
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace, innerException = ex.InnerException.ToString() });
            }
        }

        public string sendEmail(string obj) 
        {

            dtoDatosCorreo dtoDatosCorreo = null;

            try
            {
                dtoDatosCorreo = FuncionesGlobales.ChangeObjTo<dtoDatosCorreo>(obj);

                return ServicioUtilidad.EnviarCorreo("Codigo de confirmacion - Dr. Citas", dtoDatosCorreo.mensaje, "info@drcitas.com", dtoDatosCorreo.email, "smtp.drcitas.com", "info@drcitas.com", "Abcd1234").ToString();
            }
            catch (Exception ex)
            {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace});
            }
            
        }
    }

    public class dtoDatosCorreo {
        public string email { get; set; }
        public string mensaje { get; set; }
    }
}
