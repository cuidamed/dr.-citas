﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DrCitas.Servicios;
using DrCitas.Dtos;
using System.Data;

namespace DrCitas.Controlador {

    public class ControladorAdministrador {

        /// <summary>
        /// Registra a un administrador
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa el objeto que contiene los datos a registrar del administrador</param>
        /// <returns>Una cadena de caracteres que indica si se realizo o no el registro</returns>
        public string RegistrarAdministrador(string obj) {
            dtoRegistroCliente datos = null;

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoRegistroCliente>(obj);

                datos.Usuario = FuncionesGlobales.Enc(datos.Usuario);
                datos.Password = FuncionesGlobales.Enc(datos.Password);

                return new ServicioCliente().RegistrarCliente(datos).ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string buscarUsuarios(string obj) {
            dtoBusqueda datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return FuncionesGlobales.ChangeObjFrom<dtoPagClientes>(new ServicioAdministrador().buscarUsuarios(param, datos));

                return "";
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string cambiarEstadoUsuario(string obj) {
            dtoBusqueda datos = null;
            string param = "";
            
            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().cambiarEstadoUsuario(param, datos.list, datos.idEstatus).ToString();

                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace, innerException = ex.InnerException.ToString() });
            }
        }

        #region Empresa

        public string buscarEmpresas(string obj) {
            dtoBusqueda datos = null;

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                return FuncionesGlobales.ChangeObjFrom<List<dtoEmpresa>>(new ServicioAdministrador().buscarEmpresa(datos));
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string agregarEmpresa(string obj) {
            dtoEmpresa datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoEmpresa>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().agregarEmpresa(param, datos).ToString();
                
                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string actualizarEmpresa(string obj) {
            dtoEmpresa datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoEmpresa>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().actualizarEmpresa(param, datos, Convert.ToInt32(datos.target)).ToString();
                
                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string eliminarEmpresa(string obj) {
            dtoEmpresa datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoEmpresa>(obj);

                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().eliminarEmpresa(param, Convert.ToInt32(datos.target)).ToString();
                
                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        #endregion

        #region Clinica

        public string buscarClinicas(string obj) {
            dtoBusqueda datos = null;
            
            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                return FuncionesGlobales.ChangeObjFrom<List<dtoClinica>>(new ServicioAdministrador().buscarClinica(datos));
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }
        
        public string agregarClinica(string obj) {
            dtoClinica datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoClinica>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().agregarClinica(param, datos).ToString();

                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string actualizarClinica(string obj) {
            dtoClinica datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoClinica>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().actualizarClinica(param, datos, Convert.ToInt32(datos.target)).ToString();
                
                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string eliminarClinica(string obj) {
            dtoClinica datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoClinica>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().eliminarClinica(param, Convert.ToInt32(datos.target)).ToString();
                
                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        #endregion

        #region Especialidad

        public string buscarEspecialidades(string obj) {
            dtoBusqueda datos = null;

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                return FuncionesGlobales.ChangeObjFrom<List<dtoEspecialidad>>(new ServicioAdministrador().buscarEspecialidad(datos));
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }
        
        public string agregarEspecialidad(string obj) {
            dtoEspecialidad datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoEspecialidad>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().agregarEspecialidad(param, datos).ToString();

                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string actualizarEspecialidad(string obj) {
            dtoEspecialidad datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoEspecialidad>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().actualizarEspecialidad(param, datos, Convert.ToInt32(datos.target)).ToString();
                
                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string eliminarEspecialidad(string obj) {
            dtoEspecialidad datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoEspecialidad>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().eliminarEspecialidad(param, Convert.ToInt32(datos.target)).ToString();
                
                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        #endregion

        #region Cliente

        public string buscarClientes(string obj) {
            dtoBusqueda datos = null;

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                return FuncionesGlobales.ChangeObjFrom<dtoPagClientes>(new ServicioAdministrador().buscarCliente(datos));
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        #endregion

        public string buscarEspecialidadClinica(string obj) {
            string param = "";

            try {
                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(obj)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return FuncionesGlobales.ChangeObjFrom<List<dtoEspecialidad>>(new ServicioAdministrador().buscarEspecialidadClinica(Convert.ToInt32(param)));
                
                return "";
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string asociarEspecialidadClinica(string obj) {
            dtoBusqueda datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().asociarEspecialidadClinica(param, datos.list, Convert.ToInt32(datos.target)).ToString();
                
                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string desasociarEspecialidadClinica(string obj) {
            dtoBusqueda datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().desasociarEspecialidadClinica(param, datos.list, Convert.ToInt32(datos.target)).ToString();
                
                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string buscarAsistenteEmpresa(string obj) {
            dtoBusqueda datos = null;

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (!System.Text.RegularExpressions.Regex.IsMatch(FuncionesGlobales.DecryptFromBase64(datos.param), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return FuncionesGlobales.ChangeObjFrom<dtoPagClientes>(new ServicioAdministrador().buscarAsistenteEmpresa(datos));
                
                return "";
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string asociarAsistenteEmpresa(string obj) {
            dtoBusqueda datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))                    
                    return new ServicioAdministrador().asociarAsistenteEmpresa(param, datos.list, Convert.ToInt32(datos.target)).ToString();

                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string desasociarAsistenteEmpresa(string obj) {
            dtoBusqueda datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().desasociarAsistenteEmpresa(param, datos.list, Convert.ToInt32(datos.target)).ToString();

                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string buscarMedicoEmpresa(string obj) {
            dtoBusqueda datos = null;

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (!System.Text.RegularExpressions.Regex.IsMatch(FuncionesGlobales.DecryptFromBase64(datos.param), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return FuncionesGlobales.ChangeObjFrom<dtoPagClientes>(new ServicioAdministrador().buscarMedicoEmpresa(datos));
                
                return "";
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string asociarMedicoEmpresa(string obj) {
            dtoBusqueda datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().asociarMedicoEmpresa(param, datos.list, Convert.ToInt32(datos.target)).ToString();

                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string desasociarMedicoEmpresa(string obj) {
            dtoBusqueda datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().desasociarMedicoEmpresa(param, datos.list, Convert.ToInt32(datos.target)).ToString();

                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string buscarPacienteEmpresa(string obj) {
            dtoBusqueda datos = null;

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (!System.Text.RegularExpressions.Regex.IsMatch(FuncionesGlobales.DecryptFromBase64(datos.param), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return FuncionesGlobales.ChangeObjFrom<dtoPagClientes>(new ServicioAdministrador().buscarPacienteEmpresa(datos));
                
                return "";
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string asociarPacienteEmpresa(string obj) {
            dtoBusqueda datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().asociarPacienteEmpresa(param, datos.list, Convert.ToInt32(datos.target)).ToString();

                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string desasociarPacienteEmpresa(string obj) {
            dtoBusqueda datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().desasociarPacienteEmpresa(param, datos.list, Convert.ToInt32(datos.target)).ToString();

                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }
         
        public string buscarAsistenteClinica(string obj) {
            dtoBusqueda datos = null;

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (!System.Text.RegularExpressions.Regex.IsMatch(FuncionesGlobales.DecryptFromBase64(datos.param), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return FuncionesGlobales.ChangeObjFrom<dtoPagClientes>(new ServicioAdministrador().buscarAsistenteClinica(datos));
                
                return "";
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string asociarAsistenteClinica(string obj) {
            dtoBusqueda datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().asociarAsistenteClinica(param, datos.list, Convert.ToInt32(datos.target)).ToString();

                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string desasociarAsistenteClinica(string obj) {
            dtoBusqueda datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().desasociarAsistenteClinica(param, datos.list, Convert.ToInt32(datos.target)).ToString();

                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string buscarAsistenteConsultorio(string obj) {
            dtoBusqueda datos = null;

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (!System.Text.RegularExpressions.Regex.IsMatch(FuncionesGlobales.DecryptFromBase64(datos.param), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return FuncionesGlobales.ChangeObjFrom<dtoPagClientes>(new ServicioAdministrador().buscarAsistenteConsultorio(datos));

                return "";
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string permitirAccesoConsultorio(string obj) {
            dtoBusqueda datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().permitirAccesoConsultorio(param, datos.list, Convert.ToInt32(datos.target)).ToString();

                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string retirarAccesoConsultorio(string obj) {
            dtoBusqueda datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().retirarAccesoConsultorio(param, datos.list, Convert.ToInt32(datos.target)).ToString();

                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string buscarMedicoAsistente(string obj) {
            string param = "";

            try {
                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(obj)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return FuncionesGlobales.ChangeObjFrom<List<dtoClienteResume>>(new ServicioAdministrador().buscarMedicoAsistente(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param))));
                
                return "";
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string asociarMedicoAsistente(string obj) {
            dtoBusqueda datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().asociarMedicoAsistente(param, datos.list, Convert.ToInt32(FuncionesGlobales.Decry64Decry(datos.target))).ToString();

                return false.ToString();

            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string desasociarMedicoAsistente(string obj) {
            dtoBusqueda datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().desasociarMedicoAsistente(param, datos.list, Convert.ToInt32(FuncionesGlobales.Decry64Decry(datos.target))).ToString();

                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }


        public string buscarMedicosRevision(string obj) {
            dtoBusqueda datos = null;
            
            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                return FuncionesGlobales.ChangeObjFrom<dtoPagClientes>(new ServicioAdministrador().buscarMedicosRevision(datos));
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string aprobarRevisionMedico(string obj) {
            dtoBusqueda datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().aprobarRevisionMedico(param, Convert.ToInt32(FuncionesGlobales.Decry64Decry(datos.target))).ToString();

                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string buscarMedicosAprobacion(string obj) {
            dtoBusqueda datos = null;

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                return FuncionesGlobales.ChangeObjFrom<dtoPagClientes>(new ServicioAdministrador().buscarMedicosAprobacion(datos));
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string buscarMedicos(string obj)
        {
            dtoBusqueda datos = null;

            try
            {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                return FuncionesGlobales.ChangeObjFrom<dtoPagClientes>(new ServicioAdministrador().buscarMedicos(datos));
            }
            catch (Exception ex)
            {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string aprobarMedico(string obj) {
            dtoBusqueda datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().aprobarMedico(param, Convert.ToInt32(FuncionesGlobales.Decry64Decry(datos.target))).ToString();

                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string negarMedico(string obj) {
            dtoBusqueda datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioAdministrador().negarMedico(param, Convert.ToInt32(FuncionesGlobales.Decry64Decry(datos.target))).ToString();

                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        

        public string cargarClientes(string obj) {
            List<dtoRegistroCliente> clientes = null;
            ServicioCliente servicio = null;

            //string temp = "[{\"Nombre\":\"Zomaira\",\"Apellido\":\"Matos\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"zomairamatos\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Cesar\",\"Apellido\":\"ortega\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"cesarortega\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Vanessa\",\"Apellido\":\"Dos Reis\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"vanessadosreis\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Jesus\",\"Apellido\":\"Canelon\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"jesuscanelon\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Elia\",\"Apellido\":\"Carrillo\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"eliacarrillo\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Wilmary\",\"Apellido\":\"Quijada\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"wilmaryquijada\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Alejandro\",\"Apellido\":\"Salmeron\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"alejandrosalmeron\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Gilberto\",\"Apellido\":\"Serralvo\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"gilbertoserralvo\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Rosa\",\"Apellido\":\"Chercoles\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"rosachercoles\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Fernando\",\"Apellido\":\"Esquerre\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"fernandoesquerre\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Jaime\",\"Apellido\":\"Tovar\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"jaimetovar\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Hilda\",\"Apellido\":\"Gonzalez\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"hildagonzalez\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0}]";

            string temp = "[{\"Nombre\":\"Daniella\",\"Apellido\":\"Rengel\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"daniellarengel\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Norge\",\"Apellido\":\"Requena\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"norgerequena\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Rafael\",\"Apellido\":\"Castillo\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"rafaelcastillo\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Maria\",\"Apellido\":\"Rondon\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"mariarondon\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Cioly\",\"Apellido\":\"Coro\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"ciolycoro\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Marialexandra\",\"Apellido\":\"Ramos\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"marialexandraramos\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Francis\",\"Apellido\":\"Danilo\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"francisdanilo\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Jorge\",\"Apellido\":\"Prieto\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"jorgeprieto\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Felice\",\"Apellido\":\"Guido\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"feliceguido\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Daysi\",\"Apellido\":\"Barreto\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"daysibarreto\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Ricardo\",\"Apellido\":\"Castillo\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"ricardocastillo\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Angel\",\"Apellido\":\"Fonseca\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"angelfonseca\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Rodrigo\",\"Apellido\":\"Araya\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"rodrigoaraya\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Joaquin\",\"Apellido\":\"Paniagua\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"joaquinpaniagua\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Juan\",\"Apellido\":\"Vidal\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"juanvidal\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Carlos\",\"Apellido\":\"Garcia\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"carlosgarcia\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Luis\",\"Apellido\":\"Gonzalez\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"luisgonzalez\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Carlos\",\"Apellido\":\"Rodriguez\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"carlosrodriguez\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Luis\",\"Apellido\":\"Marquez\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"luismarquez\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Jose\",\"Apellido\":\"Escalona\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"joseescalona\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Maria\",\"Apellido\":\"Meinhard\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"f\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"mariameinhard\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Raul\",\"Apellido\":\"Sanhouse\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"raulsanhouse\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Boris\",\"Apellido\":\"Beltran\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"borisbeltran\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Tamara\",\"Apellido\":\"Castillo\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"tamaracastillo\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Mayli\",\"Apellido\":\"Lugo\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"maylilugo\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Ana\",\"Apellido\":\"Vasallo\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"anavasallo\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Elsa\",\"Apellido\":\"De Orta\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"elsadeorta\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Jose Maria\",\"Apellido\":\"Rizo\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"josemariarizo\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Paola\",\"Apellido\":\"Moncada\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"paolamoncada\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Adrina\",\"Apellido\":\"Goncalvez\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"M\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"adriangoncalvez\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Mercedes\",\"Apellido\":\"Santomauro\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"mercedessantomauro\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Adriana\",\"Apellido\":\"Arteaga\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"adrianaarteaga\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Esther\",\"Apellido\":\"Martinez\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"esthermartinez\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Leonor\",\"Apellido\":\"Galvis\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"leonorgalvis\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Daisa\",\"Apellido\":\"Medina\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"daisamedina\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Yamilet\",\"Apellido\":\"Rivas\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"yamiletrivas\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Romina\",\"Apellido\":\"Figueroa\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"rominafigueroa\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Damaris\",\"Apellido\":\"Quinones\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"damarisquinonis\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Solange\",\"Apellido\":\"Torrealba\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"solangetorrealba\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0},{\"Nombre\":\"Clarys\",\"Apellido\":\"Galue\",\"FechaNacimiento\":\"1969/04/13\",\"Sexo\":\"F\",\"Correo\":\"sucorreo@correo.com\",\"Usuario\":\"clarysgalue\",\"Password\":\"123456\",\"Celular\":\"4121234567\",\"TipoCliente\":3,\"RecibirCorreo\":1,\"IdUsuarioPrincipal\":0}]";

            try {
                if (string.IsNullOrEmpty(obj)) obj = temp;

                clientes = FuncionesGlobales.JsonDeserialize<List<dtoRegistroCliente>>(obj);

                if (clientes.Count > 0) {
                    servicio = new ServicioCliente();

                    clientes.ForEach(e => {
                        e.Usuario = FuncionesGlobales.Enc(FuncionesGlobales.SHA1HashEncode(e.Usuario.ToLower(), FuncionesGlobales.EncodeStyle.Base64));
                        e.Password = FuncionesGlobales.Enc(FuncionesGlobales.SHA1HashEncode(e.Password, FuncionesGlobales.EncodeStyle.Base64));

                        servicio.RegistrarCliente(e);
                    });
                    return true.ToString();
                }

                return false.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string RegistrarClientesTitularesCuidamed() {
            /*desarrollo*/
            //string StringConnDrCitas = "C3CqH6HFwPX6bmNtW/5JuKm8wEhG/B+JTw79KP9cqh4lJdv/eAXadcNyqi2NonVqFQotSA/pUrS5xWnErq83tWDBdztuC6czbhOZx4qgIu9NEijnMN05Zh5H5+i631xuapTgESDPPHg=";

            /*produccion*/
            string StringConnDrCitas = "bLaTQuRW9H3+d1sT6jsrwVsPYMIqUyAEbH0XboWU6uwmeliOa9KCXqBPuSWSl3KA/jCZrNAiQ/H0osgdldTIL7jFjwLv8fOa3Cn1fhLIjJCJ8QH86lNjSzIB3Le/StLh";
            
            try {
                return new ServicioCliente().RegistrarClientesTitularesCuidamed(FuncionesGlobales.Decrypt(StringConnDrCitas)).ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace, innerException = ex.InnerException.ToString() });
            }
        }

        public string RegistrarCompaniaClientesAsociadosCuidamed() {
            /*desarrollo*/
            //string StringConnDrCitas = "C3CqH6HFwPX6bmNtW/5JuKm8wEhG/B+JTw79KP9cqh4lJdv/eAXadcNyqi2NonVqFQotSA/pUrS5xWnErq83tWDBdztuC6czbhOZx4qgIu9NEijnMN05Zh5H5+i631xuapTgESDPPHg=";

            /*produccion*/
            string StringConnDrCitas = "bLaTQuRW9H3+d1sT6jsrwVsPYMIqUyAEbH0XboWU6uwmeliOa9KCXqBPuSWSl3KA/jCZrNAiQ/H0osgdldTIL7jFjwLv8fOa3Cn1fhLIjJCJ8QH86lNjSzIB3Le/StLh";

            try {
                return new ServicioCliente().RegistrarCompaniaClientesAsociadosCuidamed(FuncionesGlobales.Decrypt(StringConnDrCitas)).ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace, innerException = ex.InnerException.ToString() });
            }
        }
    }
}