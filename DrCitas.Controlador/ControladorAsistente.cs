﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DrCitas.Servicios;
using DrCitas.Dtos;
using System.Data;
using System.Text.RegularExpressions;

namespace DrCitas.Controlador {

    public class ControladorAsistente {
        
        /// <summary>
        /// Registra a un asistente
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa el objeto que contiene los datos a registrar del asistente</param>
        /// <returns>Una cadena de caracteres que indica si se realizo o no la actualizacion</returns>
        public string RegistrarAsistente(string obj) {
            dtoRegistroCliente datos = null;

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoRegistroCliente>(obj);

                datos.Usuario  = FuncionesGlobales.Enc(datos.Usuario);
                datos.Password = FuncionesGlobales.Enc(datos.Password);

                return new ServicioCliente().RegistrarCliente(datos).ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        /// <summary>
        /// Obtiene la lista de empresas asociadas al asistente
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa una sesion o identidicador numerico de usuario</param>
        /// <returns>Una cadena de caracteres en formato base 64 que representa la estructura JSON del objeto que contiene la lista de empresas del asistente</returns>
        public string GetMisEmpresas(string obj) {
            List<dtoEmpresa> datos = null;
            string param = "";

            try {

                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(obj)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    datos = new ServicioCliente().GetEmpresasAsistente(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)));
                else
                    datos = new ServicioCliente().GetEmpresasAsistente(param);

                return FuncionesGlobales.ChangeObjFrom<List<dtoEmpresa>>(datos);
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        /// <summary>
        /// Obtiene la lista de clinicas asociadas al asistente
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa una sesion o identidicador numerico de usuario</param>
        /// <returns>Una cadena de caracteres en formato base 64 que representa la estructura JSON del objeto que contiene la lista de clinicas del asistente</returns>
        public string GetMisClinicas(string obj) {
            List<dtoClinica> datos = null;
            string param = "";

            try {
                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(obj)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    datos = new ServicioCliente().GetClinicasAsistente(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)));
                else
                    datos = new ServicioCliente().GetClinicasAsistente(param);

                return FuncionesGlobales.ChangeObjFrom<List<dtoClinica>>(datos);
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        /// <summary>
        /// Obtiene la lista de consultorios del asistente
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa una sesion o identidicador numerico de usuario</param>
        /// <returns>Una cadena de caracteres en formato base 64 que representa la estructura JSON del objeto que contiene la lista de consultorios del asistente</returns>
        public string GetMisConsultorios(string obj) {
            List<dtoConsultorio> datos = null;
            string param = "";

            try {
                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(obj)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    datos = new ServicioCliente().GetConsultoriosAsistente(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)));
                else
                    datos = new ServicioCliente().GetConsultoriosAsistente(param);

                return FuncionesGlobales.ChangeObjFrom<List<dtoConsultorio>>(datos);
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        /// <summary>
        /// Obtiene la lista de medicos asociados al asistente
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa una sesion (o identidicador numerico) y el identificador de la clinica de usuario</param>
        /// <returns>Una cadena de caracteres en formato base 64 que representa la estructura JSON del objeto que contiene la lista de medicos del asistente</returns>
        public string GetMisMedicos(string obj) {
            dtoBusqueda datosBusq = null;
            List<dtoDatosMisMedicos> datos = null;
            string param = "";

            try {
                datosBusq = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datosBusq.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    datos = new ServicioCliente().GetMedicosAsistente(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)), datosBusq.idClinica, datosBusq.idConsultorio);
                else
                    datos = new ServicioCliente().GetMedicosAsistente(param, datosBusq.idClinica, datosBusq.idConsultorio);

                return FuncionesGlobales.ChangeObjFrom<List<dtoDatosMisMedicos>>(datos);
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string GetMedicosAsistentes(string obj) {
   
            List<dtoDatosMisMedicos> datos = null;
            string param = "";
            string idCliente = "";
            try
            {
              

                idCliente = (
                      (!Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(obj)), @"(\w{8}\-\w{4}\-\w{4}\-\w{4}\-\w{12})"))
                      ? (FuncionesGlobales.Decry64Decry(param))
                      : FuncionesGlobales.Decry64Decry(new ServicioCliente().GetDatosCliente(param).idCliente)
                 );

                datos = new ServicioCliente().GetMedicosAsistente(Convert.ToInt32(idCliente));

                return FuncionesGlobales.ChangeObjFrom<List<dtoDatosMisMedicos>>(datos);
            }
            catch (Exception ex)
            {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

    }
}
