﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DrCitas.Servicios;
using DrCitas.Dtos;
using System.Data;
using System.Text.RegularExpressions;

namespace DrCitas.Controlador
{
    public class ControladorCitas
    {

        #region Atributos y constructor

        private string StringConnDrCitas { get; set; }

        public ControladorCitas()
        {
            switch (System.Net.Dns.GetHostName().ToUpper()) {
                case "PC-DESARROLLO":
                case "CMEDC0000040-PC":
                case "CMEDC000050":
                    StringConnDrCitas = "C3CqH6HFwPWPTcGcEbmpX4cFEqpWRq8y+k5hNGV2tqhgLmYgnPJrYW7qz+6JMfJ9F+VggSAODZ03/2otJ5kgMlmYcSe9rYjh7l52pMvL6kN6M7bN+JBA+DwFwn7JsV8/";
                    break;
                default:
                    StringConnDrCitas = "bLaTQuRW9H3+d1sT6jsrwVsPYMIqUyAEbH0XboWU6uwmeliOa9KCXqBPuSWSl3KA/jCZrNAiQ/H0osgdldTIL7jFjwLv8fOa3Cn1fhLIjJCJ8QH86lNjSzIB3Le/StLh";
                    break;
            }
        } 

        #endregion

        #region Cita

        public string ReservarCita(string objRC)
            {

                ServicioCliente sc = null;

                try
                {
                    dtoRegistroCitas dtoRC  = FuncionesGlobales.ChangeObjTo<dtoRegistroCitas>(objRC.Trim());
                    dtoRC.param             = FuncionesGlobales.DecryptFromBase64(dtoRC.param);
                    dtoRC.idAgenda          = FuncionesGlobales.DecryptFromBase64(dtoRC.idAgenda);
                    dtoRC.idCreador         = FuncionesGlobales.DecryptFromBase64(dtoRC.idCreador);
                    dtoRC.idRazon           = FuncionesGlobales.DecryptFromBase64(dtoRC.idRazon);
                    dtoRC.idMedico          = FuncionesGlobales.Decry64Decry(FuncionesGlobales.DecryptFromBase64(dtoRC.idMedico));

                    sc = new ServicioCliente();

                    dtoClienteResume objCliente = (
                       !Regex.IsMatch(dtoRC.param, @"(\w{8}\-\w{4}\-\w{4}\-\w{4}\-\w{12})")
                        ? sc.GetDatosCliente(Convert.ToInt32(FuncionesGlobales.Decry64Decry(dtoRC.param)))
                        : sc.GetDatosCliente(dtoRC.param)
                    );

                    objCliente.idUsuario = FuncionesGlobales.Decry64Decry(objCliente.idUsuario);
                    objCliente.idCliente = FuncionesGlobales.Decry64Decry(objCliente.idCliente);

                     dtoRC.idCreador = (
                            Regex.IsMatch(dtoRC.idCreador, @"(\w{8}\-\w{4}\-\w{4}\-\w{4}\-\w{12})")
                            ? FuncionesGlobales.Decry64Decry(sc.GetDatosCliente(dtoRC.idCreador).idCliente).ToString()
                            : FuncionesGlobales.Decry64Decry(dtoRC.idCreador)
                         );

                    return new ServicioCita().ReservarCita(dtoRC, objCliente, FuncionesGlobales.Decrypt(this.StringConnDrCitas)).ToString();

                }
                catch (Exception ex) {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            public bool CodigoConfirmacion(string obj)
            {
                string aux = "";
                try
                {
                    dtoCodigoConfirmacion cc = FuncionesGlobales.ChangeObjTo<dtoCodigoConfirmacion>(obj.Trim());

                    cc.IdSesion = FuncionesGlobales.DecryptFromBase64(cc.IdSesion);

                    cc.Nro_Cita = FuncionesGlobales.DecryptFromBase64(cc.Nro_Cita);

                    string vCodigo = ((Convert.ToInt32(cc.Nro_Cita) / 5.0) * 10).ToString();

                    //dtoClienteXidSesion cxs = new ServicioCliente().GetDatosClienteXidSesion(cc.IdSesion.Trim(), FuncionesGlobales.Decrypt(this.StringConnDrCitas), new dtoRegistroCitas { asociado = null });

                    dtoClienteResume cxs = new ServicioCliente().GetDatosCliente(cc.IdSesion.Trim());

                    ServicioUtilidad.EnviarSMSAlCliente(Convert.ToInt32(cc.Nro_Cita), cxs.telefono, vCodigo, Convert.ToInt32(cxs.idUsuario));

                    aux = (cc.html == "" ? "Codigo de confirmacion: " + vCodigo.ToString() : cc.html.Replace("@codigo", vCodigo.ToString()));

                    ServicioUtilidad.EnviarCorreo("Codigo de confirmacion - Dr. Citas", aux , "info@drcitas.com", cxs.correo, "smtp.drcitas.com", "info@drcitas.com", "Abcd1234");

                    return true;
                }
                catch (Exception)
                {
                    return false;
                }

            }

            public string ListaDeRazones(int pIdEspecialidad)
            {
                try
                {
                    return FuncionesGlobales.JsonSerializer<List<dtoListaDeValores>>(new ServicioListaDeValores().ListaDeRazones(pIdEspecialidad));
                }
                catch (Exception ex)
                {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            public string GetDatosDeLaAgenda(int pIdAgenda, int pIdUsuarioX)
            {
                try
                {
                    //var a = new ServicioCita().DatosDeLaAgenda(pIdAgenda, pIdUsuarioX);

                    return FuncionesGlobales.JsonSerializer<dtoAgenda>(new ServicioCita().DatosDeLaAgenda(pIdAgenda, pIdUsuarioX));
                }
                catch (Exception ex)
                {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            /// <summary>
            /// Registra un asociado al unsuario conectado
            /// </summary>
            public string RegistrarAsociado(string objAsociado)
            {

                dtoClienteResume clienteDatos   = null;
                dtoAsociado asociado            = null;
                ServicioCliente sc              = null;

                try
                {
                    sc              = new ServicioCliente();
                    asociado        = FuncionesGlobales.ChangeObjTo<dtoAsociado>(objAsociado);
                    asociado.param  = FuncionesGlobales.DecryptFromBase64(asociado.param);
                    clienteDatos    = (
                       !Regex.IsMatch(asociado.param, @"(\w{8}\-\w{4}\-\w{4}\-\w{4}\-\w{12})")
                       ? new ServicioCliente().GetDatosCliente(Convert.ToInt32(FuncionesGlobales.Decry64Decry(asociado.param)))
                       : new ServicioCliente().GetDatosCliente(asociado.param)
                      );

                    clienteDatos.idCliente = FuncionesGlobales.Decry64Decry(clienteDatos.idCliente);

                    return FuncionesGlobales.Encry64Encry(sc.RegistrarAsociado(asociado, clienteDatos).ToString());
                }
                catch (Exception ex)
                {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            /// <summary>
            /// obtiene una lista de asociados relacionados al usuario 1
            /// </summary>
            /// <param name="idSesion">el id que este activo segun la sesion</param>
            /// <returns>List<dtoAsociado></returns>
            public string GetAsociados(string idSesion)
            {
                try
                {
                    List<dtoAsociado> listAsociado = null;
                    DataTable dt = new ServicioCliente().GetAsociado(idSesion, FuncionesGlobales.Decrypt(this.StringConnDrCitas));
                
                    if(dt.Rows.Count > 0)
                    {

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (i == 0)
                                listAsociado = new List<dtoAsociado>();

                            listAsociado.Add
                            (
                                new dtoAsociado
                                {
                                    nombre      = dt.Rows[i].Field<string>(0),
                                    apellido    = dt.Rows[i].Field<string>(1),
                                    sexo        = dt.Rows[i].Field<string>(2),
                                    parentesco  = dt.Rows[i].Field<string>(3),
                                    HM          = dt.Rows[i].Field<string>(4),
                                    cliente     = FuncionesGlobales.Encry64Encry(dt.Rows[i].Field<int>(5).ToString()),
                                    FN          = dt.Rows[i].Field<DateTime>(6).ToShortDateString(),
                                    dtoDA       = new dtoDatosPaciente {
                                        telefonoCasa    = dt.Rows[i].Field<string>(7),
                                        telefonoOficina = dt.Rows[i].Field<string>(8),
                                        facebook        = dt.Rows[i].Field<string>(9),
                                        twitter         = dt.Rows[i].Field<string>(10),
                                        correo          = dt.Rows[i].Field<string>(11),
                                        tipoDocumento   = dt.Rows[i].Field<string>(12),
                                        documento       = dt.Rows[i].Field<int>(13)
                                    }
                                }
                            );
                        }

                    }

                    return FuncionesGlobales.ChangeObjFrom<List<dtoAsociado>>(listAsociado);

                }
                catch (Exception ex)
                {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            public string actualizarEstadoCita(string obj) {
                dtoActualizarCita datos = null;

                try {
                    datos = FuncionesGlobales.ChangeObjTo<dtoActualizarCita>(obj);

                    return new ServicioCita().actualizarEstadoCita(datos.idCita, datos.estado).ToString();
                }
                catch (Exception ex) {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            public string crearDiagnostico(string obj) {
                dtoDiagnosticos datos = null;

                try {
                    datos          = FuncionesGlobales.ChangeObjTo<dtoDiagnosticos>(obj);
                    datos.idMedico = FuncionesGlobales.Decry64Decry(datos.idMedico);
                    return new ServicioCita().crearDiagnostico(datos.idCita, Convert.ToInt32(datos.idMedico), datos.informacion).ToString();
                }
                catch (Exception ex) {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            public string actualizarDiagnostico(string obj) {
                dtoDiagnosticos datos = null;

                try {
                    datos = FuncionesGlobales.ChangeObjTo<dtoDiagnosticos>(obj);

                    return new ServicioCita().actualizarDiagnostico(datos.informacion).ToString();
                }
                catch (Exception ex) {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            public string getDiagnostico(string obj) {
                try {
                    return FuncionesGlobales.ChangeObjFrom<List<dtoDiagnosticoCita>>(new ServicioCita().getDiagnostico(Convert.ToInt32(FuncionesGlobales.DecryptFromBase64(obj))));
                }
                catch (Exception ex) {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            public string CancelarCita(string obj) {

                dtoDatosCita     datosCita    = null;
                dtoClienteResume clienteDatos = null;


                try {
                    datosCita                      = FuncionesGlobales.ChangeObjTo<dtoDatosCita>(obj);
                    datosCita.param                = FuncionesGlobales.DecryptFromBase64(datosCita.param);
                    datosCita.idCita               = FuncionesGlobales.DecryptFromBase64(datosCita.idCita);
                    datosCita.idUsuarioSeguimiento = FuncionesGlobales.Decry64Decry(FuncionesGlobales.DecryptFromBase64(datosCita.idUsuarioSeguimiento));
                    
                    clienteDatos = (
                        !Regex.IsMatch(datosCita.param, @"(\w{8}\-\w{4}\-\w{4}\-\w{4}\-\w{12})")
                        ? new ServicioCliente().GetDatosCliente(Convert.ToInt32(FuncionesGlobales.Decry64Decry(datosCita.param)))
                        : new ServicioCliente().GetDatosCliente(datosCita.param)
                    );

                    new ServicioCita().CancelarCita(
                        Convert.ToInt32(FuncionesGlobales.Decry64Decry(clienteDatos.idCliente)),
                        Convert.ToInt32(datosCita.idCita),
                        clienteDatos.correo,
                        Convert.ToInt32(datosCita.idUsuarioSeguimiento),
                        Convert.ToInt32(FuncionesGlobales.Decry64Decry(clienteDatos.idUsuario))
                    );
                    return true.ToString();
                }
                catch (Exception ex) {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }

            }

            public string GetInterrogatorioMedico(string obj) {
                ServicioCliente servicio = null;
                string param = "";

                try {
                    servicio = new ServicioCliente();

                    if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(obj)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                        param = servicio.GetDatosCliente(param).idCliente;

                    return servicio.getInterrogatorioMedico(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)));
                }
                catch (Exception ex) {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }                
            }

            public string GetListCitasPaciente(string obj)
            {
                try 
	            {	
                    dtoCitas dtoCitas = null;
                    DataSet ds = new ServicioCita().ListarCitasPaciente(FuncionesGlobales.DecryptFromBase64(obj), FuncionesGlobales.Decrypt(this.StringConnDrCitas));

		            if (ds.Tables.Count > 0) 
                    {
                        dtoCitas = new dtoCitas();
                        List<dtoDatosCitas> lDatosCitas = null;

                        for (int i = 0; i < ds.Tables.Count; i++)
                        {

                            lDatosCitas = null;

                            for (int j = 0; j < ds.Tables[i].Rows.Count; j++)
                            {

                                if (j == 0) lDatosCitas = new List<dtoDatosCitas>();

                                lDatosCitas.Add(
                                    new dtoDatosCitas 
                                    {
                                        IdCita              = ((ds.Tables[i].Rows[j].Field<int>(0)).ToString()),
                                        NumeroCita          = ((ds.Tables[i].Rows[j].Field<int>(1)).ToString()),
                                        HoraInicio          = ds.Tables[i].Rows[j].Field<DateTime>(2),
                                        HoraFin             = ds.Tables[i].Rows[j].Field<DateTime>(3),
                                        Celular             = ds.Tables[i].Rows[j].Field<string>(4),
                                        EstatusCita         = ds.Tables[i].Rows[j].Field<string>(5),
                                        idCliente           = FuncionesGlobales.Encry64Encry((ds.Tables[i].Rows[j].Field<int>(6)).ToString()),
                                        NombrePaciente      = ds.Tables[i].Rows[j].Field<string>(7),
                                        ApellidoPaciente    = ds.Tables[i].Rows[j].Field<string>(8),
                                        CorreoPaciente      = ds.Tables[i].Rows[j].Field<string>(9),
                                        Especialidad        = ds.Tables[i].Rows[j].Field<string>(10),
                                        Razon               = ds.Tables[i].Rows[j].Field<string>(11),
                                        Consultorio         = ds.Tables[i].Rows[j].Field<string>(12),
                                        IdMedico            = FuncionesGlobales.Encry64Encry((ds.Tables[i].Rows[j].Field<int>(13)).ToString()),
                                        Medico              = ds.Tables[i].Rows[j].Field<string>(14),
                                        TelefonoConsultorio = ds.Tables[i].Rows[j].Field<string>(15),
                                        DireccionConsultorio= ds.Tables[i].Rows[j].Field<string>(16)
                                    });

                            }

                            switch (i)
                            {
                                case 0:
                                    dtoCitas.CitasActivasPaciente = lDatosCitas;
                                    break;
                                case 1:
                                    dtoCitas.CitasPasadasPaciente = lDatosCitas;
                                    break;
                                case 2:
                                    dtoCitas.CitasActivasAsociado = lDatosCitas;
                                    break;
                                case 3:
                                    dtoCitas.CitasPasadasAsociado = lDatosCitas;
                                    break;
                            }

                        }
                    }
                    return FuncionesGlobales.ChangeObjFrom<dtoCitas>(dtoCitas);
	            }
	            catch (Exception ex)
	            {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
	            }
            }

            public string GetListCitasMedico(string obj) {

                dtoPagCitas misCitas    = null;
                dtoReqPagCitas request  = null;
                string param            = "";

                try {
                    request = FuncionesGlobales.ChangeObjTo<dtoReqPagCitas>(obj);
                    param = FuncionesGlobales.DecryptFromBase64(request.param);

                    if (!Regex.IsMatch(param , @"(\w{8}\-\w{4}\-\w{4}\-\w{4}\-\w{12})"))
                        misCitas = new ServicioCita().ListarCitasMedico(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)), request.modo, request.pagina, request.elementos);
                    else
                        misCitas = new ServicioCita().ListarCitasMedico(param, request.modo, request.pagina, request.elementos);

                    return FuncionesGlobales.ChangeObjFrom<dtoPagCitas>(misCitas); 
                }
                catch (Exception ex) {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

            public string GetDiagnosticoXEspecialidad(string obj) {
                try {
                    return FuncionesGlobales.ChangeObjFrom<List<dtoDiagnostico>>(new ServicioCita().getDiagnosticoXEspecialidad(Convert.ToInt32(FuncionesGlobales.DecryptFromBase64(obj))));
                }
                catch (Exception ex) {
                    return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
                }
            }

        #endregion
    }
}
