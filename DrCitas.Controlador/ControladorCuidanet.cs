﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DrCitas.Dtos;
using DrCitas.Servicios;

namespace DrCitas.Controlador
{
    public class ControladorCuidanet
    {
        public string obtenerClienteCuidanet(string obj) 
        {
            dtoBusqueda dtoBusqueda = FuncionesGlobales.JsonDeserialize<dtoBusqueda>(obj);
            dtoRegistroCliente dtoRegistroCliente = null;
            string aux = "";
            try
            {
                //dtoBusqueda.cedula, dtoBusqueda.clienteId, dtoBusqueda.usuarioSistema, dtoBusqueda.tipoPlan
                aux = ControladorWebRequest.POST(("http://192.2.1.9/CuidanetWS/Cuidanet.asmx/getTitular"), FuncionesGlobales.EncryptFromBase64(FuncionesGlobales.JsonSerializer<dtoBusqueda>(dtoBusqueda)));
                dtoRegistroCliente = FuncionesGlobales.JsonDeserialize<dtoRegistroCliente>(aux);

                return "s";
            }
            catch (Exception ex)
            {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }
    }
}
