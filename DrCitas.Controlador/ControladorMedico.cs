﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DrCitas.Servicios;
using DrCitas.Dtos;
using System.Data;

namespace DrCitas.Controlador {

    public class ControladorMedico {

        #region Atributos

        private string StringConnDrCitas { get; set; }
     
        public ControladorMedico()
        {
            switch (System.Net.Dns.GetHostName().ToUpper()) {
                case "PC-DESARROLLO":
                case "CMEDC0000040-PC":
                case "CMEDC000050":
                    StringConnDrCitas = "C3CqH6HFwPWPTcGcEbmpX4cFEqpWRq8y+k5hNGV2tqhgLmYgnPJrYW7qz+6JMfJ9F+VggSAODZ03/2otJ5kgMlmYcSe9rYjh7l52pMvL6kN6M7bN+JBA+DwFwn7JsV8/"; 
                    break;
                default:
                    StringConnDrCitas = "bLaTQuRW9H3+d1sT6jsrwVsPYMIqUyAEbH0XboWU6uwmeliOa9KCXqBPuSWSl3KA/jCZrNAiQ/H0osgdldTIL7jFjwLv8fOa3Cn1fhLIjJCJ8QH86lNjSzIB3Le/StLh";
                    break;
            }
        }

        #endregion

        /// <summary>
        /// Registra a un medico
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa el objeto que contiene los datos a registrar del medico</param>
        /// <returns>Una cadena de caracteres que indica si se realizo o no el registro</returns>
        public string RegistrarMedico(string obj) {
            dtoRegistroCliente datos = null;

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoRegistroCliente>(obj);

                datos.Usuario = FuncionesGlobales.Enc(datos.Usuario);
                datos.Password = FuncionesGlobales.Enc(datos.Password);

                return new ServicioCliente().RegistrarCliente(datos).ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        /// <summary>
        /// Solicita revision de registro de datos del medico
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa el objeto que contiene los datos a revisar del medico</param>
        /// <returns>Una cadena de caracteres que indica si se realizo o no la operacion</returns>
        public string SolicitarRevisionRegistro(string obj) {
            ServicioCliente servicio = null;
            dtoDatosMedico datos = null;
            string param = "";

            try {
                servicio = new ServicioCliente();
                datos = FuncionesGlobales.ChangeObjTo<dtoDatosMedico>(obj);

                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return servicio.SolicitarRevisionRegistro(datos, Convert.ToInt32(FuncionesGlobales.Decry64Decry(param))).ToString();
                else
                    return servicio.SolicitarRevisionRegistro(datos, param).ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        /// <summary>
        /// Obtiene los datos adicionales de un medico
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa una sesion o identidicador numerico de usuario</param>
        /// <returns>Una cadena de caracteres en formato base 64 que representa la estructura JSON del objeto que contiene los datos del medico</returns>
        public string DatosAdicionalesMedico(string obj) {
            string param = "";

            try {
                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(obj)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return FuncionesGlobales.ChangeObjFrom<dtoDatosMedico>(new ServicioCliente().DatosAdicionalesMedico(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param))));
                else
                    return FuncionesGlobales.ChangeObjFrom<dtoDatosMedico>(new ServicioCliente().DatosAdicionalesMedico(param));
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        /// <summary>
        /// Actualiza la informacion basica asociada al medico
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa el objeto que contiene los datos a actualizar del medico</param>
        /// <returns>Una cadena de caracteres que indica si se realizo o no la actualizacion</returns>
        public string UpdateDatosBasicosMedico(string obj) {
            ServicioCliente servicio = null;
            dtoDatosMedico datos = null;
            string param = "";

            try {
                servicio = new ServicioCliente();
                datos = FuncionesGlobales.ChangeObjTo<dtoDatosMedico>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    param = servicio.GetDatosCliente(param).idCliente;

                return servicio.ActualizarDatosBasicosMedico(datos, Convert.ToInt32(FuncionesGlobales.Decry64Decry(param))).ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace, innerException = ex.InnerException.ToString() });
            }
        }

        /// <summary>
        /// Actualiza informacion adicional asociada al medico
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa el objeto que contiene los datos a actualizar del medico</param>
        /// <returns>Una cadena de caracteres que indica si se realizo o no la actualizacion</returns>
        public string UpdateOtrosDatosMedico(string obj) {
            ServicioCliente servicio = null;
            dtoDatosMedico datos = null;
            string param = "";

            try {
                servicio = new ServicioCliente();
                datos = FuncionesGlobales.ChangeObjTo<dtoDatosMedico>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    param = servicio.GetDatosCliente(param).idCliente;

                return servicio.ActualizarOtrosDatosMedico(datos, Convert.ToInt32(FuncionesGlobales.Decry64Decry(param))).ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace, innerException = ex.InnerException.ToString() });
            }
        }

        /// <summary>        
        /// Elimina una especialidad asociada al medico
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa el objeto que contiene los datos de la especialidad a eliminar</param>
        /// <returns>Una cadena de caracteres que indica si se realizo o no la eliminacion</returns>
        public string EliminarEspecialidadMedico(string obj) {
            dtoEliminar datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoEliminar>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioCliente().EliminarEspecialidad(param, Convert.ToInt32(datos.target)).ToString();
                else
                    return new ServicioCliente().EliminarEspecialidad(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)), Convert.ToInt32(datos.target)).ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace, innerException = ex.InnerException.ToString() });
            }
        }

        /// <summary>
        /// Elimina un atributo asociado al medico
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa el objeto que contiene los datos del atributo a eliminar</param>
        /// <returns>Una cadena de caracteres que indica si se realizo o no la eliminacion</returns>
        public string EliminarAtributoMedico(string obj) {
            dtoEliminar datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoEliminar>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    return new ServicioCliente().EliminarAtributoLista(param, Convert.ToInt32(datos.target)).ToString();
                else
                    return new ServicioCliente().EliminarAtributoLista(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)), Convert.ToInt32(datos.target)).ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace, innerException = ex.InnerException.ToString() });
            }
        }

        /// <summary>
        /// Obtiene la lista de pacientes asociados al medico
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa una sesion o identidicador numerico de usuario</param>
        /// <returns>Una cadena de caracteres en formato base 64 que representa la estructura JSON del objeto que contiene la lista de pacientes del medico</returns>
        public string GetMisPacientes(string obj) {
            dtoMisPacientes datos = null;
            string param = "";
            try {
                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(obj)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    datos = new ServicioCliente().GetMisPacientes(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)));
                else
                    datos = new ServicioCliente().GetMisPacientes(param);

                return FuncionesGlobales.ChangeObjFrom<dtoMisPacientes>(datos);

            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        /// <summary>
        /// Registra un consultorio
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa el objeto que contiene los datos a registrar del consultorio</param>
        /// <returns>Una cadena de caracteres que indica si se realizo o no la operacion</returns>
        public string RegistrarConsultorio(string obj) {
            dtoConsultorios datos = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoConsultorios>(obj);
                param = FuncionesGlobales.DecryptFromBase64(datos.sesion);

                if (Regex.IsMatch(param, @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    param = new ServicioCliente().GetDatosCliente(param).idCliente;

                return new ServicioConsultorio().RegistrarConsultorio(datos, Convert.ToInt32(FuncionesGlobales.Decry64Decry(param))).ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        /// <summary>
        /// Registra un consultorio
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa el objeto que contiene los datos a actualizar del consultorio</param>
        /// <returns>Una cadena de caracteres que indica si se realizo o no la actualizacion</returns>
        public string ActualizarConsultorio(string obj) {
            dtoConsultorios datos = null;

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoConsultorios>(obj);

                return new ServicioConsultorio().ActualizarConsultorio(datos, datos.IdConsultorio).ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        /// <summary>
        /// Obtiene la lista de consultorios asociados a un medico
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa una sesion o identidicador numerico de usuario</param>
        /// <returns>Una cadena de caracteres en formato base 64 que representa la estructura JSON del objeto que contiene la lista de consultorios asociados a un medico</returns>
        public string ListarConsultorios(string obj) {
            string param = "";

            try {
                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(obj)), @"(\w{8}\-\w{4}\-\w{4}\-\w{4}\-\w{12})"))
                    return FuncionesGlobales.ChangeObjFrom<List<dtoConsultorios>>(new ServicioConsultorio().ListarConsultorios(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param))));
                else
                    return FuncionesGlobales.ChangeObjFrom<List<dtoConsultorios>>(new ServicioConsultorio().ListarConsultorios(param));
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }


        /// <summary>
        /// Registra horarios a consultorios
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa el objeto que contiene los datos a registrar para un horario</param>
        /// <returns>Una cadena de caracteres que indica si se realizo o no la actualizacion</returns>
        public string RegistrarHorarios(string obj) {
            dtoConsultorioHorario datos = null;
            dtoClienteResume cliente = null;
            string param = "";

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoConsultorioHorario>(obj);

                if (!Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.IdSesion)), @"(\w{8}\-\w{4}\-\w{4}\-\w{4}\-\w{12})"))
                    cliente = new ServicioCliente().GetDatosCliente(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)));
                else
                    cliente = new ServicioCliente().GetDatosCliente(param);

                datos.IdUsuario = Convert.ToInt32(FuncionesGlobales.Decry64Decry(cliente.idUsuario));
                datos.IdCliente = Convert.ToInt32(FuncionesGlobales.Decry64Decry(cliente.idCliente));

                return new ServicioConsultorio().RegistrarHorarios(datos);
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public string GetHorarios(string obj) {

            dtoDatosGetHorarios datosGetHorarios = null;
            string param = "";

            try {
                datosGetHorarios = FuncionesGlobales.ChangeObjTo<dtoDatosGetHorarios>(obj);

                param = FuncionesGlobales.DecryptFromBase64(datosGetHorarios.idCliente);

                datosGetHorarios.idCliente =
                    (
                        (!Regex.IsMatch(param, @"(\w{8}\-\w{4}\-\w{4}\-\w{4}\-\w{12})"))
                        ? (FuncionesGlobales.Decry64Decry(param))
                        : FuncionesGlobales.Decry64Decry(new ServicioCliente().GetDatosCliente(param).idCliente.ToString())
                   );

                return FuncionesGlobales.ChangeObjFrom<HorariosSemanas>(new ServicioConsultorio().ListarHorarios(datosGetHorarios, FuncionesGlobales.Decrypt(this.StringConnDrCitas)));
                ;
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }

        }

        public string HorarioConsultorio(string obj) {

            int idConsultorio = Convert.ToInt32(obj);

            try {
                return FuncionesGlobales.JsonSerializer<List<dtoConsultorioHorario>>(new ServicioConsultorio().HorarioConsultorio(idConsultorio));
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        /// <summary>
        /// Desactiva un rango de fechas en la agenda del dr
        /// </summary>
        /// <param name="obj">recibe un objeto del tipo dtoDesactivarAgenda</param>
        /// <returns></returns>
        public string DesactivarHorarios(string obj) {

            dtoDesactivarAgenda desactivarHorarios = null;
            dtoClienteResume cliente = null;
            string param = "";

            try {

                desactivarHorarios = FuncionesGlobales.ChangeObjTo<dtoDesactivarAgenda>(obj);

                if (!Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(desactivarHorarios.sesion)), @"(\w{8}\-\w{4}\-\w{4}\-\w{4}\-\w{12})"))
                    cliente = new ServicioCliente().GetDatosCliente(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)));
                else
                    cliente = new ServicioCliente().GetDatosCliente(param);

                // dtoClienteResume cliente = new ServicioCliente().GetDatosCliente(FuncionesGlobales.DecryptFromBase64(desactivarHorarios.sesion));
                desactivarHorarios.idCliente = Convert.ToInt32(FuncionesGlobales.Decry64Decry(cliente.idCliente));
                desactivarHorarios.idUsuario = Convert.ToInt32(FuncionesGlobales.Decry64Decry(cliente.idUsuario));

                return new ServicioConsultorio().DesactivarHorarios(desactivarHorarios).ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string DesactivarHoras(string obj) {

            dtoDesactivarAgenda desactivarHorarios = null;

            try {
                desactivarHorarios = FuncionesGlobales.ChangeObjTo<dtoDesactivarAgenda>(obj);
                return new ServicioConsultorio().DesactivarHoras(desactivarHorarios).ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

    }
}