﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DrCitas.Servicios;
using DrCitas.Dtos;
using System.Data;
using System.Text.RegularExpressions;

namespace DrCitas.Controlador {

    public class ControladorPaciente {

        /// <summary>
        /// Registra a un paciente
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa el objeto que contiene los datos a registrar del paciente</param>
        /// <returns>Una cadena de caracteres que indica si se realizo o no la actualizacion</returns>
        public string RegistrarPaciente(string obj) {
            dtoRegistroCliente datos = null;

            try {
                datos = FuncionesGlobales.ChangeObjTo<dtoRegistroCliente>(obj);
                
                datos.Usuario  = FuncionesGlobales.Enc(datos.Usuario);
                datos.Password = FuncionesGlobales.Enc(datos.Password);

                return new ServicioCliente().RegistrarCliente(datos).ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        /// <summary>
        /// Obtiene los datos adicionales de un paciente
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa una sesion o identidicador numerico de usuario</param>
        /// <returns>Una cadena de caracteres en formato base 64 que representa la estructura JSON del objeto que contiene los datos del paciente</returns>
        public string DatosAdicionalesPacienteTitular(string obj) {
            dtoDatosPaciente datos = null;
            string param = "";

            try {
                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(obj)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    datos = new ServicioCliente().DatosAdicionalesTitular(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)));
                else
                    datos = new ServicioCliente().DatosAdicionalesTitular(param);

                return FuncionesGlobales.ChangeObjFrom<dtoDatosPaciente>(datos);
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        /// <summary>
        /// Actualiza la informacion asociada al paciente
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa el objeto que contiene los datos a actualizar del paciente</param>
        /// <returns>Una cadena de caracteres que indica si se realizo o no la actualizacion</returns>
        public string UpdateDatosBasicosPaciente(string obj) {
            ServicioCliente servicio = null;
            dtoDatosPaciente datos = null;
            string param = "";

            try {
                servicio = new ServicioCliente();
                datos = FuncionesGlobales.ChangeObjTo<dtoDatosPaciente>(obj);

                if (System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(datos.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    param = servicio.GetDatosCliente(param).idCliente;

                return servicio.ActualizarDatosBasicosPaciente(datos, Convert.ToInt32(FuncionesGlobales.Decry64Decry(param))).ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace, innerException = ex.InnerException.ToString() });
            }
        }

        /// <summary>
        /// Obtiene la lista de medicos asociados al paciente
        /// </summary>
        /// <param name="obj">Cadena de caracteres en formato base 64 que representa una sesion o identidicador numerico de usuario</param>
        /// <returns>Una cadena de caracteres en formato base 64 que representa la estructura JSON del objeto que contiene la lista de medicos del paciente</returns>
        public string GetMisMedicos(string obj) {
            dtoMisMedicos datos = null;
            string param = "";

            try {
                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(obj)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    datos = new ServicioCliente().GetMisMedicos(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)));
                else
                    datos = new ServicioCliente().GetMisMedicos(param);

                return FuncionesGlobales.ChangeObjFrom<dtoMisMedicos>(datos);

            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string GetMisAsociados(string obj) {
            List<dtoAsociado> datos = null;
            string param = "";

            try
            {
                if (!System.Text.RegularExpressions.Regex.IsMatch((param = FuncionesGlobales.DecryptFromBase64(obj)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    datos = new ServicioCliente().GetMisAsociados(Convert.ToInt32(FuncionesGlobales.Decry64Decry(param)));
                else
                    datos = new ServicioCliente().GetMisAsociados(param);

                return FuncionesGlobales.ChangeObjFrom<List<dtoAsociado>>(datos);

            }
            catch (Exception ex)
            {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }        
    }
}