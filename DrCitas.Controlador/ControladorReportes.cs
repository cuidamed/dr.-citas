﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DrCitas.Servicios;
using DrCitas.Dtos;
using System.Data;

namespace DrCitas.Controlador {

    public class ControladorReportes {

        public string getReporteGeneralCitas(string obj) {
            dtoBusqueda datosBusq = null;
            List<dtoReporteGeneralCitas> datos = null;

            try {
                datosBusq = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (!System.Text.RegularExpressions.Regex.IsMatch((datosBusq.param = FuncionesGlobales.DecryptFromBase64(datosBusq.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    datos = new ServicioReportes().getReporteGeneralCitas(Convert.ToInt32(FuncionesGlobales.Decry64Decry(datosBusq.param)), datosBusq.mes, datosBusq.ano);
                else
                    datos = new ServicioReportes().getReporteGeneralCitas(datosBusq.param, datosBusq.mes, datosBusq.ano);

                return FuncionesGlobales.ChangeObjFrom<List<dtoReporteGeneralCitas>>(datos);
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string getReporteCitas(string obj) {
            dtoBusqueda datosBusq = null;
            List<dtoReporteGeneralCitas> datos = null;
            
            try {
                datosBusq = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (!System.Text.RegularExpressions.Regex.IsMatch((datosBusq.param = FuncionesGlobales.DecryptFromBase64(datosBusq.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    datos = new ServicioReportes().getReporteCitas(
                        Convert.ToInt32(FuncionesGlobales.Decry64Decry(datosBusq.param)),
                        datosBusq.idEmpresa,
                        datosBusq.idClinica,
                        datosBusq.idEspecialidad,
                        datosBusq.idConsultorio,
                        (!string.IsNullOrEmpty(datosBusq.target) ? Convert.ToInt32(FuncionesGlobales.Decry64Decry(datosBusq.target)) : 0),
                        datosBusq.mes,
                        datosBusq.ano
                    );
                else
                    datos = new ServicioReportes().getReporteCitas(
                        datosBusq.param,
                        datosBusq.idEmpresa,
                        datosBusq.idClinica,
                        datosBusq.idEspecialidad,
                        datosBusq.idConsultorio,
                        (!string.IsNullOrEmpty(datosBusq.target) ? Convert.ToInt32(FuncionesGlobales.Decry64Decry(datosBusq.target)) : 0),
                        datosBusq.mes,
                        datosBusq.ano
                    );

                return FuncionesGlobales.ChangeObjFrom<List<dtoReporteGeneralCitas>>(datos);
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string getReporteDetalladoCitas(string obj) {
            dtoBusqueda datosBusq = null;
            List<dtoReporteCita> datos = null;

            try {
                datosBusq = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (!System.Text.RegularExpressions.Regex.IsMatch((datosBusq.param = FuncionesGlobales.DecryptFromBase64(datosBusq.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    datos = new ServicioReportes().getReporteDetalladoCitas(
                        Convert.ToInt32(FuncionesGlobales.Decry64Decry(datosBusq.param)),
                        datosBusq.idEmpresa,
                        datosBusq.idClinica,
                        datosBusq.idEspecialidad,
                        datosBusq.idConsultorio,
                        (!string.IsNullOrEmpty(datosBusq.target) ? Convert.ToInt32(FuncionesGlobales.Decry64Decry(datosBusq.target)) : 0),
                        datosBusq.idEstatus,
                        datosBusq.mes,
                        datosBusq.ano
                    );
                else
                    datos = new ServicioReportes().getReporteDetalladoCitas(
                        datosBusq.param,
                        datosBusq.idEmpresa,
                        datosBusq.idClinica,
                        datosBusq.idEspecialidad,
                        datosBusq.idConsultorio,
                        (!string.IsNullOrEmpty(datosBusq.target) ? Convert.ToInt32(FuncionesGlobales.Decry64Decry(datosBusq.target)) : 0),
                        datosBusq.idEstatus,
                        datosBusq.mes,
                        datosBusq.ano
                    );

                return FuncionesGlobales.ChangeObjFrom<List<dtoReporteCita>>(datos);
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string exportarReporte(string obj) {
            dtoBusqueda datosBusq = null;

            try {
                datosBusq = FuncionesGlobales.ChangeObjTo<dtoBusqueda>(obj);

                if (!System.Text.RegularExpressions.Regex.IsMatch((datosBusq.param = FuncionesGlobales.DecryptFromBase64(datosBusq.param)), @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}"))
                    new ServicioReportes().exportarReporte(
                        Convert.ToInt32(FuncionesGlobales.Decry64Decry(datosBusq.param)),
                        datosBusq.idEmpresa,
                        datosBusq.idClinica,
                        datosBusq.idEspecialidad,
                        datosBusq.idConsultorio,
                        (!string.IsNullOrEmpty(datosBusq.target) ? Convert.ToInt32(FuncionesGlobales.Decry64Decry(datosBusq.target)) : 0),
                        datosBusq.idEstatus,
                        datosBusq.mes,
                        datosBusq.ano
                    );
                else
                    new ServicioReportes().exportarReporte(
                        datosBusq.param,
                        datosBusq.idEmpresa,
                        datosBusq.idClinica,
                        datosBusq.idEspecialidad,
                        datosBusq.idConsultorio,
                        (!string.IsNullOrEmpty(datosBusq.target) ? Convert.ToInt32(FuncionesGlobales.Decry64Decry(datosBusq.target)) : 0),
                        datosBusq.idEstatus,
                        datosBusq.mes,
                        datosBusq.ano
                    );

                return true.ToString();
            }
            catch (Exception ex) {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }
    }
}
