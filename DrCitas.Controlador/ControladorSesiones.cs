﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DrCitas.Servicios;
using DrCitas.Dtos;
using System.Data;

namespace DrCitas.Controlador
{
    public class ControladorSesiones
    {

        #region Atributos y constructor

        private string StringConnDrCitas { get; set; }

        public ControladorSesiones()
        {
            switch (System.Net.Dns.GetHostName().ToUpper()) {
                case "PC-DESARROLLO":
                case "CMEDC0000040-PC":
                case "CMEDC000050":
                    StringConnDrCitas = "C3CqH6HFwPWPTcGcEbmpX4cFEqpWRq8y+k5hNGV2tqhgLmYgnPJrYW7qz+6JMfJ9F+VggSAODZ03/2otJ5kgMlmYcSe9rYjh7l52pMvL6kN6M7bN+JBA+DwFwn7JsV8/";
                    break;
                default:
                    StringConnDrCitas = "bLaTQuRW9H3+d1sT6jsrwVsPYMIqUyAEbH0XboWU6uwmeliOa9KCXqBPuSWSl3KA/jCZrNAiQ/H0osgdldTIL7jFjwLv8fOa3Cn1fhLIjJCJ8QH86lNjSzIB3Le/StLh";
                    break;
            }
        } 

        #endregion

        public string ConectarUsuario(string objSesion)
        {
            try
            {

                dtoUserPassw osp = FuncionesGlobales.ChangeObjTo<dtoUserPassw>(objSesion.Trim());
                osp.Usuario = FuncionesGlobales.Enc(osp.Usuario);
                osp.Password = FuncionesGlobales.Enc(osp.Password);

                return FuncionesGlobales.ChangeObjFrom<dtoObjSesion>(GetObject(new ServicioCliente().ConectarUsuario(osp, FuncionesGlobales.Decrypt(this.StringConnDrCitas))));

            }
            catch (Exception ex)
            {
                return FuncionesGlobales.JsonSerializer<dtoError>(new dtoError { message = ex.Message, stackTrace = ex.StackTrace });
            }
        }

        public string ValidarSesionUsuario(string objActual)
        {

            dtoObjSesion oa     = FuncionesGlobales.ChangeObjTo<dtoObjSesion>(objActual.Trim());
            DateTime date2      = DateTime.Now;
            DateTime dateSesion = Convert.ToDateTime(oa.FechaUlOper);
            int min             = ((TimeSpan)(date2 - dateSesion)).Minutes;

            if (min > 60)
            {
                oa.SesionOut = "OK";
                DesconectarSesion(objActual);
                return FuncionesGlobales.ChangeObjFrom<dtoObjSesion>(oa);
            }
            else
            {
                return FuncionesGlobales.ChangeObjFrom<dtoObjSesion>(GetObject(new ServicioCliente().ActualizarSesion(oa, FuncionesGlobales.Decrypt(this.StringConnDrCitas))));
            }

        }

        public bool DesconectarSesion(string objActual)
        {
            dtoObjSesion oa = FuncionesGlobales.ChangeObjTo<dtoObjSesion>(objActual);
            return new ServicioCliente().DesconectarSesion(oa);
        }

        private dtoObjSesion GetObject(DataTable dt)
        {
            dtoObjSesion aux = null;

            for (int i = 0; i < dt.Rows.Count; i++)
                aux = new dtoObjSesion
                {
                    Sexo            = dt.Rows[i].Field<string>(2).Trim()                ,
                    Nombre          = dt.Rows[i].Field<string>(3).Trim()                ,
                    Apellido        = dt.Rows[i].Field<string>(4).Trim()                ,
                    Correo          = dt.Rows[i].Field<string>(5).Trim()                ,
                    FechaNacimiento = dt.Rows[i].Field<DateTime>(6).ToString().Trim()   ,
                    FechaUlOper     = dt.Rows[i].Field<DateTime>(7).ToString().Trim()   ,
                    TipoCliente     = dt.Rows[i].Field<Int32>(8)                        ,
                    TipoClienteDesc = dt.Rows[i].Field<string>(9).Trim()                ,
                    idSesion        = dt.Rows[i].Field<string>(10).Trim()               ,
                    Telefono        = dt.Rows[i].Field<string>(11).Trim()               ,
                    SesionOut       = "",
                    Activo          = dt.Rows[i].Field<string>(13)
                };

            return aux;
        }

    
    }

    
}
