﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace DrCitas.Controlador
{
    public static class ControladorWebRequest
    {

        public static string POST(string Url, string Data, int timeOut = 100000)
        {
            string Result = String.Empty;
            System.Net.WebRequest Request = System.Net.WebRequest.Create(Url);
            try
            {
                Request.Method = "POST";
                Request.Timeout = timeOut;
                Request.ContentType = "application/x-www-form-urlencoded";
                byte[] sentData = Encoding.UTF8.GetBytes( Data );
                Request.ContentLength = sentData.Length;
                using (System.IO.Stream sendStream = Request.GetRequestStream())
                {
                    sendStream.Write(sentData, 0, sentData.Length);
                    sendStream.Close();
                }

                Result = GetResponse(Request);

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error! - ArgumentException {0}", ex.Message));
            }

            return Result;
        }

        public static string GET(string Url, string Data)
        {
            string Result = String.Empty;
            System.Net.WebRequest Request = System.Net.WebRequest.Create(Url + (string.IsNullOrEmpty(Data) ? "" : "?" + Data));
            try
            {
                Result = GetResponse(Request);
            }
            catch (ArgumentException ex)
            {
                Result = string.Format("Error! - ArgumentException {0}", ex.Message);
            }
            catch (WebException ex)
            {
                Result = string.Format("Error! WebException {0}", ex.Message);
            }
            catch (Exception ex)
            {
                Result = string.Format("Error! - {0}", ex.Message);
            }

            return Result;
        }
           
        private static string GetResponse(System.Net.WebRequest request)
        {
            string result = "";
            try
            {
                System.Net.WebResponse Response = request.GetResponse();
                using (System.IO.Stream stream = Response.GetResponseStream())
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(stream))
                    {
                        result = sr.ReadToEnd();
                        sr.Close();
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error! - Exception {0}", ex.Message));
            }
        }

    }
}
