﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrCitas.Dtos {

    public class dtoMisMedicos {

        public List<dtoDatosMisMedicos> MedicosTitular   { get; set; }

        public List<dtoAsociadoMedicos> MedicosAsociados { get; set; } 
    }

    public class dtoDatosMisMedicos {

        public string Nombre     { get; set; }
                                 
        public string idDr       { get; set; }

        public string celular    { get; set; }
                                 
        public string tlfCasa    { get; set; }

        public string tlfOficina { get; set; }

        public string Correo     { get; set; }

        public string Imagen     { get; set; }

        public string AcercaDeMi { get; set; }
        
        public List<dtoEspecialidad> Especialidades { get; set; }

        public class dtoDatosMisMedicosComparer : IEqualityComparer<dtoDatosMisMedicos> {
            public bool Equals(dtoDatosMisMedicos x, dtoDatosMisMedicos y) {
                if (x.idDr == y.idDr)
                    return true;
                else
                    return false;
            }

            public int GetHashCode(dtoDatosMisMedicos obj) {
                return obj.idDr.GetHashCode();
            }
        }
    }

    public class dtoEspecialidad {

        public string param          { get; set; }

        public string target         { get; set; }

        public int    idEspecialidad { get; set; }
        
        public string Especialidad   { get; set; }
                                     
        public string codigo         { get; set; }
                                     
        public string tipo           { get; set; }
                                     
        public string activo         { get; set; }
    }

    public class dtoAsociadoMedicos {
        
        public string Nombre     { get; set; }
        
        public string Parentesco { get; set; }
        
        public string idCliente  { get; set; }

        public List<dtoDatosMisMedicos> Medicos { get; set; }
    }
}
