﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrCitas.Dtos {

    public class dtoMisPacientes {

        public List<dtoDatosMisPacientes> Pacientes { get; set; }
    }

    public class dtoDatosMisPacientes {

        public string Nombre     { get; set; }

        public string idPaciente { get; set; }

        public string celular    { get; set; }

        public string tlfCasa    { get; set; }

        public string tlfOficina { get; set; }
        
        public string Correo     { get; set; }

        public string Imagen     { get; set; }
    }
}
