﻿using System;

namespace DrCitas.Dtos
{
    public class dtoAgenda
    {
        public int IdAgenda              { get; set; }
        public string IdCliente          { get; set; }
        public int IdConsultorio         { get; set; }
        public int IdClienteEspecialidad { get; set; }
        public int IdConsultorioHorario  { get; set; }
        public int DiaSemana             { get; set; }
        public int Cupos                 { get; set; }
        public int CupoActual            { get; set; }
        public int Duracion              { get; set; }

        public decimal? Estrellas { get; set; }

        public string TipoConsulta { get; set; }
        public string Activo       { get; set; }

        public DateTime FechaHoraInicio { get; set; }
        public DateTime FechaHoraFin    { get; set; }

        public int    IdEspecialidad { get; set; }

        public string Coordenadas     { get; set; }
        public string Especialidad    { get; set; }
        public string Direccion       { get; set; }
        public string Consultorio     { get; set; }
        public string Telefono        { get; set; }
        public string Titulo          { get; set; }
        public string NombreCliente   { get; set; }
        public string ApellidoCliente { get; set; }
        public string Correo          { get; set; }
        public string Facebook        { get; set; }
        public string Twitter         { get; set; }
        public string Foto            { get; set; }
        public string Ciudad          { get; set; }
        public string Estado          { get; set; }
    }
}
