﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrCitas.Dtos {

    public class dtoAsociado {

        public string nombre        { get; set; }

        public string apellido      { get; set; }

        public string tipoDocumento { get; set; }

        public int    documento     { get; set; }

        public string FN            { get; set; }

        public string parentesco    { get; set; }

        public string sexo          { get; set; }

        public string HM            { get; set; }

        public string param         { get; set; }

        public string cliente       { get; set; }

        public dtoDatosPaciente dtoDA    { get; set; }

        public List<dtoEmpresa> empresas { get; set; }
    }
}
