﻿using System;
using System.Collections.Generic;
using DrCitas.Dtos;

namespace DrCitas.Dtos
{
    public class dtoAtributo
    {
        public IEnumerable<dtoListaDeValores> ValoresDeAtributo { get; set; }

        public int    IdAtributo  { get; set; }
        public string Descripcion { get; set; }
    }
}
