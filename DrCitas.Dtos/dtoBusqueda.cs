﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrCitas.Dtos {
    
    public class dtoBusqueda
    {

        #region DrCitas

            public string param          { get; set; }

            public string tipoDocumento  { get; set; }

            public int    documento      { get; set; }
                                     
            public string nombre         { get; set; }

            public string apellido       { get; set; }

            public string sexo           { get; set; }

            public string codigo         { get; set; }
                                     
            public string tipo           { get; set; }

            public int    idConsultorio  { get; set; }

            public int    idClinica      { get; set; }
                                     
            public int    idEmpresa      { get; set; }

            public int    idEspecialidad { get; set; }
                                  
            public int    idCiudad       { get; set; }

            public int    idEstatus      { get; set; }

            public string inicio         { get; set; }
        
            public string final          { get; set; }

            public int    mes            { get; set; }

            public int    ano            { get; set; } 

            public string list           { get; set; }

            public string target         { get; set; }

        #endregion

        #region Cuidanet
            public string cedula { get; set; }

            public string tipoPlan { get; set; }

            public string result { get; set; }

            public int? clienteId { get; set; }

            public int? titularId { get; set; }

            public int? usuarioSistema { get; set; }

            public decimal? montoCobertura { get; set; }

            public decimal? montoConsumido { get; set; }

            public decimal? montoDisponible { get; set; }    
        #endregion

        //Paginar
        public int    pagina         { get; set; }

        public int    elementos      { get; set; }

        public int    total          { get; set; }
    }

    public class dtoPagClientes {

        public int totalPaginas { get; set; }

        public int pagina       { get; set; }

        public List<dtoClienteResume> clientes { get; set; }

        public dtoPagClientes() {
            this.totalPaginas   = 0;
            this.pagina         = 0;
            this.clientes       = new List<dtoClienteResume>();
        }
    }

    public class dtoPagMedicos {

        public int totalPaginas { get; set; }

        public int pagina       { get; set; }

        public List<dtoMedicoResume> medicos { get; set; }

        public dtoPagMedicos() {
            this.totalPaginas = 0;
            this.pagina       = 0;
            this.medicos      = new List<dtoMedicoResume>();
        }
    }
}