﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrCitas.Dtos {

    public class dtoCambioUsuario {
         
        public string param       { get; set; }

        public string oldUser     { get; set; }

        public string newUser     { get; set; }

        public string oldPassword { get; set; }
        
        public string newPassword { get; set; }
    }
}