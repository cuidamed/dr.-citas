﻿using System;
using System.Collections.Generic;

namespace DrCitas.Dtos {

    public class dtoCita {

        public int      IdCita          { get; set; }
                                        
        public string   IdPaciente      { get; set; }
                        
        public string   IdMedico        { get; set; }
                                        
        public int      IdAgenda        { get; set; }
                                        
        public int      IdEstatus       { get; set; }
                                        
        public int      IdRazon         { get; set; }
                        
        public int      IdConsultorio   { get; set; }
                                        
        public int?     IdEspecialidad  { get; set; }
                        
        public int      Numero          { get; set; }
                                        
        public int?     Documento       { get; set; }
                        
        public string   TipoDocumento   { get; set; }
                        
        public string   Correo          { get; set; }

        public DateTime FechaNacimiento { get; set; }

        public string   Sexo            { get; set; }
        
        public DateTime HoraInicio      { get; set; }
                                        
        public DateTime HoraFin         { get; set; }
                                        
        public string   Medico          { get; set; }
                                        
        public string   Razon           { get; set; }
                                        
        public string   Paciente        { get; set; }
                        
        public string   Consultorio     { get; set; }
                                        
        public string   Coordenadas     { get; set; }
                        
        public string   Titulo          { get; set; }
                                        
        public string   Estatus         { get; set; }
                                        
        public string   Especialidad    { get; set; }

        public string   Celular         { get; set; }
                                        
        public string   Recordatorio    { get; set; }
                                        
        public string   Puntaje         { get; set; }
                                        
        public string   Activo          { get; set; }

        public List<dtoEmpresa> empresas { get; set; }
    }

    public class dtoCitas {

        public List<dtoDatosCitas> CitasActivasPaciente { get; set; }
        
        public List<dtoDatosCitas> CitasPasadasPaciente { get; set; }
        
        public List<dtoDatosCitas> CitasActivasAsociado { get; set; }
        
        public List<dtoDatosCitas> CitasPasadasAsociado { get; set; }
    }

    public class dtoDatosCitas {

        public string IdCita               { get; set; }
                                           
        public string NumeroCita           { get; set; }
                                           
        public DateTime HoraInicio         { get; set; }
                                           
        public DateTime HoraFin            { get; set; }
                                           
        public string Celular              { get; set; }
                                           
        public string EstatusCita          { get; set; }
                                           
        public string idCliente            { get; set; }
                                           
        public string NombrePaciente       { get; set; }
                                           
        public string ApellidoPaciente     { get; set; }
                                           
        public string CorreoPaciente       { get; set; }
                                           
        public string Especialidad         { get; set; }
                                           
        public string Razon                { get; set; }
                                           
        public string Consultorio          { get; set; }
                                           
        public string IdMedico             { get; set; }
                                           
        public string Medico               { get; set; }
        
        public string TelefonoConsultorio  { get; set; }
        
        public string DireccionConsultorio { get; set; }
    }
        
    public class dtoDatosCita {

        public string param                { get; set; }

        public string idCita               { get; set; }

        public string idUsuarioSeguimiento { get; set; }
    }

    public class dtoReqPagCitas {

        public string param  { get; set; }

        public int modo      { get; set; }

        public int pagina    { get; set; }

        public int elementos { get; set; }
    }

    public class dtoPagCitas {

        public int totalPaginas     { get; set; }

        public int pagina           { get; set; }

        public List<dtoCita> citas { get; set; }

        public dtoPagCitas() {
            this.totalPaginas = 0;
            this.pagina       = 0;
            this.citas        = null;
        }
    }

    public class dtoActualizarCita {

        public int idCita { get; set; }

        public int estado { get; set; }
    }

    public class dtoDiagnostico {

        public int    idDiagnostico { get; set; }

        public string codigo        { get; set; }

        public string diagnostico   { get; set; }
    }

    public class dtoDiagnosticoCita {

        public int    idDiagnosticoCita { get; set; }

        public int    idCita            { get; set; }
                                        
        public string idMedico          { get; set; }

        public int?   idDiagnostico     { get; set; }

        public string diagnostico       { get; set; }
                                        
        public string observacion       { get; set; }
    }

    public class dtoDiagnosticos {

        public int    idCita   { get; set; }

        public string idMedico { get; set; }

        public List<dtoDiagnosticoInfo> informacion { get; set; }
    }

    public class dtoDiagnosticoInfo {

        public int    idDiagnosticoCita { get; set; }

        public int?   idDiagnostico     { get; set; }
                                        
        public string diagnostico       { get; set; }
                                        
        public string observacion       { get; set; }
    }
}