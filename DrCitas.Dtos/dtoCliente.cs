﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace DrCitas.Dtos
{
     [DataContract()]
    public class dtoCliente
    {
          [DataMemberAttribute]
        public List<dtoListaDeValores> Especialidades;
          [DataMemberAttribute]
        public List<dtoListaDeValores> Consultorios;
          [DataMemberAttribute]
          public List<dtoAtributo> AtributosCliente;
          [DataMemberAttribute]
          public List<dtoHorario> Horarios;
          [DataMemberAttribute]
          public HorariosSemanas Horario;
          [DataMemberAttribute]
        public string  IdCliente  { get; set; }
          [DataMemberAttribute]
        public int  IdCiudad   { get; set; }
          [DataMemberAttribute]
        public int  IdEstado   { get; set; }
          [DataMemberAttribute]
        public int  IdUsuario  { get; set; }
          [DataMemberAttribute]
        public int  IdTitulo   { get; set; }
          [DataMemberAttribute]
        public int  IdPerfil   { get; set; }
          [DataMemberAttribute]
        public int? IdCliente1 { get; set; }
          [DataMemberAttribute]
        public int  IdEstatus  { get; set; }
          [DataMemberAttribute]
        public int  Documento  { get; set; }
          [DataMemberAttribute]
        public int  Puntos     { get; set; }
          [DataMemberAttribute]
        public int  IdLenguaje { get; set; }
          [DataMemberAttribute]
        public decimal Estrellas { get; set; }
          [DataMemberAttribute]
        public DateTime  FechaIngreso    { get; set; }
          [DataMemberAttribute]
        public DateTime? FechaNacimiento { get; set; }
          [DataMemberAttribute]
        public DateTime? FechaActivacion { get; set; }
          [DataMemberAttribute]
        public string TipoCliente       { get; set; }
          [DataMemberAttribute]
        public string TipoDocumento     { get; set; }
          [DataMemberAttribute]
        public string Nombre            { get; set; }
          [DataMemberAttribute]
        public string Apellido          { get; set; }
          [DataMemberAttribute]
        public string Correo            { get; set; }
          [DataMemberAttribute]
        public string Celular           { get; set; }
          [DataMemberAttribute]
        public string Sexo              { get; set; }
          [DataMemberAttribute]
        public string TelefonoCasa      { get; set; }
          [DataMemberAttribute]
        public string TelefonoOficina   { get; set; }
          [DataMemberAttribute]
        public string TelefonoPrincipal { get; set; }
          [DataMemberAttribute]
        public string RecibirEmails     { get; set; }
          [DataMemberAttribute]
        public string Zip               { get; set; }
          [DataMemberAttribute]
        public string Facebook          { get; set; }
          [DataMemberAttribute]
        public string Twitter           { get; set; }
          [DataMemberAttribute]
        public string Comentario        { get; set; }
          [DataMemberAttribute]
        public string Activo            { get; set; }
          [DataMemberAttribute]
        public string Foto              { get; set; }
          [DataMemberAttribute]
        public string Login             { get; set; }
          [DataMemberAttribute]
        public string Clave             { get; set; }
          [DataMemberAttribute]
        public string ClaveNueva        { get; set; }
          [DataMemberAttribute]
        public int    IdEspecialidad { get; set; }
          [DataMemberAttribute]
        public string Especialidad   { get; set; }
          [DataMemberAttribute]
        public string Titulo         { get; set; }
          [DataMemberAttribute]
        public string Ciudad         { get; set; }
          [DataMemberAttribute]
        public string Estado         { get; set; }
          [DataMemberAttribute]
          public string TipoConsulta { get; set; }
    }
}
