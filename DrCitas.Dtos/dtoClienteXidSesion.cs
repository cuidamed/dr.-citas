﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrCitas.Dtos {

    public class dtoClienteXidSesion {

        public int    idCliente { get; set; }
        
        public int    idUsuario { get; set; }
        
        public int    idCiudad  { get; set; }
        
        public string correo    { get; set; }
        
        public string cedula    { get; set; }
        
        public string telefono  { get; set; }
        
        public string facebook  { get; set; }
        
        public string twitter   { get; set; }
    }

    public class dtoClienteResume {

        public string idCliente      { get; set; }
                                     
        public string idUsuario      { get; set; }
                                     
        public string nombre         { get; set; }
                                     
        public string apellido       { get; set; }
                                     
        public string tipoCliente    { get; set; }
                                     
        public int    idCiudad       { get; set; }
                                     
        public string ciudad         { get; set; }
                                     
        public string correo         { get; set; }
                                     
        public string tipoDocumento  { get; set; }
                                     
        public int?   documento      { get; set; }
                                     
        public string telefono       { get; set; }

        public DateTime fechaIngreso { get; set; }

        public string numeroColegio  { get; set; }
                                     
        public string matriculaSAS   { get; set; }
                                     
        public string facebook       { get; set; }
                                     
        public string twitter        { get; set; }

        public string activo         { get; set; }

        public List<dtoAsociado> misAsociados   { get; set; } 

        public List<dtoEspecialidad> especialidades { get; set; }

        public List<dtoEmpresa> empresas { get; set; }



        public class dtoClienteResumeComparer : IEqualityComparer<dtoClienteResume> {
            public bool Equals(dtoClienteResume x, dtoClienteResume y) {
                if (x.idCliente == y.idCliente)
                    return true;
                else
                    return false;
            }

            public int GetHashCode(dtoClienteResume obj) {
                return obj.idCliente.GetHashCode();
            }
        }
    }

    public class dtoMedicoResume {

        public string   idCliente                       { get; set; }
                                                        
        public string   titulo                          { get; set; }
                                                        
        public string   nombre                          { get; set; }
                                                        
        public string   apellido                        { get; set; }
                                                        
        public string   sexo                            { get; set; }
                                                        
        public int      idEstado                        { get; set; }
                                                        
        public string   estado                          { get; set; }
                                                        
        public int      idCiudad                        { get; set; }
                                                        
        public string   ciudad                          { get; set; }
                                                        
        public string   correo                          { get; set; }
                                                        
        public string   tipoDocumento                   { get; set; }
                                                        
        public int?     documento                       { get; set; }
                                                        
        public string   celular                         { get; set; }
                                                        
        public string   telefono                        { get; set; }
                                                        
        public DateTime fechaNacimiento                 { get; set; }
                                                        
        public DateTime fechaIngreso                    { get; set; }
                                                        
        public string   facebook                        { get; set; }
                                                        
        public string   twitter                         { get; set; }
                                                        
        public int?     puntos                          { get; set; }
                                                        
        public decimal? estrellas                       { get; set; }
                                                        
        public string   comentario                      { get; set; }
                                                        
        public int      idConsultorio                   { get; set; }
                                                        
        public string   consultorio                     { get; set; }
                                                        
        public string   direccion                       { get; set; }
                                                        
        public string   tipoConsulta                    { get; set; }

        public string   imagen                          { get; set; }
                                                        
        public dtoCoordenada coordConsultorio           { get; set; }
                                                        
        public HorariosSemanas horarios                 { get; set; }

        public List<dtoDatosMisEstudios> especialidades { get; set; }

        public List<dtoEmpresa> empresas                { get; set; }

        public class dtoMedicoResumeComparer : IEqualityComparer<dtoMedicoResume> {
            public bool Equals(dtoMedicoResume x, dtoMedicoResume y) {
                if (x.idCliente == y.idCliente && x.idConsultorio == y.idConsultorio && x.tipoConsulta == y.tipoConsulta)
                    return true;
                else
                    return false;
            }

            public int GetHashCode(dtoMedicoResume obj) {
                return obj.idCliente.GetHashCode();
            }
        }
    }
}