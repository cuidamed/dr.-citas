﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace DrCitas.Dtos {

    public class dtoClinica
    {

        public string param { get; set; }

        public string target { get; set; }

        public int? idClinica { get; set; }

        public int idCiudad { get; set; }

        public string descripcion { get; set; }

        public string direccion { get; set; }

        public string rif { get; set; }

        public string telefono { get; set; }

        public string activo { get; set; }

        [DataMemberAttribute]
        public dtoCoordenada coordenadas { get; set; }

    }

    public class dtoCoordenada {


        public double x { get; set; }

        public double y { get; set; }

        public dtoCoordenada() {
            this.x = 0;
            this.y = 0;
        }

        public dtoCoordenada(string coordenadas) {
            if (!string.IsNullOrEmpty(coordenadas)) {
                this.x = double.Parse((coordenadas.Split(',')[0]).Replace(".",","));
                this.y = double.Parse((coordenadas.Split(',')[1]).Replace(".", ","));
            }
        }

        public string ToString() {
            return this.x.ToString() + "," + this.y.ToString();
        }
    }
    
}
