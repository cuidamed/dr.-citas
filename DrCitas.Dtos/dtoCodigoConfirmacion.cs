﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrCitas.Dtos
{
    public class dtoCodigoConfirmacion
    {
        public string   IdSesion    { get; set; }
        public string   Nro_Cita    { get; set; }
        public string   html { get; set; }
    }
}
