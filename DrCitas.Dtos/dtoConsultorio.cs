﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DrCitas.Dtos {

    public class dtoConsultorio {

        public List<dtoConsultorioHorario> ConsultorioHorario { get; set;}

        public int IdCliente      { get; set; }
        public int IdConsultorio  { get; set; }
        public int IdEspecialidad { get; set; }
        public int IdEstado       { get; set; }
        public int IdCiudad       { get; set; }

        public string Direccion     { get; set; }
        public string DireccionMapa { get; set; }
        public string Telefono      { get; set; }
        public string Coordenadas   { get; set; }
        public string Estado        { get; set; }
        public string Ciudad        { get; set; }
        public string Especialidad  { get; set; }
        public string Nombre        { get; set; }
        public string Activo        { get; set; }

        public class dtoConsultorioComparer : IEqualityComparer<dtoConsultorio> {
            public bool Equals(dtoConsultorio x, dtoConsultorio y) {
                if (x.IdConsultorio == y.IdConsultorio && x.Nombre == y.Nombre)
                    return true;
                else
                    return false;
            }

            public int GetHashCode(dtoConsultorio obj) {
                return obj.IdConsultorio.GetHashCode();
            }
        }
    }

    public class dtoConsultorios {

        public List<dtoConsultorioHorario> ConsultorioHorario { get; set; }

        public int IdCliente        { get; set; }
        public int IdConsultorio    { get; set; }
        public int IdEspecialidad   { get; set; }
        public int IdClinica        { get; set; }
        public int IdEstado         { get; set; }
        public int IdCiudad         { get; set; }

        public string IdUsuario     { get; set; }
        public string Direccion     { get; set; }
        public string Telefono      { get; set; }
        public string Nombre        { get; set; }
        public string Activo        { get; set; }

        public string Estado        { get; set; }
        public string Ciudad        { get; set; }
        public string Especialidad  { get; set; }
        public string Coordenadas   { get; set; }
        public string Clinica       { get; set; }
        public string sesion        { get; set; }

        [DataMemberAttribute]
        public dtoCoordenada coordenadas { get; set; }
    }
}