﻿using System;
using System.Collections.Generic;

namespace DrCitas.Dtos {

    public class dtoConsultorioHorario {

        public int    IdConsultorioHorario  { get; set; }

        public int    IdConsultorio         { get; set; }

        public int    IdUsuario             { get; set; }

        public int    IdCliente             { get; set; }

        public string Dias                  { get; set; }

        public int    Meses                 { get; set; }

        public string TipoConsulta          { get; set; }

        public string HoraDesde             { get; set; }

        public string HoraHasta             { get; set; }

        public int    Duracion              { get; set; }

        public int    Cupos                 { get; set; }

        public int    allDays               { get; set; }

        public string Activo                { get; set; }

        public string IdSesion              { get; set; }

        public DateTime? FechaInicio        { get; set; }

        public DateTime FechaFin            { get; set; }

        public List<dtoHorasDias> HorasDias { get; set; }
    }

    public class dtoHorasDias {

        public string dia       { get; set; }

        public string horaDesde { get; set; }

        public string horaHasta { get; set; }

        public int    cupos     { get; set; }

        public int    duracion  { get; set; }
    }
}
