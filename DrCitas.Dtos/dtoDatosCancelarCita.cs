﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrCitas.Dtos
{
    public class dtoDatosCancelarCita
    {
        public int idCita       { get; set; }
        public int idCliente    { get; set; }
        public int idAgenda     { get; set; }
        public int Numero       { get; set; }
        public string Celular   { get; set; }
        public string Correo    { get; set; }
    }
}
