﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrCitas.Dtos {

    public class dtoDatosGetHorarios {

        public string idCliente { get; set; }

        public int idEspecialidad { get; set; }

        public string idConsultorio { get; set; }

        public string fechaInicio { get; set; }

        public string idUsuario { get; set; }

        public string tipoConsulta { get; set; }
    }
}