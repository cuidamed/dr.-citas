﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DrCitas.Dtos {

    public class dtoDatosMedico {

        public string param           { get; set; }
        
        public string nombre          { get; set; }

        public string apellido        { get; set; }

        public string fechaNacimiento { get; set; }
        
        public string tipoDocumento   { get; set; }

        public int?   documento       { get; set; }

        public string RIF             { get; set; }
                  
        public string sexo            { get; set; }
                                      
        public string correo          { get; set; }

        public string celular         { get; set; }

        public string telefonoCasa    { get; set; }

        public string telefonoOficina { get; set; }

        public string numeroColegio   { get; set; }
                                      
        public string matriculaSAS    { get; set; }

        public int    idCiudad        { get; set; }

        public int    idEstado        { get; set; }

        public int    idPais          { get; set; }
                                      
        public string acercaDeMi      { get; set; }
                                      
        public string facebook        { get; set; }
                                      
        public string twitter         { get; set; }

        public string titulo          { get; set; }
                                      
        public string imagen          { get; set; }
                                      
        public string recibirCorreo   { get; set; }
                        
        public List<dtoDatosMisEstudios>            estudios      { get; set; }

        public List<dtoDatosMisCursos>              cursos        { get; set; }

        public List<dtoDatosMisActividadesDocentes> actividades   { get; set; }

        public List<dtoDatosMisLibros>              libros        { get; set; }

        public List<dtoDatosMisPublicaciones>       publicaciones { get; set; }

        public List<dtoDatosMisIdiomas>             idiomas       { get; set; }

        public List<dtoConsultorios>                consultorios  { get; set; }

    }

    public class dtoDatosMisEstudios {

        public int    idEspecialidad  { get; set; }

        public int    idNEspecialidad { get; set; }

        public string Especialidad    { get; set; }
        
        public int    idUniversidad   { get; set; }

        public string Universidad     { get; set; }

        public string fechaGrado      { get; set; }
    }

    public class dtoDatosMisCursos {

        public int idAtributoLista { get; set; }

        public string Curso        { get; set; }

        public string Mes          { get; set; }

        public string Anio         { get; set; }
    }

    public class dtoDatosMisActividadesDocentes {

        public int    idAtributoLista { get; set; }

        public string Actividad       { get; set; }

        public string Mes             { get; set; }

        public string Anio            { get; set; }
    }

    public class dtoDatosMisLibros {

        public int    idAtributoLista { get; set; }
        
        public string Libro           { get; set; }

        public string Mes             { get; set; }

        public string Anio            { get; set; }
    }

    public class dtoDatosMisPublicaciones {

        public int    idAtributoLista { get; set; }

        public string Publicacion     { get; set; }

        public string Mes             { get; set; }

        public string Anio            { get; set; }
    }

    public class dtoDatosMisIdiomas {

        public int    idAtributoLista { get; set; }
        
        public string Idioma          { get; set; }
    }
}