﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrCitas.Dtos {

    public class dtoDatosPaciente {

        public string   param                { get; set; }

        public string   nombre               { get; set; }

        public string   apellido             { get; set; }

        public string   fechaNacimiento      { get; set; }

        public DateTime FechaNacimiento      { get; set; }  
        
        public string   tipoDocumento        { get; set; }

        public int?     documento            { get; set; }

        public string   sexo                 { get; set; }

        public string   correo               { get; set; }

        public string   celular              { get; set; }

        public string   telefonoCasa         { get; set; }

        public string   telefonoOficina      { get; set; }

        public int      idCiudad             { get; set; }

        public int      idEstado             { get; set; }

        public int      idPais               { get; set; }

        public string   facebook             { get; set; }

        public string   twitter              { get; set; }

        public string   imagen               { get; set; }

        public string   recibirCorreo        { get; set; }

        public string   interrogatorioMedico { get; set; }

        public List<dtoEmpresa> aseguradoras { get; set; }
    }
}