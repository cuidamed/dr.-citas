﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrCitas.Dtos
{
    public class dtoDesactivarAgenda
    {
        public int       idCliente	   { get; set; }
        public int       idUsuario	   { get; set; }
        public int       idConsultorio { get; set; }
        public DateTime? fechaDesde	   { get; set; }
        public DateTime? fechaHasta	   { get; set; }
        public string    sesion        { get; set; }
        public List<dtoDesabilitar> desactivarHoras { get; set; }
    }
    
    public class dtoDesabilitar
    {
        public int idMedico  { get; set; }
        public int nroCita  { get; set; }
    }
    
}
