﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DrCitas.Dtos {

    public class dtoEmpresa {

        public string param         { get; set; }

        public string target        { get; set; }
                                    
        public int    idEmpresa     { get; set; }
                                    
        public string nombre        { get; set; }

        public string tipoDocumento { get; set; }

        public int    documento     { get; set; }

        public string direccion     { get; set; }
                                    
        public string telefono      { get; set; }
                                    
        public bool   activo        { get; set; }
    }
}
