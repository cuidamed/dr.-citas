﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrCitas.Dtos
{
    public class dtoEnviarInfUsuario
    {
        public string param     { get; set; }

        public string Nombre    { get; set; }

        public string Correo    { get; set; }

        public string Telefono  { get; set; }

        public string Twitter   { get; set; }

        public string Facebook  { get; set; }
    }
}
