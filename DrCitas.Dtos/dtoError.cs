﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrCitas.Dtos
{
    public class dtoError
    {
        public string message           { get; set; }
        public string stackTrace        { get; set; }
        public string innerException    { get; set; }
    }
}
