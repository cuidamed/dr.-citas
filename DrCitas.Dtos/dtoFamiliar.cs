﻿using System;

namespace DrCitas.Dtos
{
    public class dtoFamiliar
    {
        public int    IdCliente       { get; set; }
        public string Nombre          { get; set; }
        public string Apellido        { get; set; }
        public string Sexo            { get; set; }
        public string FechaNacimiento { get; set; }
        public string Celular         { get; set; }
        public string Correo          { get; set; }
    }
}
