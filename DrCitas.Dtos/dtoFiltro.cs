﻿using System;

namespace DrCitas.Dtos
{
    public class dtoFiltro
    {
        public int IdEspecialidad { get; set; }
        public int IdEstado       { get; set; }
        public int IdCiudad       { get; set; }
        public int IdTitulo       { get; set; }
        public int IdLenguage     { get; set; }

        public int Estrellas { get; set; }
        public int EdadMenor { get; set; }
        public int EdadMayor { get; set; }

        public string TipoConsulta  { get; set; }
        public string TipoDocumento { get; set; }
        public string Sexo          { get; set; }
        public string HoraDesde     { get; set; }
        public string HoraHasta     { get; set; }
        public string Nombre        { get; set; }
        public string Apellido      { get; set; }
        public string Consultorio   { get; set; }
        public string Dias          { get; set; }
        public string MisMedicos    { get; set; }
    }
}
