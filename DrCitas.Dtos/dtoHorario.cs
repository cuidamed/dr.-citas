﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DrCitas.Dtos
{
      [DataContract()]
    public class dtoHorario
    {
        [DataMemberAttribute]
        public int IdAgendaDia1 { get; set; }
        [DataMemberAttribute]
        public int IdAgendaDia2 { get; set; }
        [DataMemberAttribute]
        public int IdAgendaDia3 { get; set; }
        [DataMemberAttribute]
        public int IdAgendaDia4 { get; set; }
        [DataMemberAttribute]
        public int IdAgendaDia5 { get; set; }
        [DataMemberAttribute]
        public int IdAgendaDia6 { get; set; }
        [DataMemberAttribute]
        public int IdAgendaDia7 { get; set; }
        [DataMemberAttribute]
        public string Dia1 { get; set; }
        [DataMemberAttribute]
        public string Dia2 { get; set; }
        [DataMemberAttribute]
        public string Dia3 { get; set; }
        [DataMemberAttribute]
        public string Dia4 { get; set; }
        [DataMemberAttribute]
        public string Dia5 { get; set; }
        [DataMemberAttribute]
        public string Dia6 { get; set; }
        [DataMemberAttribute]
        public string Dia7 { get; set; }
        [DataMemberAttribute]
        public string UrlDia1 { get; set; }
        [DataMemberAttribute]
        public string UrlDia2 { get; set; }
        [DataMemberAttribute]
        public string UrlDia3 { get; set; }
        [DataMemberAttribute]
        public string UrlDia4 { get; set; }
        [DataMemberAttribute]
        public string UrlDia5 { get; set; }
        [DataMemberAttribute]
        public string UrlDia6 { get; set; }
        [DataMemberAttribute]
        public string UrlDia7 { get; set; }

          [DataMemberAttribute]
        public string TipoConsulta  { get; set; }
        [DataMemberAttribute]
        public int    Cupos         { get; set; }
        [DataMemberAttribute]
        public int    IdConsultorio { get; set; }

    }

      [DataContract()]
    public class ListDtoHorario 
    {
         [DataMemberAttribute]
        public string semana { get; set; }
         [DataMemberAttribute]
        public List<dtoHorario> lhorarios { get; set; }

        public ListDtoHorario(string sem, List<dtoHorario> l) 
        {
            this.semana = sem;
            this.lhorarios = l;
        }
    }

    [DataContract()]
    public class HorariosSemanas 
    {
         [DataMemberAttribute]
        List<ListDtoHorario> HorariosSem { get; set; }

        public HorariosSemanas(List<ListDtoHorario> l) 
        {
            this.HorariosSem = l;
        }
    }
}
