﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrCitas.Dtos {

    public class dtoImagen {

        public string param     { get; set; }
                                
        public string imagen    { get; set; }

        public string extension { get; set; }
    }
}
