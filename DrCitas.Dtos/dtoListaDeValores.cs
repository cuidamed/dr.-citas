﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace DrCitas.Dtos
{
     [DataContract()]
    public class dtoListaDeValores
    {
        [DataMemberAttribute]
        public int    Id            { get; set; }
        [DataMemberAttribute]
        public string Codigo        { get; set; }
        [DataMemberAttribute]
        public string Descripcion   { get; set; }
        [DataMemberAttribute]
        public string Direccion     { get; set; }
        [DataMemberAttribute]
        public string Telefonos     { get; set; }
        [DataMemberAttribute]
        public HorariosSemanas Horarios  { get; set; }

    }
}
