﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DrCitas.Dtos
{
    public class dtoMensaje
    {
        public int IdError        { get; set; }
        public string Mensaje     { get; set; }
        public string Descripcion { get; set; }
    }
}