﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrCitas.Dtos
{
    public class dtoObjSesion
    {
        //public int idCliente            { get; set; }

        //public int idUsuario            { get; set; }

        public string Sexo              { get; set; }

        public string Nombre            { get; set; }

        public string Apellido          { get; set; }

        public string Correo            { get; set; }

        public string FechaNacimiento   { get; set; }

        public string FechaUlOper       { get; set; }

        public int TipoCliente          { get; set; }

        public string TipoClienteDesc   { get; set; }

        public string idSesion          { get; set; }

        public string SesionOut         { get; set; }

        public string Telefono          { get; set; }

        public string Activo            { get; set; } 

    }
}
