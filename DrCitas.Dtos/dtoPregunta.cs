﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrCitas.Dtos
{
    public class dtoPregunta
    {
        public int    IdPregunta  { get; set; }
        public string Descripcion { get; set; }
        public int    Valor       { get; set; }
        public bool   Evaluada    { get; set; }
    }
}
