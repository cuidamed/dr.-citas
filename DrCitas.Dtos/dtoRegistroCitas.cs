﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrCitas.Dtos {

    public class dtoRegistroCitas {

        public string param         { get; set; }
        
        public string idAgenda      { get; set; }

        public string idMedico      { get; set; }
                                    
        public string idRazon       { get; set; }
                                            
        public string idCreador     { get; set; }

        public string jsonIM        { get; set; }

        public string idEspecialidad { get; set; }

        public dtoAsociado asociado { get; set; }
    }
}
