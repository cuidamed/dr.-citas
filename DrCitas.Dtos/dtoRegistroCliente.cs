﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrCitas.Dtos {

    public class dtoRegistroCliente {

        public string Nombre             { get; set; }
                                         
        public string Apellido           { get; set; }
                                         
        public string FechaNacimiento    { get; set; }
                                         
        public string Sexo               { get; set; }
                                         
        public string Correo             { get; set; }
                                         
        public string Usuario            { get; set; }
                                         
        public string Password           { get; set; }
                                         
        public string Celular            { get; set; }
                                         
        public string TipoCliente        { get; set; }
                                         
        public string RecibirCorreo      { get; set; }
                                         
        public string numeroColegio      { get; set; }
                                         
        public string matriculaSAS       { get; set; }
                                         
        public string tipoDocumento      { get; set; }

        public string tipoPlan { get; set; }

        public int? usuarioSistema { get; set; }

        public int    documento          { get; set; }

        public int    idCiudad           { get; set; }

        public int    IdUsuarioPrincipal { get; set; }

        public int clienteId { get; set; }

        public int titularId { get; set; }

        public dtoEmpresa empresa { get; set; }

    }
}