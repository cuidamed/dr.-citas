﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrCitas.Dtos {
    
    public class dtoReporteGeneralCitas {

        public int    idEstado { get; set; }

        public string estado   { get; set; }

        public int    cantidad { get; set; }

        public object[] ToArray() {
            return new object[] { this.idEstado, this.estado, this.cantidad };
        }
    }

    public class dtoReporteCita {

        public int?    idCita           { get; set; }
                                       
        public int?    idEstatus        { get; set; }
                                       
        public string  estatus          { get; set; }
                                       
        public int?    idPaciente       { get; set; }
                                     
        public string  nombrePaciente   { get; set; }
                                     
        public string  apellidoPaciente { get; set; }

        public int?    idMedico         { get; set; }

        public string  nombreMedico     { get; set; }

        public string  apellidoMedico   { get; set; }

        public int?    idRazon          { get; set; }

        public string  razon            { get; set; }                    
                                       
        public int?    idConsultorio    { get; set; }
                                       
        public string  consultorio      { get; set; }
                                               
        public int?    idClinica        { get; set; }
                                       
        public string  clinica          { get; set; }
                                       
        public int?    idEmpresa        { get; set; }
                                       
        public string  empresa          { get; set; }

        public object[] ToArray() {
            return new object[] {
                this.idCita          ,
                this.idEstatus       ,
                this.estatus         ,
                this.idPaciente      ,
                this.nombrePaciente  ,
                this.apellidoPaciente,
                this.idMedico        ,
                this.nombreMedico    ,
                this.apellidoMedico  ,
                this.idRazon         ,
                this.razon           ,
                this.idConsultorio   ,
                this.consultorio     ,
                this.idClinica       ,
                this.clinica         ,
                this.idEmpresa       ,
                this.empresa
            };
        }
    }
}