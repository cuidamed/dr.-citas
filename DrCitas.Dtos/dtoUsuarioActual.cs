﻿using System;
using System.Linq;

namespace DrCitas.Dtos
{
    public class dtoUsuarioActual
    {
        public int IdSesion   { get; set; }
        public int IdCliente  { get; set; }
        public int IdUsuario  { get; set; }
        public int IdEstado   { get; set; }
        public int IdCiudad   { get; set; }
        public int IdPerfil   { get; set; }
        public int IdLenguaje { get; set; }
        public int Mensajes   { get; set; }

        public string Sexo            { get; set; }
        public string TipoCliente     { get; set; }
        public string Titulo          { get; set; }
        public string Nombre          { get; set; }
        public string Apellido        { get; set; }
        public string FechaNacimiento { get; set; }
        public string Telefono        { get; set; }
        public string Correo          { get; set; }
        public string UltimaConexion  { get; set; }
    }
}
