﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace DrCitas.Servicios
{
    internal class ADO : IDisposable
    {
        private string DrCitasStringConn { get; set; }
        private string StringConnActual  { get; set; }
        SqlDataAdapter da           = null;
        SqlConnection dbConnection  = null;
        DataSet ds                  = null;
        DataTable dt                = null;
        SqlCommand cmd              = null;

        public ADO(string dbStringConn)
        {

            this.StringConnActual = dbStringConn;
            dbConnection = new SqlConnection(this.StringConnActual);
            if (dbConnection.State == ConnectionState.Closed) dbConnection.Open();
        }

        public DataSet GetDataSetQuery(string sql) 
        {
            try
            {
                using (cmd = new SqlCommand(sql , dbConnection))
                {
                    da = new SqlDataAdapter(cmd);
                    ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " StackTrace: " + ex.StackTrace , ex.InnerException);
            }
        }

        public DataTable GetDataTableQuery(string sql)
        {
            try
            {
                using (cmd = new SqlCommand(sql, dbConnection))
                {
                    da = new SqlDataAdapter(cmd);
                    dt = new DataTable();
                    da.Fill(dt);
                    return dt;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " StackTrace: " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool execQuery(string sql)
        {
            using (cmd = new SqlCommand(sql, dbConnection))
            {
                try 
	            {	        
		            this.cmd.ExecuteScalar();
                    return true;
	            }
	            catch (Exception)
	            {
		            return true;
	            }
            }

        }

        public string execScalarQuery(string sql)
        {
            object data;

            using (cmd = new SqlCommand(sql, dbConnection))
            {
                try
                {
                    this.cmd.ExecuteScalar();
                    return ((data = this.cmd.ExecuteScalar()) != null ? data.ToString().Trim() : "");
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);;
                }
            }

        }

        protected void Dispose(Boolean free)
        {
            if (free)
            {
                if (this.dbConnection != null)
                {
                    if (dbConnection.State == ConnectionState.Open) dbConnection.Close();
                    this.dbConnection.Dispose();
                    this.dbConnection = null;
                }
                if (this.cmd != null)
                {
                    this.cmd.Dispose();
                    this.cmd = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ADO()
        {
            Dispose(false);
        }


    }
}
