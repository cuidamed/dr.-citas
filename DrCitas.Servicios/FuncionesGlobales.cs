﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Security.Cryptography;
using System.Data;

namespace DrCitas.Servicios
{
    public class FuncionesGlobales
    {

        private static string key = 
                        (new Func<string>(() => {
                            switch (System.Net.Dns.GetHostName().ToUpper())
                            {
                                case "PC-DESARROLLO":
                                case "CMEDC0000040-PC":
                                case "CMEDC000050":
                                    return "algunaClave";
                                default:
                                   return "Ki8oL3dlYi8pLnRlc3QoU3RyaW5nKGRlc2Fycm9sbG8yNTZkcmNpdGFzKSkvKg==";
                            }
                        }))();
      
        /*for SHA-1*/

        public enum EncodeStyle {
            Dig,
            Hex,
            Base64
        };

        public static string JsonSerializer<T>(T t)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream();
            ser.WriteObject(ms, t);
            string jsonString = Encoding.UTF8.GetString(ms.ToArray());
            ms.Close();
            return jsonString;
        }

        public static T JsonDeserialize<T>(string jsonString)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        public static string GetJsonp(string json, bool modo = true)
        {
            System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", " filename=prova.json");
            System.Web.HttpContext.Current.Response.ContentType = "application/json";
            System.Web.HttpContext.Current.Response.Write((modo) ? AppendJsonpCallback(HttpContext.Current.Request.Url.AbsoluteUri, json, HttpContext.Current.Request) : json);
            System.Web.HttpContext.Current.Response.End();
            return "";
        }

        private static string AppendJsonpCallback(string url, string strBuffer, HttpRequest request)
        {
            return request.Params["callback"] + "(" + strBuffer + ");";
        }

        public static string Encrypt(string cadena)
        {
            //arreglo de bytes donde guardaremos la llave
            byte[] keyArray;
            //arreglo de bytes donde guardaremos el texto
            //que vamos a encriptar
            byte[] Arreglo_a_Cifrar =
            UTF8Encoding.UTF8.GetBytes(cadena);

            //se utilizan las clases de encriptación
            //provistas por el Framework
            //Algoritmo MD5
            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            //se guarda la llave para que se le realice
            //hashing
            keyArray = hashmd5.ComputeHash( UTF8Encoding.UTF8.GetBytes(key) );
            hashmd5.Clear();
            //Algoritmo 3DAS
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();

            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            //se empieza con la transformación de la cadena
            ICryptoTransform cTransform = tdes.CreateEncryptor();

            //arreglo de bytes donde se guarda la
            //cadena cifrada
            byte[] ArrayResultado 
                = cTransform.TransformFinalBlock(Arreglo_a_Cifrar,0, Arreglo_a_Cifrar.Length);

            tdes.Clear();

            //se regresa el resultado en forma de una cadena
            return Convert.ToBase64String(ArrayResultado, 0, ArrayResultado.Length);

        }

        public static string Decrypt(string clave)
        {
            byte[] keyArray;
            //convierte el texto en una secuencia de bytes
            byte[] Array_a_Descifrar = Convert.FromBase64String(clave);

            //se llama a las clases que tienen los algoritmos
            //de encriptación se le aplica hashing
            //algoritmo MD5
            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();

            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));

            hashmd5.Clear();

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();

            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();

            byte[] resultArray =
            cTransform.TransformFinalBlock(Array_a_Descifrar, 0, Array_a_Descifrar.Length);

            tdes.Clear();
            //se regresa en forma de cadena
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        public static string DecryptFromBase64(string cadena)
        {
            var encoder = new System.Text.UTF8Encoding();
            var utf8Decode = encoder.GetDecoder();

            byte[] cadenaByte = Convert.FromBase64String(cadena);
            int charCount = utf8Decode.GetCharCount(cadenaByte, 0, cadenaByte.Length);
            char[] decodedChar = new char[charCount];
            utf8Decode.GetChars(cadenaByte, 0, cadenaByte.Length, decodedChar, 0);
            return new String(decodedChar);
        }

        public static string EncryptFromBase64(string cadena)
        {
            byte[] cadenaByte = new byte[cadena.Length];
            cadenaByte = System.Text.Encoding.UTF8.GetBytes(cadena);
            return  Convert.ToBase64String(cadenaByte);
        }

        /// <summary>
        /// Encripta base 64 y luego retorna un encryptado hash 256
        /// </summary>
        public static string Encry64Encry(string value) 
        {
            return Encrypt(EncryptFromBase64(value));
        }

        /// <summary>
        /// Desencripta hash 256 y luego retorna un desencriptado base 64 (Encry64Encry * -1)
        /// </summary>
        public static string Decry64Decry(string value)
        {
            return DecryptFromBase64((Decrypt(value)));//Encrypt(EncryptFromBase64(value));
        }

        /// <summary>
        /// obtiene un string base 64 como parametro, lo desencrypta base64 y lo serializa
        /// </summary>
        /// <typeparam name="T">es el tipo que se va a deserializar</typeparam>
        /// <param name="obj">es un string obtenido de la vista, se debe encontrar en base 64</param>
        /// <returns></returns>
        public static T ChangeObjTo<T>(string obj) 
        {
            return FuncionesGlobales.JsonDeserialize<T>(FuncionesGlobales.DecryptFromBase64(obj));
        }

        /// <summary>
        /// jsonSerializer a un objeto determinado y lo encrypta para devolverlo a la vista
        /// </summary>
        /// <typeparam name="T">el tipo de objeto que va como parametro</typeparam>
        /// <param name="obj">objeto que se va a serializar a json</param>
        /// <returns></returns>
        public static string ChangeObjFrom<T>(T obj)
        {
            return FuncionesGlobales.EncryptFromBase64(FuncionesGlobales.JsonSerializer<T>(obj));
        }

        public static string Enc(string value) 
        {
            return FuncionesGlobales.Encrypt(FuncionesGlobales.DecryptFromBase64(value));
        }

        public static string Dec(string value)
        {
            return FuncionesGlobales.EncryptFromBase64(FuncionesGlobales.Decrypt(value));
        }

        ///---------------------------------------------------------------------------------------------------------------------
        ///<summary>Operación para obtener un atributo de una agenda.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public static string ObtenerValor(string pDato, string pAgenda, int idCliente = 0, int idEspecialidad = 0)
        {
            if (pAgenda == null) return "";

            string[] vDatos = pAgenda.Split(new char[] { '+' });

            switch (pDato)
            {
                case "Hora":
                    if (vDatos[3] == "O")
                        return Convert.ToDateTime("01/01/1900 " + vDatos[1]).ToShortTimeString() + " a<br/>" + Convert.ToDateTime("01/01/1900 " + vDatos[2]).ToShortTimeString();
                    return Convert.ToDateTime("01/01/1900 " + vDatos[1]).ToShortTimeString();
                case "Url":
                    return "TuCita.html?IdCliente=" + Encry64Encry(idCliente.ToString()) + "&IdEspecialidad=" + idEspecialidad.ToString() + "&NroCita=" + vDatos[0];
                default:
                    break;
            }
            return "";
        }

        public static string FixParam(string param)
        {
            if (param.Contains(","))
                return (
                            new Func<string, string>(
                                    (f) =>
                                    {
                                        string aux = "";
                                        for (int i = 0; i < param.Split(',').Length; i++)
                                            aux += (i == 0 ? "" : ",") + "'" + param.Split(',')[i].Trim() + "'";
                                        return aux;
                                    })
                       )(param);
            else
                return "'" + param + "'";
        }

        private static string ByteArrayToString(byte[] input, EncodeStyle encode) {
            StringBuilder sOutput = new StringBuilder(input.Length);
            
            if (EncodeStyle.Base64 == encode) {
                return Convert.ToBase64String(input);
            }

            for (int i = 0; i < input.Length; i++) {
                switch (encode) {
                    case EncodeStyle.Dig:
                        //encode to decimal with 3 digits so 7 will be 007 (as range of 8 bit is 127 to -128)
                        sOutput.Append(input[i].ToString("D3"));
                        break;
                    case EncodeStyle.Hex:
                        sOutput.Append(input[i].ToString("X2"));
                        break;
                }
            }
            return sOutput.ToString();
        }

        public static string SHA1HashEncode(string str, EncodeStyle encode) {
            SHA1 a = new SHA1CryptoServiceProvider();
            byte[] arr = new byte[60];
            string hash = "";
            
            arr = a.ComputeHash(Encoding.UTF8.GetBytes(str));
            hash = ByteArrayToString(arr, encode);
            
            return hash;
        }
    }
}
