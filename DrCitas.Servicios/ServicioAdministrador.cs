﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text.RegularExpressions;
using DrCitas.Datos;
using DrCitas.Dtos;
using DrCitas.Servicios;

namespace DrCitas.Servicios {
    
    public class ServicioAdministrador {

        DrCitasEntidades vCnx = null;

        public ServicioAdministrador() {
            vCnx = new DrCitasEntidades(FuncionesGlobales.Decry64Decry(System.Configuration.ConfigurationManager.ConnectionStrings["DrCitasEntidades"].ConnectionString));
        }

        #region Empresa

        public List<dtoEmpresa> buscarEmpresa(dtoBusqueda datos) {
            List<dtoEmpresa> empresas = new List<dtoEmpresa>();

            try {
                empresas = vCnx.T_Empresa.Where(te => ((te.nombre.Contains(datos.nombre) || datos.nombre == "") && (te.tipoDocumento == datos.tipoDocumento || datos.tipoDocumento == "") && (te.documento == datos.documento || datos.documento == 0)) && te.Activo == true )
                    .Select(e => new dtoEmpresa {
                        idEmpresa     = e.id_empresa,
                        nombre        = e.nombre,
                        tipoDocumento = e.tipoDocumento,
                        documento     = e.documento,
                        direccion     = e.direccion,
                        telefono      = e.telefono,
                        activo        = e.Activo
                    }).ToList();

                return empresas;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool agregarEmpresa(string miSesion, dtoEmpresa datos) {
            Tupla<int, string> cliente = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                return this.agregarEmpresa(datos);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool agregarEmpresa(dtoEmpresa datos) {
            try {
                vCnx.T_Empresa.AddObject(new T_Empresa {
                    nombre        = datos.nombre.ToUpper(),
                    tipoDocumento = datos.tipoDocumento.ToUpper(),
                    documento     = datos.documento,
                    direccion     = datos.direccion.ToUpper(),
                    telefono      = datos.telefono,
                    Activo        = true
                });

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool actualizarEmpresa(string miSesion, dtoEmpresa datos, int idEmpresa) {
            Tupla<int, string> cliente = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                return this.actualizarEmpresa(datos, idEmpresa);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool actualizarEmpresa(dtoEmpresa datos, int idEmpresa) {
            T_Empresa empresa = null;

            try {
                empresa = vCnx.T_Empresa.Where(te => te.id_empresa == idEmpresa).FirstOrDefault();

                if (empresa == null) return false;

                empresa.nombre        = (!string.IsNullOrEmpty(datos.nombre)        ? datos.nombre.ToUpper()        : empresa.nombre);

                empresa.tipoDocumento = (!string.IsNullOrEmpty(datos.tipoDocumento) ? datos.tipoDocumento.ToUpper() : empresa.tipoDocumento);

                empresa.documento     = (datos.documento > 0                        ? datos.documento               : empresa.documento);

                empresa.direccion     = (!string.IsNullOrEmpty(datos.direccion)     ? datos.direccion.ToUpper()     : empresa.direccion);
                                                                                    
                empresa.telefono      = (!string.IsNullOrEmpty(datos.telefono)      ? datos.telefono                : empresa.telefono);
                                                                                    
                empresa.Activo        = (!empresa.Activo && !datos.activo           ? empresa.Activo                : datos.activo);

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool eliminarEmpresa(string miSesion, int idEmpresa) {
            Tupla<int, string> cliente = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                return this.eliminarEmpresa(idEmpresa);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool eliminarEmpresa(int idEmpresa) {
            T_Empresa empresa = null;

            try {
                empresa = vCnx.T_Empresa.Where(te => te.id_empresa == idEmpresa).FirstOrDefault();

                if (empresa == null) return false;

                empresa.Activo = false;

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        #endregion

        #region Clinica

        public List<dtoClinica> buscarClinica(dtoBusqueda datos) {
            List<dtoClinica> clinicas = new List<dtoClinica>();

            try {
                vCnx.Sp_BuscarClinica(datos.nombre, datos.idCiudad, datos.idEspecialidad, datos.idEmpresa).ToList()
                    .ForEach(e => {
                        clinicas.Add(new dtoClinica {
                            idClinica   = e.idClinica,
                            idCiudad    = e.idCiudad,
                            descripcion = e.Descripcion,
                            coordenadas = (!string.IsNullOrEmpty(e.Coordenada) ? new dtoCoordenada(e.Coordenada) : null),
                            rif         = e.RIF,
                            telefono    = e.Telefono
                        });
                    }
                );

                return clinicas;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool agregarClinica(string miSesion, dtoClinica datos) {
            Tupla<int, string> cliente = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                return this.agregarClinica(datos);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool agregarClinica(dtoClinica datos) {
            try {
                vCnx.Tb_Clinica.AddObject(new Tb_Clinica {
                    idCiudad    = datos.idCiudad,
                    Descripcion = datos.descripcion.ToUpper(),
                    Direccion   = datos.direccion.ToUpper(),
                    Coordenada  = datos.coordenadas.ToString(),
                    RIF_CI      = datos.rif.ToUpper(),
                    Telefono    = datos.telefono
                });

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool actualizarClinica(string miSesion, dtoClinica datos, int idClinica) {
            Tupla<int, string> cliente = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                return this.actualizarClinica(datos, idClinica);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool actualizarClinica(dtoClinica datos, int idClinica) {
            Tb_Clinica clinica = null;

            try {
                clinica = vCnx.Tb_Clinica.Where(tc => tc.idClinica == idClinica).FirstOrDefault();

                if (clinica == null) return false;

                clinica.idCiudad    = (datos.idCiudad > 0                       ? datos.idCiudad               : clinica.idCiudad);

                clinica.Descripcion = (!string.IsNullOrEmpty(datos.descripcion) ? datos.descripcion.ToUpper()  : clinica.Descripcion);

                clinica.Direccion   = (!string.IsNullOrEmpty(datos.direccion)   ? datos.direccion.ToUpper()    : clinica.Direccion);

                clinica.Coordenada  = (datos.coordenadas != null                ? datos.coordenadas.ToString() : clinica.Coordenada);

                clinica.RIF_CI      = (!string.IsNullOrEmpty(datos.rif)         ? datos.rif.ToUpper()          : clinica.RIF_CI);
                                                                                                               
                clinica.Telefono    = (!string.IsNullOrEmpty(datos.telefono)    ? datos.telefono               : clinica.Telefono);
                                                                                                               
                clinica.Activo      = (!string.IsNullOrEmpty(datos.activo)      ? datos.activo                 : clinica.Activo);

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool eliminarClinica(string miSesion, int idClinica) {
            Tupla<int, string> cliente = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                return this.eliminarClinica(idClinica);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool eliminarClinica(int idClinica) {
            Tb_Clinica clinica = null;

            try {
                clinica = vCnx.Tb_Clinica.Where(tc => tc.idClinica == idClinica).FirstOrDefault();

                if (clinica == null) return false;

                clinica.Activo = "0";

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        #endregion


        #region Especialidad

        public List<dtoEspecialidad> buscarEspecialidad(dtoBusqueda datos) {
            List<dtoEspecialidad> especialidades = new List<dtoEspecialidad>();

            try {
                vCnx.Sp_BuscarEspecialidad(datos.codigo, datos.nombre, datos.tipo).ToList()
                    .ForEach(e => {
                        especialidades.Add(new dtoEspecialidad {
                            idEspecialidad = e.idEspecialidad,
                            Especialidad   = e.Descripcion,
                            codigo         = e.Codigo,
                            tipo           = e.Tipo,
                            activo         = e.Activo
                        });
                    }
                );

                return especialidades;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool agregarEspecialidad(string miSesion, dtoEspecialidad datos) {
            Tupla<int, string> cliente = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                return this.agregarEspecialidad(datos);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool agregarEspecialidad(dtoEspecialidad datos) {
            try {
                vCnx.Tb_Especialidad.AddObject(
                    new Tb_Especialidad {
                        Codigo      = datos.codigo,
                        Descripcion = datos.Especialidad,
                        Puntos      = 1,
                        Tipo        = datos.tipo,
                        Activo      = datos.activo,
                        idEspecialidad = vCnx.Tb_Especialidad.OrderByDescending(o=>o.idEspecialidad).Select(s=>s.idEspecialidad).FirstOrDefault() + 1
                });

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool actualizarEspecialidad(string miSesion, dtoEspecialidad datos, int idEspecialidad) {
            Tupla<int, string> cliente = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                return this.actualizarEspecialidad(datos, idEspecialidad);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool actualizarEspecialidad(dtoEspecialidad datos, int idEspecialidad) {
            Tb_Especialidad especialidad = null;

            try {
                especialidad = vCnx.Tb_Especialidad.Where(tbe => tbe.idEspecialidad == idEspecialidad).FirstOrDefault();

                if (especialidad == null) return false;

                especialidad.Codigo      = (!string.IsNullOrEmpty(datos.codigo)       ? datos.codigo.ToUpper() : especialidad.Codigo);

                especialidad.Descripcion = (!string.IsNullOrEmpty(datos.Especialidad) ? datos.Especialidad     : especialidad.Descripcion);
                                                                                                               
                especialidad.Tipo        = (!string.IsNullOrEmpty(datos.tipo)         ? datos.tipo             : especialidad.Tipo);
                                                                                                               
                especialidad.Activo      = (!string.IsNullOrEmpty(datos.activo)       ? datos.activo           : especialidad.Activo);

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool eliminarEspecialidad(string miSesion, int idEspecialidad) {
            Tupla<int, string> cliente = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                return this.eliminarEspecialidad(idEspecialidad);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool eliminarEspecialidad(int idEspecialidad) {
            Tb_Especialidad especialidad = null;

            try {
                especialidad = vCnx.Tb_Especialidad.Where(tbe => tbe.idEspecialidad == idEspecialidad).FirstOrDefault();

                if (especialidad == null) return false;

                especialidad.Activo = "0";

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        #endregion

        #region Cliente

        public dtoPagClientes buscarUsuarios(string miSesion, dtoBusqueda datos) {
            dtoPagClientes resultado = new dtoPagClientes();

            int id = -1, idAdmin = -1, total = 0;

            try {
                idAdmin = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                if (idAdmin > 0) {
                    vCnx.Sp_BuscarUsuario(datos.tipoDocumento, datos.documento, datos.nombre, datos.apellido, datos.tipo, datos.idCiudad, datos.idEmpresa, idAdmin, datos.idEstatus).ToList()
                        .ForEach(e => {
                            resultado.clientes.Add(new dtoClienteResume {
                                idCliente     = FuncionesGlobales.Encry64Encry(e.idCliente.ToString()),
                                nombre        = e.Nombre,
                                apellido      = e.Apellido,
                                tipoCliente   = e.TipoCliente,
                                tipoDocumento = e.TipoDocumento,
                                documento     = e.Documento,
                                correo        = e.Correo,
                                telefono      = e.Celular
                            });
                        }
                    );

                    foreach (var c in resultado.clientes) {
                        id = Convert.ToInt32(FuncionesGlobales.Decry64Decry(c.idCliente));
                        
                        if (c.tipoCliente == "L") {
                            c.empresas = vCnx.T_AsistenteEmpresa.Where(tae => tae.id_asistente == id)
                                .Select(e => new dtoEmpresa {
                                    idEmpresa     = e.T_Empresa.id_empresa,
                                    nombre        = e.T_Empresa.nombre,
                                    direccion     = e.T_Empresa.direccion,
                                    tipoDocumento = e.T_Empresa.tipoDocumento,
                                    documento     = e.T_Empresa.documento,
                                    telefono      = e.T_Empresa.telefono,
                                    activo        = e.Activo
                                }).ToList();
                        }
                        else if (c.tipoCliente == "S") {
                            c.empresas = vCnx.T_AsistenteEmpresa.Where(tae => tae.id_asistente == id)
                                .Select(e => new dtoEmpresa {
                                    idEmpresa     = e.T_Empresa.id_empresa,
                                    nombre        = e.T_Empresa.nombre,
                                    direccion     = e.T_Empresa.direccion,
                                    tipoDocumento = e.T_Empresa.tipoDocumento,
                                    documento     = e.T_Empresa.documento,
                                    telefono      = e.T_Empresa.telefono,
                                    activo        = e.Activo
                                }).ToList();
                        }
                        else if (c.tipoCliente == "M") {
                            c.empresas = vCnx.T_MedicoEmpresa.Where(tme => tme.id_medico == id)
                                .Select(e => new dtoEmpresa {
                                    idEmpresa     = e.T_Empresa.id_empresa,
                                    nombre        = e.T_Empresa.nombre,
                                    direccion     = e.T_Empresa.direccion,
                                    tipoDocumento = e.T_Empresa.tipoDocumento,
                                    documento     = e.T_Empresa.documento,
                                    telefono      = e.T_Empresa.telefono,
                                    activo        = e.Activo
                                }).ToList();
                            c.especialidades = vCnx.Tb_ClienteEspecialidad.Where(tce => tce.idCliente == id)
                                .Select(e => new dtoEspecialidad {
                                    idEspecialidad = e.Tb_Especialidad.idEspecialidad,
                                    Especialidad   = e.Tb_Especialidad.Descripcion
                                }).ToList();
                        }
                        else if (c.tipoCliente == "P") {
                            c.empresas = vCnx.T_PacienteEmpresa.Where(tpe => tpe.id_paciente == id)
                                .Select(e => new dtoEmpresa {
                                    idEmpresa     = e.T_Empresa.id_empresa,
                                    nombre        = e.T_Empresa.nombre,
                                    direccion     = e.T_Empresa.direccion,
                                    tipoDocumento = e.T_Empresa.tipoDocumento,
                                    documento     = e.T_Empresa.documento,
                                    telefono      = e.T_Empresa.telefono,
                                    activo        = e.Activo
                                }).ToList();
                        }
                        else if (c.tipoCliente == "C") {
                            c.empresas = vCnx.T_AsistenteEmpresa.Where(tae => tae.id_asistente == id)
                                .Select(e => new dtoEmpresa {
                                    idEmpresa     = e.T_Empresa.id_empresa,
                                    nombre        = e.T_Empresa.nombre,
                                    direccion     = e.T_Empresa.direccion,
                                    tipoDocumento = e.T_Empresa.tipoDocumento,
                                    documento     = e.T_Empresa.documento,
                                    telefono      = e.T_Empresa.telefono,
                                    activo        = e.Activo
                                }).ToList();
                        }
                    }

                    if (resultado.clientes != null) {
                        resultado.clientes = resultado.clientes.OrderBy(e => e.apellido).ToList();

                        if (datos.pagina == -1) {
                            total = (int)Math.Ceiling((double)(resultado.clientes.Count() / datos.elementos));
                            datos.pagina = 1;
                        }

                        resultado.clientes = resultado.clientes.Skip((datos.pagina - 1) * datos.elementos).Take(datos.elementos).ToList();
                        resultado.totalPaginas = datos.total == 0 ? total : datos.total;
                        resultado.pagina = datos.pagina;
                    }
                }
                return resultado;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool cambiarEstadoUsuario(string miSesion, string idClientes, int estado) {
            Tupla<int, string> cliente = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                if (string.IsNullOrEmpty(idClientes)) return false;

                if (estado != 0 && estado != 1) return false;

                return this.cambiarEstadoUsuario(FuncionesGlobales.ChangeObjTo<List<int>>(idClientes), estado);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool cambiarEstadoUsuario(List<int> idClientes, int estado) {
            T_UsuarioActivo usuario = null;
            int idUsuario = -1;
            bool flag = false;

            try {
                foreach (var c in idClientes) {
                    idUsuario = vCnx.T_ClienteUsuario.Where(tcu => tcu.id_cliente == c).Select(e => e.id_usuario).FirstOrDefault();

                    if (idUsuario <= 0) continue;

                    usuario = vCnx.T_UsuarioActivo.Where(tua => tua.id_usuario == idUsuario).FirstOrDefault();

                    if (usuario == null) continue;

                    flag = true;
                    usuario.activo = estado;
                }

                if (flag)
                    vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public dtoPagClientes buscarCliente(dtoBusqueda datos) { 
            dtoPagClientes resultado = new dtoPagClientes();
           
            int id = -1, total = 0;

            try {
                vCnx.Sp_BuscarCliente(datos.tipoDocumento, datos.documento, datos.nombre, datos.apellido, datos.tipo, datos.idCiudad, datos.idEspecialidad, datos.idEmpresa).ToList()
                    .ForEach(e => {
                        resultado.clientes.Add(new dtoClienteResume {
                            idCliente = FuncionesGlobales.Encry64Encry(e.idCliente.ToString()),
                            nombre        = e.Nombre,
                            apellido      = e.Apellido,
                            tipoCliente   = e.TipoCliente,
                            tipoDocumento = e.TipoDocumento,
                            documento     = e.Documento,
                            correo        = e.Correo,
                            telefono      = e.Celular
                        });
                    }
                );

                foreach (var c in resultado.clientes) {
                    id = Convert.ToInt32(FuncionesGlobales.Decry64Decry(c.idCliente));
                    if (c.tipoCliente == "M") {
                        c.empresas = vCnx.T_MedicoEmpresa.Where(tme => tme.id_medico == id)
                            .Select(e => new dtoEmpresa {
                                idEmpresa     = e.T_Empresa.id_empresa,
                                nombre        = e.T_Empresa.nombre,
                                direccion     = e.T_Empresa.direccion,
                                tipoDocumento = e.T_Empresa.tipoDocumento,
                                documento     = e.T_Empresa.documento,
                                telefono      = e.T_Empresa.telefono,
                                activo        = e.Activo
                            }).ToList();
                        c.especialidades = vCnx.Tb_ClienteEspecialidad.Where(tce => tce.idCliente == id)
                            .Select(e => new dtoEspecialidad {
                                idEspecialidad = e.Tb_Especialidad.idEspecialidad,
                                Especialidad   = e.Tb_Especialidad.Descripcion
                            }).ToList();
                    }
                    else if (c.tipoCliente == "P") {
                        c.empresas = vCnx.T_PacienteEmpresa.Where(tpe => tpe.id_paciente == id)
                            .Select(e => new dtoEmpresa {
                                idEmpresa     = e.T_Empresa.id_empresa,
                                nombre        = e.T_Empresa.nombre,
                                direccion     = e.T_Empresa.direccion,
                                tipoDocumento = e.T_Empresa.tipoDocumento,
                                documento     = e.T_Empresa.documento,
                                telefono      = e.T_Empresa.telefono,
                                activo        = e.Activo
                            }).ToList();

                        c.misAsociados = new ServicioCliente().GetMisAsociados(Convert.ToInt32(FuncionesGlobales.Decry64Decry(c.idCliente)));
                    }
                }

                if (resultado.clientes != null) {
                    resultado.clientes = resultado.clientes.OrderBy(e => e.apellido).ToList();

                    if (datos.pagina == -1) {
                        total = (int)Math.Ceiling((double)(resultado.clientes.Count() / (double)datos.elementos));
                        datos.pagina = 1;
                    }

                    resultado.clientes = resultado.clientes.Skip((datos.pagina - 1) * datos.elementos).Take(datos.elementos).ToList();
                    resultado.totalPaginas = datos.total == 0 ? total : datos.total;
                    resultado.pagina = datos.pagina;
                }
                
                return resultado;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        #endregion


        /**********************************************************************************************************************
         * Asociar/Desasociar especialidades a clinicas                                                                       *
         **********************************************************************************************************************/
        public List<dtoEspecialidad> buscarEspecialidadClinica(int idClinica) {
            List<dtoEspecialidad> especialidades = null;

            try {
                especialidades = vCnx.Tb_Clinicas_Especialidad.Where(tce => tce.idClinica == idClinica)
                    .Select(e => new dtoEspecialidad {
                        idEspecialidad = e.idEspecialidad,
                        Especialidad   = e.Tb_Especialidad.Descripcion
                    }).ToList();

                return especialidades;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool asociarEspecialidadClinica(string miSesion, string idEspecialidades, int idClinica) {
            Tupla<int, string> cliente = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                if (string.IsNullOrEmpty(idEspecialidades)) return false;

                return this.asociarEspecialidadClinica(FuncionesGlobales.ChangeObjTo<List<int>>(idEspecialidades), idClinica);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool asociarEspecialidadClinica(List<int> idEspecialidades, int idClinica) {
            Tb_Clinicas_Especialidad ce = null;
            bool flag = false;
            
            try {
                foreach (var e in idEspecialidades) {
                    ce = vCnx.Tb_Clinicas_Especialidad.Where(tce => tce.idClinica == idClinica && tce.idEspecialidad == e).FirstOrDefault();

                    if (ce == null) {
                        flag = true;
                        vCnx.Tb_Clinicas_Especialidad.AddObject(new Tb_Clinicas_Especialidad { idClinica = idClinica, idEspecialidad = e });
                    }
                    else {
                        if (!ce.Activo) {
                            flag = true;
                            ce.Activo = true;
                        }
                    }
                }

                if (flag)
                    vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool desasociarEspecialidadClinica(string miSesion, string idEspecialidades, int idClinica) {
            Tupla<int, string> cliente = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                if (string.IsNullOrEmpty(idEspecialidades)) return false;

                return this.desasociarEspecialidadClinica(FuncionesGlobales.ChangeObjTo<List<int>>(idEspecialidades), idClinica);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool desasociarEspecialidadClinica(List<int> idEspecialidades, int idClinica) {
            Tb_Clinicas_Especialidad ce = null;
            bool flag = false;

            try {
                foreach (var e in idEspecialidades) {
                    ce = vCnx.Tb_Clinicas_Especialidad.Where(tce => tce.idClinica == idClinica && tce.idEspecialidad == e).FirstOrDefault();

                    if (ce != null) {
                        flag = true;
                        ce.Activo = false;
                    }
                }

                if (flag)
                    vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }
        /**********************************************************************************************************************/


        /**********************************************************************************************************************
         * Asociar/Desasociar asistentes a empresas                                                                           *
         **********************************************************************************************************************/
        public dtoPagClientes buscarAsistenteEmpresa(dtoBusqueda datos) {
            dtoPagClientes resultado = new dtoPagClientes();
            int idEmpresa = -1, total = 0;

            try {
                idEmpresa = Convert.ToInt32(datos.param);

                if (idEmpresa > 0) {
                    resultado.clientes = vCnx.T_AsistenteEmpresa.Where(tae => tae.id_empresa == idEmpresa)
                        .Select(e => new {
                            idCliente     = e.id_asistente,
                            nombre        = e.Tb_Cliente.Nombre,
                            apellido      = e.Tb_Cliente.Apellido,
                            tipoDocumento = e.Tb_Cliente.TipoDocumento,
                            documento     = e.Tb_Cliente.Documento
                        }).AsEnumerable().Select(e => new dtoClienteResume {
                            idCliente     = FuncionesGlobales.Encry64Encry(e.idCliente.ToString()),
                            nombre        = e.nombre,
                            apellido      = e.apellido,
                            tipoDocumento = e.tipoDocumento,
                            documento     = e.documento
                        }).ToList();

                    if (resultado.clientes != null) {
                        resultado.clientes = resultado.clientes.OrderBy(e => e.apellido).ToList();

                        if (datos.pagina == -1) {
                            total = (int)Math.Ceiling((double)(resultado.clientes.Count() / (double)datos.elementos));
                            datos.pagina = 1;
                        }

                        resultado.clientes = resultado.clientes.Skip((datos.pagina - 1) * datos.elementos).Take(datos.elementos).ToList();
                        resultado.totalPaginas = datos.total == 0 ? total : datos.total;
                        resultado.pagina = datos.pagina;
                    }
                }

                return resultado;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool asociarAsistenteEmpresa(string miSesion, string idAsistentes, int idEmpresa) {
            Tupla<int, string> cliente = null;
            List<int> ids = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                if (string.IsNullOrEmpty(idAsistentes)) return false;

                ids = new List<int>();
                FuncionesGlobales.ChangeObjTo<List<string>>(idAsistentes).ForEach(e => {
                    ids.Add(Convert.ToInt32(FuncionesGlobales.Decry64Decry(e)));
                });

                return this.asociarAsistenteEmpresa(ids, idEmpresa);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool asociarAsistenteEmpresa(List<int> idAsistentes, int idEmpresa) {
            T_AsistenteEmpresa ae = null;
            bool flag = false;

            try {
                foreach (var a in idAsistentes) {
                    ae = vCnx.T_AsistenteEmpresa.Where(tae => tae.id_empresa == idEmpresa && tae.id_asistente == a).FirstOrDefault();

                    if (ae == null) {
                        flag = true;
                        vCnx.T_AsistenteEmpresa.AddObject(new T_AsistenteEmpresa { id_asistente = a, id_empresa = idEmpresa });
                    }
                    else {
                        if (!ae.Activo) {
                            flag = true;
                            ae.Activo = true;
                        }
                    }
                }

                if (flag)
                    vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool desasociarAsistenteEmpresa(string miSesion, string idAsistentes, int idEmpresa) {
            Tupla<int, string> cliente = null;
            List<int> ids = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                if (string.IsNullOrEmpty(idAsistentes)) return false;

                ids = new List<int>();
                FuncionesGlobales.ChangeObjTo<List<string>>(idAsistentes).ForEach(e => {
                    ids.Add(Convert.ToInt32(FuncionesGlobales.Decry64Decry(e)));
                });

                return this.desasociarAsistenteEmpresa(ids, idEmpresa);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool desasociarAsistenteEmpresa(List<int> idAsistentes, int idEmpresa) {
            T_AsistenteEmpresa ae = null;
            bool flag = false;

            try {
                foreach (var a in idAsistentes) {
                    ae = vCnx.T_AsistenteEmpresa.Where(tae => tae.id_empresa == idEmpresa && tae.id_asistente == a).FirstOrDefault();

                    if (ae != null) {
                        flag = true;
                        ae.Activo = false;
                    }
                }

                if (flag)
                    vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }
        /**********************************************************************************************************************/


        /**********************************************************************************************************************
         * Asociar/Desasociar medicos a empresas                                                                              *
         **********************************************************************************************************************/
        public dtoPagClientes buscarMedicoEmpresa(dtoBusqueda datos) {
            dtoPagClientes resultado = new dtoPagClientes();
            int idEmpresa = -1, total = 0;

            try {
                idEmpresa = Convert.ToInt32(datos.param);

                if (idEmpresa > 0) {
                    resultado.clientes = vCnx.T_MedicoEmpresa.Where(tme => tme.id_empresa == idEmpresa)
                        .Select(e => new {
                            idCliente     = e.id_medico,
                            nombre        = e.Tb_Cliente.Nombre,
                            apellido      = e.Tb_Cliente.Apellido,
                            tipoDocumento = e.Tb_Cliente.TipoDocumento,
                            documento     = e.Tb_Cliente.Documento
                        }).AsEnumerable().Select(e => new dtoClienteResume {
                            idCliente     = FuncionesGlobales.Encry64Encry(e.idCliente.ToString()),
                            nombre        = e.nombre,
                            apellido      = e.apellido,
                            tipoDocumento = e.tipoDocumento,
                            documento     = e.documento
                        }).ToList();

                    if (resultado.clientes != null) {
                        resultado.clientes = resultado.clientes.OrderBy(e => e.apellido).ToList();

                        if (datos.pagina == -1) {
                            total = (int)Math.Ceiling((double)(resultado.clientes.Count() / (double)datos.elementos));
                            datos.pagina = 1;
                        }

                        resultado.clientes = resultado.clientes.Skip((datos.pagina - 1) * datos.elementos).Take(datos.elementos).ToList();
                        resultado.totalPaginas = datos.total == 0 ? total : datos.total;
                        resultado.pagina = datos.pagina;
                    }
                }
                return resultado;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool asociarMedicoEmpresa(string miSesion, string idMedicos, int idEmpresa) {
            Tupla<int, string> cliente = null;
            List<int> ids = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                if (string.IsNullOrEmpty(idMedicos)) return false;

                ids = new List<int>();
                FuncionesGlobales.ChangeObjTo<List<string>>(idMedicos).ForEach(e => {
                    ids.Add(Convert.ToInt32(FuncionesGlobales.Decry64Decry(e)));
                });

                return this.asociarPacienteEmpresa(ids, idEmpresa);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool asociarMedicoEmpresa(List<int> idMedicos, int idEmpresa) {
            T_MedicoEmpresa me = null;
            bool flag = false;

            try {
                foreach (var m in idMedicos) {
                    me = vCnx.T_MedicoEmpresa.Where(tme => tme.id_empresa == idEmpresa && tme.id_medico == m).FirstOrDefault();

                    if (me == null) {
                        flag = true;
                        vCnx.T_MedicoEmpresa.AddObject(new T_MedicoEmpresa { id_medico = m, id_empresa = idEmpresa });
                    }
                    else {
                        if (!me.Activo) {
                            flag = true;
                            me.Activo = true;
                        }
                    }
                }

                if (flag)
                    vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool desasociarMedicoEmpresa(string miSesion, string idMedicos, int idEmpresa) {
            Tupla<int, string> cliente = null;
            List<int> ids = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                if (string.IsNullOrEmpty(idMedicos)) return false;

                ids = new List<int>();
                FuncionesGlobales.ChangeObjTo<List<string>>(idMedicos).ForEach(e => {
                    ids.Add(Convert.ToInt32(FuncionesGlobales.Decry64Decry(e)));
                });

                return this.desasociarMedicoEmpresa(ids, idEmpresa);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool desasociarMedicoEmpresa(List<int> idMedicos, int idEmpresa) {
            T_MedicoEmpresa me = null;
            bool flag = false;

            try {
                foreach (var m in idMedicos) {
                    me = vCnx.T_MedicoEmpresa.Where(tme => tme.id_empresa == idEmpresa && tme.id_medico == m).FirstOrDefault();

                    if (me != null) {
                        flag = true;
                        me.Activo = false;
                    }
                }

                if (flag)
                    vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }
        /**********************************************************************************************************************/


        /**********************************************************************************************************************
         * Asociar/Desasociar pacientes a empresas                                                                            *
         **********************************************************************************************************************/
        public dtoPagClientes buscarPacienteEmpresa(dtoBusqueda datos) {
            dtoPagClientes resultado = new dtoPagClientes();
            int idEmpresa = -1, total = 0;

            try {
                idEmpresa = Convert.ToInt32(datos.param);

                if (idEmpresa > 0) {
                    resultado.clientes = vCnx.T_PacienteEmpresa.Where(tpe => tpe.id_empresa == idEmpresa)
                        .Select(e => new {
                            idCliente     = e.id_paciente,
                            nombre        = e.Tb_Cliente.Nombre,
                            apellido      = e.Tb_Cliente.Apellido,
                            tipoDocumento = e.Tb_Cliente.TipoDocumento,
                            documento     = e.Tb_Cliente.Documento
                        }).AsEnumerable().Select(e => new dtoClienteResume {
                            idCliente     = FuncionesGlobales.Encry64Encry(e.idCliente.ToString()),
                            nombre        = e.nombre,
                            apellido      = e.apellido,
                            tipoDocumento = e.tipoDocumento,
                            documento     = e.documento
                        }).ToList();

                    if (resultado.clientes != null) {
                        resultado.clientes = resultado.clientes.OrderBy(e => e.apellido).ToList();

                        if (datos.pagina == -1) {
                            total = (int)Math.Ceiling((double)(resultado.clientes.Count() / (double)datos.elementos));
                            datos.pagina = 1;
                        }

                        resultado.clientes = resultado.clientes.Skip((datos.pagina - 1) * datos.elementos).Take(datos.elementos).ToList();
                        resultado.totalPaginas = datos.total == 0 ? total : datos.total;
                        resultado.pagina = datos.pagina;
                    }
                }
                return resultado;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool asociarPacienteEmpresa(string miSesion, string idPacientes, int idEmpresa) {
            Tupla<int, string> cliente = null;
            List<int> ids = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                if (string.IsNullOrEmpty(idPacientes)) return false;

                ids = new List<int>();
                FuncionesGlobales.ChangeObjTo<List<string>>(idPacientes).ForEach(e => {
                    ids.Add(Convert.ToInt32(FuncionesGlobales.Decry64Decry(e)));
                });

                return this.asociarPacienteEmpresa(ids, idEmpresa);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool asociarPacienteEmpresa(List<int> idPacientes, int idEmpresa) {
            T_PacienteEmpresa pe = null;
            bool flag = false;

            try {
                foreach (var p in idPacientes) {
                    pe = vCnx.T_PacienteEmpresa.Where(tpe => tpe.id_empresa == idEmpresa && tpe.id_paciente == p).FirstOrDefault();

                    if (pe == null) {
                        flag = true;
                        vCnx.T_PacienteEmpresa.AddObject(new T_PacienteEmpresa { id_paciente = p, id_empresa = idEmpresa });
                    }
                    else {
                        if (!pe.Activo) {
                            flag = true;
                            pe.Activo = true;
                        }
                    }
                }

                if (flag)
                    vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool desasociarPacienteEmpresa(string miSesion, string idPacientes, int idEmpresa) {
            Tupla<int, string> cliente = null;
            List<int> ids = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                if (string.IsNullOrEmpty(idPacientes)) return false;

                ids = new List<int>();
                FuncionesGlobales.ChangeObjTo<List<string>>(idPacientes).ForEach(e => {
                    ids.Add(Convert.ToInt32(FuncionesGlobales.Decry64Decry(e)));
                });

                return this.desasociarPacienteEmpresa(ids, idEmpresa);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool desasociarPacienteEmpresa(List<int> idPacientes, int idEmpresa) {
            T_PacienteEmpresa pe = null;
            bool flag = false;

            try {
                foreach (var p in idPacientes) {
                    pe = vCnx.T_PacienteEmpresa.Where(tpe => tpe.id_empresa == idEmpresa && tpe.id_paciente == p).FirstOrDefault();

                    if (pe != null) {
                        flag = true;
                        pe.Activo = false;
                    }
                }

                if (flag)
                    vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }
        /**********************************************************************************************************************/


        /**********************************************************************************************************************
         * Asociar/Desasociar asistentes a clinicas                                                                           *
         **********************************************************************************************************************/
        public dtoPagClientes buscarAsistenteClinica(dtoBusqueda datos) {
            dtoPagClientes resultado = new dtoPagClientes();
            int idClinica = -1, total = 0;

            try {
                idClinica = Convert.ToInt32(datos.param);

                if (idClinica > 0) {
                    resultado.clientes = vCnx.T_AsistenteClinica.Where(tac => tac.id_clinica == idClinica)
                        .Select(e => new {
                            idCliente = e.id_asistente,
                            nombre = e.Tb_Cliente.Nombre,
                            apellido = e.Tb_Cliente.Apellido,
                            tipoDocumento = e.Tb_Cliente.TipoDocumento,
                            documento = e.Tb_Cliente.Documento
                        }).AsEnumerable().Select(e => new dtoClienteResume {
                            idCliente = FuncionesGlobales.Encry64Encry(e.idCliente.ToString()),
                            nombre = e.nombre,
                            apellido = e.apellido,
                            tipoDocumento = e.tipoDocumento,
                            documento = e.documento
                        }).ToList();

                    if (resultado.clientes != null) {
                        resultado.clientes = resultado.clientes.OrderBy(e => e.apellido).ToList();

                        if (datos.pagina == -1) {
                            total = (int)Math.Ceiling((double)(resultado.clientes.Count() / (double)datos.elementos));
                            datos.pagina = 1;
                        }

                        resultado.clientes = resultado.clientes.Skip((datos.pagina - 1) * datos.elementos).Take(datos.elementos).ToList();
                        resultado.totalPaginas = datos.total == 0 ? total : datos.total;
                        resultado.pagina = datos.pagina;
                    }
                }
                return resultado;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool asociarAsistenteClinica(string miSesion, string idAsistentes, int idClinica) {
            Tupla<int, string> cliente = null;
            List<int> ids = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                if (string.IsNullOrEmpty(idAsistentes)) return false;

                ids = new List<int>();
                FuncionesGlobales.ChangeObjTo<List<string>>(idAsistentes).ForEach(e => {
                    ids.Add(Convert.ToInt32(FuncionesGlobales.Decry64Decry(e)));
                });

                return this.asociarAsistenteClinica(ids, idClinica);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool asociarAsistenteClinica(List<int> idAsistentes, int idClinica) {
            T_AsistenteClinica ac = null;
            bool flag = false;

            try {
                foreach (var a in idAsistentes) {
                    ac = vCnx.T_AsistenteClinica.Where(tac => tac.id_clinica == idClinica && tac.id_asistente == a).FirstOrDefault();

                    if (ac == null) {
                        flag = true;
                        vCnx.T_AsistenteClinica.AddObject(new T_AsistenteClinica { id_asistente = a, id_clinica = idClinica });
                    }
                    else {
                        if (!ac.Activo) {
                            flag = true;
                            ac.Activo = true;
                        }
                    }
                }

                if (flag)
                    vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool desasociarAsistenteClinica(string miSesion, string idAsistentes, int idClinica) {
            Tupla<int, string> cliente = null;
            List<int> ids = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                if (string.IsNullOrEmpty(idAsistentes)) return false;

                ids = new List<int>();
                FuncionesGlobales.ChangeObjTo<List<string>>(idAsistentes).ForEach(e => {
                    ids.Add(Convert.ToInt32(FuncionesGlobales.Decry64Decry(e)));
                });

                return this.desasociarAsistenteClinica(ids, idClinica);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool desasociarAsistenteClinica(List<int> idAsistentes, int idClinica) {
            T_AsistenteClinica ac = null;
            bool flag = false;

            try {
                foreach (var a in idAsistentes) {
                    ac = vCnx.T_AsistenteClinica.Where(tac => tac.id_clinica == idClinica && tac.id_asistente == a).FirstOrDefault();

                    if (ac != null) {
                        flag = true;
                        ac.Activo = false;
                    }
                }

                if (flag)
                    vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }
        /**********************************************************************************************************************/


        /**********************************************************************************************************************
         * Permitir/Retirar accesos para asistentes a consultorios                                                            *
         **********************************************************************************************************************/
        public dtoPagClientes buscarAsistenteConsultorio(dtoBusqueda datos) {
            dtoPagClientes resultado = new dtoPagClientes();
            int idConsultorio = -1, total = 0;

            try {
                idConsultorio = Convert.ToInt32(datos.param);

                if (idConsultorio > 0) {
                    resultado.clientes =
                        (
                            from consultorio in vCnx.Tb_Consultorio
                            join acceso in vCnx.T_AccesoConsultorio on consultorio.idConsultorio equals acceso.id_consultorio
                            join asistente in vCnx.Tb_Cliente on acceso.id_clientePermisado equals asistente.idCliente
                            where acceso.id_consultorio == idConsultorio
                            select new {
                                idCliente     = asistente.idCliente,
                                nombre        = asistente.Nombre,
                                apellido      = asistente.Apellido,
                                tipoDocumento = asistente.TipoDocumento,
                                documento     = asistente.Documento
                        }).AsEnumerable().Select(e => new dtoClienteResume {
                            idCliente     = FuncionesGlobales.Encry64Encry(e.idCliente.ToString()),
                            nombre        = e.nombre,
                            apellido      = e.apellido,
                            tipoDocumento = e.tipoDocumento,
                            documento     = e.documento
                        }).ToList();

                    if (resultado.clientes != null) {
                        resultado.clientes = resultado.clientes.OrderBy(e => e.apellido).ToList();

                        if (datos.pagina == -1) {
                            total = (int)Math.Ceiling((double)(resultado.clientes.Count() / (double)datos.elementos));
                            datos.pagina = 1;
                        }

                        resultado.clientes = resultado.clientes.Skip((datos.pagina - 1) * datos.elementos).Take(datos.elementos).ToList();
                        resultado.totalPaginas = datos.total == 0 ? total : datos.total;
                        resultado.pagina = datos.pagina;
                    }
                }
                return resultado;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool permitirAccesoConsultorio(string miSesion, string idAsistentes, int idConsultorio) {
            Tupla<int, string> cliente = null;
            List<int> ids = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || !System.Text.RegularExpressions.Regex.IsMatch(cliente.item2, "A|M")) return false;

                if (string.IsNullOrEmpty(idAsistentes)) return false;

                ids = new List<int>();
                FuncionesGlobales.ChangeObjTo<List<string>>(idAsistentes).ForEach(e => {
                    ids.Add(Convert.ToInt32(FuncionesGlobales.Decry64Decry(e)));
                });

                return this.permitirAccesoConsultorio(cliente.item1, ids, idConsultorio);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool permitirAccesoConsultorio(int idCliente, List<int> idAsistentes, int idConsultorio) {
            T_AccesoConsultorio ac = null;
            bool flag = false;

            try {
                foreach (var a in idAsistentes) {
                    ac = vCnx.T_AccesoConsultorio.Where(tac => tac.id_consultorio == idConsultorio && tac.id_clientePermisado == a).FirstOrDefault();

                    if (ac == null) {
                        flag = true;
                        vCnx.T_AccesoConsultorio.AddObject(new T_AccesoConsultorio { id_clientePermisado = a, id_consultorio = idConsultorio, id_clientePermitidor = idCliente });
                    }
                }

                if (flag)
                    vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool retirarAccesoConsultorio(string miSesion, string idAsistentes, int idConsultorio) {
            Tupla<int, string> cliente = null;
            List<int> ids = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || !System.Text.RegularExpressions.Regex.IsMatch(cliente.item2, "A|M")) return false;

                if (string.IsNullOrEmpty(idAsistentes)) return false;

                ids = new List<int>();
                FuncionesGlobales.ChangeObjTo<List<string>>(idAsistentes).ForEach(e => {
                    ids.Add(Convert.ToInt32(FuncionesGlobales.Decry64Decry(e)));
                });

                return this.retirarAccesoConsultorio(ids, idConsultorio);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool retirarAccesoConsultorio(List<int> idAsistentes, int idConsultorio) {
            T_AccesoConsultorio ac = null;
            bool flag = false;

            try {
                foreach (var a in idAsistentes) {
                    ac = vCnx.T_AccesoConsultorio.Where(tac => tac.id_consultorio == idConsultorio && tac.id_clientePermisado == a).FirstOrDefault();

                    if (ac != null) {
                        flag = true;
                        ac.Activo = false;
                    }
                }

                if (flag)
                    vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }
        /**********************************************************************************************************************/


        /**********************************************************************************************************************
         * Asociar/Desasociar medicos a asistentes                                                                            *
         **********************************************************************************************************************/
        public List<dtoClienteResume> buscarMedicoAsistente(int idAsistente) {
            List<dtoClienteResume> medicos = null;

            try {
                medicos = vCnx.T_AsistenteMedico.Where(tam => tam.id_asistente == idAsistente)
                    .Select(e => new {
                        idCliente     = e.id_medico,
                        nombre        = e.Tb_Cliente1.Nombre,
                        apellido      = e.Tb_Cliente1.Apellido,
                        tipoDocumento = e.Tb_Cliente1.TipoDocumento,
                        documento     = e.Tb_Cliente1.Documento
                    }).AsEnumerable().Select(e => new dtoClienteResume {
                        idCliente     = FuncionesGlobales.Encry64Encry(e.idCliente.ToString()),
                        nombre        = e.nombre,
                        apellido      = e.apellido,
                        tipoDocumento = e.tipoDocumento,
                        documento     = e.documento
                    }).ToList();

                return medicos;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool asociarMedicoAsistente(string miSesion, string idMedicos, int idAsistente) {
            Tupla<int, string> cliente = null;
            List<int> ids = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || !System.Text.RegularExpressions.Regex.IsMatch(cliente.item2, "A|M")) return false;

                if (string.IsNullOrEmpty(idMedicos)) return false;

                ids = new List<int>();

                if (cliente.item2 == "M")
                    ids.Add(cliente.item1);
                else
                    FuncionesGlobales.ChangeObjTo<List<string>>(idMedicos).ForEach(e => {
                        ids.Add(Convert.ToInt32(FuncionesGlobales.Decry64Decry(e)));
                    });

                return this.asociarMedicoAsistente(ids, idAsistente);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }
                                 
        private bool asociarMedicoAsistente(List<int> idMedicos, int idAsistente) {
            T_AsistenteMedico am = null;
            bool flag = false;

            try {
                foreach (var m in idMedicos) {
                    am = vCnx.T_AsistenteMedico.Where(tam => tam.id_asistente == idAsistente && tam.id_medico == m).FirstOrDefault();

                    if (am == null) {
                        flag = true;
                        vCnx.T_AsistenteMedico.AddObject(new T_AsistenteMedico { id_asistente = idAsistente, id_medico = m });
                    }
                    else {
                        if (!am.Activo) {
                            flag = true;
                            am.Activo = true;
                        }
                    }
                }

                if (flag)
                    vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool desasociarMedicoAsistente(string miSesion, string idMedicos, int idAsistente) {
            Tupla<int, string> cliente = null;
            List<int> ids = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || !System.Text.RegularExpressions.Regex.IsMatch(cliente.item2, "A|M")) return false;

                if (string.IsNullOrEmpty(idMedicos)) return false;

                ids = new List<int>();

                if (cliente.item2 == "M")
                    ids.Add(cliente.item1);
                else
                    FuncionesGlobales.ChangeObjTo<List<string>>(idMedicos).ForEach(e => {
                        ids.Add(Convert.ToInt32(FuncionesGlobales.Decry64Decry(e)));
                    });

                return this.desasociarMedicoAsistente(ids, idAsistente);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool desasociarMedicoAsistente(List<int> idMedicos, int idAsistente) {
            T_AsistenteMedico am = null;
            bool flag = false;

            try {
                foreach (var m in idMedicos) {
                    am = vCnx.T_AsistenteMedico.Where(tam => tam.id_asistente == idAsistente && tam.id_medico == m).FirstOrDefault();

                    if (am != null) {
                        flag = true;
                        am.Activo = false;
                    }
                }

                if (flag)
                    vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }
        /**********************************************************************************************************************/


        /**********************************************************************************************************************
         * Aprobar revision de medicos nuevos                                                                                 *
         **********************************************************************************************************************/
        public dtoPagClientes buscarMedicosRevision(dtoBusqueda datos) {
            dtoPagClientes resultado = new dtoPagClientes();
            int total = 0;

            try {
                resultado.clientes = vCnx.Tb_Cliente.Where(tc => tc.Activo == "2")
                    .Select(e => new {
                        idCliente    = e.idCliente,
                        nombre       = e.Nombre,
                        apellido     = e.Apellido,
                        correo       = e.Correo,
                        telefono     = e.Celular,
                        fechaIngreso = e.FechaIngreso ,
                        activo       = e.Activo
                    }).AsEnumerable().Select(e => new dtoClienteResume {
                        idCliente     = FuncionesGlobales.Encry64Encry(e.idCliente.ToString()),
                        nombre        = e.nombre,
                        apellido      = e.apellido,
                        correo        = e.correo,
                        telefono      = e.telefono,
                        fechaIngreso  = e.fechaIngreso,
                        activo        = e.activo
                    }).ToList();

                if (resultado.clientes != null) {
                    resultado.clientes = resultado.clientes.OrderBy(e => e.fechaIngreso).ToList();

                    if (datos.pagina == -1) {
                        total = (int)(Math.Ceiling((double)(resultado.clientes.Count() / (double)datos.elementos)));
                        datos.pagina = 1;
                    }

                    resultado.clientes = resultado.clientes.Skip((datos.pagina - 1) * datos.elementos).Take(datos.elementos).ToList();
                    resultado.totalPaginas = datos.total == -1 ? total : datos.total;
                    resultado.pagina = datos.pagina;
                }

                return resultado;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool aprobarRevisionMedico(string miSesion, int idCliente) {
            Tupla<int, string> cliente = null;
            dtoClienteResume datos = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                datos = new ServicioCliente().GetDatosCliente(idCliente);

                return this.aprobarRevisionMedico(datos, idCliente);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool aprobarRevisionMedico(dtoClienteResume datos, int idCliente) {
            T_UsuarioActivo usuario = null;
            Tb_Cliente cliente = null;
            int idUsuario = -1;

            try {
                idUsuario = vCnx.T_ClienteUsuario.Where(tcu => tcu.id_cliente == idCliente).Select(e => e.id_usuario).FirstOrDefault();

                if (idUsuario <= 0) return false;

                usuario = vCnx.T_UsuarioActivo.Where(tua => tua.id_usuario == idUsuario).FirstOrDefault();

                cliente = vCnx.Tb_Cliente.Where(tc => tc.idCliente == idCliente).FirstOrDefault();

                if (usuario == null || cliente == null) return false;

                usuario.activo = 1;

                cliente.Activo = "3";

                vCnx.SaveChanges();

                ServicioUtilidad.NotificarAlCliente("Aprobacion registro medico - Dr. Citas", "Se ha aprobado su registro como medico dentro de Dr. Citas.\n\nPor favor ingrese al aplicativo para completar la informacion de su perfil de usuario.", datos.correo);

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }
        /**********************************************************************************************************************/
        /**********************************************************************************************************************
         * Buscar medico inactivos o activos                                                                     *
         **********************************************************************************************************************/
        public dtoPagClientes buscarMedicos(dtoBusqueda datos)
        {
            dtoPagClientes resultado = new dtoPagClientes();
            int total = 0;

            try
            {
                resultado.clientes =
                    vCnx.Tb_Cliente.Where(tc => tc.Activo == datos.param && tc.TipoCliente == "M")
                    .Select(e => new
                    {
                        idCliente   = e.idCliente,
                        nombre      = e.Nombre,
                        apellido    = e.Apellido,
                        correo      = e.Correo,
                        telefono    = e.Celular,
                        fechaIngreso = e.FechaIngreso,
                        activo      = e.Activo
                    }).AsEnumerable().Select(e => new dtoClienteResume
                    {
                        idCliente = FuncionesGlobales.Encry64Encry(e.idCliente.ToString()),
                        nombre      = e.nombre,
                        apellido    = e.apellido,
                        correo      = e.correo,
                        telefono    = e.telefono,
                        fechaIngreso = e.fechaIngreso,
                        activo      = e.activo
                    }).ToList();

                if (resultado.clientes.Count > 0)
                {
                    resultado.clientes = resultado.clientes.OrderBy(e => e.fechaIngreso).ToList();

                    if (datos.pagina == -1)
                    {
                        total = (int)Math.Ceiling((double)(resultado.clientes.Count() / (double)datos.elementos));
                        datos.pagina = 1;
                    }

                    resultado.clientes = resultado.clientes.Skip((datos.pagina - 1) * datos.elementos).Take(datos.elementos).ToList();
                    resultado.totalPaginas = datos.total == -1 ? total : datos.total;
                    resultado.pagina = datos.pagina;
                }

                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        /**********************************************************************************************************************
         * Aprobar/Negar publicacion de medicos nuevos                                                                        *
         **********************************************************************************************************************/
        public dtoPagClientes buscarMedicosAprobacion(dtoBusqueda datos) {
            dtoPagClientes resultado = new dtoPagClientes();
            int total = 0;

            try {
                resultado.clientes = 
                    vCnx.Tb_Cliente.Where(tc => tc.Activo == "4")
                    .Select(e => new {
                        idCliente    = e.idCliente,
                        nombre       = e.Nombre,
                        apellido     = e.Apellido,
                        correo       = e.Correo,
                        telefono     = e.Celular,
                        fechaIngreso = e.FechaIngreso,
                        activo       = e.Activo
                    }).AsEnumerable().Select(e => new dtoClienteResume {
                        idCliente    = FuncionesGlobales.Encry64Encry(e.idCliente.ToString()),
                        nombre       = e.nombre,
                        apellido     = e.apellido,
                        correo       = e.correo,
                        telefono     = e.telefono,
                        fechaIngreso = e.fechaIngreso,
                        activo       = e.activo
                    }).ToList();

                if (resultado.clientes.Count > 0) {
                    resultado.clientes = resultado.clientes.OrderBy(e => e.fechaIngreso).ToList();

                    if (datos.pagina == -1) {
                        total = (int)Math.Ceiling((double)(resultado.clientes.Count() / (double)datos.elementos));
                        datos.pagina = 1;
                    }

                    resultado.clientes = resultado.clientes.Skip((datos.pagina - 1) * datos.elementos).Take(datos.elementos).ToList();
                    resultado.totalPaginas = datos.total == -1 ? total : datos.total;
                    resultado.pagina = datos.pagina;
                }

                return resultado;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool aprobarMedico(string miSesion, int idCliente) {
            Tupla<int, string> cliente = null;
            dtoClienteResume datos = null;
    
            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                datos = new ServicioCliente().GetDatosCliente(idCliente);

                return this.aprobarMedico(datos, idCliente);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool aprobarMedico(dtoClienteResume datos, int idCliente) {
            T_UsuarioActivo usuario = null;
            Tb_Cliente cliente = null;
            int idUsuario = -1;
            
            try {
                idUsuario = vCnx.T_ClienteUsuario.Where(tcu => tcu.id_cliente == idCliente).Select(e => e.id_usuario).FirstOrDefault();

                if (idUsuario <= 0) return false;

                usuario = vCnx.T_UsuarioActivo.Where(tua => tua.id_usuario == idUsuario).FirstOrDefault();

                cliente = vCnx.Tb_Cliente.Where(tc => tc.idCliente == idCliente).FirstOrDefault();

                if (usuario == null || cliente == null) return false;

                usuario.activo = 1;

                cliente.Activo = "1";

                vCnx.SaveChanges();

                ServicioUtilidad.NotificarAlCliente("Aprobacion publicacion perfil medico - Dr. Citas", "Se ha aprobado la publicacion de su perfil medico dentro de Dr. Citas.\n\nPor favor administre su agenda y consultorios para que pueda aparecer en los resultados de busqueda.", datos.correo);

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool negarMedico(string miSesion, int idCliente) {
            Tupla<int, string> cliente = null;
            dtoClienteResume datos = null;

            try {
                cliente = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                if (cliente.item1 <= 0 || cliente.item2 != "A") return false;

                datos = new ServicioCliente().GetDatosCliente(idCliente);

                return this.negarMedico(datos, idCliente);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool negarMedico(dtoClienteResume datos, int idCliente) {
            T_UsuarioActivo usuario = null;
            Tb_Cliente cliente = null;
            int idUsuario = -1;

            try {
                idUsuario = vCnx.T_ClienteUsuario.Where(tcu => tcu.id_cliente == idCliente).Select(e => e.id_usuario).FirstOrDefault();

                if (idUsuario <= 0) return false;

                usuario = vCnx.T_UsuarioActivo.Where(tua => tua.id_usuario == idUsuario).FirstOrDefault();

                cliente = vCnx.Tb_Cliente.Where(tc => tc.idCliente == idCliente).FirstOrDefault();

                if (usuario == null || cliente == null) return false;

                usuario.activo = 0;

                cliente.Activo = "0";

                vCnx.SaveChanges();

                ServicioUtilidad.NotificarAlCliente("Aprobacion publicacion perfil medico - Dr. Citas", "Por favor revise la informacion registrada y realice los cambios pertinentes para la aprobacion de su perfil medico dentro de Dr. Citas.", datos.correo);

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }
        /**********************************************************************************************************************/
    }
}