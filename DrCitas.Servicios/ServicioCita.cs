﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.Objects;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using DrCitas.Datos;
using DrCitas.Dtos;
using DrCitas.Servicios;
using System.Text.RegularExpressions;

namespace DrCitas.Servicios {

    public class ServicioCita {

        DrCitasEntidades vCnx = null;
        ObjectParameter vResultado = null;
        
        public ServicioCita() {
            vCnx = new DrCitasEntidades(FuncionesGlobales.Decry64Decry(System.Configuration.ConfigurationManager.ConnectionStrings["DrCitasEntidades"].ConnectionString));
            vResultado = new ObjectParameter("pResultado", "TRUE:");
        }

        public dtoAgenda DatosDeLaAgenda(int pIdAgenda, int pIdUsuarioX) {

            dtoAgenda dtoA = null;

            var vConsulta = vCnx.Sp_Cita_C_xAgenda(pIdAgenda, pIdUsuarioX).ToList();

            if (vConsulta.ToString() == "FALSE") throw new Exception("Error al obtener datos del consultorio");

            dtoA = (
                    from vTbl in vConsulta
                    select new {
                        IdAgenda        = vTbl.idAgenda,
                        IdCliente       = vTbl.idCliente,
                        FechaHoraInicio = vTbl.FechaHoraInicio,
                        FechaHoraFin    = vTbl.FechaHoraFin,
                        IdConsultorio   = vTbl.idConsultorio,
                        TipoConsulta    = vTbl.TipoConsulta,
                        Direccion       = vTbl.Direccion,
                        Telefono        = vTbl.Telefono,
                        Consultorio     = vTbl.Consultorio,
                        Titulo          = vTbl.Titulo,
                        NombreCliente   = vTbl.NombreCliente,
                        ApellidoCliente = vTbl.ApellidoCliente,
                        Estrellas       = vTbl.Estrellas,
                        Correo          = vTbl.Correo,
                        Facebook        = vTbl.Facebook,
                        Twitter         = vTbl.Twitter
                    }
                ).AsEnumerable().Select(s => new dtoAgenda {
                    IdCliente       = FuncionesGlobales.Encry64Encry(s.IdCliente.ToString()),
                    IdAgenda        = s.IdAgenda,
                    FechaHoraInicio = s.FechaHoraInicio,
                    FechaHoraFin    = s.FechaHoraFin,
                    IdConsultorio   = s.IdConsultorio,
                    TipoConsulta    = s.TipoConsulta,
                    Direccion       = s.Direccion,
                    Telefono        = s.Telefono,
                    Consultorio     = s.Consultorio,
                    Titulo          = s.Titulo,
                    NombreCliente   = s.NombreCliente,
                    ApellidoCliente = s.ApellidoCliente,
                    Estrellas       = s.Estrellas,
                    Correo          = s.Correo,
                    Facebook        = s.Facebook,
                    Twitter         = s.Twitter,
                    Foto            = new ServicioCliente().getImagenPerfil(s.IdCliente)
                }).FirstOrDefault();

            return dtoA;
        }
       
        ///---------------------------------------------------------------------------------------------------------------------
        ///<summary>Operación para reservar una cita.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public string ReservarCita(dtoRegistroCitas dtoRC, dtoClienteResume objCliente, string conn) {

            DataTable dt        = null;
            string  sql         = "", 
                    respuesta   = "";
            int vNumeroCita     = 0, 
                Agenda          = 0,
                id              = -1,
                idMedico        = -1,
                idPaciente      = -1;

            try {
                
                dtoRC.idCreador                 = ((Convert.ToInt32(dtoRC.idCreador) == 0) ? FuncionesGlobales.Decry64Decry(objCliente.idCliente) : dtoRC.idCreador);

                using (ADO ado = new ADO(conn)) {

                    sql = "EXEC [DrCitas].[Sp_Cita_P_Crear] " + (objCliente.idCliente) + " , " + dtoRC.idAgenda + " , " + dtoRC.idRazon + " , '" + objCliente.telefono + "' , '" + objCliente.correo + "' , '1' , '1' , " + (objCliente.idUsuario) + " , " + dtoRC.idCreador + " , " + (dtoRC.idEspecialidad);

                    dt = ado.GetDataTableQuery(sql);

                    if (dt != null)
                        respuesta = dt.Rows[0].Field<string>(0);
                    else
                        return vNumeroCita.ToString();
                }

                if (Regex.IsMatch(respuesta, @"^[0-9]+$")) {
                    
                    vNumeroCita = Convert.ToInt32(respuesta);
                    id = Convert.ToInt32(objCliente.idCliente);

                    T_InterrogatorioMedico IM = vCnx.T_InterrogatorioMedico.Where(f => f.idCliente == id).FirstOrDefault();

                    if (IM != null)
                        IM.JsonIM = dtoRC.jsonIM;
                    else {
                        IM = new T_InterrogatorioMedico {
                            JsonIM = dtoRC.jsonIM,
                            idCliente = (id)
                        };
                        vCnx.T_InterrogatorioMedico.AddObject(IM);
                    }

                    idMedico    = Convert.ToInt32(dtoRC.idMedico);
                    idPaciente  = Convert.ToInt32(objCliente.idCliente);

                    if (!vCnx.T_MedicoPaciente.Any(w => w.id_medico == idMedico && w.id_paciente == idPaciente)) 
                    {
                        vCnx.T_MedicoPaciente.AddObject (
                                new T_MedicoPaciente 
                                {
                                    id_medico   = idMedico,
                                    id_paciente = idPaciente
                                }
                            );                    
                    }

                    vCnx.SaveChanges();

                    Agenda = Convert.ToInt32(dtoRC.idAgenda);

                    ServicioUtilidad.NotificarAlCliente (
                        "Dr. Citas - Información",
                        ("Le informamos que se agendo su cita bajo el numero: " + vNumeroCita.ToString() + " para la dia " + vCnx.Tb_Agenda.Single(w => w.idAgenda == Agenda).FechaHoraInicio.ToShortDateString()).ToString(), 
                        objCliente.correo
                    );
                }
                else if (respuesta.IndexOf(":") > 0)
                {
                    return respuesta.Replace( (Regex.Match(respuesta, @"ERR\-\d{2}\-\d{1}\:")).ToString() , "");
                }

                return vNumeroCita.ToString();
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }
        
        public DataSet ListarCitasPaciente(string idSesion, string conn) {
            string sql = "";

            try {
                sql = "EXEC [DrCitas].[Sp_Cita_C_Paciente] '" + idSesion + "'";
                   
                return new ADO(conn).GetDataSetQuery(sql);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        /// <summary>
        ///  0 para solicitadas y/o atendidas para este momento --- 1 para todas las pasadas de este momento 
        /// </summary>
        /// <param name="miSesion"></param>
        /// <returns></returns>
        public dtoPagCitas ListarCitasMedico(string miSesion, int modo, int pagina = -1, int elementos = 20) {
            int idCliente = -1;

            try {
                idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                return this.ListarCitasMedico(idCliente, modo, pagina, elementos);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        /// <summary>
        ///  0 para solicitadas y/o atendidas para este momento --- 1 para todas las pasadas de este momento
        /// </summary>
        /// <returns></returns>
        public dtoPagCitas ListarCitasMedico(int idCliente, int modo, int pagina = -1, int elementos = 20)
        {
            dtoPagCitas misCitas = new dtoPagCitas();
            int total = 0, id = -1;

            try {
                if (idCliente > 0) {

                    misCitas.citas = (
                           from cita in vCnx.Tb_Cita
                           from razon in vCnx.Tb_Razon
                           from estatus in vCnx.Tb_Estatus
                           from agenda in vCnx.Tb_Agenda
                           from paciente in vCnx.Tb_Cliente
                           from consultorio in vCnx.Tb_Consultorio
                           from medico in vCnx.Tb_Cliente
                           where
                               cita.idRazon == razon.idRazon &&
                               cita.idEstatus == estatus.idEstatus &&
                               cita.idAgenda == agenda.idAgenda &&
                               cita.idCliente == paciente.idCliente &&
                               agenda.idConsultorio == consultorio.idConsultorio &&
                               consultorio.idCliente == medico.idCliente &&
                               medico.idCliente == idCliente &&
                               paciente.idCliente != null
                           select new {
                               IdCita          = cita.idCita,
                               IdPaciente      = paciente.idCliente,
                               IdMedico        = medico.idCliente,
                               IdAgenda        = agenda.idAgenda,
                               IdEstatus       = cita.idEstatus,
                               Estatus         = estatus.Descripcion,
                               IdRazon         = cita.idRazon,
                               IdConsultorio   = consultorio.idConsultorio,
                               IdEspecialidad  = consultorio.id_Especialidad,
                               Numero          = cita.Numero,
                               HoraInicio      = cita.HoraInicio,
                               HoraFin         = cita.HoraFin,
                               Medico          = medico.Nombre + " " + medico.Apellido,
                               Razon           = razon.Descripcion,
                               Paciente        = paciente.Nombre + " " + paciente.Apellido,
                               Consultorio     = consultorio.Nombre,
                               Correo          = paciente.Correo,
                               Documento       = paciente.Documento,
                               TipoDocumento   = paciente.TipoDocumento,
                               FechaNacimiento = paciente.FechaNacimiento,
                               Sexo            = paciente.Sexo
                           }
                       )
                       .AsEnumerable()
                       .Select(s => new dtoCita {
                           IdCita          = s.IdCita,
                           IdPaciente      = FuncionesGlobales.Encry64Encry(s.IdPaciente.ToString()),
                           IdMedico        = FuncionesGlobales.Encry64Encry(s.IdMedico.ToString()),
                           IdAgenda        = s.IdEstatus,
                           IdEstatus       = s.IdEstatus,
                           Estatus         = s.Estatus,
                           IdRazon         = s.IdRazon,
                           IdConsultorio   = s.IdConsultorio,
                           IdEspecialidad  = s.IdEspecialidad,
                           Numero          = s.Numero,
                           HoraInicio      = s.HoraInicio,
                           HoraFin         = s.HoraFin,
                           Medico          = s.Medico,
                           Razon           = s.Razon,
                           Paciente        = s.Paciente,
                           Consultorio     = s.Consultorio,
                           Correo          = s.Correo,
                           Documento       = s.Documento,
                           TipoDocumento   = s.TipoDocumento,
                           FechaNacimiento = s.FechaNacimiento,
                           Sexo            = s.Sexo
                       })
                       .ToList();

                    if (misCitas.citas != null) {
                        if (modo == 0)
                            misCitas.citas = misCitas.citas.Where(e => (e.IdEstatus == 1 || e.IdEstatus == 4) && e.HoraInicio != null && e.HoraInicio >= DateTime.Now).OrderBy(e => e.HoraInicio).ToList();
                        else
                            misCitas.citas = misCitas.citas.Where(e => e.HoraInicio != null && (e.IdEstatus != 1 && e.IdEstatus != 4)).OrderByDescending(e => e.HoraInicio).ToList();

                        foreach (var c in misCitas.citas) {
                            id = Convert.ToInt32(FuncionesGlobales.Decry64Decry(c.IdPaciente));
                            c.empresas = vCnx.T_PacienteEmpresa.Where(tpe => tpe.id_paciente == id && tpe.T_Empresa.Activo == true)
                                .Select(e => new dtoEmpresa {
                                    idEmpresa     = e.id_empresa,
                                    nombre        = e.T_Empresa.nombre,
                                    direccion     = e.T_Empresa.direccion,
                                    tipoDocumento = e.T_Empresa.tipoDocumento,
                                    documento     = e.T_Empresa.documento,
                                    telefono      = e.T_Empresa.telefono,
                                    activo        = e.T_Empresa.Activo
                                }).ToList();
                        }


                        if (pagina == -1) {
                            total = (int)Math.Ceiling((double)(misCitas.citas.Count() / (double)elementos));
                            pagina = 1;
                        }

                        misCitas.citas = misCitas.citas.Skip((pagina - 1) * elementos).Take(elementos).ToList();
                        misCitas.totalPaginas = total;
                        misCitas.pagina = pagina;
                    }
                }
                return misCitas;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool actualizarEstadoCita(int idCita, int estado) {
            Tb_Cita cita = null;
            
            try {
                cita = vCnx.Tb_Cita.Where(tbc => tbc.idCita == idCita).FirstOrDefault();

                if (cita == null) return false;

                cita.idEstatus = estado;

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool crearDiagnostico(int idCita, int idMedico, List<dtoDiagnosticoInfo> datos) {

            try {
                for (int i = 0; i < datos.Count; i++) {
                    vCnx.T_DiagnosticoCita.AddObject(
                        new T_DiagnosticoCita {
                            idCita        = idCita,
                            idMedico      = idMedico,
                            idDiagnostico = datos[i].idDiagnostico,
                            observacion   = datos[i].observacion
                        }
                    );
                }
                
                vCnx.SaveChanges();
                
                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool actualizarDiagnostico(List<dtoDiagnosticoInfo> datos) {
            T_DiagnosticoCita diagnostico = null;

            try {
                for (int i = 0; i < datos.Count; i++) {
                    diagnostico = vCnx.T_DiagnosticoCita.Where(td => td.idDiagnosticoCita == datos[i].idDiagnosticoCita).FirstOrDefault();

                    if (diagnostico == null) continue;

                    diagnostico.idDiagnostico = datos[i].idDiagnostico;
                    diagnostico.observacion   = datos[i].observacion;
                }

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoDiagnosticoCita> getDiagnostico(int idCita) {
            List<dtoDiagnosticoCita> diagnosticos = null;

            try {
                diagnosticos = vCnx.T_DiagnosticoCita.Where(td => td.idCita == idCita)
                    .Select(e => new {
                        idDiagnosticoCita = e.idDiagnosticoCita,
                        idCita            = e.idCita,
                        idMedico          = e.idMedico,
                        idDiagnostico     = e.idDiagnostico,
                        observacion       = e.observacion
                    }).AsEnumerable()
                    .Select(e => new dtoDiagnosticoCita {
                        idDiagnosticoCita = e.idDiagnosticoCita,
                        idCita            = e.idCita,
                        idMedico          = FuncionesGlobales.Encry64Encry(e.idMedico.ToString()),
                        idDiagnostico     = e.idDiagnostico,
                        diagnostico       = vCnx.T_Diagnostico.Where(td => td.idDiagnostico == e.idDiagnostico).Select(t => t.descripcion).FirstOrDefault(),
                        observacion       = e.observacion
                    }).ToList();

                return diagnosticos;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para cancelar una cita.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public void CancelarCita(int pIdCliente, int pIdCita, string pCorreo, int UsuarioSeguimiento, int pIdUsuarioX) {
            try {
                vCnx.Sp_Cita_P_Cancelar(pIdCita, (int?)pIdUsuarioX, UsuarioSeguimiento, vResultado);
                if (vResultado.Value.ToString() != "TRUE:") throw new Exception(vResultado.Value.ToString());
                ServicioUtilidad.NotificarCancelacionCita(pIdCliente, pIdUsuarioX, UsuarioSeguimiento);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace);
            }
        }
       
        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para regresar una lista de preguntas.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public List<dtoPregunta> ListarPreguntas() {
            var vTabla = vCnx.CreateObjectSet<Tb_Pregunta>();
            return (
                from vTbl in vTabla
                where vTbl.Activo == "1"
                select new dtoPregunta {
                    IdPregunta  = vTbl.idPregunta,
                    Descripcion = vTbl.Descripcion,
                    Evaluada    = false
                }
            ).ToList<dtoPregunta>();
        }
       
        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para evaluar una cita.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public void EvaluarCita(int pIdCita, List<dtoPregunta> pEvaluacion, int pIdUsuarioX) {
            DataTable dt = new DataTable();
            dt.Columns.Add("Consecutivo", typeof(Int32));
            dt.Columns.Add("idPregunta", typeof(Int32));
            dt.Columns.Add("Valor", typeof(Int32));

            foreach (dtoPregunta Eval in pEvaluacion) {
                //AgendaConsultorioDto dr = new AgendaConsultorioDto();
                DataRow dr;
                dr = dt.NewRow();
                dr["Consecutivo"] = Eval.IdPregunta;
                dr["IdPregunta"]  = Eval.IdPregunta;
                dr["Valor"]       = Eval.Valor;
                dt.Rows.Add(dr);
            }

            var vIdCita = new SqlParameter("pIdCita ", pIdCita);
            //idCita.Direction = ParameterDirection.Input;
            //idCita.SqlDbType = SqlDbType.Int;

            //Use DbType.Structured for TVP
            var Evaluaciones = new SqlParameter();
            Evaluaciones.ParameterName = "pRespuestas";
            Evaluaciones.SqlDbType = SqlDbType.Structured;
            Evaluaciones.TypeName = "DrCitas.Tt_Respuestas";
            Evaluaciones.Value = dt;
            ////Parameter for SP output
            var vResultado = new SqlParameter("pResultado", "");
            vResultado.Direction = ParameterDirection.Output;
            vResultado.Size = 150;

            var idEvaluacion = new SqlParameter("pIdEvaluacion", 0);
            idEvaluacion.Direction = ParameterDirection.Output;
            idEvaluacion.SqlDbType = SqlDbType.Int;

            var p_IdUsuarioX = new SqlParameter("pIdUsuarioX", pIdUsuarioX);

            var parameters = new object[] {
                vIdCita,
                Evaluaciones,
                p_IdUsuarioX,
                vResultado,
                idEvaluacion
            };

            vCnx.ExecuteStoreCommand(
                @"[CuidaNet.DrCitas].[DrCitas].[Sp_Evaluacion_P_Crear] 
                @pIdCita
                ,@pRespuestas
                ,@pIdUsuarioX
                ,@pResultado OUTPUT
                ,@pIdEvaluacion OUTPUT",
                parameters
            );
            if (vResultado.Value.ToString() != "TRUE:") throw new Exception(vResultado.Value.ToString());
        }
       
        ///---------------------------------------------------------------------------------------------------------------------
        ///<summary>Operación para marcar días libres en la agenda del cliente.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public void AgendarDiasLibres(int pIdCliente, int? pIdEspecialidad, int? pIdConsultorio, DateTime pFechaDesde, DateTime pFechaHasta, string pRazon, int pIdUsuarioX) {
            var vConsulta = vCnx.Sp_AgendaLibre_P_Generar(pIdCliente, pIdEspecialidad, pIdConsultorio, pFechaDesde, pFechaHasta, pRazon, pIdUsuarioX, vResultado);
            if (vResultado.Value.ToString() != "TRUE:") throw new Exception(vResultado.Value.ToString());
        }
    
        ///---------------------------------------------------------------------------------------------------------------------
        ///<summary>Operación para marcar días libres en la agenda del cliente.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public void EliminarDiasLibres(int pIdCita, int pIdUsuarioX) {
            var vConsulta = vCnx.Sp_AgendaLibre_P_Eliminar(pIdCita, pIdUsuarioX, vResultado);
            if (vResultado.Value.ToString() != "TRUE:") throw new Exception(vResultado.Value.ToString());
        }
        ///---------------------------------------------------------------------------------------------------------------------
        ///

        public List<dtoDiagnostico> getDiagnosticoXEspecialidad(int idEspecialidad) {
            List<dtoDiagnostico> diagnosticos = null;

            try {
                diagnosticos = vCnx.T_DiagnosticoEspecialidad.Where(tde => tde.idEspecialidad == idEspecialidad)
                    .Select(e => new dtoDiagnostico {
                        idDiagnostico = e.idDiagnostico,
                        codigo        = e.T_Diagnostico.codigo,
                        diagnostico   = e.T_Diagnostico.descripcion,
                    }).ToList();

                return diagnosticos;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }
    }
}
