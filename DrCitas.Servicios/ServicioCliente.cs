﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using DrCitas.Datos;
using DrCitas.Dtos;
using DrCitas.Servicios;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.IO;

namespace DrCitas.Servicios {

    public class ServicioCliente {

        DrCitasEntidades vCnx = null;
        ObjectParameter vResultado = null;

        public ServicioCliente() {
            vCnx = new DrCitasEntidades(FuncionesGlobales.Decry64Decry(System.Configuration.ConfigurationManager.ConnectionStrings["DrCitasEntidades"].ConnectionString));
            vResultado = new ObjectParameter("pResultado", "TRUE:");
        }

        ///---------------------------------------------------------------------------------------------------------------------
        ///<summary>Operación para verificar si el login esta disponible.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public dtoEnviarInfUsuario GetInfUsuario(string pLogin) {
            try {
                return (
                    from tu in vCnx.T_Usuario
                    join tbcu in vCnx.T_ClienteUsuario on tu.id_usuario equals tbcu.id_usuario
                    join tbc in vCnx.Tb_Cliente on tbcu.id_cliente equals tbc.idCliente
                    where tu.usuario.Trim() == pLogin.Trim() || tu.usuario_m.Trim() == pLogin.Trim()
                    select new {
                        param    = tbcu.id_cliente,
                        Nombre   = tbc.Nombre + " " + tbc.Apellido,
                        Correo   = tbc.Correo,
                        Telefono = tbc.TelefonoPrincipal,
                        Twitter  = tbc.Twitter,
                        Facebook = tbc.Facebook
                    }
                ).AsEnumerable().Select(e => new dtoEnviarInfUsuario {
                    param    = FuncionesGlobales.Encry64Encry(e.param.ToString()),
                    Nombre   = e.Nombre,
                    Correo   = e.Correo,
                    Telefono = e.Telefono,
                    Twitter  = e.Twitter,
                    Facebook = e.Facebook
                }).FirstOrDefault();
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public string GetUsuario(string miSesion) {
            int idCliente = -1;
            string username = "";

            try {
                idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                if (idCliente > 0)
                    username = vCnx.T_ClienteUsuario.Where(tcu => tcu.id_cliente == idCliente).Select(e => e.T_Usuario.usuario).FirstOrDefault();

                return username;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public dtoPagMedicos GetListDr(dtoBusqueda datos, string conn) {
            dtoPagMedicos resultado = new dtoPagMedicos();
            ServicioConsultorio servicio = new ServicioConsultorio();
            int idCliente = 0, total = 0;
            string coord = "";

            try {
                if (!string.IsNullOrEmpty(datos.param)) {
                    idCliente = Convert.ToInt32(FuncionesGlobales.Decry64Decry(datos.param));
                }

                resultado.medicos = vCnx.Sp_BuscarMedicos(idCliente, datos.nombre, datos.apellido, datos.sexo, datos.idCiudad, datos.idEspecialidad, datos.tipo)
                    .Select(e => new {
                        idCliente       = e.idCliente,
                        nombre          = e.Nombre,
                        apellido        = e.Apellido,
                        tipoDocumento   = e.TipoDocumento,
                        documento       = e.Documento,
                        sexo            = e.Sexo,
                        correo          = e.Correo,
                        fechaNacimiento = e.FechaNacimiento,
                        fechaIngreso    = e.FechaIngreso,
                        puntos          = e.Puntos,
                        estrellas       = e.Estrellas,
                        facebook        = e.Facebook,
                        twitter         = e.Twitter,
                        comentario      = e.Comentario,
                        idEstado        = e.idEstado,
                        estado          = e.Estado,
                        idCiudad        = e.idCiudad,
                        ciudad          = e.Ciudad,
                        idConsultorio   = e.idConsultorio,
                        consultorio     = e.Consultorio,
                        direccion       = e.Direccion,
                        telefono        = e.Telefono,
                        tipoConsulta    = e.TipoConsulta,
                        titulo          = e.Titulo
                    })
                    .AsEnumerable().Select(e => new dtoMedicoResume {
                        idCliente       = FuncionesGlobales.Encry64Encry(e.idCliente.ToString()),
                        nombre          = e.nombre,
                        apellido        = e.apellido,
                        tipoDocumento   = e.tipoDocumento,
                        documento       = e.documento,
                        sexo            = e.sexo,
                        correo          = e.correo,
                        fechaNacimiento = e.fechaNacimiento,
                        fechaIngreso    = e.fechaIngreso,
                        puntos          = e.puntos,
                        estrellas       = e.estrellas,
                        facebook        = e.facebook,
                        twitter         = e.twitter,
                        comentario      = e.comentario,
                        idEstado        = e.idEstado,
                        estado          = e.estado,
                        idCiudad        = e.idCiudad,
                        ciudad          = e.ciudad,
                        idConsultorio   = e.idConsultorio,
                        consultorio     = e.consultorio,
                        direccion       = e.direccion,
                        telefono        = e.telefono,
                        tipoConsulta    = e.tipoConsulta,
                        titulo          = e.titulo,
                        imagen          = getImagenPerfil(e.idCliente)
                    }).ToList();

                if (resultado.medicos != null) {
                    resultado.medicos = resultado.medicos.OrderBy(e => e.apellido).ToList();

                    if (datos.pagina == -1) {
                        total = (int)Math.Ceiling((double)(resultado.medicos.Count() / (double)datos.elementos));
                        datos.pagina = 1;
                    }

                    resultado.medicos = resultado.medicos.Skip((datos.pagina - 1) * datos.elementos).Take(datos.elementos).ToList();

                    resultado.medicos.ForEach(e => {
                        e.horarios = servicio.ListarHorarios(new dtoDatosGetHorarios {
                            idCliente      = FuncionesGlobales.Decry64Decry(e.idCliente),
                            idConsultorio  = e.idConsultorio.ToString(),
                            tipoConsulta   = e.tipoConsulta,
                            idEspecialidad = datos.idEspecialidad,

                        }, conn);

                        coord = vCnx.Tb_Consultorio.Where(tc => tc.idConsultorio == e.idConsultorio).Select(c => c.Coordenadas).FirstOrDefault();

                        if (!string.IsNullOrEmpty(coord))
                            e.coordConsultorio = new dtoCoordenada(coord);

                        idCliente = Convert.ToInt32(FuncionesGlobales.Decry64Decry(e.idCliente));
                        e.especialidades = vCnx.Tb_ClienteEspecialidad.Where(tce => tce.idCliente == idCliente)
                            .Select(c => new {
                                idEspecialidad = c.idEspecialidad,
                                Especialidad   = c.Tb_Especialidad.Descripcion,
                                fechaGrado     = c.FechaGrado
                            }).AsEnumerable().Select(c => new dtoDatosMisEstudios {
                                idEspecialidad = c.idEspecialidad,
                                Especialidad   = c.Especialidad,
                                fechaGrado     = c.fechaGrado.ToString()
                            }).ToList();

                        e.empresas = vCnx.T_MedicoEmpresa.Where(tme => tme.id_medico == idCliente)
                            .Select(c => new dtoEmpresa {
                                idEmpresa = c.id_empresa,
                                nombre    = c.T_Empresa.nombre,
                                activo    = c.Activo
                            }).ToList();
                    });

                    resultado.totalPaginas = datos.total == 0 ? total : datos.total;
                    resultado.pagina       = datos.pagina;
                }

                return resultado;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public dtoClienteXidSesion GetDatosClienteXidSesion(string idSesion, string conn, dtoRegistroCitas asociado) {
            DataTable dt = null;
            dtoClienteXidSesion obj = null;
            string sql = "";

            try {
                sql =
                    "SELECT " +
                        "c.idCliente, " +
                        "c.idCiudad, " +
                        "ISNULL(c.TipoDocumento + CONVERT(varchar, c.Documento), ''), " +
                        "c.Correo, " +
                        "b.id_usuario, " +
                        "c.Celular, " +
                        "ISNULL(c.Facebook, ''), " +
                        "ISNULL(c.Twitter, '') " +
                    "FROM " +
                        "DrCitas.T_Usuario AS a " +
                        "INNER JOIN " +
                        "DrCitas.T_ClienteUsuario AS b " +
                        "ON a.id_usuario = b.id_usuario " +
                        "INNER JOIN " +
                        "DrCitas.T_Sesion AS s " +
                        "ON a.id_usuario = s.id_usuario " +
                        (
                            new Func<dtoRegistroCitas, string>((a) => {
                                if (a != null)
                                    if (a.asociado != null)
                                        return "inner join DrCitas.T_ClienteAsociado d on b.id_cliente = d.idCliente inner join DrCitas.Tb_Cliente c on d.idAsociado = c.idCliente ";
                                return "inner join DrCitas.Tb_Cliente c on b.id_cliente = c.idCliente ";
                            })
                        )(asociado) +
                    "WHERE " +
                        (
                            new Func<dtoRegistroCitas, string>((a) => {
                                if (a != null)
                                    if (a.asociado != null)
                                        return "c.idCliente = " + a.asociado.cliente + " AND ";
                                return "";
                            })
                        )(asociado) +
                        "s.sesion = '" + idSesion + "'";

                dt = new ADO(conn).GetDataTableQuery(sql);

                for (int i = 0; i < dt.Rows.Count; i++) {
                    obj = new dtoClienteXidSesion();
                    obj.idCliente = dt.Rows[i].Field<int>(0);
                    obj.idCiudad  = dt.Rows[i].Field<int>(1);
                    obj.cedula    = dt.Rows[i].Field<string>(2).Trim();
                    obj.correo    = dt.Rows[i].Field<string>(3).Trim();
                    obj.idUsuario = dt.Rows[i].Field<int>(4);
                    obj.telefono  = dt.Rows[i].Field<string>(5).Trim();
                    obj.facebook  = dt.Rows[i].Field<string>(6).Trim();
                    obj.twitter   = dt.Rows[i].Field<string>(6).Trim();
                }

                return obj;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public dtoClienteResume GetDatosCliente(string miSesion) {
            int idCliente = -1;

            try {
                idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                return this.GetDatosCliente(idCliente);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public dtoClienteResume GetDatosCliente(int idCliente) {
            dtoClienteResume datos = null;

            try {
                datos =
                    (
                        from cliente in vCnx.Tb_Cliente
                        where cliente.idCliente == idCliente
                        select new {
                            idCliente     = cliente.idCliente,
                            idCiudad      = cliente.idCiudad,
                            tipoDocumento = cliente.TipoDocumento,
                            documento     = cliente.Documento,
                            correo        = cliente.Correo,
                            telefono      = cliente.Celular,
                            facebook      = cliente.Facebook,
                            twitter       = cliente.Twitter
                        }
                    ).AsEnumerable().Select(e => new dtoClienteResume {
                        idCliente     = FuncionesGlobales.Encry64Encry(e.idCliente.ToString()),
                        idCiudad      = e.idCiudad,
                        tipoDocumento = e.tipoDocumento,
                        documento     = e.documento,
                        correo        = e.correo,
                        idUsuario     = FuncionesGlobales.Encry64Encry(vCnx.T_ClienteUsuario.Where(tcu => tcu.id_cliente == e.idCliente).Select(s => s.id_usuario).FirstOrDefault().ToString()),
                        telefono      = e.telefono,
                        facebook      = e.facebook,
                        twitter       = e.twitter,
                    }).FirstOrDefault();

                return datos;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public HorariosSemanas ListarHorariosxSemanas(int pIdCliente, int pIdEspecialidad, string pIdConsultorio, string pFechaInicio, int pIdUsuarioX, string tc, string stringConn) {

            DataSet ds = null;
            HorariosSemanas HoriosSemanas = null;
            List<ListDtoHorario> lgtoh = null;
            dtoHorario _dtoHorario = null;
            List<dtoHorario> listDtoHorario = null;

            int IdConsultorio = ((pIdConsultorio.Trim() != "") ? Convert.ToInt32(pIdConsultorio.Trim()) : vCnx.Tb_Consultorio.Where(w => w.idCliente == pIdCliente).Select(s => s.idConsultorio).FirstOrDefault());

            using (ADO ado = new ADO(stringConn)) {
                ds = ado.GetDataSetQuery("DECLARE @sql VARCHAR(MAX) EXEC [DrCitas].[Sp_Horario_C_xClienteEspecialidad]  " + pIdCliente + " , " + pIdEspecialidad + " , " + IdConsultorio + ",null," + pIdUsuarioX + ", '" + tc + "' , @sql");
            }

            for (int i = 0; i < ds.Tables.Count; i++) {

                if (i == 0)
                    lgtoh = new List<ListDtoHorario>();

                listDtoHorario = new List<dtoHorario>();

                for (int j = 0; j < ds.Tables[i].Rows.Count; j++) {
                    _dtoHorario = new dtoHorario();
                    _dtoHorario.Dia1 = ObtenerValor("Hora", ds.Tables[i].Rows[j].Field<string>(1), FuncionesGlobales.Encry64Encry(pIdCliente.ToString()), pIdEspecialidad);
                    _dtoHorario.Dia2 = ObtenerValor("Hora", ds.Tables[i].Rows[j].Field<string>(2), FuncionesGlobales.Encry64Encry(pIdCliente.ToString()), pIdEspecialidad);
                    _dtoHorario.Dia3 = ObtenerValor("Hora", ds.Tables[i].Rows[j].Field<string>(3), FuncionesGlobales.Encry64Encry(pIdCliente.ToString()), pIdEspecialidad);
                    _dtoHorario.Dia4 = ObtenerValor("Hora", ds.Tables[i].Rows[j].Field<string>(4), FuncionesGlobales.Encry64Encry(pIdCliente.ToString()), pIdEspecialidad);
                    _dtoHorario.Dia5 = ObtenerValor("Hora", ds.Tables[i].Rows[j].Field<string>(5), FuncionesGlobales.Encry64Encry(pIdCliente.ToString()), pIdEspecialidad);
                    _dtoHorario.Dia6 = ObtenerValor("Hora", ds.Tables[i].Rows[j].Field<string>(6), FuncionesGlobales.Encry64Encry(pIdCliente.ToString()), pIdEspecialidad);
                    _dtoHorario.Dia7 = ObtenerValor("Hora", ds.Tables[i].Rows[j].Field<string>(7), FuncionesGlobales.Encry64Encry(pIdCliente.ToString()), pIdEspecialidad);
                    _dtoHorario.UrlDia1 = ObtenerValor("Url", ds.Tables[i].Rows[j].Field<string>(1), FuncionesGlobales.Encry64Encry(pIdCliente.ToString()), pIdEspecialidad);
                    _dtoHorario.UrlDia2 = ObtenerValor("Url", ds.Tables[i].Rows[j].Field<string>(2), FuncionesGlobales.Encry64Encry(pIdCliente.ToString()), pIdEspecialidad);
                    _dtoHorario.UrlDia3 = ObtenerValor("Url", ds.Tables[i].Rows[j].Field<string>(3), FuncionesGlobales.Encry64Encry(pIdCliente.ToString()), pIdEspecialidad);
                    _dtoHorario.UrlDia4 = ObtenerValor("Url", ds.Tables[i].Rows[j].Field<string>(4), FuncionesGlobales.Encry64Encry(pIdCliente.ToString()), pIdEspecialidad);
                    _dtoHorario.UrlDia5 = ObtenerValor("Url", ds.Tables[i].Rows[j].Field<string>(5), FuncionesGlobales.Encry64Encry(pIdCliente.ToString()), pIdEspecialidad);
                    _dtoHorario.UrlDia6 = ObtenerValor("Url", ds.Tables[i].Rows[j].Field<string>(6), FuncionesGlobales.Encry64Encry(pIdCliente.ToString()), pIdEspecialidad);
                    _dtoHorario.UrlDia7 = ObtenerValor("Url", ds.Tables[i].Rows[j].Field<string>(7), FuncionesGlobales.Encry64Encry(pIdCliente.ToString()), pIdEspecialidad);
                    listDtoHorario.Add(_dtoHorario);
                }
                lgtoh.Add(new ListDtoHorario("semana" + i, listDtoHorario));
            }

            if (lgtoh != null)
                HoriosSemanas = new HorariosSemanas(lgtoh);

            if (vResultado.Value.ToString() != "TRUE:")
                throw new Exception(vResultado.Value.ToString());

            return HoriosSemanas;
        }

        ///---------------------------------------------------------------------------------------------------------------------
        ///<summary>Operación para obtener un atributo de una agenda.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public static string ObtenerValor(string pDato, string pAgenda, string idCliente = "", int idEspecialidad = 0) {
            if (pAgenda == null)
                return "";

            string[] vDatos = pAgenda.Split(new char[] { '+' });

            switch (pDato) {
                case "Hora":
                if (vDatos[3] == "O")
                    return Convert.ToDateTime("01/01/1900 " + vDatos[1]).ToShortTimeString() + " a<br/>" + Convert.ToDateTime("01/01/1900 " + vDatos[2]).ToShortTimeString();
                return Convert.ToDateTime("01/01/1900 " + vDatos[1]).ToShortTimeString();
                case "Url":
                return "TuCita.html?IdCliente=" + idCliente.ToString() + "&IdEspecialidad=" + idEspecialidad.ToString() + "&NroCita=" + vDatos[0];
                default:
                break;
            }
            return "";
        }

        ///---------------------------------------------------------------------------------------------------------------------
        ///<summary>Operación para obtener los horarios de un consultorio.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public List<dtoConsultorioHorario> ListaDeConsultorioHorarios(int pIdConsultorio, int? pIdEspecialidad, int pIdUsuarioX) {

            var vConsulta = vCnx.Sp_ConsultorioHorario_C_xEspecialidad(pIdConsultorio, pIdEspecialidad, "1", pIdUsuarioX, vResultado);

            if (vResultado.Value.ToString() != "TRUE:")
                throw new Exception(vResultado.Value.ToString());

            return (
                from vTbl in vConsulta
                select new dtoConsultorioHorario {
                    IdConsultorioHorario = vTbl.IdConsultorioHorario,
                    IdConsultorio        = vTbl.idConsultorio,
                    //IdEspecialidad       = vTbl.idEspecialidad,
                    Dias                 = vTbl.Dias,
                    //DiasNombre           = vTbl.Dias.Replace("1", "Lun").Replace("2", "Mar").Replace("3", "Mie").Replace("4", "Jue").Replace("5", "Vie").Replace("6", "Sab").Replace("7", "Dom"),
                    TipoConsulta         = vTbl.TipoConsulta,
                    HoraDesde            = Convert.ToDateTime(vTbl.HoraDesde).GetDateTimeFormats()[80],
                    HoraHasta            = Convert.ToDateTime(vTbl.HoraHasta).GetDateTimeFormats()[80],
                    Cupos                = vTbl.Cupos,
                    Duracion             = vTbl.Duracion//,
                    //Accion               = "Editar"
                }
            ).ToList();
        }

        ///---------------------------------------------------------------------------------------------------------------------
        ///<summary>Operación para consultar los consultorios de un cliente.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public List<dtoConsultorio> ListarConsultorios(int pIdCliente, int? pIdEspecialidad, int pIdUsuarioX) {

            var vConsulta = vCnx.Sp_Consultorio_C_xCliente(pIdCliente, pIdEspecialidad, "1", pIdUsuarioX, vResultado);

            var vConsultorios = (
                from vTbl in vConsulta
                select new dtoConsultorio {
                    IdConsultorio      = vTbl.IdConsultorio,
                    IdEspecialidad     = vTbl.IdEspecialidad,
                    IdEstado           = vTbl.IdEstado,
                    IdCiudad           = vTbl.IdCiudad,
                    Ciudad             = vTbl.Ciudad,
                    Estado             = vTbl.Estado,
                    Direccion          = vTbl.Direccion,
                    DireccionMapa      = vTbl.Direccion + ", " + vTbl.Ciudad + ", " + vTbl.Estado,
                    Telefono           = vTbl.Telefono,
                    Coordenadas        = vTbl.Coordenadas,
                    Especialidad       = vTbl.Especialidad,
                    Nombre             = vTbl.Nombre,
                    ConsultorioHorario = ListaDeConsultorioHorarios(vTbl.IdConsultorio, pIdEspecialidad, pIdUsuarioX)
                }
            ).ToList();

            if (vResultado.Value.ToString() != "TRUE:")
                throw new Exception(vResultado.Value.ToString());

            return vConsultorios;
        }

        public bool ExistUser(string user) {
            return vCnx.T_Usuario.Any(a => a.usuario.Trim() == user.Trim());
        }

        public bool ExistEmail(string email) {
            return vCnx.T_Usuario.Any(a => a.usuario_m.Trim() == email.Trim());
        }

        /// <summary>
        /// SEXO poner id de paciente
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public string getInterrogatorioMedico(int idCliente) {
            string im = "";

            try {
                im = vCnx.T_InterrogatorioMedico.Where(tim => tim.idCliente == idCliente).Select(e => e.JsonIM).FirstOrDefault();
                return !string.IsNullOrEmpty(im) ? im : "";
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public DataTable GetAsociado(string idSesion, string conn) {
            try {
                string sql =
                    "SELECT " +
                       "ISNULL(cliente.Nombre, '')          AS nombre, " +
                       "ISNULL(cliente.Apellido, '')        AS apellido, " +
                       "cliente.Sexo                        AS sexo, " +
                       "ISNULL(TB0.parentesco , '')         AS parentesco, " +
                       "ISNULL(inte.JsonIM , '')            AS im ," +
                       "cliente.idCliente                   AS cliente , " +
                       "cliente.FechaNacimiento		 	    AS FN, " +
                       "ISNULL(cliente.TelefonoCasa, '')	AS TC, " +
                       "ISNULL(cliente.TelefonoOficina, '') AS tOfi, " +
                       "ISNULL(cliente.Facebook, '')	 	AS facebook, " +
                       "ISNULL(cliente.Twitter, '')	 	    AS twiter, " +
                       "ISNULL(cliente.Correo,'')           AS correo," +
                       "ISNULL(cliente.TipoDocumento, '')   AS tipoDocumento, " +
                       "ISNULL(cliente.Documento, '')       AS documento " +
                    "FROM " +
                        "DrCitas.Tb_Cliente AS cliente " +
                        "INNER JOIN " +
                        "( " +
                            "SELECT DISTINCT " +
                                "asociado.idAsociado, asociado.parentesco AS parentesco " +
                            "FROM " +
                                "DrCitas.T_Sesion AS sesion " +
                                "LEFT JOIN DrCitas.T_ClienteUsuario  AS usuario  ON sesion.id_usuario = usuario.id_usuario " +
                                "LEFT JOIN DrCitas.T_ClienteAsociado AS asociado ON usuario.id_cliente = asociado.idCliente " +
                            "WHERE " +
                                "sesion.sesion = '" + idSesion + "' AND " +
                                "sesion.sesionActiva = 1 " +
                        ") AS TB0 ON cliente.idCliente = TB0.idAsociado " +
                        "LEFT JOIN DrCitas.T_InterrogatorioMedico AS inte ON cliente.idCliente = inte.idCliente " +
                    "ORDER BY " +
                        "inte.idIM DESC";

                return new ADO(conn).GetDataTableQuery(sql);

            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoAsociado> GetMisAsociados(string miSesion) {
            int idCliente = -1;

            try {
                idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                return this.GetMisAsociados(idCliente);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoAsociado> GetMisAsociados(int idCliente) {
            List<dtoAsociado> asociados = null;
            T_ClienteAsociado T_ClienteAsociado = null;

            try {
                if (idCliente > 0) {

                    T_ClienteAsociado = vCnx.T_ClienteAsociado.Where(w => w.idCliente == idCliente).FirstOrDefault();

                    if (T_ClienteAsociado == null)
                        return asociados;

                    asociados =
                            vCnx.Tb_Cliente
                                .Where(w => w.idCliente == T_ClienteAsociado.idAsociado)
                                .Select(e => new {
                                    nombre     = e.Nombre,
                                    apellido   = e.Apellido,
                                    sexo       = e.Sexo,
                                    parentesco = T_ClienteAsociado.parentesco,
                                    cliente    = e.idCliente,
                                    FN         = e.FechaNacimiento,
                                }).AsEnumerable().Select(e => new dtoAsociado {
                                    nombre     = e.nombre,
                                    apellido   = e.apellido,
                                    sexo       = e.sexo,
                                    parentesco = e.parentesco,
                                    HM         = vCnx.T_InterrogatorioMedico.Where(tim => tim.idCliente == e.cliente).Select(t => t.JsonIM).FirstOrDefault(),
                                    cliente    = FuncionesGlobales.Encry64Encry(e.cliente.ToString()),
                                    FN         = e.FN.ToString("yyyy-MM-dd"),
                                    empresas   = vCnx.T_PacienteEmpresa.Where(tpe => tpe.id_paciente == e.cliente)
                                        .Select(s => new dtoEmpresa {
                                            idEmpresa     = s.T_Empresa.id_empresa,
                                            nombre        = s.T_Empresa.nombre,
                                            direccion     = s.T_Empresa.direccion,
                                            tipoDocumento = s.T_Empresa.tipoDocumento,
                                            documento     = s.T_Empresa.documento,
                                            telefono      = s.T_Empresa.telefono,
                                            activo        = s.Activo
                                        }).ToList(),
                                    dtoDA = vCnx.Tb_Cliente.Where(tc => tc.idCliente == e.cliente).Select(t => new dtoDatosPaciente {
                                        telefonoCasa    = t.TelefonoCasa,
                                        telefonoOficina = t.TelefonoOficina,
                                        correo          = t.Correo,
                                        tipoDocumento   = t.TipoDocumento,
                                        documento       = t.Documento
                                    }).FirstOrDefault()
                                }).ToList();

                }

                return asociados;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public int RegistrarAsociado(dtoAsociado asociado, dtoClienteResume clienteActual) {
            try {
                int res = 0;
                int idCliente = Convert.ToInt32(clienteActual.idCliente);

                List<T_ClienteAsociado> Asociado = (vCnx.T_ClienteAsociado.Where(w => w.idCliente == idCliente).ToList());

                if (Asociado == null)
                    res = _RegistroAsociado(asociado, clienteActual);
                else {
                    Tb_Cliente Cliente = (
                        from cliente in vCnx.Tb_Cliente
                        join asoc in vCnx.T_ClienteAsociado on cliente.idCliente equals asoc.idAsociado
                        where cliente.Nombre == (asociado.nombre.Trim()) && cliente.Sexo == (asociado.sexo.Trim()) && asoc.parentesco == (asociado.parentesco.Trim())
                        select cliente
                    ).FirstOrDefault();

                    if (Cliente == null)
                        res = _RegistroAsociado(asociado, clienteActual);
                    else
                        throw new Exception("Ya existe la persona que desea asociar");
                }
                return res;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private int _RegistroAsociado(dtoAsociado asociado, dtoClienteResume clienteActual) {
            try {
                DateTime fn = Convert.ToDateTime(asociado.FN);
                DateTime fe = DateTime.Now;
                int nroID = Convert.ToInt32(vCnx.Tb_Cliente.OrderByDescending(o => o.idCliente).Select(s => s.idCliente).FirstOrDefault() + 1);
                int? documento = (!string.IsNullOrEmpty(asociado.documento.ToString()) || asociado.documento == 0 ? Convert.ToInt32(Regex.Match(asociado.documento.ToString(), "\\d+").ToString()) : clienteActual.documento);
                int idCliente = Convert.ToInt32(clienteActual.idCliente);
                string td = (!string.IsNullOrEmpty(asociado.tipoDocumento) || asociado.documento == 0 ? asociado.tipoDocumento : clienteActual.tipoDocumento);

                Tb_Cliente tbCliente = new Tb_Cliente {
                    idCliente       = nroID,
                    idCiudad        = clienteActual.idCiudad,
                    Nombre          = asociado.nombre,
                    Apellido        = asociado.apellido,
                    Documento       = documento,
                    TipoDocumento   = td,
                    idEstatus       = 1,
                    TipoCliente     = "P",
                    Correo          = clienteActual.correo,
                    Celular         = clienteActual.telefono,
                    Sexo            = asociado.sexo,
                    RecibirEmails   = "1",
                    FechaNacimiento = fn,
                    FechaIngreso    = fe,
                    Activo          = "1",
                    idTitulo        = (asociado.sexo == "M" ? 2 : 3)
                };

                T_ClienteAsociado tClienteAsociado = new T_ClienteAsociado {
                    idCliente  = idCliente,
                    idAsociado = nroID,
                    parentesco = asociado.parentesco
                };

                vCnx.Tb_Cliente.AddObject(tbCliente);
                vCnx.T_ClienteAsociado.AddObject(tClienteAsociado);
                vCnx.SaveChanges();
                return nroID;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        /// <summary>
        /// registra un nuevo paciente
        /// </summary>
        public bool RegistrarCliente(dtoRegistroCliente datos) {
            string respuesta = "";
            T_Usuario usr = null;

            try {
                respuesta = vCnx.Sp_Cliente_P_Crear(datos.TipoCliente, datos.Nombre, datos.Apellido, datos.Sexo, datos.Correo, datos.Usuario, datos.Password, datos.Celular, datos.RecibirCorreo, datos.FechaNacimiento, datos.IdUsuarioPrincipal, datos.numeroColegio, datos.matriculaSAS, datos.tipoDocumento, datos.documento, datos.idCiudad).FirstOrDefault();
                //respuesta = vCnx.Sp_Cliente_P_Crear1(datos.TipoCliente, datos.Nombre, datos.Apellido, datos.Sexo, datos.Correo, datos.Usuario, datos.Password, datos.Celular, datos.RecibirCorreo, datos.FechaNacimiento, datos.IdUsuarioPrincipal, datos.numeroColegio, datos.matriculaSAS).FirstOrDefault();

                if (System.Text.RegularExpressions.Regex.IsMatch(respuesta, "OK")) {
                    usr = vCnx.T_Usuario.Where(tu => tu.usuario == datos.Usuario && tu.contrasena == datos.Password).FirstOrDefault();

                    if (usr != null && !string.IsNullOrEmpty(datos.Correo)) {
                        usr.usuario_m = FuncionesGlobales.Enc((FuncionesGlobales.SHA1HashEncode(datos.Correo, FuncionesGlobales.EncodeStyle.Base64)));
                        vCnx.SaveChanges();
                    }

                    return true;
                }

                return false;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool SolicitarRevisionRegistro(dtoDatosMedico dtoDM, string miSesion) {
            int idCliente = -1;

            try {
                idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                return this.SolicitarRevisionRegistro(dtoDM, idCliente);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool SolicitarRevisionRegistro(dtoDatosMedico dtoDM, int idCliente) {
            T_UsuarioActivo usuario = null;
            Tb_Cliente cliente = null;
            int idUsuario = -1;

            try {
                idUsuario = vCnx.T_ClienteUsuario.Where(tcu => tcu.id_cliente == idCliente).Select(e => e.id_usuario).FirstOrDefault();

                if (idUsuario <= 0)
                    return false;

                usuario = vCnx.T_UsuarioActivo.Where(tua => tua.id_usuario == idUsuario).FirstOrDefault();

                cliente = vCnx.Tb_Cliente.Where(tc => tc.idCliente == idCliente).FirstOrDefault();

                if (usuario == null || cliente == null)
                    return false;

                if (!this.ActualizarDatosBasicosMedico(dtoDM, idCliente))
                    return false;

                if (!this.ActualizarOtrosDatosMedico(dtoDM, idCliente))
                    return false;

                usuario.activo = 0;

                cliente.Activo = "4";

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        /// <summary>
        /// registra cualquier tipo de cliente (pacientes, medicos y administradores)
        /// </summary>
        public bool RegistroCliente(dtoRegistroCliente obj, string conn) {
            string sql
                = "EXEC [DrCitas].[Sp_Cliente_P_Crear1] '" + obj.TipoCliente + "' , '" + obj.Nombre + "' , '" + obj.Apellido + "'" +
                ", '" + obj.Sexo + "' , '" + obj.Correo + "' , '" + obj.Usuario + "' , '" + obj.Password + "' , '" + obj.Celular + "' , '" + obj.RecibirCorreo + "' , '" + obj.FechaNacimiento + "' , '" + obj.IdUsuarioPrincipal + "'";
            try {
                return new ADO(conn).execQuery(sql);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + ex.StackTrace, ex.InnerException);
            }
        }

        public dtoDatosPaciente sac(string idSesion) {
            try {
                var algo = (
                    from sesion in vCnx.T_Sesion
                    join cliUser in vCnx.T_ClienteUsuario on sesion.id_usuario equals cliUser.id_usuario
                    join cliente in vCnx.Tb_Cliente on cliUser.id_cliente equals cliente.idCliente
                    where sesion.sesion == idSesion && sesion.sesionActiva == 1
                    select new dtoDatosPaciente {
                        tipoDocumento   = cliente.TipoDocumento,
                        documento       = cliente.Documento,
                        telefonoCasa    = cliente.TelefonoCasa,
                        telefonoOficina = cliente.TelefonoOficina,
                        facebook        = cliente.Facebook,
                        twitter         = cliente.Twitter,
                        recibirCorreo   = cliente.RecibirEmails
                    }
                ).FirstOrDefault<dtoDatosPaciente>();

                return algo;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public dtoDatosPaciente DatosAdicionalesTitular(string miSesion) {
            int idCliente = -1;

            try {

                idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                return this.DatosAdicionalesTitular(idCliente);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public dtoDatosPaciente DatosAdicionalesTitular(int idCliente) {
            Tb_Cliente cliente = null;
            dtoDatosPaciente datosAdicionales = null;

            try {

                if (idCliente > 0) {
                    cliente = vCnx.Tb_Cliente.Where(tbc => tbc.idCliente == idCliente).FirstOrDefault();

                    if (cliente != null) {
                        datosAdicionales = new dtoDatosPaciente {
                            nombre               = cliente.Nombre,
                            apellido             = cliente.Apellido,
                            correo               = cliente.Correo,
                            sexo                 = cliente.Sexo,
                            tipoDocumento        = cliente.TipoDocumento,
                            documento            = cliente.Documento,
                            telefonoCasa         = cliente.TelefonoCasa,
                            telefonoOficina      = cliente.TelefonoOficina,
                            idCiudad             = cliente.idCiudad,
                            idEstado             = vCnx.Tb_Ciudad.Where(w => w.idCiudad == cliente.idCiudad).Select(s => s.idEstado).FirstOrDefault(),
                            celular              = cliente.Celular,
                            facebook             = cliente.Facebook,
                            twitter              = cliente.Twitter,
                            recibirCorreo        = cliente.RecibirEmails,
                            FechaNacimiento      = cliente.FechaNacimiento,
                            param                = FuncionesGlobales.Encry64Encry(cliente.idCliente.ToString()),
                            imagen               = "",
                            interrogatorioMedico = getInterrogatorioMedico(cliente.idCliente)
                        };

                        if (datosAdicionales.idEstado > 0)
                            datosAdicionales.idPais = vCnx.Tb_Estado.Where(w => w.idEstado == datosAdicionales.idEstado).Select(s => s.idPais).FirstOrDefault();

                        if (datosAdicionales.idCiudad > 0)
                            datosAdicionales.idEstado = vCnx.Tb_Ciudad.Where(w => w.idCiudad == datosAdicionales.idCiudad).Select(s => s.idEstado).FirstOrDefault();

                        if (datosAdicionales.idEstado > 0)
                            datosAdicionales.idPais = vCnx.Tb_Estado.Where(w => w.idEstado == datosAdicionales.idEstado).Select(s => s.idPais).FirstOrDefault();

                        datosAdicionales.imagen = this.getImagenPerfil(idCliente);

                        datosAdicionales.aseguradoras = vCnx.T_PacienteEmpresa.Where(tpe => tpe.id_paciente == idCliente)
                            .Select(e => new dtoEmpresa {
                                idEmpresa = e.T_Empresa.id_empresa,
                                nombre = e.T_Empresa.nombre
                            }).ToList();
                    }
                }
                return datosAdicionales;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public dtoDatosMedico DatosAdicionalesMedico(string miSesion) {
            int idCliente = -1;

            try {

                idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                return this.DatosAdicionalesMedico(idCliente);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public dtoDatosMedico DatosAdicionalesMedico(int idCliente) {
            Tb_Cliente cliente = null;
            dtoDatosMedico datosAdicionales = null;

            try {

                if (idCliente > 0) {

                    cliente = vCnx.Tb_Cliente.Where(tbc => tbc.idCliente == idCliente).FirstOrDefault();

                    if (cliente != null) {
                        datosAdicionales = new dtoDatosMedico {
                            nombre          = cliente.Nombre,
                            apellido        = cliente.Apellido,
                            correo          = cliente.Correo,
                            sexo            = cliente.Sexo,
                            tipoDocumento   = cliente.TipoDocumento,
                            documento       = cliente.Documento,
                            telefonoCasa    = cliente.TelefonoCasa,
                            telefonoOficina = cliente.TelefonoOficina,
                            celular         = cliente.Celular,
                            facebook        = cliente.Facebook,
                            twitter         = cliente.Twitter,
                            recibirCorreo   = cliente.RecibirEmails,
                            acercaDeMi      = cliente.Comentario,
                            idCiudad        = cliente.idCiudad,
                            titulo          = vCnx.Tb_Titulo.Where(w => w.idTitulo == cliente.idTitulo).Select(s => s.Descripcion).FirstOrDefault(),
                            consultorios    = new ServicioConsultorio().ListarConsultorios(idCliente),
                            imagen          = ""
                        };

                        if (datosAdicionales.idCiudad > 0)
                            datosAdicionales.idEstado = vCnx.Tb_Ciudad.Where(w => w.idCiudad == datosAdicionales.idCiudad).Select(s => s.idEstado).FirstOrDefault();

                        if (datosAdicionales.idEstado > 0)
                            datosAdicionales.idPais = vCnx.Tb_Estado.Where(w => w.idEstado == datosAdicionales.idEstado).Select(s => s.idPais).FirstOrDefault();

                        datosAdicionales.imagen = this.getImagenPerfil(idCliente);

                        datosAdicionales.numeroColegio = vCnx.Tb_ClienteAtributoLista.Where(tbcal => tbcal.IdCliente == idCliente && tbcal.Tb_AtributoLista.idAtributo == 4).Select(e => e.Descripcion).FirstOrDefault();

                        datosAdicionales.matriculaSAS = vCnx.Tb_ClienteAtributoLista.Where(tbcal => tbcal.IdCliente == idCliente && tbcal.Tb_AtributoLista.idAtributo == 3).Select(e => e.Descripcion).FirstOrDefault();

                        datosAdicionales.estudios = (
                            from clienteEsp in vCnx.Tb_ClienteEspecialidad
                            join especialidad in vCnx.Tb_Especialidad
                            on clienteEsp.idEspecialidad equals especialidad.idEspecialidad
                            join universidad in vCnx.Tb_Universidad
                            on clienteEsp.idUniversidad equals universidad.idUniversidad
                            where clienteEsp.idCliente == idCliente && especialidad.Tipo == "M"
                            select new {
                                idEspecialidad = especialidad.idEspecialidad,
                                Especialidad   = especialidad.Descripcion,
                                idUniversidad  = universidad.idUniversidad,
                                Universidad    = universidad.Descripcion,
                                fechaGrado     = clienteEsp.FechaGrado
                            }
                        ).AsEnumerable().Select(s => new dtoDatosMisEstudios {
                            idEspecialidad = s.idEspecialidad,
                            Especialidad   = s.Especialidad,
                            idUniversidad  = s.idUniversidad,
                            Universidad    = s.Universidad,
                            fechaGrado     = s.fechaGrado.ToString("yyyy-MM-dd")
                        }).ToList();

                        datosAdicionales.cursos = (
                            from CAL in vCnx.Tb_ClienteAtributoLista
                            join AL in vCnx.Tb_AtributoLista
                            on CAL.IdAtributoLista equals AL.idAtributoLista
                            join A in vCnx.Tb_Atributo
                            on AL.idAtributo equals A.idAtributo
                            where A.idAtributo == 13 && CAL.IdCliente == idCliente
                            select new dtoDatosMisCursos {
                                idAtributoLista = AL.idAtributoLista,
                                Curso = AL.Descripcion,
                                Mes   = CAL.MesAsignacion,
                                Anio  = CAL.AnioAsignacion
                            }
                        ).ToList();

                        datosAdicionales.actividades = (
                            from CAL in vCnx.Tb_ClienteAtributoLista
                            join AL in vCnx.Tb_AtributoLista
                            on CAL.IdAtributoLista equals AL.idAtributoLista
                            join A in vCnx.Tb_Atributo
                            on AL.idAtributo equals A.idAtributo
                            where A.idAtributo == 11 && CAL.IdCliente == idCliente
                            select new dtoDatosMisActividadesDocentes {
                                idAtributoLista = AL.idAtributoLista,
                                Actividad       = AL.Descripcion,
                                Mes             = CAL.MesAsignacion,
                                Anio            = CAL.AnioAsignacion
                            }
                        ).ToList();

                        datosAdicionales.libros = (
                            from CAL in vCnx.Tb_ClienteAtributoLista
                            join AL in vCnx.Tb_AtributoLista
                            on CAL.IdAtributoLista equals AL.idAtributoLista
                            join A in vCnx.Tb_Atributo
                            on AL.idAtributo equals A.idAtributo
                            where A.idAtributo == 7 && CAL.IdCliente == idCliente
                            select new dtoDatosMisLibros {
                                idAtributoLista = AL.idAtributoLista,
                                Libro           = AL.Descripcion,
                                Mes             = CAL.MesAsignacion,
                                Anio            = CAL.AnioAsignacion
                            }
                        ).ToList();

                        datosAdicionales.publicaciones = (
                            from CAL in vCnx.Tb_ClienteAtributoLista
                            join AL in vCnx.Tb_AtributoLista
                            on CAL.IdAtributoLista equals AL.idAtributoLista
                            join A in vCnx.Tb_Atributo
                            on AL.idAtributo equals A.idAtributo
                            where A.idAtributo == 8 && CAL.IdCliente == idCliente
                            select new dtoDatosMisPublicaciones {
                                idAtributoLista = AL.idAtributoLista,
                                Publicacion     = AL.Descripcion,
                                Mes             = CAL.MesAsignacion,
                                Anio            = CAL.AnioAsignacion
                            }
                        ).ToList();

                        datosAdicionales.idiomas = (
                            from CAL in vCnx.Tb_ClienteAtributoLista
                            join AL in vCnx.Tb_AtributoLista
                            on CAL.IdAtributoLista equals AL.idAtributoLista
                            join A in vCnx.Tb_Atributo
                            on AL.idAtributo equals A.idAtributo
                            where A.idAtributo == 6 && CAL.IdCliente == idCliente
                            select new dtoDatosMisIdiomas {
                                idAtributoLista = AL.idAtributoLista,
                                Idioma          = AL.Descripcion
                            }
                        ).ToList();
                    }
                }
                return datosAdicionales;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public DataTable ConectarUsuario(dtoUserPassw OS, string conn) {

            string sql = "DECLARE @sql VARCHAR(MAX) EXEC [DrCitas].[Sp_Usuario_P_Conectar1] '" + OS.Usuario + "','" + OS.Password + "', 1 , @sql";

            try {
                return new ADO(conn).GetDataTableQuery(sql);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public DataTable ActualizarSesion(dtoObjSesion os, string conn) {
            int idUsuario = -1;

            try {
                idUsuario = (vCnx.T_Sesion.Where(w => w.sesion == os.idSesion && w.sesionActiva == 1).Select(s => s.id_usuario).FirstOrDefault());
                return new ADO(conn).GetDataTableQuery("EXEC [DrCitas].[Sp_ActualizarSesion] '" + os.idSesion + "' , " + idUsuario + "");
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool DesconectarSesion(dtoObjSesion os) {
            int idUsuario = -1;
            T_Sesion ts = null;

            try {
                idUsuario = (vCnx.T_Sesion.Where(w => w.sesion == os.idSesion && w.sesionActiva == 1).Select(s => s.id_usuario).FirstOrDefault());
                if (idUsuario > 0) {
                    ts = vCnx.T_Sesion.First(i => i.sesion == os.idSesion && i.id_usuario == idUsuario && i.sesionActiva == 1);
                    ts.sesionActiva = 0;
                    ts.sesion = "";
                    vCnx.SaveChanges();
                }
                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool ActualizarNombreUsuario(dtoCambioUsuario datos, int idCliente) {
            T_Usuario usr = null;

            try {
                usr = vCnx.T_ClienteUsuario.Where(tcu => tcu.id_cliente == idCliente).Select(e => e.T_Usuario).FirstOrDefault();

                if (usr == null)
                    return false;

                if (vCnx.T_Usuario.Where(tu => tu.usuario == datos.newUser).FirstOrDefault() != null)
                    return false;

                usr.usuario = datos.newUser;

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool ActualizarContrasenaUsuario(dtoCambioUsuario datos, int idCliente) {
            T_Usuario usr = null;

            try {
                if (!string.IsNullOrEmpty(datos.oldPassword))
                    usr = vCnx.T_ClienteUsuario.Where(tcu => tcu.T_Usuario.contrasena == datos.oldPassword && tcu.id_cliente == idCliente).Select(e => e.T_Usuario).FirstOrDefault();
                else
                    usr = vCnx.T_ClienteUsuario.Where(tcu => tcu.id_cliente == idCliente).Select(e => e.T_Usuario).FirstOrDefault();

                if (usr == null)
                    return false;

                usr.contrasena = datos.newPassword;

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool ActualizarDatosBasicosPaciente(dtoDatosPaciente dtoDP, int idCliente) {
            T_Usuario tbUsuario = null;
            Tb_Cliente tbCliente = null;

            try {

                tbCliente = vCnx.Tb_Cliente.Where(tbc => tbc.idCliente == idCliente).FirstOrDefault();

                if (tbCliente == null)
                    return false;

                tbCliente.Nombre          = (!string.IsNullOrEmpty(dtoDP.nombre)               ? dtoDP.nombre                              : tbCliente.Nombre);

                tbCliente.Apellido        = (!string.IsNullOrEmpty(dtoDP.apellido)             ? dtoDP.apellido                            : tbCliente.Apellido);

                tbCliente.FechaNacimiento = (!string.IsNullOrEmpty(dtoDP.fechaNacimiento)      ? Convert.ToDateTime(dtoDP.fechaNacimiento) : tbCliente.FechaNacimiento);

                tbCliente.Documento       = (!string.IsNullOrEmpty(dtoDP.documento.ToString()) ? Convert.ToInt32(dtoDP.documento)          : tbCliente.Documento);

                tbCliente.TipoDocumento   = (!string.IsNullOrEmpty(dtoDP.tipoDocumento)        ? dtoDP.tipoDocumento                       : tbCliente.TipoDocumento);

                tbCliente.Sexo            = (!string.IsNullOrEmpty(dtoDP.sexo)                 ? dtoDP.sexo.Substring(0, 1)                : tbCliente.Sexo);

                tbCliente.Correo          = (!string.IsNullOrEmpty(dtoDP.correo)               ? dtoDP.correo                              : tbCliente.Correo);

                tbCliente.Celular         = (!string.IsNullOrEmpty(dtoDP.celular)              ? dtoDP.celular                             : tbCliente.Celular);

                tbCliente.TelefonoCasa    = (!string.IsNullOrEmpty(dtoDP.telefonoCasa)         ? dtoDP.telefonoCasa                        : tbCliente.TelefonoCasa);

                tbCliente.TelefonoOficina = (!string.IsNullOrEmpty(dtoDP.telefonoOficina)      ? dtoDP.telefonoOficina                     : tbCliente.TelefonoOficina);

                tbCliente.idCiudad        = (dtoDP.idCiudad > 0                                ? dtoDP.idCiudad                            : tbCliente.idCiudad);

                tbCliente.Facebook        = (!string.IsNullOrEmpty(dtoDP.facebook)             ? dtoDP.facebook                            : tbCliente.TelefonoOficina);

                tbCliente.Twitter         = (!string.IsNullOrEmpty(dtoDP.twitter)              ? dtoDP.twitter                             : tbCliente.Twitter);

                tbCliente.RecibirEmails   = (!string.IsNullOrEmpty(dtoDP.recibirCorreo)        ? dtoDP.recibirCorreo                       : tbCliente.RecibirEmails);

                if (!string.IsNullOrEmpty(dtoDP.correo)) {
                    tbUsuario = vCnx.T_ClienteUsuario.Where(tcu => tcu.id_cliente == idCliente).Select(e => e.T_Usuario).FirstOrDefault();

                    if (tbUsuario != null)
                        tbUsuario.usuario_m = FuncionesGlobales.Enc((FuncionesGlobales.SHA1HashEncode(dtoDP.correo, FuncionesGlobales.EncodeStyle.Base64)));
                }

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool ActualizarDatosBasicosMedico(dtoDatosMedico dtoDM, int idCliente) {
            T_Usuario tbUsuario = null;
            Tb_Cliente tbCliente = null;
            Tb_ClienteAtributoLista tbClienteAtributoLista = null;
            Sp_AtributoLista_N_Result result = null;
            Sp_ClienteAtributoLista_N_Result result_ = null;

            int idUser = -1;

            try {

                tbCliente = vCnx.Tb_Cliente.Where(tbc => tbc.idCliente == idCliente).FirstOrDefault();

                if (tbCliente == null)
                    return false;

                tbCliente.Nombre          = (!string.IsNullOrEmpty(dtoDM.nombre)          ? dtoDM.nombre                              : tbCliente.Nombre);

                tbCliente.Apellido        = (!string.IsNullOrEmpty(dtoDM.apellido)        ? dtoDM.apellido                            : tbCliente.Apellido);

                tbCliente.FechaNacimiento = (!string.IsNullOrEmpty(dtoDM.fechaNacimiento) ? Convert.ToDateTime(dtoDM.fechaNacimiento) : tbCliente.FechaNacimiento);

                tbCliente.Documento       = (dtoDM.documento > 0                          ? dtoDM.documento                           : tbCliente.Documento);

                tbCliente.TipoDocumento   = (!string.IsNullOrEmpty(dtoDM.tipoDocumento)   ? dtoDM.tipoDocumento                       : tbCliente.TipoDocumento);

                tbCliente.RIF             = (!string.IsNullOrEmpty(dtoDM.RIF)             ? dtoDM.RIF                                 : tbCliente.RIF);

                tbCliente.Sexo            = (!string.IsNullOrEmpty(dtoDM.sexo)            ? dtoDM.sexo.Substring(0, 1)                : tbCliente.Sexo);

                tbCliente.Correo          = (!string.IsNullOrEmpty(dtoDM.correo)          ? dtoDM.correo                              : tbCliente.Correo);

                tbCliente.Celular         = (!string.IsNullOrEmpty(dtoDM.celular)         ? dtoDM.celular                             : tbCliente.Celular);

                tbCliente.TelefonoCasa    = (!string.IsNullOrEmpty(dtoDM.telefonoCasa)    ? dtoDM.telefonoCasa                        : tbCliente.TelefonoCasa);

                tbCliente.TelefonoOficina = (!string.IsNullOrEmpty(dtoDM.telefonoOficina) ? dtoDM.telefonoOficina                     : tbCliente.TelefonoOficina);

                tbCliente.idCiudad        = (dtoDM.idCiudad > 0                           ? dtoDM.idCiudad                            : tbCliente.idCiudad);

                tbCliente.Facebook        = (!string.IsNullOrEmpty(dtoDM.facebook)        ? dtoDM.facebook                            : tbCliente.TelefonoOficina);

                tbCliente.Twitter         = (!string.IsNullOrEmpty(dtoDM.twitter)         ? dtoDM.twitter                             : tbCliente.Twitter);

                tbCliente.RecibirEmails   = (!string.IsNullOrEmpty(dtoDM.recibirCorreo)   ? dtoDM.recibirCorreo                       : tbCliente.RecibirEmails);

                if (!string.IsNullOrEmpty(dtoDM.correo)) {
                    tbUsuario = vCnx.T_ClienteUsuario.Where(tcu => tcu.id_cliente == idCliente).Select(e => e.T_Usuario).FirstOrDefault();

                    if (tbUsuario != null)
                        tbUsuario.usuario_m = FuncionesGlobales.Enc((FuncionesGlobales.SHA1HashEncode(dtoDM.correo, FuncionesGlobales.EncodeStyle.Base64)));
                }

                if (!string.IsNullOrEmpty(dtoDM.numeroColegio)) {
                    tbClienteAtributoLista = vCnx.Tb_ClienteAtributoLista.Where(tbcal => tbcal.IdCliente == idCliente && tbcal.Tb_AtributoLista.idAtributo == 4).FirstOrDefault();

                    if (tbClienteAtributoLista != null) {
                        if (tbClienteAtributoLista.Descripcion != dtoDM.numeroColegio) {
                            tbClienteAtributoLista.Descripcion = dtoDM.numeroColegio;
                            tbClienteAtributoLista.Tb_AtributoLista.Descripcion = dtoDM.numeroColegio;
                        }
                    }
                    else {
                        idUser = vCnx.T_ClienteUsuario.Where(cu => cu.id_cliente == idCliente).Select(e => e.id_usuario).FirstOrDefault();

                        result = vCnx.Sp_AtributoLista_N(4, 1, dtoDM.numeroColegio, 1, idUser).FirstOrDefault();
                        result_ = vCnx.Sp_ClienteAtributoLista_N(idCliente, result.ID, 1, dtoDM.numeroColegio, null, DateTime.Now.Year.ToString(), "1", idUser).FirstOrDefault();
                    }
                }

                if (!string.IsNullOrEmpty(dtoDM.matriculaSAS)) {
                    tbClienteAtributoLista = vCnx.Tb_ClienteAtributoLista.Where(tbcal => tbcal.IdCliente == idCliente && tbcal.Tb_AtributoLista.idAtributo == 3).FirstOrDefault();

                    if (tbClienteAtributoLista != null) {
                        if (tbClienteAtributoLista.Descripcion != dtoDM.matriculaSAS) {
                            tbClienteAtributoLista.Descripcion = dtoDM.matriculaSAS;
                            tbClienteAtributoLista.Tb_AtributoLista.Descripcion = dtoDM.matriculaSAS;
                        }
                    }
                    else {
                        idUser = vCnx.T_ClienteUsuario.Where(cu => cu.id_cliente == idCliente).Select(e => e.id_usuario).FirstOrDefault();

                        result = vCnx.Sp_AtributoLista_N(3, 1, dtoDM.matriculaSAS, 1, idUser).FirstOrDefault();
                        result_ = vCnx.Sp_ClienteAtributoLista_N(idCliente, result.ID, 1, dtoDM.matriculaSAS, null, DateTime.Now.Year.ToString(), "1", idUser).FirstOrDefault();
                    }
                }

                if (tbCliente.Comentario != dtoDM.acercaDeMi)
                    tbCliente.Comentario = dtoDM.acercaDeMi;

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool ActualizarOtrosDatosMedico(dtoDatosMedico dtoDM, int idCliente) {
            Tb_ClienteEspecialidad tbClienteEspecialidad = null;
            List<Tb_ClienteAtributoLista> tbClienteAtributoLista = null;

            Sp_AtributoLista_N_Result result = null;
            Sp_ClienteAtributoLista_N_Result result_ = null;

            int idUser = -1;
            bool flag = false;

            try {
                idUser = vCnx.T_ClienteUsuario.Where(cu => cu.id_cliente == idCliente).Select(e => e.id_usuario).FirstOrDefault();

                if (idUser <= 0)
                    return false;

                if (dtoDM.estudios != null) {

                    foreach (var e in dtoDM.estudios) {
                        tbClienteEspecialidad = vCnx.Tb_ClienteEspecialidad.Where(tbce => tbce.idCliente == idCliente && tbce.idEspecialidad == e.idEspecialidad).FirstOrDefault();

                        if (tbClienteEspecialidad != null) {
                            if (tbClienteEspecialidad.idUniversidad != e.idUniversidad)
                                tbClienteEspecialidad.idUniversidad = e.idUniversidad;

                            if (!string.IsNullOrEmpty(e.fechaGrado) && tbClienteEspecialidad.FechaGrado != DateTime.Parse(e.fechaGrado))
                                tbClienteEspecialidad.FechaGrado = DateTime.Parse(e.fechaGrado);

                            if (e.idNEspecialidad > 0 && tbClienteEspecialidad.idEspecialidad != e.idNEspecialidad)
                                tbClienteEspecialidad.idEspecialidad = e.idNEspecialidad;
                        }
                        else
                            vCnx.Sp_ClienteEspecialidad_N(idCliente, e.idEspecialidad, e.idUniversidad, "1", (!string.IsNullOrEmpty(e.fechaGrado) ? DateTime.Parse(e.fechaGrado) : DateTime.Now), "1", idUser).FirstOrDefault();
                    }
                }

                if (dtoDM.cursos != null) {

                    tbClienteAtributoLista = vCnx.Tb_ClienteAtributoLista.Where(tbcal => tbcal.IdCliente == idCliente && tbcal.Tb_AtributoLista.idAtributo == 13).ToList();

                    foreach (var e in dtoDM.cursos) {
                        flag = false;

                        foreach (var t in tbClienteAtributoLista) {

                            if (t.IdAtributoLista == e.idAtributoLista) {
                                flag = true;
                                if (t.Tb_AtributoLista.Descripcion != e.Curso) {
                                    t.Descripcion = e.Curso;
                                    t.Tb_AtributoLista.Descripcion = e.Curso;
                                }
                                if (t.MesAsignacion != e.Mes)
                                    t.MesAsignacion = e.Mes;
                                if (t.AnioAsignacion != e.Anio)
                                    t.AnioAsignacion = e.Anio;
                            }
                            else if (t.Tb_AtributoLista.Descripcion == e.Curso)
                                flag = true;

                            if (flag)
                                break;
                        }

                        if (!flag) {
                            result = vCnx.Sp_AtributoLista_N(13, tbClienteAtributoLista.Count + 1, e.Curso, 1, idUser).FirstOrDefault();
                            result_ = vCnx.Sp_ClienteAtributoLista_N(idCliente, result.ID, tbClienteAtributoLista.Count + 1, e.Curso, e.Mes, e.Anio, "1", idUser).FirstOrDefault();
                        }
                    }
                }

                if (dtoDM.actividades != null) {

                    tbClienteAtributoLista = vCnx.Tb_ClienteAtributoLista.Where(tbcal => tbcal.IdCliente == idCliente && tbcal.Tb_AtributoLista.idAtributo == 11).ToList();

                    foreach (var e in dtoDM.actividades) {
                        flag = false;

                        foreach (var t in tbClienteAtributoLista) {

                            if (t.IdAtributoLista == e.idAtributoLista) {
                                flag = true;
                                if (t.Tb_AtributoLista.Descripcion != e.Actividad) {
                                    t.Descripcion = e.Actividad;
                                    t.Tb_AtributoLista.Descripcion = e.Actividad;
                                }
                                if (t.MesAsignacion != e.Mes)
                                    t.MesAsignacion = e.Mes;
                                if (t.AnioAsignacion != e.Anio)
                                    t.AnioAsignacion = e.Anio;
                            }
                            else if (t.Tb_AtributoLista.Descripcion == e.Actividad)
                                flag = true;

                            if (flag)
                                break;
                        }

                        if (!flag) {
                            result = vCnx.Sp_AtributoLista_N(11, tbClienteAtributoLista.Count + 1, e.Actividad, 1, idUser).FirstOrDefault();
                            result_ = vCnx.Sp_ClienteAtributoLista_N(idCliente, result.ID, tbClienteAtributoLista.Count + 1, e.Actividad, null, DateTime.Now.Year.ToString(), "1", idUser).FirstOrDefault();
                        }
                    }
                }

                if (dtoDM.libros != null) {

                    tbClienteAtributoLista = vCnx.Tb_ClienteAtributoLista.Where(tbcal => tbcal.IdCliente == idCliente && tbcal.Tb_AtributoLista.idAtributo == 7).ToList();

                    foreach (var e in dtoDM.libros) {
                        flag = false;

                        foreach (var t in tbClienteAtributoLista) {

                            if (t.IdAtributoLista == e.idAtributoLista) {
                                flag = true;
                                if (t.Tb_AtributoLista.Descripcion != e.Libro) {
                                    t.Descripcion = e.Libro;
                                    t.Tb_AtributoLista.Descripcion = e.Libro;
                                }
                                if (t.MesAsignacion != e.Mes)
                                    t.MesAsignacion = e.Mes;
                                if (t.AnioAsignacion != e.Anio)
                                    t.AnioAsignacion = e.Anio;
                            }
                            else if (t.Tb_AtributoLista.Descripcion == e.Libro)
                                flag = true;

                            if (flag)
                                break;
                        }

                        if (!flag) {
                            result = vCnx.Sp_AtributoLista_N(7, tbClienteAtributoLista.Count + 1, e.Libro, 1, idUser).FirstOrDefault();
                            result_ = vCnx.Sp_ClienteAtributoLista_N(idCliente, result.ID, tbClienteAtributoLista.Count + 1, e.Libro, null, DateTime.Now.Year.ToString(), "1", idUser).FirstOrDefault();
                        }
                    }
                }

                if (dtoDM.publicaciones != null) {

                    tbClienteAtributoLista = vCnx.Tb_ClienteAtributoLista.Where(tbcal => tbcal.IdCliente == idCliente && tbcal.Tb_AtributoLista.idAtributo == 8).ToList();

                    foreach (var e in dtoDM.publicaciones) {
                        flag = false;

                        foreach (var t in tbClienteAtributoLista) {

                            if (t.IdAtributoLista == e.idAtributoLista) {
                                flag = true;
                                if (t.Tb_AtributoLista.Descripcion != e.Publicacion) {
                                    t.Descripcion = e.Publicacion;
                                    t.Tb_AtributoLista.Descripcion = e.Publicacion;
                                }
                                if (t.MesAsignacion != e.Mes)
                                    t.MesAsignacion = e.Mes;
                                if (t.AnioAsignacion != e.Anio)
                                    t.AnioAsignacion = e.Anio;
                            }
                            else if (t.Tb_AtributoLista.Descripcion == e.Publicacion)
                                flag = true;

                            if (flag)
                                break;
                        }

                        if (!flag) {
                            result = vCnx.Sp_AtributoLista_N(8, tbClienteAtributoLista.Count + 1, e.Publicacion, 1, idUser).FirstOrDefault();
                            result_ = vCnx.Sp_ClienteAtributoLista_N(idCliente, result.ID, tbClienteAtributoLista.Count + 1, e.Publicacion, null, DateTime.Now.Year.ToString(), "1", idUser).FirstOrDefault();
                        }
                    }
                }

                if (dtoDM.idiomas != null) {

                    tbClienteAtributoLista = vCnx.Tb_ClienteAtributoLista.Where(tbcal => tbcal.IdCliente == idCliente && tbcal.Tb_AtributoLista.idAtributo == 6).ToList();

                    foreach (var e in dtoDM.idiomas) {
                        flag = false;

                        foreach (var t in tbClienteAtributoLista) {

                            if (t.IdAtributoLista == e.idAtributoLista) {
                                flag = true;
                                if (t.Tb_AtributoLista.Descripcion != e.Idioma) {
                                    t.Descripcion = e.Idioma;
                                    t.Tb_AtributoLista.Descripcion = e.Idioma;
                                }
                            }
                            else if (t.Tb_AtributoLista.Descripcion == e.Idioma)
                                flag = true;

                            if (flag)
                                break;
                        }

                        if (!flag) {
                            result = vCnx.Sp_AtributoLista_N(6, tbClienteAtributoLista.Count + 1, e.Idioma, 1, idUser).FirstOrDefault();
                            result_ = vCnx.Sp_ClienteAtributoLista_N(idCliente, result.ID, tbClienteAtributoLista.Count + 1, e.Idioma, null, DateTime.Now.Year.ToString(), "1", idUser).FirstOrDefault();
                        }
                    }
                }

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoEmpresa> GetEmpresasAsistente(string miSesion) {
            int idCliente = -1;

            try {

                idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                return this.GetEmpresasAsistente(idCliente);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoEmpresa> GetEmpresasAsistente(int idCliente) {
            List<dtoEmpresa> misEmpresas = null;

            try {
                if (idCliente > 0) {
                    misEmpresas = vCnx.T_AsistenteEmpresa.Where(tae => tae.id_asistente == idCliente && tae.T_Empresa.Activo == true)
                        .Select(e => new dtoEmpresa {
                            idEmpresa = e.id_empresa,
                            nombre    = e.T_Empresa.nombre,
                            direccion = e.T_Empresa.direccion,
                            telefono  = e.T_Empresa.telefono
                        }).ToList();
                }

                return misEmpresas;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoClinica> GetClinicasAsistente(string miSesion) {
            int idCliente = -1;

            try {
                idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                return this.GetClinicasAsistente(idCliente);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoClinica> GetClinicasAsistente(int idCliente) {
            List<dtoClinica> misClinicas = null;

            try {
                if (idCliente > 0) {
                    misClinicas = vCnx.T_AsistenteClinica.Where(tac => tac.id_asistente == idCliente && tac.Tb_Clinica.Activo == "1")
                        .Select(e => new dtoClinica {
                            idClinica   = e.id_clinica,
                            idCiudad    = e.Tb_Clinica.idCiudad,
                            descripcion = e.Tb_Clinica.Descripcion
                        }).ToList();
                }

                return misClinicas;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoConsultorio> GetConsultoriosAsistente(string miSesion) {
            int idCliente = -1;

            try {
                idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                return this.GetConsultoriosAsistente(idCliente);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoConsultorio> GetConsultoriosAsistente(int idCliente) {
            List<dtoConsultorio> misCconsultorios = null;

            try {
                if (idCliente > 0) {
                    misCconsultorios =
                        (
                            from consultorio in vCnx.Tb_Consultorio
                            join acceso in vCnx.T_AccesoConsultorio on consultorio.idConsultorio equals acceso.id_consultorio
                            where acceso.id_clientePermisado == idCliente && consultorio.Activo == "1"
                            select new dtoConsultorio {
                                IdConsultorio = consultorio.idConsultorio,
                                IdCiudad      = consultorio.idCiudad,
                                Nombre        = consultorio.Nombre,
                                Direccion     = consultorio.Direccion,
                                Telefono      = consultorio.Telefono
                            }
                        ).ToList();
                }

                return misCconsultorios;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoDatosMisMedicos> GetMedicosAsistente(string miSesion, int idClinica, int idConsultorio) {
            int idCliente = -1;

            try {

                idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                return this.GetMedicosAsistente(idCliente, idClinica, idConsultorio);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoDatosMisMedicos> GetMedicosAsistente(int idCliente) {
            List<dtoDatosMisMedicos> misMedicos = null;
            int id = -1;

            try {
                if (idCliente > 0) {
                    var idMedicos = (
                            from asistenteMedico in vCnx.T_AsistenteMedico
                            where asistenteMedico.id_asistente == idCliente
                            select new {
                                idMedicos = asistenteMedico.id_medico
                            }
                         ).ToList();

                    if (idMedicos.Count == 0)
                        throw new Exception("No posee medicos asociados");

                    misMedicos =
                        (
                            from medicos in idMedicos
                            join clientes in vCnx.Tb_Cliente on medicos.idMedicos equals clientes.idCliente
                            select new {
                                Nombre     = clientes.Nombre + " " + clientes.Apellido,
                                idDr       = clientes.idCliente,
                                celular    = clientes.Celular,
                                tlfCasa    = clientes.TelefonoCasa,
                                tlfOficina = clientes.TelefonoOficina,
                                Correo     = clientes.Correo
                            }
                        ).Distinct().AsEnumerable().Select(e => new dtoDatosMisMedicos {
                            Nombre     = e.Nombre,
                            idDr       = FuncionesGlobales.Encry64Encry(e.idDr.ToString()),
                            celular    = e.celular,
                            tlfCasa    = e.tlfCasa,
                            tlfOficina = e.tlfOficina,
                            Correo     = e.Correo,
                            Imagen     = this.getImagenPerfil(id),
                            Especialidades = (vCnx.Tb_ClienteEspecialidad.Where(ce => ce.idCliente == id)
                                .Select(s => new dtoEspecialidad {
                                    Especialidad   = s.Tb_Especialidad.Descripcion,
                                    idEspecialidad = s.Tb_Especialidad.idEspecialidad
                                }).ToList())
                        }).ToList();
                }

                return misMedicos;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoDatosMisMedicos> GetMedicosAsistente(int idCliente, int idClinica, int idConsultorio) {
            List<dtoDatosMisMedicos> misMedicos = null;
            int id = -1;

            try {
                if (idCliente > 0) {
                    misMedicos =
                        (
                            from asistenteMedico in vCnx.T_AsistenteMedico
                            join consultorio in vCnx.Tb_Consultorio on asistenteMedico.id_medico equals consultorio.idCliente
                            where asistenteMedico.id_asistente == idCliente && (consultorio.id_clinica == idClinica || idClinica == 0) && (consultorio.idConsultorio == idConsultorio || idConsultorio == 0)
                            select new {
                                Nombre     = asistenteMedico.Tb_Cliente1.Nombre + " " + asistenteMedico.Tb_Cliente1.Apellido,
                                idDr       = asistenteMedico.id_medico,
                                celular    = asistenteMedico.Tb_Cliente1.Celular,
                                tlfCasa    = asistenteMedico.Tb_Cliente1.TelefonoCasa,
                                tlfOficina = asistenteMedico.Tb_Cliente1.TelefonoOficina,
                                Correo     = asistenteMedico.Tb_Cliente1.Correo
                            }
                        ).Distinct().AsEnumerable().Select(e => new dtoDatosMisMedicos {
                            Nombre     = e.Nombre,
                            idDr       = FuncionesGlobales.Encry64Encry(e.idDr.ToString()),
                            celular    = e.celular,
                            tlfCasa    = e.tlfCasa,
                            tlfOficina = e.tlfOficina,
                            Correo     = e.Correo,
                            Imagen     = ""
                        }).ToList();

                    if (misMedicos.Count > 0) {

                        foreach (var m in misMedicos) {
                            id = Convert.ToInt32(FuncionesGlobales.Decry64Decry(m.idDr));
                            m.Imagen = this.getImagenPerfil(id);
                            m.Especialidades = vCnx.Tb_ClienteEspecialidad.Where(ce => ce.idCliente == id)
                                .Select(e => new dtoEspecialidad {
                                    Especialidad   = e.Tb_Especialidad.Descripcion,
                                    idEspecialidad = e.Tb_Especialidad.idEspecialidad
                                }).ToList();
                        }
                    }
                }

                return misMedicos;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public dtoMisMedicos GetMisMedicos(string miSesion) {
            int idCliente = -1;

            try {

                idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                return this.GetMisMedicos(idCliente);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public dtoMisMedicos GetMisMedicos(int idCliente) {
            dtoMisMedicos misMedicos = new dtoMisMedicos();
            int id = -1;

            try {
                if (idCliente > 0) {
                    misMedicos.MedicosTitular = vCnx.T_MedicoPaciente.Where(tmp => tmp.id_paciente == idCliente)
                        .Select(e => new {
                            Nombre     = e.Tb_Cliente.Nombre + " " + e.Tb_Cliente.Apellido,
                            idDr       = e.Tb_Cliente.idCliente,
                            celular    = e.Tb_Cliente.Celular,
                            tlfCasa    = e.Tb_Cliente.TelefonoCasa,
                            tlfOficina = e.Tb_Cliente.TelefonoOficina,
                            Correo     = e.Tb_Cliente.Correo,
                            AcercaDrMi = e.Tb_Cliente.Comentario
                        }).AsEnumerable().Select(e => new dtoDatosMisMedicos {
                            Nombre     = e.Nombre,
                            idDr       = FuncionesGlobales.Encry64Encry(e.idDr.ToString()),
                            celular    = e.celular,
                            tlfCasa    = e.tlfCasa,
                            tlfOficina = e.tlfOficina,
                            Correo     = e.Correo,
                            Imagen     = "",
                            AcercaDeMi = e.AcercaDrMi
                        }).ToList();

                    if (misMedicos.MedicosTitular.Count > 0) {

                        foreach (var m in misMedicos.MedicosTitular) {
                            id = Convert.ToInt32(FuncionesGlobales.Decry64Decry(m.idDr));
                            m.Imagen = this.getImagenPerfil(id);
                            m.Especialidades = vCnx.Tb_ClienteEspecialidad.Where(tce => tce.idCliente == id)
                                .Select(e => new dtoEspecialidad {
                                    Especialidad   = e.Tb_Especialidad.Descripcion,
                                    idEspecialidad = e.Tb_Especialidad.idEspecialidad
                                }).ToList();
                        }

                        misMedicos.MedicosAsociados = vCnx.T_ClienteAsociado.Where(tca => tca.idCliente == idCliente)
                            .Select(e => new {
                                Nombre     = e.Tb_Cliente.Nombre + " " + e.Tb_Cliente.Apellido,
                                Parentesco = e.parentesco,
                                idCliente  = e.idAsociado
                            }).AsEnumerable().Select(e => new dtoAsociadoMedicos {
                                Nombre     = e.Nombre,
                                Parentesco = e.Parentesco,
                                idCliente  = FuncionesGlobales.Encry64Encry(e.idCliente.ToString())
                            }).ToList();

                        if (misMedicos.MedicosAsociados.Count > 0) {

                            foreach (var a in misMedicos.MedicosAsociados) {
                                id = Convert.ToInt32(FuncionesGlobales.Decry64Decry(a.idCliente));
                                a.Medicos = vCnx.T_MedicoPaciente.Where(tmp => tmp.id_paciente == id)
                                    .Select(e => new {
                                        Nombre     = e.Tb_Cliente.Nombre + " " + e.Tb_Cliente.Apellido,
                                        idDr       = e.Tb_Cliente.idCliente,
                                        celular    = e.Tb_Cliente.Celular,
                                        tlfCasa    = e.Tb_Cliente.TelefonoCasa,
                                        tlfOficina = e.Tb_Cliente.TelefonoOficina,
                                        Correo     = e.Tb_Cliente.Correo,
                                        AcercaDrMi = e.Tb_Cliente.Comentario
                                    }).AsEnumerable().Select(e => new dtoDatosMisMedicos {
                                        Nombre     = e.Nombre,
                                        idDr       = FuncionesGlobales.Encry64Encry(e.idDr.ToString()),
                                        celular    = e.celular,
                                        tlfCasa    = e.tlfCasa,
                                        tlfOficina = e.tlfOficina,
                                        Correo     = e.Correo,
                                        Imagen     = "",
                                        AcercaDeMi = e.AcercaDrMi
                                    }).ToList();
                            }

                            foreach (var a in misMedicos.MedicosAsociados) {
                                foreach (var m in a.Medicos) {
                                    id = Convert.ToInt32(FuncionesGlobales.Decry64Decry(m.idDr));
                                    m.Imagen = this.getImagenPerfil(id);
                                    m.Especialidades = vCnx.Tb_ClienteEspecialidad.Where(ce => ce.idCliente == id)
                                        .Select(e => new dtoEspecialidad {
                                            Especialidad   = e.Tb_Especialidad.Descripcion,
                                            idEspecialidad = e.Tb_Especialidad.idEspecialidad
                                        }).ToList();
                                }
                            }
                        }
                    }
                }

                return misMedicos;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public dtoMisPacientes GetMisPacientes(string miSesion) {
            int idCliente = -1;

            try {

                idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                return this.GetMisPacientes(idCliente);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public dtoMisPacientes GetMisPacientes(int idCliente) {
            dtoMisPacientes misPacientes = new dtoMisPacientes();
            int id = -1;

            try {
                if (idCliente > 0) {
                    misPacientes.Pacientes =
                        vCnx.T_MedicoPaciente.Where(tmp => tmp.id_medico == idCliente)
                        .Select(e => new {
                            Nombre     = e.Tb_Cliente1.Nombre + " " + e.Tb_Cliente1.Apellido,
                            idPaciente = e.Tb_Cliente1.idCliente,
                            celular    = e.Tb_Cliente1.Celular,
                            tlfCasa    = e.Tb_Cliente1.TelefonoCasa,
                            Correo     = e.Tb_Cliente1.Correo
                        }).AsEnumerable().Select(e => new dtoDatosMisPacientes {
                            Nombre     = e.Nombre,
                            idPaciente = FuncionesGlobales.Encry64Encry(e.idPaciente.ToString()),
                            celular    = e.celular,
                            tlfCasa    = e.tlfCasa,
                            Correo     = e.Correo,
                            Imagen     = ""
                        }).ToList();
                }

                foreach (var p in misPacientes.Pacientes) {
                    id = Convert.ToInt32(FuncionesGlobales.Decry64Decry(p.idPaciente));
                    p.Imagen = this.getImagenPerfil(p.idPaciente);
                }

                return misPacientes;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool EliminarEspecialidad(string miSesion, int idEspecialidad) {
            int idCliente = -1;

            try {
                idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                return this.EliminarEspecialidad(idCliente, idEspecialidad);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool EliminarEspecialidad(int idCliente, int idEspecialidad) {
            int idUser = -1, idEsp = -1;

            try {

                idUser = vCnx.T_ClienteUsuario.Where(tcu => tcu.id_cliente == idCliente).Select(e => e.id_usuario).FirstOrDefault();
                idEsp = vCnx.Tb_ClienteEspecialidad.Where(tce => tce.idCliente == idCliente && tce.idEspecialidad == idEspecialidad).Select(e => e.idClienteEspecialidad).FirstOrDefault();

                if (idUser <= 0 || idEsp <= 0)
                    return false;

                vCnx.Sp_ClienteEspecialidad_E(idEsp, idUser).FirstOrDefault();

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool EliminarAtributoLista(string miSesion, int idAtributoLista) {
            int idCliente = -1;

            try {
                idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                return this.EliminarAtributoLista(idCliente, idAtributoLista);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool EliminarAtributoLista(int idCliente, int idAtributoLista) {
            int idUser = -1, idCAL = -1, idAL = -1;

            try {
                idUser = vCnx.T_ClienteUsuario.Where(tcu => tcu.id_cliente == idCliente).Select(e => e.id_usuario).FirstOrDefault();
                idCAL = vCnx.Tb_ClienteAtributoLista.Where(tcal => tcal.IdCliente == idCliente && tcal.IdAtributoLista == idAtributoLista).Select(e => e.idClienteAtributoLista).FirstOrDefault();
                idAL = vCnx.Tb_AtributoLista.Where(tal => tal.idAtributoLista == idAtributoLista).Select(e => e.idAtributoLista).FirstOrDefault();

                if (idUser <= 0 || idCAL <= 0 || idAL <= 0)
                    return false;

                vCnx.Sp_ClienteAtributoLista_E(idCAL, idUser).FirstOrDefault();
                vCnx.Sp_AtributoLista_E(idAL, idUser).FirstOrDefault();

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private bool verificarGuardarImagenPerfil(Tupla<int, int> cliente, string img, string ext) {
            Tb_ClienteImagen clienteImg = null;
            string file = "";

            try {
                if (cliente == null)
                    return false;

                if (cliente.item1 <= 0 || (cliente.item2 != 3 && cliente.item2 != 5))
                    return false;

                if (System.Text.RegularExpressions.Regex.IsMatch((file = ServicioUtilidad.manageImageProfile(cliente.item1, cliente.item2, 0, img, ext)), "Paciente|Medico|Asistente"))
                    clienteImg = vCnx.Tb_ClienteImagen.Where(tci => tci.idCliente == cliente.item1).FirstOrDefault();

                if (clienteImg == null)
                    vCnx.Tb_ClienteImagen.AddObject(new Tb_ClienteImagen {
                        idClienteImagen = vCnx.Tb_ClienteImagen.OrderByDescending(o => o.idClienteImagen).Select(s => s.idClienteImagen + 1).FirstOrDefault(),
                        idCliente       = cliente.item1,
                        Descripcion     = "Foto Cliente",
                        TipoImagen      = "F",
                        Imagen          = file,
                        Activo          = "1"
                    });
                else
                    clienteImg.Imagen = file;

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool guardarImagenPerfil(string miSesion, string img, string ext) {
            Tupla<int, int> cliente = null;

            try {
                cliente =
                    (
                        from usuario in vCnx.T_ClienteUsuario
                        from usuarioPerfil in vCnx.T_UsuarioPerfil
                        from sesion in vCnx.T_Sesion
                        where usuario.id_usuario == usuarioPerfil.idUsuario && usuario.id_usuario == sesion.id_usuario && sesion.sesion == miSesion && sesion.sesionActiva == 1
                        select new Tupla<int, int> { item1 = usuario.id_cliente, item2 = usuarioPerfil.idUsuarioPerfil }
                    ).FirstOrDefault();

                return this.verificarGuardarImagenPerfil(cliente, img, ext);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool guardarImagenPerfil(int idCliente, string img, string ext) {
            Tupla<int, int> cliente = null;

            try {
                cliente =
                    (
                        from usuario in vCnx.T_ClienteUsuario
                        from usuarioPerfil in vCnx.T_UsuarioPerfil
                        where usuario.id_usuario == usuarioPerfil.idUsuario && usuario.id_cliente == idCliente
                        select new Tupla<int, int> { item1 = usuario.id_cliente, item2 = usuarioPerfil.idUsuarioPerfil }
                    ).FirstOrDefault();

                return this.verificarGuardarImagenPerfil(cliente, img, ext);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private string verificarGetImagenPerfil(Tupla<int, int> cliente) {
            try {
                if (cliente == null)
                    return "";

                if (cliente.item1 <= 0 || (cliente.item2 != 3 && cliente.item2 != 5))
                    return "";

                return ServicioUtilidad.manageImageProfile(cliente.item1, cliente.item2, 1);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public string getImagenPerfil(string miSesion) {
            Tupla<int, int> cliente = null;

            try {
                cliente =
                    (
                        from usuario in vCnx.T_ClienteUsuario
                        from usuarioPerfil in vCnx.T_UsuarioPerfil
                        from sesion in vCnx.T_Sesion
                        where usuario.id_usuario == usuarioPerfil.idUsuario && usuario.id_usuario == sesion.id_usuario && sesion.sesion == miSesion && sesion.sesionActiva == 1
                        select new Tupla<int, int> { item1 = usuario.id_cliente, item2 = usuarioPerfil.idUsuarioPerfil }
                    ).FirstOrDefault();

                return this.verificarGetImagenPerfil(cliente);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public string getImagenPerfil(int idCliente) {
            Tupla<int, int> cliente = null;

            try {
                cliente =
                    (
                        from usuario in vCnx.T_ClienteUsuario
                        from usuarioPerfil in vCnx.T_UsuarioPerfil
                        where usuario.id_usuario == usuarioPerfil.idUsuario && usuario.id_cliente == idCliente
                        select new Tupla<int, int> { item1 = usuario.id_cliente, item2 = usuarioPerfil.idUsuarioPerfil }
                    ).FirstOrDefault();

                return this.verificarGetImagenPerfil(cliente);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoEmpresa> getEmpresas(string miSesion) {
            int idCliente = -1;

            try {
                idCliente = idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                return this.getEmpresas(idCliente);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoEmpresa> getEmpresas(int idCliente) {
            List<dtoEmpresa> empresas = null;
            string tipo = "";

            try {
                tipo = vCnx.Tb_Cliente.Where(tbc => tbc.idCliente == idCliente).Select(e => e.TipoCliente).FirstOrDefault();

                if (tipo == "A") {
                    empresas = vCnx.T_Empresa.Select(e => new dtoEmpresa {
                        idEmpresa = e.id_empresa,
                        nombre    = e.nombre
                    }).ToList();
                }
                else if (tipo == "L") {
                    empresas = vCnx.T_AsistenteEmpresa.Where(tae => tae.id_asistente == idCliente && tae.T_Empresa.Activo == true)
                        .Select(empresa => new dtoEmpresa {
                            idEmpresa = empresa.id_empresa,
                            nombre    = empresa.T_Empresa.nombre
                        }).ToList();
                }
                else if (tipo == "S") {
                    empresas =
                        (
                            from empresa in vCnx.T_AsistenteEmpresa
                            join asistenteMedico in vCnx.T_AsistenteMedico on empresa.id_asistente equals asistenteMedico.id_asistente
                            where empresa.id_asistente == idCliente && empresa.T_Empresa.Activo == true
                            select new dtoEmpresa {
                                idEmpresa = empresa.id_empresa,
                                nombre    = empresa.T_Empresa.nombre
                            }
                        ).ToList();

                }
                else if (tipo == "M") {
                    empresas = vCnx.T_MedicoEmpresa.Where(tme => tme.id_medico == idCliente && tme.T_Empresa.Activo == true)
                        .Select(e => new dtoEmpresa {
                            idEmpresa = e.id_empresa,
                            nombre    = e.T_Empresa.nombre
                        }).ToList();
                }

                return empresas;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoClinica> getClinicas(string miSesion, int idEmpresa) {
            int idCliente = -1;

            try {
                idCliente = idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                return this.getClinicas(idCliente, idEmpresa);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoClinica> getClinicas(int idCliente, int idEmpresa) {
            List<dtoClinica> clinicas = null;
            string tipo = "";

            try {
                tipo = vCnx.Tb_Cliente.Where(tbc => tbc.idCliente == idCliente).Select(e => e.TipoCliente).FirstOrDefault();

                if (tipo == "A") {
                    clinicas = vCnx.Tb_Clinica.Select(e => new dtoClinica { idClinica = e.idClinica, descripcion = e.Descripcion }).ToList();
                }
                else if (tipo == "L") {
                    clinicas =
                        (
                            from clinica in vCnx.T_AsistenteClinica
                            join empresa in vCnx.T_AsistenteEmpresa on clinica.id_asistente equals empresa.id_asistente
                            where clinica.id_asistente == idCliente && empresa.id_empresa == idEmpresa && clinica.Tb_Clinica.Activo == "1"
                            select new dtoClinica {
                                idClinica   = clinica.id_clinica,
                                descripcion = clinica.Tb_Clinica.Descripcion
                            }
                        ).ToList();
                }
                else if (tipo == "S") {
                    clinicas = vCnx.T_AsistenteClinica.Where(tac => tac.id_asistente == idCliente && tac.Tb_Clinica.Activo == "1")
                        .Select(e => new dtoClinica {
                            idClinica   = e.id_clinica,
                            descripcion = e.Tb_Clinica.Descripcion
                        }).ToList();
                }
                else if (tipo == "M") {
                    clinicas =
                        (
                            from consultorio in vCnx.Tb_Consultorio
                            where consultorio.idCliente == idCliente && consultorio.Tb_Clinica.Activo == "1"
                            select new {
                                idClinica   = consultorio.id_clinica,
                                descripcion = consultorio.Tb_Clinica.Descripcion
                            }
                        ).Distinct().AsEnumerable().Select(e => new dtoClinica {
                            idClinica   = e.idClinica,
                            descripcion = e.descripcion
                        }).ToList();
                }

                return clinicas;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoEspecialidad> getEspecialidades(string miSesion, int idEmpresa, int idClinica) {
            int idCliente = -1;

            try {
                idCliente = idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                return this.getEspecialidades(idCliente, idEmpresa, idClinica);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoEspecialidad> getEspecialidades(int idCliente, int idEmpresa, int idClinica) {
            List<dtoEspecialidad> especialidades = null;
            string tipo = "";

            try {
                tipo = vCnx.Tb_Cliente.Where(tbc => tbc.idCliente == idCliente).Select(e => e.TipoCliente).FirstOrDefault();

                if (tipo == "A") {
                    especialidades = vCnx.Tb_Clinicas_Especialidad.Where(tce => tce.idClinica == idClinica)
                        .Select(e => new dtoEspecialidad {
                            idEspecialidad = e.idEspecialidad,
                            Especialidad   = e.Tb_Clinica.Descripcion
                        }).ToList();
                }
                else if (tipo == "L") {
                    especialidades =
                        (
                            from especialidad in vCnx.Tb_Clinicas_Especialidad
                            join clinica in vCnx.T_AsistenteClinica on especialidad.idClinica equals clinica.id_clinica
                            join empresa in vCnx.T_AsistenteEmpresa on clinica.id_asistente equals empresa.id_asistente
                            where clinica.id_asistente == idCliente && empresa.id_empresa == idEmpresa && clinica.id_clinica == idClinica && especialidad.Tb_Especialidad.Activo == "1"
                            select new dtoEspecialidad {
                                idEspecialidad = especialidad.idEspecialidad,
                                Especialidad   = especialidad.Tb_Especialidad.Descripcion
                            }
                        ).ToList();
                }
                else if (tipo == "S") {
                    especialidades =
                        (
                            from especialidad in vCnx.Tb_ClienteEspecialidad
                            join asistenteMedico in vCnx.T_AsistenteMedico on especialidad.idCliente equals asistenteMedico.id_medico
                            where asistenteMedico.id_asistente == idCliente && especialidad.Tb_Especialidad.Activo == "1"
                            select new dtoEspecialidad {
                                idEspecialidad = especialidad.idEspecialidad,
                                Especialidad   = especialidad.Tb_Especialidad.Descripcion
                            }
                        ).ToList();
                }
                else if (tipo == "M") {
                    especialidades =
                        (
                            from especialidad in vCnx.Tb_ClienteEspecialidad
                            where especialidad.idCliente == idCliente && especialidad.Tb_Especialidad.Activo == "1"
                            select new dtoEspecialidad {
                                idEspecialidad = especialidad.idEspecialidad,
                                Especialidad   = especialidad.Tb_Especialidad.Descripcion
                            }
                        ).ToList();
                }

                return especialidades;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoConsultorio> getConsultorios(string miSesion, int idEmpresa, int idClinica) {
            int idCliente = -1;

            try {
                idCliente = idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                return this.getConsultorios(idCliente, idEmpresa, idClinica);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoConsultorio> getConsultorios(int idCliente, int idEmpresa, int idClinica) {
            List<dtoConsultorio> consultorios = null;
            string tipo = "";

            try {
                tipo = vCnx.Tb_Cliente.Where(tbc => tbc.idCliente == idCliente).Select(e => e.TipoCliente).FirstOrDefault();

                if (tipo == "A") {
                    consultorios =
                        (
                            from consultorio in vCnx.Tb_Consultorio
                            where (consultorio.id_clinica == idClinica || idClinica == 0)
                            select new {
                                IdConsultorio = consultorio.idConsultorio,
                                Nombre        = "Nombre: " + consultorio.Nombre + " - Direccion: " + consultorio.Direccion
                            }
                        ).Distinct().AsEnumerable().Select(e => new dtoConsultorio {
                            IdConsultorio = e.IdConsultorio,
                            Nombre = e.Nombre
                        }).ToList();
                }
                else if (tipo == "L") {
                    consultorios =
                        (
                            from consultorio in vCnx.Tb_Consultorio
                            join clinica in vCnx.T_AsistenteClinica on consultorio.id_clinica equals clinica.id_clinica
                            join empresa in vCnx.T_AsistenteEmpresa on clinica.id_asistente equals empresa.id_asistente
                            where clinica.id_asistente == idCliente && empresa.id_empresa == idEmpresa && clinica.id_clinica == idClinica && consultorio.Activo == "1"
                            select new {
                                IdConsultorio = consultorio.idConsultorio,
                                Nombre        = "Nombre: " + consultorio.Nombre + " - Direccion: " + consultorio.Direccion
                            }
                        ).Distinct().AsEnumerable().Select(e => new dtoConsultorio {
                            IdConsultorio = e.IdConsultorio,
                            Nombre        = e.Nombre
                        }).ToList();
                }
                else if (tipo == "S") {
                    consultorios =
                        (
                            from consultorio in vCnx.Tb_Consultorio
                            join asistenteMedico in vCnx.T_AsistenteMedico on consultorio.idCliente equals asistenteMedico.id_medico
                            where asistenteMedico.id_asistente == idCliente && (consultorio.id_clinica == idClinica || idClinica == 0) && consultorio.Activo == "1"
                            select new {
                                IdConsultorio = consultorio.idConsultorio,
                                Nombre        = "Nombre: " + consultorio.Nombre + " - Direccion: " + consultorio.Direccion
                            }
                        ).Distinct().AsEnumerable().Select(e => new dtoConsultorio {
                            IdConsultorio = e.IdConsultorio,
                            Nombre        = e.Nombre
                        }).ToList();
                }
                else if (tipo == "M") {
                    consultorios =
                        (
                            from consultorio in vCnx.Tb_Consultorio
                            where consultorio.idCliente == idCliente && (consultorio.id_clinica == idClinica || idClinica == 0) && consultorio.Activo == "1"
                            select new {
                                IdConsultorio = consultorio.idConsultorio,
                                Nombre        = "Nombre: " + consultorio.Nombre + " - Direccion: " + consultorio.Direccion
                            }
                        ).Distinct().AsEnumerable().Select(e => new dtoConsultorio {
                            IdConsultorio = e.IdConsultorio,
                            Nombre        = e.Nombre
                        }).ToList();
                }

                return consultorios;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoClienteResume> getMedicos(string miSesion, int idEmpresa, int idClinica, int idEspecialidad, int idConsultorio) {
            int idCliente = -1;

            try {
                idCliente = idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                return this.getMedicos(idCliente, idEmpresa, idClinica, idEspecialidad, idConsultorio);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoClienteResume> getMedicos(int idCliente, int idEmpresa, int idClinica, int idEspecialidad, int idConsultorio) {
            List<dtoClienteResume> medicos = null;
            string tipo = "";

            try {
                tipo = vCnx.Tb_Cliente.Where(tbc => tbc.idCliente == idCliente).Select(e => e.TipoCliente).FirstOrDefault();

                if (tipo == "A") {
                    medicos =
                        (
                            from cliente in vCnx.Tb_Cliente
                            join consultorio in vCnx.Tb_Consultorio on cliente.idCliente equals consultorio.idCliente
                            join especialidad in vCnx.Tb_ClienteEspecialidad on cliente.idCliente equals especialidad.idCliente
                            where (consultorio.id_clinica == idClinica || idClinica == 0) && (especialidad.idEspecialidad == idEspecialidad || idEspecialidad == 0) && consultorio.idConsultorio == idConsultorio
                            select new {
                                idCliente = cliente.idCliente,
                                nombre    = cliente.Nombre,
                                apellido  = cliente.Apellido
                            }
                        ).Distinct().AsEnumerable().Select(e => new dtoClienteResume {
                            idCliente = FuncionesGlobales.Encry64Encry(e.idCliente.ToString()),
                            nombre    = e.nombre,
                            apellido  = e.apellido
                        }).ToList();
                }
                else if (tipo == "L") {
                    medicos =
                        (
                            from cliente in vCnx.Tb_Cliente
                            join consultorio in vCnx.Tb_Consultorio on cliente.idCliente equals consultorio.idCliente
                            join clinica in vCnx.T_AsistenteClinica on consultorio.id_clinica equals clinica.id_clinica
                            join especialidad in vCnx.Tb_Clinicas_Especialidad on consultorio.id_clinica equals especialidad.idClinica
                            join empresa in vCnx.T_AsistenteEmpresa on clinica.id_asistente equals empresa.id_asistente
                            where clinica.id_asistente == idCliente && empresa.id_empresa == idEmpresa && clinica.id_clinica == idClinica && (especialidad.idEspecialidad == idEspecialidad || idEspecialidad == 0) && consultorio.Activo == "1"
                            select new {
                                idCliente = cliente.idCliente,
                                nombre    = cliente.Nombre,
                                apellido  = cliente.Apellido
                            }
                        ).Distinct().AsEnumerable().Select(e => new dtoClienteResume {
                            idCliente = FuncionesGlobales.Decry64Decry(e.idCliente.ToString()),
                            nombre    = e.nombre,
                            apellido  = e.apellido
                        }).ToList();
                }
                else if (tipo == "S") {
                    medicos =
                        (
                            from cliente in vCnx.Tb_Cliente
                            join consultorio in vCnx.Tb_Consultorio on cliente.idCliente equals consultorio.idCliente
                            join asistenteMedico in vCnx.T_AsistenteMedico on consultorio.idCliente equals asistenteMedico.id_medico
                            join especialidad in vCnx.Tb_ClienteEspecialidad on cliente.idCliente equals especialidad.idCliente
                            where asistenteMedico.id_asistente == idCliente && (consultorio.id_clinica == idClinica || idClinica == 0) && (especialidad.idEspecialidad == idEspecialidad || idEspecialidad == 0) && cliente.Activo == "1"
                            select new {
                                idCliente = cliente.idCliente,
                                nombre    = cliente.Nombre,
                                apellido  = cliente.Apellido
                            }
                        ).Distinct().AsEnumerable().Select(e => new dtoClienteResume {
                            idCliente = FuncionesGlobales.Decry64Decry(e.idCliente.ToString()),
                            nombre    = e.nombre,
                            apellido  = e.apellido
                        }).ToList();
                }
                else if (tipo == "M") {
                    medicos =
                        (
                            from cliente in vCnx.Tb_Cliente
                            join consultorio in vCnx.Tb_Consultorio on cliente.idCliente equals consultorio.idCliente
                            join especialidad in vCnx.Tb_ClienteEspecialidad on cliente.idCliente equals especialidad.idCliente
                            where cliente.idCliente == idCliente && (consultorio.id_clinica == idClinica || idClinica == 0) && (especialidad.idEspecialidad == idEspecialidad || idEspecialidad == 0) && cliente.Activo == "1"
                            select new {
                                idCliente = cliente.idCliente,
                                nombre    = cliente.Nombre,
                                apellido  = cliente.Apellido
                            }
                        ).Distinct().AsEnumerable().Select(e => new dtoClienteResume {
                            idCliente = FuncionesGlobales.Decry64Decry(e.idCliente.ToString()),
                            nombre    = e.nombre,
                            apellido  = e.apellido
                        }).ToList();
                }

                return medicos;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool RegistrarClientesTitularesCuidamed(string conn) {
            List<dtoRegistroCliente> resultado = new List<dtoRegistroCliente>();
            DataTable dt = null;
            ADO ado = new ADO(conn);
            string sql = "";

            try {
                sql =
                    "SELECT " +
                        "Cedula, " +
                        "Nombre, " +
                        "370, " +
                        "ISNULL(FechaNacimiento, CONVERT(DATETIME, '1960-01-01')), " +
                        "CASE " +
                            "WHEN Sexo IS NULL OR RTRIM(LTRIM(Sexo)) = '' THEN 'M' " +
                            "ELSE SUBSTRING(Sexo, 1, 1) " +
                        "END, " +
                        "Telefono, " +
                        "CASE " +
                            "WHEN Email IS NULL OR RTRIM(LTRIM(Email)) = '' OR LEN(EMAIL) < 5 THEN 'mail@mail.com' " +
                            "ELSE Email " +
                        "END, " +
                        "TitularId " +
                    "FROM " +
                        "[cuidamed].[dbo].[Beneficiario] " +
                    "WHERE " +
                        "TitularId IS NOT NULL AND " +
                        "RTRIM(LTRIM(Parentesco)) = 'TITULAR' AND ISNUMERIC(Cedula) = 1";

                dt = ado.GetDataTableQuery(sql);

                for (int i = 0; i < dt.Rows.Count; i++) {
                    resultado.Add(new dtoRegistroCliente {
                        documento          = Convert.ToInt32(dt.Rows[i].Field<string>(0)),
                        Nombre             = dt.Rows[i].Field<string>(1),
                        Apellido           = "*" + dt.Rows[i].Field<int>(7) + "*",
                        idCiudad           = dt.Rows[i].Field<int>(2),
                        FechaNacimiento    = dt.Rows[i].Field<DateTime>(3).ToString("yyyy-MM-dd"),
                        Sexo               = dt.Rows[i].Field<string>(4),
                        Correo             = dt.Rows[i].Field<string>(6),
                        Usuario            = FuncionesGlobales.Enc(FuncionesGlobales.SHA1HashEncode(dt.Rows[i].Field<string>(0).Trim(), FuncionesGlobales.EncodeStyle.Base64)),
                        Password           = FuncionesGlobales.Enc(FuncionesGlobales.SHA1HashEncode("123456", FuncionesGlobales.EncodeStyle.Base64)),
                        Celular            = "4121234567",
                        TipoCliente        = "5",
                        RecibirCorreo      = "1",
                        IdUsuarioPrincipal = 0
                    });
                }

                for (int i = 0; i < resultado.Count; i++) {
                    if (!this.ExistUser(resultado[i].Usuario)) {
                        if (!this.RegistrarCliente(resultado[i])) {
                            sql =
                                "INSERT INTO [CuidaNet.DrCitas].[DrCitas].[Tb_MigTitularesLOG] (idValor, valor) " +
                                "VALUES ('" + resultado[i].Apellido.Substring(1, resultado[i].Apellido.Length - 2) + "', '" + resultado[i].documento + "')";

                            string k = ado.execScalarQuery(sql);
                        }
                    }
                }

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool RegistrarCompaniaClientesAsociadosCuidamed(string conn) {
            dtoClienteResume cliente = null;
            dtoAsociado asociado = null;
            DataTable dt = null, dt_ = null;
            ADO ado = new ADO(conn);
            string sql = "";
            int idCliente = -1, idCompania = -1;

            try {
                sql =
                    "SELECT " +
                        "A.idCliente, " +
                        "A.idCiudad, " +
                        "A.TipoDocumento, " +
                        "A.Documento, " +
                        "A.Nombre, " +
                        "A.Apellido, " +
                        "A.Correo, " +
                        "A.Celular, " +
                        "A.Sexo, " +
                        "A.FechaNacimiento, " +
                        "C.RazonSocial, " +
                        "D.id_compania " +
                    "FROM " +
                        "[CuidaNet.DrCitas].[DrCitas].[Tb_Cliente] A " +
                        "LEFT OUTER JOIN " +
                        "[cuidamed].[dbo].[Titular] B " +
                        "ON CONVERT(INTEGER, SUBSTRING(A.Apellido, 2, LEN(A.Apellido) - 2)) = B.TitularId " +
                        "LEFT OUTER JOIN " +
                        "[cuidamed].[dbo].[Cliente] C " +
                        "ON B.ClienteId = C.ClienteId " +
                        "LEFT OUTER JOIN " +
                        "[CuidaNet.DrCitas].[DrCitas].[Tb_Compania] D " +
                        "ON C.RazonSocial COLLATE SQL_Latin1_General_CP1_CI_AS LIKE '%' + D.nombre + '%' " +
                    "WHERE " +
                        "A.Apellido LIKE '*%*' AND C.RazonSocial NOT LIKE 'PARTICULARES'";

                dt = ado.GetDataTableQuery(sql);

                for (int i = 0; i < dt.Rows.Count; i++) {
                    cliente = new dtoClienteResume {
                        idCliente     = dt.Rows[i].Field<int>(0).ToString(),
                        tipoDocumento = dt.Rows[i].Field<string>(2),
                        documento     = dt.Rows[i].Field<int>(3),
                        correo        = dt.Rows[i].Field<string>(6),
                        telefono      = dt.Rows[i].Field<string>(7),
                        idCiudad      = dt.Rows[i].Field<int>(1)
                    };

                    idCliente  = dt.Rows[i].Field<int>(0);
                    idCompania = dt.Rows[i].Field<int>(11);

                    if (idCliente <= 0 || idCompania <= 0)
                        continue;

                    if (!vCnx.T_ClienteCompania.Any(tcc => tcc.id_cliente == idCliente && tcc.id_compania == idCompania))
                        vCnx.T_ClienteCompania.AddObject(new T_ClienteCompania { id_cliente = idCliente, id_compania = idCompania, activo = true });

                    sql =
                        "SELECT " +
                            "Nombre, " +
                            "CASE " +
                                "WHEN Cedula IS NULL THEN '0' " +
                                "WHEN ISNUMERIC(Cedula) = 0 THEN '0' " +
                                "ELSE Cedula " +
                            "END, " +
                            "ISNULL(FechaNacimiento, CONVERT(DATETIME, '1960-01-01')), " +
                            "CASE " +
                                "WHEN Sexo IS NULL OR RTRIM(LTRIM(Sexo)) = '' THEN 'M' " +
                                "ELSE SUBSTRING(Sexo, 1, 1) " +
                            "END, " +
                            "UPPER(LEFT(Parentesco, 1)) + LOWER(SUBSTRING(Parentesco, 2, LEN(Parentesco))) " +
                        "FROM " +
                            "[cuidamed].[dbo].[Beneficiario] " +
                        "WHERE " +
                            "RTRIM(LTRIM(Parentesco)) <> 'TITULAR' AND " +
                            "Parentesco IS NOT NULL AND " +
                            "TitularId = " + dt.Rows[i].Field<string>(5).Substring(1, dt.Rows[i].Field<string>(5).Length - 2);

                    dt_ = ado.GetDataTableQuery(sql);

                    for (int j = 0; j < dt_.Rows.Count; j++) {
                        int documento = Convert.ToInt32(dt_.Rows[j].Field<string>(1));
                        string nombre = dt_.Rows[j].Field<string>(0);

                        if (vCnx.Tb_Cliente.Any(tc => tc.Nombre == nombre))
                            continue;

                        asociado = new dtoAsociado {
                            FN         = dt_.Rows[j].Field<DateTime>(2).ToString("yyyy-MM-dd"),
                            documento  = Convert.ToInt32(dt_.Rows[j].Field<string>(1)),
                            nombre     = dt_.Rows[j].Field<string>(0),
                            sexo       = dt_.Rows[j].Field<string>(3),
                            parentesco = dt_.Rows[j].Field<string>(4)
                        };

                        this._RegistroAsociado(asociado, cliente);
                    }
                }

                vCnx.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

    }
}
