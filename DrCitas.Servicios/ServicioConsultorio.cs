﻿using System;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Linq;
using DrCitas.Datos;
using DrCitas.Dtos;
using System.Collections.Generic;
using System.Data;

namespace DrCitas.Servicios {

    public class ServicioConsultorio {

        DrCitasEntidades vCnx      = null;
        ObjectParameter vResultado = null;

        public ServicioConsultorio() {
            vCnx = new DrCitasEntidades(FuncionesGlobales.Decry64Decry(System.Configuration.ConfigurationManager.ConnectionStrings["DrCitasEntidades"].ConnectionString));
            vResultado = new ObjectParameter("pResultado", "TRUE:");
        }

        #region Métodos Publicos

        public bool RegistrarConsultorio(dtoConsultorios datos, int idCliente) {
            int idConsultorio = -1;
            Tb_Consultorio consultorio = null;

            try {

                if (!vCnx.Tb_Consultorio.Any(a => a.idCliente == datos.IdCliente && a.Nombre.Trim() == datos.Nombre.Trim() && a.Direccion.Trim() == datos.Direccion.Trim())) {

                    idConsultorio = vCnx.Tb_Consultorio.OrderByDescending(o => o.idConsultorio).Select(s => s.idConsultorio).FirstOrDefault() + 1;

                    consultorio = new Tb_Consultorio {
                        idConsultorio = idConsultorio,
                        idCliente     = idCliente,
                        idCiudad      = datos.IdCiudad,
                        Nombre        = datos.Nombre,
                        Direccion     = datos.Direccion,
                        Telefono      = datos.Telefono,
                        Activo        = "1"
                    };

                    vCnx.Tb_Consultorio.AddObject(consultorio);

                    if (datos.IdClinica > 0) {
                        consultorio.Coordenadas = vCnx.Tb_Clinica.Where(tc => tc.idClinica == datos.IdClinica).Select(e => e.Coordenada).FirstOrDefault();

                        vCnx.Tb_ConsultorioClinica.AddObject(new Tb_ConsultorioClinica { idConsultorio = idConsultorio, idClinica = datos.IdClinica });
                    }
                    else {
                        if (!string.IsNullOrEmpty(datos.Coordenadas))
                            consultorio.Coordenadas = datos.coordenadas.ToString();
                    }

                    vCnx.SaveChanges();

                    return true;
                }

                return false;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool ActualizarConsultorio(dtoConsultorios datos, int idConsultorio) {
            Tb_Consultorio consultorio = null;
            Tb_ConsultorioClinica cc = null;

            try {

                consultorio = vCnx.Tb_Consultorio.Where(tc => tc.idConsultorio == idConsultorio).FirstOrDefault();

                if (consultorio != null) {

                    consultorio.idCiudad  = (datos.IdCiudad > 0                      ? datos.IdCiudad  : consultorio.idCiudad);
                    consultorio.Direccion = (!string.IsNullOrEmpty(datos.Direccion)) ? datos.Direccion : consultorio.Direccion;
                    consultorio.Nombre    = (!string.IsNullOrEmpty(datos.Nombre))    ? datos.Nombre    : consultorio.Nombre;
                    consultorio.Telefono  = (!string.IsNullOrEmpty(datos.Telefono)) ? datos.Telefono   : consultorio.Telefono;

                    if (datos.IdClinica > 0) {
                        cc = vCnx.Tb_ConsultorioClinica.Where(tcc => tcc.idConsultorio == idConsultorio && tcc.idClinica == datos.IdClinica).FirstOrDefault();

                        if (cc != null)
                            cc.idClinica = datos.IdClinica;
                        else
                            vCnx.Tb_ConsultorioClinica.AddObject(new Tb_ConsultorioClinica { idConsultorio = idConsultorio, idClinica = datos.IdClinica });
                    }
                    else {
                        cc = vCnx.Tb_ConsultorioClinica.Where(tcc => tcc.idConsultorio == idConsultorio).FirstOrDefault();

                        if (cc != null)
                            vCnx.Tb_ConsultorioClinica.DeleteObject(cc);
                    }

                    vCnx.SaveChanges();

                    return true;
                }

                return false;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoConsultorios> ListarConsultorios(string miSesion) {
            int idCliente = -1;

            try {
                idCliente = ServicioUtilidad.buscarIdCliente(vCnx, miSesion);

                return this.ListarConsultorios(idCliente);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoConsultorios> ListarConsultorios(int idCliente) {
            List<dtoConsultorios> misConsultorios = null;

            try {
                misConsultorios =
                    (
                        from consultorios in vCnx.Tb_Consultorio
                        join ciudad in vCnx.Tb_Ciudad on consultorios.idCiudad equals ciudad.idCiudad
                        join estado in vCnx.Tb_Estado on ciudad.idEstado equals estado.idEstado
                        where consultorios.idCliente == idCliente && consultorios.Activo == "1"
                        select new {
                            IdConsultorio = consultorios.idConsultorio,
                            Nombre        = consultorios.Nombre,
                            Direccion     = consultorios.Direccion,
                            Telefono      = consultorios.Telefono,
                            IdEstado      = estado.idEstado,
                            Estado        = estado.Descripcion,
                            IdCiudad      = ciudad.idCiudad,
                            Ciudad        = ciudad.Descripcion,
                            Coordenadas   = consultorios.Coordenadas
                        }
                    ).AsEnumerable().Select(e => new dtoConsultorios {
                        IdConsultorio = e.IdConsultorio,
                        Nombre        = e.Nombre,
                        Direccion     = e.Direccion,
                        Telefono      = e.Telefono,
                        IdEstado      = e.IdEstado,
                        Estado        = e.Estado,
                        IdCiudad      = e.IdCiudad,
                        Ciudad        = e.Ciudad,
                        coordenadas   = (!string.IsNullOrEmpty(e.Coordenadas) ? new dtoCoordenada(e.Coordenadas) : null),
                    }).ToList();

                misConsultorios.ForEach(e => {
                    var t = vCnx.Tb_ConsultorioClinica.Where(tcc => tcc.idConsultorio == e.IdConsultorio && tcc.Tb_Clinica.Activo == "1").Select(s => new { idClinica = s.idClinica, descripcion = s.Tb_Clinica.Descripcion }).FirstOrDefault();
                    if (t != null) {
                        e.IdClinica = t.idClinica;
                        e.Clinica   = t.descripcion;
                    }

                    var coor = new dtoCoordenada(vCnx.Tb_Consultorio.Where(tc => tc.idConsultorio == e.IdConsultorio).Select(s => s.Coordenadas).FirstOrDefault());
                    if (coor != null)
                        e.coordenadas = coor;
                });

                return misConsultorios;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public string RegistrarHorarios(dtoConsultorioHorario consultorioHorario, string aux) {
            int idConsultorioHorario = -1;
            string respuesta = "";

            try {
                if (vCnx.Tb_ConsultorioHorario.Any(a => a.Dias == consultorioHorario.Dias && a.TipoConsulta == consultorioHorario.TipoConsulta && a.HoraDesde == consultorioHorario.HoraDesde))
                    return "El consultorio ya posee un horario asociado";

                idConsultorioHorario = (vCnx.Tb_ConsultorioHorario.OrderByDescending(o => o.idConsultorioHorario).Select(s => s.idConsultorioHorario).FirstOrDefault() + 1);

                vCnx.Tb_ConsultorioHorario.AddObject(
                    new Tb_ConsultorioHorario {
                        idConsultorioHorario = idConsultorioHorario,
                        idConsultorio        = consultorioHorario.IdConsultorio,
                        Dias                 = consultorioHorario.Dias,
                        TipoConsulta         = consultorioHorario.TipoConsulta,
                        HoraDesde            = consultorioHorario.HoraDesde,
                        HoraHasta            = consultorioHorario.HoraHasta,
                        Duracion             = consultorioHorario.Duracion,
                        Cupos                = consultorioHorario.Cupos,
                        Activo               = consultorioHorario.Activo
                    }
                );

                vCnx.SaveChanges();

                respuesta = vCnx.Sp_Agenda_Crear_Horarios(consultorioHorario.IdConsultorio, 0, idConsultorioHorario, consultorioHorario.Dias, consultorioHorario.TipoConsulta, consultorioHorario.HoraDesde, consultorioHorario.HoraHasta, consultorioHorario.Duracion, consultorioHorario.Cupos, null, consultorioHorario.Meses, consultorioHorario.Activo, consultorioHorario.IdUsuario, consultorioHorario.allDays).FirstOrDefault();

                return (respuesta.Contains("ERR")) ? respuesta : "OK";
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public string RegistrarHorarios(dtoConsultorioHorario consultorioHorario) {
            int idConsultorioHorario = -1, aux = -1;
            string respuesta = "", res = "";
            bool ok = false;
            int month = new DateTime(DateTime.Now.Year, 12, 31).Month - DateTime.Today.Month;
            List<consulHorario> _consulHorario = new List<consulHorario>();
            List<Tb_ConsultorioHorario> tbConsultorio = null;

            try {

                idConsultorioHorario = vCnx.Tb_ConsultorioHorario.Where(w => w.idConsultorio == consultorioHorario.IdConsultorio).Select(s => s.idConsultorioHorario).FirstOrDefault();

                tbConsultorio = vCnx.Tb_ConsultorioHorario.Where(w => w.idConsultorio == consultorioHorario.IdConsultorio).ToList();

                if (tbConsultorio.Count > 0) {
                    foreach (var item in tbConsultorio) {
                        if (item.Dias.Contains(consultorioHorario.Dias)) {
                            ok = true;
                        }
                    }
                }

                if (!ok) {
                    aux = (vCnx.Tb_ConsultorioHorario.OrderByDescending(o => o.idConsultorioHorario).Select(s => s.idConsultorioHorario).FirstOrDefault() + 1);

                    vCnx.Tb_ConsultorioHorario.AddObject(
                       new Tb_ConsultorioHorario {
                           idConsultorioHorario = aux,
                           idConsultorio        = consultorioHorario.IdConsultorio,
                           TipoConsulta         = consultorioHorario.TipoConsulta,
                           HoraDesde            = consultorioHorario.HorasDias[0].horaDesde,
                           HoraHasta            = consultorioHorario.HorasDias[0].horaHasta,
                           Duracion             = consultorioHorario.Duracion,
                           Cupos                = consultorioHorario.Cupos,
                           Activo               = consultorioHorario.Activo,
                           Dias                 = consultorioHorario.Dias
                       }
                   );

                    vCnx.SaveChanges();

                    idConsultorioHorario = (aux != -1 ? aux : idConsultorioHorario);

                    foreach (var item in consultorioHorario.HorasDias) {

                        respuesta = vCnx.Sp_Agenda_Crear_Horarios(
                                consultorioHorario.IdConsultorio,
                                0,
                                idConsultorioHorario,
                                item.dia,
                                consultorioHorario.TipoConsulta,
                                item.horaDesde,
                                item.horaHasta,
                                item.duracion,
                                item.cupos,
                                consultorioHorario.FechaInicio,
                                month,
                                consultorioHorario.Activo,
                                consultorioHorario.IdUsuario,
                                consultorioHorario.allDays
                            ).FirstOrDefault();

                    }

                    res = "OK";

                }

                return res;

            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoConsultorioHorario> HorarioConsultorio(int idConsultorio) {
            List<dtoConsultorioHorario> horarioConsultorio = null;

            try {
                horarioConsultorio =
                    (
                        from q in vCnx.Tb_ConsultorioHorario
                        where q.idConsultorio == idConsultorio
                        select new dtoConsultorioHorario {
                            Dias         = q.Dias,
                            TipoConsulta = q.TipoConsulta,
                            HoraDesde    = q.HoraDesde,
                            HoraHasta    = q.HoraHasta,
                            Duracion     = q.Duracion,
                            Cupos        = q.Cupos,
                            FechaInicio  = vCnx.Tb_Agenda.OrderBy(o => o.idAgenda).Where(w => w.idConsultorioHorario == q.idConsultorioHorario).Select(s => s.FechaHoraInicio).FirstOrDefault(),
                            FechaFin     = vCnx.Tb_Agenda.OrderByDescending(o => o.idAgenda).Where(w => w.idConsultorioHorario == q.idConsultorioHorario).Select(s => s.FechaHoraInicio).FirstOrDefault()
                        }
                    ).ToList();

                return horarioConsultorio;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        ///---------------------------------------------------------------------------------------------------------------------
        ///<summary>Método para eliminar un consultorio del cliente.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public bool EliminarConsultorio(int pIdConsultorio, int pIdUsuarioX) {
            using (vCnx) {
                vCnx.ExecuteStoreCommand
                    (
                        @"[CuidaNet.DrCitas].[DrCitas].[Sp_Consultorio_P_Eliminar]
                            @pIdConsultorio,
                            @pIdUsuarioX,
                            @pResultado",
                        new object[]{
                                new SqlParameter("@pIdConsultorio", pIdConsultorio),
                                new SqlParameter("@pIdUsuarioX"   , pIdUsuarioX),
                                new SqlParameter("@pResultado"    , vResultado)
                            }
                    );

                if (vResultado.Value.ToString() != "TRUE:")
                    throw new Exception(vResultado.Value.ToString());

                return true;
            }
        }

        public bool DesactivarHorarios(dtoDesactivarAgenda desactivarAgenda) {
            List<dtoDatosCancelarCita> ldtoDatosCitas = null;
            Tb_Agenda tbAgenda = null;

            try {
                DateTime dt1 = Convert.ToDateTime(Convert.ToDateTime(desactivarAgenda.fechaDesde).ToString("yyyy-MM-dd") + " 06:00:00");
                DateTime dt2 = ((desactivarAgenda.fechaHasta == null) ? Convert.ToDateTime(Convert.ToDateTime(desactivarAgenda.fechaDesde).ToString("yyyy-MM-dd") + " 23:00:00") : Convert.ToDateTime(Convert.ToDateTime(desactivarAgenda.fechaHasta).ToString("yyyy-MM-dd") + " 23:00:00"));

                ldtoDatosCitas =
                    (
                        from cita in vCnx.Tb_Cita
                        join agenda in vCnx.Tb_Agenda on cita.idAgenda equals agenda.idAgenda
                        join consultorio in vCnx.Tb_Consultorio on agenda.idConsultorio equals consultorio.idConsultorio
                        where
                            cita.idCliente == desactivarAgenda.idCliente &&
                            agenda.FechaHoraInicio > DateTime.Now &&
                            agenda.FechaHoraInicio >= dt1 &&
                            agenda.FechaHoraFin <= dt2 &&
                            agenda.Activo == "1"
                        select new dtoDatosCancelarCita {
                            idCliente = cita.idCliente,
                            idCita    = cita.idCita,
                            idAgenda  = cita.idAgenda,
                            Numero    = cita.Numero,
                            Correo    = cita.Correo,
                            Celular   = cita.Celular
                        }
                    ).ToList();

                if (ldtoDatosCitas.Count > 0) {
                    foreach (var datos in ldtoDatosCitas)
                        new ServicioCita().CancelarCita(datos.idCliente, datos.idCita, datos.Correo, desactivarAgenda.idCliente, desactivarAgenda.idUsuario);
                }

                var datosAgenda =
                    (
                        from agenda in vCnx.Tb_Agenda
                        join consultorio in vCnx.Tb_Consultorio on agenda.idConsultorio equals consultorio.idConsultorio
                        join cliente in vCnx.Tb_Cliente on consultorio.idCliente equals cliente.idCliente
                        where
                            cliente.idCliente == desactivarAgenda.idCliente &&
                            agenda.FechaHoraInicio > DateTime.Now &&
                            agenda.FechaHoraInicio >= dt1 &&
                            agenda.FechaHoraFin <= dt2 &&
                            agenda.Activo == "1"
                        select new { agenda.idAgenda }
                    ).ToList();

                foreach (var idAgenda in datosAgenda) {
                    tbAgenda = vCnx.Tb_Agenda.Where(w => w.idAgenda == idAgenda.idAgenda).FirstOrDefault();

                    if (tbAgenda != null) {
                        tbAgenda.Activo = "0";
                    }
                }
                vCnx.SaveChanges();
                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public bool DesactivarHoras(dtoDesactivarAgenda desactivarAgenda) {
            Tb_Agenda tb_Agenda = null;
            try {
                foreach (var consultorio in desactivarAgenda.desactivarHoras) {
                    tb_Agenda = vCnx.Tb_Agenda.Where(w => w.idConsultorio == desactivarAgenda.idConsultorio && w.idAgenda == consultorio.nroCita).First();

                    if (tb_Agenda != null) {
                        tb_Agenda.Activo = "0";
                    }
                }
                vCnx.SaveChanges();
                return true;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public HorariosSemanas ListarHorarios(dtoDatosGetHorarios paramHorarios, string conn) {
            DataSet ds = null;
            HorariosSemanas HoriosSemanas = null;
            List<ListDtoHorario> lgtoh = null;
            dtoHorario _dtoHorario = null;
            List<dtoHorario> listDtoHorario = null;
            int IdConsultorio = 0, idCliente = 0;
            List<string> listaAux = null;
            int iaux = 0;
            bool tipoConsulta = false;

            try {
                idCliente = Convert.ToInt32(paramHorarios.idCliente);

                IdConsultorio = ((paramHorarios.idConsultorio.Trim() != "") ? Convert.ToInt32(paramHorarios.idConsultorio.Trim()) : vCnx.Tb_Consultorio.Where(w => w.idCliente == idCliente).Select(s => s.idConsultorio).FirstOrDefault());

                if (paramHorarios.tipoConsulta == "") {
                    listaAux = vCnx.Tb_ConsultorioHorario.Where(w => w.idConsultorio == IdConsultorio).Select(s => s.TipoConsulta).ToList();

                    foreach (var item in listaAux) {
                        paramHorarios.tipoConsulta += (iaux == 0 ? item : "," + item);
                        iaux += 1;
                    }

                }


                if (paramHorarios.tipoConsulta != "") {

                    using (ADO ado = new ADO(conn)) {
                        ds = ado.GetDataSetQuery("EXEC [DrCitas].[Sp_Horario_Cliente] " + paramHorarios.idCliente + " , 0, " + IdConsultorio + ", null , '" + (paramHorarios.tipoConsulta.Contains(",") ? (paramHorarios.tipoConsulta.Split(','))[0] : paramHorarios.tipoConsulta) + "' , '" + (paramHorarios.tipoConsulta.Contains(",") ? (paramHorarios.tipoConsulta.Split(','))[1] : paramHorarios.tipoConsulta) + "'");
                    }

                    if (ds.Tables.Count > 0) {
                        for (int i = 0; i < ds.Tables.Count; i++) {
                            if (i == 0)
                                lgtoh = new List<ListDtoHorario>();

                            listDtoHorario = new List<dtoHorario>();

                            for (int j = 0; j < ds.Tables[i].Rows.Count; j++) {
                                _dtoHorario = new dtoHorario();
                                _dtoHorario.Dia1 = ObtenerValor("Hora", ds.Tables[i].Rows[j].Field<string>(1), FuncionesGlobales.Encry64Encry(idCliente.ToString()), paramHorarios.idEspecialidad);
                                _dtoHorario.Dia2 = ObtenerValor("Hora", ds.Tables[i].Rows[j].Field<string>(2), FuncionesGlobales.Encry64Encry(idCliente.ToString()), paramHorarios.idEspecialidad);
                                _dtoHorario.Dia3 = ObtenerValor("Hora", ds.Tables[i].Rows[j].Field<string>(3), FuncionesGlobales.Encry64Encry(idCliente.ToString()), paramHorarios.idEspecialidad);
                                _dtoHorario.Dia4 = ObtenerValor("Hora", ds.Tables[i].Rows[j].Field<string>(4), FuncionesGlobales.Encry64Encry(idCliente.ToString()), paramHorarios.idEspecialidad);
                                _dtoHorario.Dia5 = ObtenerValor("Hora", ds.Tables[i].Rows[j].Field<string>(5), FuncionesGlobales.Encry64Encry(idCliente.ToString()), paramHorarios.idEspecialidad);
                                _dtoHorario.Dia6 = ObtenerValor("Hora", ds.Tables[i].Rows[j].Field<string>(6), FuncionesGlobales.Encry64Encry(idCliente.ToString()), paramHorarios.idEspecialidad);
                                _dtoHorario.Dia7 = ObtenerValor("Hora", ds.Tables[i].Rows[j].Field<string>(7), FuncionesGlobales.Encry64Encry(idCliente.ToString()), paramHorarios.idEspecialidad);
                                _dtoHorario.UrlDia1 = ObtenerValor("Url", ds.Tables[i].Rows[j].Field<string>(1), FuncionesGlobales.Encry64Encry(idCliente.ToString()), paramHorarios.idEspecialidad);
                                _dtoHorario.UrlDia2 = ObtenerValor("Url", ds.Tables[i].Rows[j].Field<string>(2), FuncionesGlobales.Encry64Encry(idCliente.ToString()), paramHorarios.idEspecialidad);
                                _dtoHorario.UrlDia3 = ObtenerValor("Url", ds.Tables[i].Rows[j].Field<string>(3), FuncionesGlobales.Encry64Encry(idCliente.ToString()), paramHorarios.idEspecialidad);
                                _dtoHorario.UrlDia4 = ObtenerValor("Url", ds.Tables[i].Rows[j].Field<string>(4), FuncionesGlobales.Encry64Encry(idCliente.ToString()), paramHorarios.idEspecialidad);
                                _dtoHorario.UrlDia5 = ObtenerValor("Url", ds.Tables[i].Rows[j].Field<string>(5), FuncionesGlobales.Encry64Encry(idCliente.ToString()), paramHorarios.idEspecialidad);
                                _dtoHorario.UrlDia6 = ObtenerValor("Url", ds.Tables[i].Rows[j].Field<string>(6), FuncionesGlobales.Encry64Encry(idCliente.ToString()), paramHorarios.idEspecialidad);
                                _dtoHorario.UrlDia7 = ObtenerValor("Url", ds.Tables[i].Rows[j].Field<string>(7), FuncionesGlobales.Encry64Encry(idCliente.ToString()), paramHorarios.idEspecialidad);
                                listDtoHorario.Add(_dtoHorario);
                            }

                            lgtoh.Add(new ListDtoHorario("semana" + i, listDtoHorario));
                        }

                        if (lgtoh != null)
                            HoriosSemanas = new HorariosSemanas(lgtoh);

                        if (vResultado.Value.ToString() != "TRUE:")
                            throw new Exception(vResultado.Value.ToString());
                    }
                }
                return HoriosSemanas;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        ///---------------------------------------------------------------------------------------------------------------------
        ///<summary>Operación para obtener un atributo de una agenda.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public static string ObtenerValor(string pDato, string pAgenda, string idCliente = "", int idEspecialidad = 0) {
            if (pAgenda == null)
                return "";

            string[] vDatos = pAgenda.Split(new char[] { '+' });

            switch (pDato) {
                case "Hora":
                if (vDatos[3] == "O")
                    return Convert.ToDateTime("01/01/1900 " + vDatos[1]).ToShortTimeString() + " a<br/>" + Convert.ToDateTime("01/01/1900 " + vDatos[2]).ToShortTimeString();
                return Convert.ToDateTime("01/01/1900 " + vDatos[1]).ToShortTimeString();
                case "Url":
                return "TuCita.html?IdCliente=" + idCliente + "&IdEspecialidad=" + idEspecialidad.ToString() + "&NroCita=" + vDatos[0];
                default:
                break;
            }
            return "";
        }

        #endregion
    }

    public class consulHorario {
        public string dia { get; set; }
        public string HD { get; set; }
        public string HH { get; set; }
        public string respuesta { get; set; }
    }

}
