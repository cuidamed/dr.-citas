﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DrCitas.Datos;
using DrCitas.Dtos;
using System.Data.Objects;
using System.Globalization;
using System.Threading;


namespace DrCitas.Servicios
{
   
    public class ServicioListaDeValores
    {
        DrCitasEntidades vCnx = null;

        public ServicioListaDeValores() { 
            vCnx = new DrCitasEntidades(FuncionesGlobales.Decry64Decry(System.Configuration.ConfigurationManager.ConnectionStrings["DrCitasEntidades"].ConnectionString));
        }

        #region Métodos Públicos
        ///---------------------------------------------------------------------------------------------------------------------
        ///<summary>Operación para obtener los consultorios de un cliente.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public List<dtoListaDeValores> ListaDeConsultoriosDelCliente(int pIdCliente, int idEspecialidad)
        {
            var vTabla = vCnx.CreateObjectSet<Vw_Consultorio>();
            ServicioCliente sv = new ServicioCliente(); 

            var algo =  (
                            from    vTbl in vTabla
                            where   vTbl.Consultorio_idCliente == pIdCliente && vTbl.Consultorio_Activo == "1"
                            orderby vTbl.Consultorio_idConsultorio
                            select  new dtoListaDeValores
                            {
                                Id          = vTbl.Consultorio_idConsultorio ,
                                Descripcion = vTbl.Consultorio_Nombre        ,
                                Codigo      = vTbl.Consultorio_Coordenadas   ,
                                Direccion   = vTbl.Consultorio_Direccion     ,
                                Telefonos   = vTbl.Consultorio_Telefono      //,
                            //  Horarios    = sv.ListarHorariosxSemanas(pIdCliente, idEspecialidad, vTbl.Consultorio_idConsultorio.ToString(), DateTime.Now.ToShortTimeString(), 1)
                            }
                        ).ToList<dtoListaDeValores>();

            return algo;
        }

        public List<dtoListaDeValores> ListaDeConsultoriosDelCliente_(int pIdCliente, int idEspecialidad,string stringConn)
        {
             List<dtoListaDeValores> lv = new List<dtoListaDeValores>();

             var consultorios = vCnx.Tb_Consultorio.Where(w => w.idCliente == pIdCliente).ToList();

             foreach (var item in consultorios)
             {
                     lv.Add(new dtoListaDeValores
                            {
                                Id = item.idConsultorio,
                                Descripcion = item.Nombre,
                                Codigo = item.Coordenadas,
                                Direccion = item.Direccion,
                                Telefonos = item.Telefono//,
                               // Horarios = new ServicioCliente().ListarHorariosxSemanas(pIdCliente, idEspecialidad, item.idConsultorio.ToString(), DateTime.Now.ToString("yyyy-MM-yy"), 1, stringConn)
                            });
             }

             return lv;

        }
        ///---------------------------------------------------------------------------------------------------------------------
        ///<summary>Operación para obtener las especialidades de un cliente.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public List<dtoListaDeValores> ListaDeEspecialidadesDelCliente(int pIdCliente)
        {
            var vTabla = vCnx.CreateObjectSet<Vw_ClienteEspecialidad>();
            return (
                        from    vTbl in vTabla
                        where   vTbl.ClienteEspecialidad_idCliente == pIdCliente && vTbl.ClienteEspecialidad_Activo == "1"
                        orderby vTbl.Especialidad_Descripcion
                        select new dtoListaDeValores
                        {
                            Id          = vTbl.ClienteEspecialidad_idEspecialidad,
                            Descripcion = vTbl.Especialidad_Descripcion
                        }
                    ).ToList<dtoListaDeValores>();
        }
        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para obtener una lista de especialidades según el tipo.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public List<dtoListaDeValores> ListaDeEspecialidades(string pTipo)
        {
            var vTabla = vCnx.CreateObjectSet<Tb_Especialidad>();

            return
                vCnx.Tb_Especialidad
                    .OrderBy(o => o.Descripcion)
                    .Where(w => w.Activo == "1" && w.Tipo == pTipo)
                    .Select(s => new dtoListaDeValores { Id = s.idEspecialidad, Descripcion = s.Descripcion }).ToList<dtoListaDeValores>();
        }

        public EstadoCiudad GetEstadoCiudad(EstadoCiudad ec) 
        {
            int _idEstado = Convert.ToInt32(ec.idEstado);
            int _idCiudad = Convert.ToInt32(ec.idCiudad);
            return  (
                        from e in vCnx.Tb_Estado
                        join c in vCnx.Tb_Ciudad on e.idEstado equals c.idEstado
                        where e.idEstado == _idEstado && c.idCiudad == _idCiudad
                        select new EstadoCiudad
                        {
                            Estado = e.Descripcion,
                            Ciudad = c.Descripcion
                        }
                    ).FirstOrDefault<EstadoCiudad>();
        }

        /// <summary>
        /// Operación para obtener una lista de paises.
        /// </summary>
        /// <returns></returns>
        public List<dtoListaDeValores> ListaPaises() 
        {
            return
                vCnx.Tb_Pais
                    .OrderBy(o => o.Descripcion).Where(w => w.Activo == "1")
                    .Select(s => new dtoListaDeValores
                                {
                                    Id = s.idPais,
                                    Descripcion = s.Descripcion
                                }
                            ).ToList<dtoListaDeValores>();
        }


        /// <summary>
        /// Operación para v una lista de lenguajes
        /// </summary>
        /// <returns></returns>
        public List<dtoListaDeValores> ListaDeLenguajes()
        {
            var vTabla = vCnx.CreateObjectSet<Tb_Lenguaje>();
            return (
                        from vTbl in vTabla
                        where vTbl.Activo == "1"
                        orderby vTbl.Descripcion
                        select new dtoListaDeValores
                        {
                            Id = vTbl.idLenguaje,
                            Descripcion = vTbl.Descripcion
                        }
                    ).ToList<dtoListaDeValores>();
        }

        /// <summary>
        /// Operación para obtener una lista de estados.
        /// </summary>
        /// <returns></returns>
        public List<dtoListaDeValores> ListaDeEstados()
        {
            return
                vCnx.Tb_Estado
                    .OrderBy(o => o.Descripcion)
                    .Where(w => w.Activo == "1")
                    .Select(s => new dtoListaDeValores { Id = s.idEstado, Descripcion = s.Descripcion }).ToList<dtoListaDeValores>();

        }

        public List<dtoListaDeValores> ListaDeEstados(int idPais)
        {
            return
                vCnx.Tb_Estado
                    .OrderBy(o => o.Descripcion)
                    .Where(w => w.Activo == "1" && w.idPais == idPais)
                    .Select(s => new dtoListaDeValores { Id = s.idEstado, Descripcion = s.Descripcion }).ToList<dtoListaDeValores>();

        }
        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para obtener una lista de ciudades.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public List<dtoListaDeValores> ListaDeCiudades(int pIdEstado)
        {
            return
                vCnx.Tb_Ciudad
                    .OrderBy(o => o.Descripcion)
                    .Where(w => w.Activo == "1" && w.idEstado == pIdEstado)
                    .Select( s => new dtoListaDeValores {  Id = s.idCiudad, Descripcion = s.Descripcion }).ToList<dtoListaDeValores>();
        }

        /// <summary>
        /// optiene un clista de clinicas pasandole como parametro el id de la ciudad
        /// </summary>
        /// <param name="idCiudad"></param>
        /// <returns></returns>
        public List<dtoListaDeValores> ListaCLinicasXciudad(int idCiudad) 
        {

            var ciudades =
                vCnx.Tb_Clinica
                .OrderBy(o => o.Descripcion)
                .Where(w => w.Activo == "1" && w.idCiudad == idCiudad)
                .Select(s => new dtoListaDeValores
                            {
                                Id = s.idClinica,
                                Descripcion = s.Descripcion
                            }
                       ).ToList<dtoListaDeValores>();

            return ciudades;
        }

        /// <summary>
        /// obtnien una lista de especialidades por cada clinica
        /// <returns></returns>
        public List<dtoListaDeValores> ListaEspecialidadesXClinica(int idClinica) 
        {
            var ListaDeValores =
                (
                    from ce in vCnx.Tb_Clinicas_Especialidad
                    join e in vCnx.Tb_Especialidad on ce.idEspecialidad equals e.idEspecialidad
                    where ce.idClinica == idClinica
                    select new dtoListaDeValores
                            {
                                Id = ce.idEspecialidad,
                                Direccion = e.Descripcion
                            }
                ).ToList<dtoListaDeValores>();

            return ListaDeValores;

        }

        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para obtener una lista de títulos.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public IEnumerable<dtoListaDeValores> ListaDeTitulos(string pTipo)
        {
            var vTabla = vCnx.CreateObjectSet<Tb_Titulo>();
            return  (
                        from    vTbl in vTabla
                        where   vTbl.Activo == "1" && vTbl.Tipo == pTipo
                        orderby vTbl.Descripcion
                        select  new dtoListaDeValores
                        {
                            Id          = vTbl.idTitulo,
                            Descripcion = vTbl.Descripcion
                        }
                    ).AsEnumerable<dtoListaDeValores>();
        }
        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para obtener una lista de perfiles.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public IEnumerable<dtoListaDeValores> ListaDePerfiles()
        {
            var vTabla = vCnx.CreateObjectSet<Tb_Perfil>();
            return  (
                        from vTbl in vTabla
                        where vTbl.Activo == "1"
                        orderby vTbl.Descripcion
                        select new dtoListaDeValores
                        {
                             Id          = vTbl.idPerfil,
                             Descripcion = vTbl.Descripcion
                        }
                    ).AsEnumerable<dtoListaDeValores>();
        }
        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para obtener una lista de razones.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public List<dtoListaDeValores> ListaDeRazones(int pIdEspecialidad)
        {
            var vTabla = vCnx.CreateObjectSet<Tb_Razon>();

            return vCnx.Tb_Razon
                .Where(w => (w.idEspecialidad == pIdEspecialidad || pIdEspecialidad == 0) && w.Activo == "1")
                .Select(s => new dtoListaDeValores
                        {
                            Id = s.idRazon,
                            Descripcion = s.Descripcion
                        }).Distinct().ToList();
        }
        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para obtener una lista de tipos de documento.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public IEnumerable<dtoListaDeValores> ListaDeTiposDeDocumento()
        {
            var vTabla = vCnx.CreateObjectSet<Vw_TipoDocumento>();
            return  (
                        from    vTbl in vTabla
                        orderby vTbl.Descripcion
                        where   vTbl.Activo == "1"
                        select  new dtoListaDeValores
                        {
                            Id          = vTbl.idTipoDocumento,
                            Codigo      = vTbl.Codigo,
                            Descripcion = vTbl.Descripcion
                        }
                    ).AsEnumerable<dtoListaDeValores>();
        }
        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para obtener una lista de sexos.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public IEnumerable<dtoListaDeValores> ListaDeSexos()
        {
            var vTabla = vCnx.CreateObjectSet<Vw_Sexo>();
            return  (
                        from    vTbl in vTabla
                        where   vTbl.Activo == "1"
                        orderby vTbl.Descripcion
                        select  new dtoListaDeValores
                        {
                            Id          = vTbl.idSexo,
                            Codigo      = vTbl.Codigo,
                            Descripcion = vTbl.Descripcion
                        }
                    ).AsEnumerable<dtoListaDeValores>();
        }
        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para obtener una lista de tipos de consulta.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public IEnumerable<dtoListaDeValores> ListaDeTiposDeConsulta()
        {
            var vTabla = vCnx.CreateObjectSet<Vw_TipoConsulta>();
            return  (
                        from    vTbl in vTabla
                        where   vTbl.Activo == "1"
                        orderby vTbl.Descripcion
                        select  new dtoListaDeValores
                        {
                            Id          = vTbl.idTipoConsulta,
                            Codigo      = vTbl.Codigo,
                            Descripcion = vTbl.Descripcion
                        }
                    ).AsEnumerable<dtoListaDeValores>();
        }
        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para obtener una lista de tipos de cliente.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public IEnumerable<dtoListaDeValores> ListaDeTiposDeCliente()
        {
            var vTabla = vCnx.CreateObjectSet<Vw_TipoCliente>();
            return  (
                        from    vTbl in vTabla
                        where   vTbl.Activo == "1"
                        orderby vTbl.Descripcion
                        select  new dtoListaDeValores
                        {
                             Id          = vTbl.idTipoCliente,
                             Codigo      = vTbl.Codigo,
                             Descripcion = vTbl.Descripcion
                        }
                    ).AsEnumerable<dtoListaDeValores>();
        }
        
        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para obtener una lista de teléfono principal.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public IEnumerable<dtoListaDeValores> ListaDeTelefonoPrincipal()
        {
            var vTabla = vCnx.CreateObjectSet<Vw_TelefonoPrincipal>();
            return  (
                        from    vTbl in vTabla
                        where   vTbl.Activo == "1"
                        orderby vTbl.Descripcion
                        select  new dtoListaDeValores
                        {
                            Id          = vTbl.idTelefonoPrincipal,
                            Codigo      = vTbl.Codigo,
                            Descripcion = vTbl.Descripcion
                        }
                    ).AsEnumerable<dtoListaDeValores>();
        }
        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para obtener una lista de estatus.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public IEnumerable<dtoListaDeValores> ListaDeEstatus(int pTipoEstatus)
        {
            var vTabla = vCnx.CreateObjectSet<Tb_Estatus>();
            return  (
                        from    vTbl in vTabla
                        where   vTbl.idTipoEstatus == pTipoEstatus && vTbl.Activo == "1"
                        orderby vTbl.Descripcion
                        select  new dtoListaDeValores
                        {
                            Id          = vTbl.idEstatus,
                            Descripcion = vTbl.Descripcion
                        }
                    ).AsEnumerable<dtoListaDeValores>();
        }
        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para obtener una lista de edades mayores.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public IEnumerable<dtoListaDeValores> ListaDeEdadesMayores()
        {
            var vTabla = vCnx.CreateObjectSet<Vw_EdadMayor>();
            return  (
                        from    vTbl in vTabla
                        where   vTbl.Activo == "1"
                        orderby vTbl.Codigo
                        select  new dtoListaDeValores
                        {
                            Id          = vTbl.idEdadMayor,
                            Codigo      = vTbl.Codigo,
                            Descripcion = vTbl.Descripcion
                        }
                    ).AsEnumerable<dtoListaDeValores>();
        }
        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para obtener una lista de edades menores.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public IEnumerable<dtoListaDeValores> ListaDeEdadesMenores()
        {
            var vTabla = vCnx.CreateObjectSet<Vw_EdadMenor>();
            return  (
                        from    vTbl in vTabla
                        where   vTbl.Activo == "1"
                        orderby vTbl.Codigo
                        select  new dtoListaDeValores
                        {
                            Id          = vTbl.idEdadMenor,
                            Codigo      = vTbl.Codigo,
                            Descripcion = vTbl.Descripcion
                        }
                    ).AsEnumerable<dtoListaDeValores>();
        }
        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para regresar una lista de estrellas.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public IEnumerable<dtoListaDeValores> ListaDeEstrellas()
        {
            var vTabla = vCnx.CreateObjectSet<Vw_Estrellas>();
            return  (
                        from    vTbl in vTabla
                        where   vTbl.Activo == "1"
                        orderby vTbl.Codigo
                        select  new dtoListaDeValores
                        {
                            Id          = vTbl.idEstrella,
                            Codigo      = vTbl.Codigo,
                            Descripcion = vTbl.Descripcion
                        }
                    ).AsEnumerable<dtoListaDeValores>();
        }
        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para regresar una lista de horas desde.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public IEnumerable<dtoListaDeValores> ListaDeHorasDesde()
        {
            var vTabla = vCnx.CreateObjectSet<Vw_HoraDesde>();
            return  (
                        from    vTbl in vTabla
                        where   vTbl.Activo == "1"
                        orderby vTbl.Codigo
                        select new dtoListaDeValores
                        {
                            Id          = vTbl.idHoraDesde,
                            Codigo      = vTbl.Codigo,
                            Descripcion = vTbl.Descripcion
                        }
                    ).AsEnumerable<dtoListaDeValores>();
        }
        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para regresar una lista de horas hasta.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public IEnumerable<dtoListaDeValores> ListaDeHorasHasta()
        {
            var vTabla = vCnx.CreateObjectSet<Vw_HoraHasta>();
            return  (
                        from    vTbl in vTabla
                        where   vTbl.Activo == "1"
                        orderby vTbl.Codigo
                        select new dtoListaDeValores
                        {
                            Id          = vTbl.idHoraHasta,
                            Codigo      = vTbl.Codigo,
                            Descripcion = vTbl.Descripcion
                        }
                    ).AsEnumerable<dtoListaDeValores>();
        }
        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para regresar una lista de tipos de ordenamiento.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public IEnumerable<dtoListaDeValores> ListaDeTiposDeOrden()
        {
            var vTabla = vCnx.CreateObjectSet<Vw_OrdenarPor>();
            return  (
                        from    vTbl in vTabla
                        where   vTbl.Activo == "1"
                        orderby vTbl.Codigo
                        select new dtoListaDeValores
                        {
                            Id          = vTbl.idOrdenarPor,
                            Codigo      = vTbl.Codigo,
                            Descripcion = vTbl.Descripcion
                        }
                    ).AsEnumerable<dtoListaDeValores>();
        }

        public List<dtoListaDeValores> ListaUniversidades()
        {
            return (
                    from q in vCnx.Tb_Universidad
                    where q.Activo == "1"
                    orderby q.idUniversidad
                    select new dtoListaDeValores
                        {
                            Id = q.idUniversidad,
                            Descripcion = q.Descripcion
                        }
                 ).ToList<dtoListaDeValores>();
        }

        public List<dtoListaDeValores> ListaDeEmpresas()
        {
            return (
                vCnx.T_Empresa.Where(te => te.Activo == true)
                    .Select(q => new dtoListaDeValores
                    {
                         Id = q.id_empresa,
                         Descripcion = q.nombre
                    }).ToList()
            );

        }

        public List<dtoListaDeValores> ListaTiposCliente() {
            List<dtoListaDeValores> ltc = null;

            try {
                ltc = vCnx.T_GrupoPerfiles.Select(s => new dtoListaDeValores { Id = s.idGrupoPerfil, Descripcion = s.descGrupoPerfil }).ToList<dtoListaDeValores>();
                return ltc;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + " " + ex.StackTrace);
            }
        }

        public List<dtoListaDeValores> ListaEstatusCita() {
            return
                vCnx.Tb_Estatus
                    .OrderBy(o => o.idEstatus)
                    .Where(w => w.Activo == "1")
                    .Select(s => new dtoListaDeValores { Id = s.idEstatus, Descripcion = s.Descripcion }).ToList<dtoListaDeValores>();

        }
        #endregion
        ///---------------------------------------------------------------------------------------------------------------------
    }

    public class EstadoCiudad
    {
        public string idEstado { get; set; }
        public string idCiudad { get; set; }
        public string Estado { get; set; }
        public string Ciudad { get; set; }
    }

}
