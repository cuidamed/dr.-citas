﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text.RegularExpressions;
using DrCitas.Datos;
using DrCitas.Dtos;
using DrCitas.Servicios;

namespace DrCitas.Servicios
{

    public class ServicioReportes
    {

        DrCitasEntidades vCnx = null;

        public ServicioReportes() {
            vCnx = new DrCitasEntidades(FuncionesGlobales.Decry64Decry(System.Configuration.ConfigurationManager.ConnectionStrings["DrCitasEntidades"].ConnectionString));
        }

        public List<dtoReporteGeneralCitas> getReporteGeneralCitas(string miSesion, int mes, int ano) {
            Tupla<int, string> datos = null;

            try
            {
                datos = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                return this.getReporteGeneralCitas(datos.item1, datos.item2, mes, ano);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoReporteGeneralCitas> getReporteGeneralCitas(int idCliente, int mes, int ano) {
            string tipoCliente = "";

            try
            {
                tipoCliente = ServicioUtilidad.buscarTipoCliente(vCnx, idCliente);

                return this.getReporteGeneralCitas(idCliente, tipoCliente, mes, ano);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private List<dtoReporteGeneralCitas> getReporteGeneralCitas(int idCliente, string tipoCliente, int mes, int ano) {
            List<dtoReporteGeneralCitas> reporte = null;

            try
            {
                if (string.IsNullOrEmpty(tipoCliente)) return null;

                if (tipoCliente == "A") {
                    reporte = vCnx.Tb_Cita.Where(tbc => tbc.HoraInicio.Month == mes && tbc.HoraInicio.Year == ano)
                        .GroupBy(g => new {
                            idEstado = g.idEstatus,
                            estado   = g.Tb_Estatus.Descripcion
                        }).Select(e => new dtoReporteGeneralCitas
                        {
                            idEstado = e.Key.idEstado,
                            estado   = e.Key.estado,
                            cantidad = e.Count()
                        }).ToList();
                }
                else if (tipoCliente == "L")
                {
                    reporte =
                        (
                            from cita in vCnx.Tb_Cita
                            join agenda in vCnx.Tb_Agenda on cita.idAgenda equals agenda.idAgenda
                            join consultorio in vCnx.Tb_Consultorio on agenda.idConsultorio equals consultorio.idConsultorio
                            join asistenteMedico in vCnx.T_AsistenteMedico on consultorio.idCliente equals asistenteMedico.id_medico
                            join asistenteClinica in vCnx.T_AsistenteClinica on asistenteMedico.id_asistente equals asistenteClinica.id_asistente
                            join asistenteEmpresa in vCnx.T_AsistenteEmpresa on asistenteMedico.id_asistente equals asistenteEmpresa.id_asistente
                            where
                                asistenteMedico.id_asistente == idCliente &&
                                cita.HoraInicio.Month == mes && cita.HoraInicio.Year == ano
                            select new {
                                cita.idEstatus,
                                cita.Tb_Estatus.Descripcion
                            }
                        ).GroupBy(g => new
                        {
                            idEstado = g.idEstatus,
                            estado   = g.Descripcion
                        }).Select(e => new dtoReporteGeneralCitas
                        {
                            idEstado = e.Key.idEstado,
                            estado   = e.Key.estado,
                            cantidad = e.Count()
                        }).ToList();
                }
                else if (tipoCliente == "S")
                {
                    reporte =
                        (
                            from cita in vCnx.Tb_Cita
                            join agenda in vCnx.Tb_Agenda on cita.idAgenda equals agenda.idAgenda
                            join consultorio in vCnx.Tb_Consultorio on agenda.idConsultorio equals consultorio.idConsultorio
                            join asistenteMedico in vCnx.T_AsistenteMedico on consultorio.idCliente equals asistenteMedico.id_medico
                            join asistenteClinica in vCnx.T_AsistenteClinica on asistenteMedico.id_asistente equals asistenteClinica.id_asistente
                            where
                                asistenteMedico.id_asistente == idCliente &&
                                cita.HoraInicio.Month == mes && cita.HoraInicio.Year == ano
                            select new {
                                cita.idEstatus,
                                cita.Tb_Estatus.Descripcion
                            }
                        ).GroupBy(g => new
                        {
                            idEstado = g.idEstatus,
                            estado   = g.Descripcion
                        }).Select(e => new dtoReporteGeneralCitas
                        {
                            idEstado = e.Key.idEstado,
                            estado   = e.Key.estado,
                            cantidad = e.Count()
                        }).ToList();
                }
		else if (tipoCliente == "M") {
                    reporte = vCnx.Tb_Cita.Where(tbc => tbc.Tb_Agenda.Tb_Consultorio.idCliente == idCliente && tbc.HoraInicio.Month == mes && tbc.HoraInicio.Year == ano)
                        .GroupBy(g => new {
                            idEstado = g.idEstatus,
                            estado   = g.Tb_Estatus.Descripcion
                        }).Select(e => new dtoReporteGeneralCitas
                        {
                            idEstado = e.Key.idEstado,
                            estado   = e.Key.estado,
                            cantidad = e.Count()
                        }).ToList();
                }

                return reporte;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoReporteGeneralCitas> getReporteCitas(string miSesion, int idEmpresa, int idClinica, int idEspecialidad, int idConsultorio, int idMedico, int mes, int ano) {
            Tupla<int, string> datos = null;

            try
            {
                datos = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                return this.getReporteCitas(datos.item1, datos.item2, idEmpresa, idClinica, idEspecialidad, idConsultorio, idMedico, mes, ano);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoReporteGeneralCitas> getReporteCitas(int idCliente, int idEmpresa, int idClinica, int idEspecialidad, int idConsultorio, int idMedico, int mes, int ano) {
            string tipoCliente = "";

            try
            {
                tipoCliente = ServicioUtilidad.buscarTipoCliente(vCnx, idCliente);

                return this.getReporteCitas(idCliente, tipoCliente, idEmpresa, idClinica, idEspecialidad, idConsultorio, idMedico, mes, ano);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private List<dtoReporteGeneralCitas> getReporteCitas(int idCliente, string tipoCliente, int idEmpresa, int idClinica, int idEspecialidad, int idConsultorio, int idMedico, int mes, int ano) {
            List<dtoReporteGeneralCitas> reporte = null;

            try
            {
                if (string.IsNullOrEmpty(tipoCliente)) return null;

                if (tipoCliente == "A")
                {

                    reporte =
                        (
                            from cita in vCnx.Tb_Cita
                            join agenda in vCnx.Tb_Agenda on cita.idAgenda equals agenda.idAgenda
                            join consultorio in vCnx.Tb_Consultorio on agenda.idConsultorio equals consultorio.idConsultorio into cons_
                            from cons in cons_.DefaultIfEmpty()
                            join clinica in vCnx.Tb_Clinica on cons.id_clinica equals clinica.idClinica into cli_
                            from cli in cli_.DefaultIfEmpty()
                            join empresa in vCnx.T_MedicoEmpresa on cons.idCliente equals empresa.id_medico into emp_
                            from emp in emp_.DefaultIfEmpty()
                            join cliente in vCnx.Tb_ClienteEspecialidad on cons.idCliente equals cliente.idCliente
                            where
                                (cliente.idEspecialidad == idEspecialidad || idEspecialidad == 0) &&
                                (cons.idConsultorio == idConsultorio || idConsultorio == 0) &&
                                (cli.idClinica == idClinica || idClinica == 0) &&
                                (emp.id_empresa == idEmpresa || idEmpresa == 0) &&
                                cita.HoraInicio.Month == mes && cita.HoraInicio.Year == ano
                            select new {
                                cita.idEstatus,
                                cita.Tb_Estatus.Descripcion
                            }
                        ).GroupBy(g => new
                        {
                            idEstado = g.idEstatus,
                            estado   = g.Descripcion
                        }).Select(e => new dtoReporteGeneralCitas
                        {
                            idEstado = e.Key.idEstado,
                            estado   = e.Key.estado,
                            cantidad = e.Count()
                        }).ToList();
                }
                else if (tipoCliente == "L")
                {
                    reporte =
                        (
                            from cita in vCnx.Tb_Cita
                            join agenda in vCnx.Tb_Agenda on cita.idAgenda equals agenda.idAgenda
                            join consultorio in vCnx.Tb_Consultorio on agenda.idConsultorio equals consultorio.idConsultorio
                            join asistenteMedico in vCnx.T_AsistenteMedico on consultorio.idCliente equals asistenteMedico.id_medico
                            join clienteEsp in vCnx.Tb_ClienteEspecialidad on consultorio.idCliente equals clienteEsp.idCliente
                            join asistenteClinica in vCnx.T_AsistenteClinica on asistenteMedico.id_asistente equals asistenteClinica.id_asistente
                            join asistenteEmpresa in vCnx.T_AsistenteEmpresa on asistenteMedico.id_asistente equals asistenteEmpresa.id_asistente
                            where
                                asistenteMedico.id_asistente == idCliente &&
                                (clienteEsp.idEspecialidad == idEspecialidad || idEspecialidad == 0) &&
                                (consultorio.idConsultorio == idConsultorio || idConsultorio == 0) &&
                                (asistenteClinica.id_clinica == idClinica || idClinica == 0) &&
                                (asistenteEmpresa.id_empresa == idEmpresa || idEmpresa == 0) &&
                                cita.HoraInicio.Month == mes && cita.HoraInicio.Year == ano
                            select new {
                                cita.idEstatus,
                                cita.Tb_Estatus.Descripcion
                            }
                        ).GroupBy(g => new
                        {
                            idEstado = g.idEstatus,
                            estado   = g.Descripcion
                        }).Select(e => new dtoReporteGeneralCitas
                        {
                            idEstado = e.Key.idEstado,
                            estado   = e.Key.estado,
                            cantidad = e.Count()
                        }).ToList();
                }
                else if (tipoCliente == "S")
                {
                    reporte =
                        (
                            from cita in vCnx.Tb_Cita
                            join agenda in vCnx.Tb_Agenda on cita.idAgenda equals agenda.idAgenda
                            join consultorio in vCnx.Tb_Consultorio on agenda.idConsultorio equals consultorio.idConsultorio
                            join asistenteMedico in vCnx.T_AsistenteMedico on consultorio.idCliente equals asistenteMedico.id_medico
                            where
                                asistenteMedico.id_asistente == idCliente &&
                                cita.HoraInicio.Month == mes && cita.HoraInicio.Year == ano
                            select new {
                                cita.idEstatus,
                                cita.Tb_Estatus.Descripcion
                            }
                        ).GroupBy(g => new
                        {
                            idEstado = g.idEstatus,
                            estado   = g.Descripcion
                        }).Select(e => new dtoReporteGeneralCitas
                        {
                            idEstado = e.Key.idEstado,
                            estado   = e.Key.estado,
                            cantidad = e.Count()
                        }).ToList();
                }
                else if (tipoCliente == "M") {
                    reporte = vCnx.Tb_Cita.Where(tbc => tbc.Tb_Agenda.Tb_Consultorio.idCliente == idCliente && tbc.HoraInicio.Month == mes && tbc.HoraInicio.Year == ano)
                        .GroupBy(g => new {
                            idEstado = g.idEstatus,
                            estado   = g.Tb_Estatus.Descripcion
                        }).Select(e => new dtoReporteGeneralCitas
                        {
                            idEstado = e.Key.idEstado,
                            estado   = e.Key.estado,
                            cantidad = e.Count()
                        }).ToList();
                }

                return reporte;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoReporteCita> getReporteDetalladoCitas(string miSesion, int idEmpresa, int idClinica, int idEspecialidad, int idConsultorio, int idMedico, int idEstatus, int mes, int ano) {
            Tupla<int, string> datos = null;

            try
            {
                datos = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                return this.getReporteDetalladoCitas(datos.item1, datos.item2, idEmpresa, idClinica, idEspecialidad, idConsultorio, idMedico, idEstatus, mes, ano);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public List<dtoReporteCita> getReporteDetalladoCitas(int idCliente, int idEmpresa, int idClinica, int idEspecialidad, int idConsultorio, int idMedico, int idEstatus, int mes, int ano) {
            string tipoCliente = "";

            try
            {
                tipoCliente = ServicioUtilidad.buscarTipoCliente(vCnx, idCliente);

                return this.getReporteDetalladoCitas(idCliente, tipoCliente, idEmpresa, idClinica, idEspecialidad, idConsultorio, idMedico, idEstatus, mes, ano);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private List<dtoReporteCita> getReporteDetalladoCitas(int idCliente, string tipoCliente, int idEmpresa, int idClinica, int idEspecialidad, int idConsultorio, int idMedico, int idEstatus, int mes, int ano) {
            try { 
                List<dtoReporteCita> reporte = null;

                if (string.IsNullOrEmpty(tipoCliente)) return null;

                if (tipoCliente == "A")
                {

                    reporte =
                        (
                            from cita in vCnx.Tb_Cita
                            join agenda in vCnx.Tb_Agenda on cita.idAgenda equals agenda.idAgenda
                            join consultorio in vCnx.Tb_Consultorio on agenda.idConsultorio equals consultorio.idConsultorio into cons_
                            from cons in cons_.DefaultIfEmpty()
                            join clinica in vCnx.Tb_Clinica on cons.id_clinica equals clinica.idClinica into cli_
                            from cli in cli_.DefaultIfEmpty()
                            join empresa in vCnx.T_MedicoEmpresa on cons.idCliente equals empresa.id_medico into emp_
                            from emp in emp_.DefaultIfEmpty()
                            join cliente in vCnx.Tb_ClienteEspecialidad on cons.idCliente equals cliente.idCliente
                            where
                                (cliente.idEspecialidad == idEspecialidad || idEspecialidad == 0) &&
                                (cons.idConsultorio == idConsultorio || idConsultorio == 0) &&
                                (cli.idClinica == idClinica || idClinica == 0) &&
                                (emp.id_empresa == idEmpresa || idEmpresa == 0) &&
                                (cita.idEstatus == idEstatus || idEstatus == 0) &&
                                cita.HoraInicio.Month == mes && cita.HoraInicio.Year == ano
                            select new dtoReporteCita {
                                idCita           = cita.idCita,
                                idEstatus        = cita.idEstatus,
                                estatus          = cita.Tb_Estatus.Descripcion,
                                nombrePaciente   = cita.Tb_Cliente.Nombre,
                                apellidoPaciente = cita.Tb_Cliente.Apellido,
                                nombreMedico     = cons.Tb_Cliente.Nombre,
                                apellidoMedico   = cons.Tb_Cliente.Apellido,
                                idRazon          = cita.idRazon,
                                razon            = cita.Tb_Razon.Descripcion,
                                idConsultorio    = cons.idConsultorio,
                                consultorio      = cons.Nombre,
                                idClinica        = cli.idClinica,
                                clinica          = cli.Descripcion,
                                idEmpresa        = emp.id_empresa,
                                empresa          = emp.T_Empresa.nombre
                            }
                        ).ToList();
                }
                else if (tipoCliente == "L")
                {
                    reporte =
                        (
                            from cita in vCnx.Tb_Cita
                            join agenda in vCnx.Tb_Agenda on cita.idAgenda equals agenda.idAgenda
                            join consultorio in vCnx.Tb_Consultorio on agenda.idConsultorio equals consultorio.idConsultorio
                            join asistenteMedico in vCnx.T_AsistenteMedico on consultorio.idCliente equals asistenteMedico.id_medico
                            join clienteEsp in vCnx.Tb_ClienteEspecialidad on consultorio.idCliente equals clienteEsp.idCliente
                            join asistenteClinica in vCnx.T_AsistenteClinica on asistenteMedico.id_asistente equals asistenteClinica.id_asistente
                            join asistenteEmpresa in vCnx.T_AsistenteEmpresa on asistenteMedico.id_asistente equals asistenteEmpresa.id_asistente
                            where
                                asistenteMedico.id_asistente == idCliente &&
                                (clienteEsp.idEspecialidad == idEspecialidad || idEspecialidad == 0) &&
                                (consultorio.idConsultorio == idConsultorio || idConsultorio == 0) &&
                                (asistenteClinica.id_clinica == idClinica || idClinica == 0) &&
                                (asistenteEmpresa.id_empresa == idEmpresa || idEmpresa == 0) &&
                                (cita.idEstatus == idEstatus || idEstatus == 0) &&
                                cita.HoraInicio.Month == mes && cita.HoraInicio.Year == ano
                            select new dtoReporteCita {
                                idCita           = cita.idCita,
                                idEstatus        = cita.idEstatus,
                                estatus          = cita.Tb_Estatus.Descripcion,
                                nombrePaciente   = cita.Tb_Cliente.Nombre,
                                apellidoPaciente = cita.Tb_Cliente.Apellido,
                                nombreMedico     = consultorio.Tb_Cliente.Nombre,
                                apellidoMedico   = consultorio.Tb_Cliente.Apellido,
                                idRazon          = cita.idRazon,
                                razon            = cita.Tb_Razon.Descripcion,
                                idConsultorio    = consultorio.idConsultorio,
                                consultorio      = consultorio.Nombre,
                                idClinica        = asistenteClinica.id_clinica,
                                clinica          = asistenteClinica.Tb_Clinica.Descripcion,
                                idEmpresa        = asistenteEmpresa.id_empresa,
                                empresa          = asistenteEmpresa.T_Empresa.nombre
                            }
                        ).ToList();
                }
                else if (tipoCliente == "S")
                {
                    reporte =
                        (
                            from cita in vCnx.Tb_Cita
                            join agenda in vCnx.Tb_Agenda on cita.idAgenda equals agenda.idAgenda
                            join consultorio in vCnx.Tb_Consultorio on agenda.idConsultorio equals consultorio.idConsultorio
                            join asistenteMedico in vCnx.T_AsistenteMedico on consultorio.idCliente equals asistenteMedico.id_medico
                            where
                                asistenteMedico.id_asistente == idCliente &&
                                (cita.idEstatus == idEstatus || idEstatus == 0) &&
                                cita.HoraInicio.Month == mes && cita.HoraInicio.Year == ano
                            select new dtoReporteCita {
                                idCita           = cita.idCita,
                                idEstatus        = cita.idEstatus,
                                estatus          = cita.Tb_Estatus.Descripcion,
                                nombrePaciente   = cita.Tb_Cliente.Nombre,
                                apellidoPaciente = cita.Tb_Cliente.Apellido,
                                nombreMedico     = consultorio.Tb_Cliente.Nombre,
                                apellidoMedico   = consultorio.Tb_Cliente.Apellido,
                                idRazon          = cita.idRazon,
                                razon            = cita.Tb_Razon.Descripcion,
                                idConsultorio    = consultorio.idConsultorio,
                                consultorio      = consultorio.Nombre
                            }
                        ).ToList();
                }
                else if (tipoCliente == "M") {
                    reporte = vCnx.Tb_Cita.Where(tbc => tbc.Tb_Agenda.Tb_Consultorio.idCliente == idCliente && tbc.HoraInicio.Month == mes && tbc.HoraInicio.Year == ano)
                        .Select(e => new dtoReporteCita {
                                idCita           = e.idCita,
                                idEstatus        = e.idEstatus,
                                estatus          = e.Tb_Estatus.Descripcion,
                                nombrePaciente   = e.Tb_Cliente.Nombre,
                                apellidoPaciente = e.Tb_Cliente.Apellido,
                                idRazon          = e.idRazon,
                                razon            = e.Tb_Razon.Descripcion,
                                idConsultorio    = e.Tb_Agenda.idConsultorio,
                                consultorio      = e.Tb_Agenda.Tb_Consultorio.Nombre
                            }).ToList();
                }

                return reporte;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public void exportarReporte(string miSesion, int idEmpresa, int idClinica, int idEspecialidad, int idConsultorio, int idMedico, int idEstatus, int mes, int ano)
        {
            Tupla<int, string> datos = null;

            try
            {
                datos = ServicioUtilidad.buscarIdTipoCliente(vCnx, miSesion);

                this.exportarReporte(datos.item1, datos.item2, idEmpresa, idClinica, idEspecialidad, idConsultorio, idMedico, idEstatus, mes, ano);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        public void exportarReporte(int idCliente, int idEmpresa, int idClinica, int idEspecialidad, int idConsultorio, int idMedico, int idEstatus, int mes, int ano)
        {
            string tipoCliente = "";

            try
            {
                tipoCliente = ServicioUtilidad.buscarTipoCliente(vCnx, idCliente);

                this.exportarReporte(idCliente, tipoCliente, idEmpresa, idClinica, idEspecialidad, idConsultorio, idMedico, idEstatus, mes, ano);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }

        private void exportarReporte(int idCliente, string tipoCliente, int idEmpresa, int idClinica, int idEspecialidad, int idConsultorio, int idMedico, int idEstatus, int mes, int ano)
        {
            List<dtoReporteGeneralCitas> reporte_1 = null;
            List<dtoReporteCita> reporte_2 = null;
            List<object[]> r1 = null, r2 = null;

            try
            {
                reporte_1 = this.getReporteCitas(idCliente, idEmpresa, idClinica, idEspecialidad, idConsultorio, idMedico, mes, ano);

                reporte_2 = this.getReporteDetalladoCitas(idCliente, idEmpresa, idClinica, idEspecialidad, idConsultorio, idMedico, idEstatus, mes, ano);

                if (reporte_1 != null && reporte_2 != null)
                {
                    r1 = new List<object[]>();

                    reporte_1.ForEach(e => r1.Add(e.ToArray()));

                    r2 = new List<object[]>();

                    reporte_2.ForEach(e => r2.Add(e.ToArray()));

                    System.Threading.ThreadPool.QueueUserWorkItem(
                        new System.Threading.WaitCallback(ServicioUtilidad.DescargarArchivo),
                        new Tupla<string, string> { item1 = ServicioUtilidad.CrearArchivoCSV(r1), item2 = "ReporteGeneral_" + DateTime.Now.ToString("dd/MM/yyyy") + ".csv" }
                    );

                    System.Threading.ThreadPool.QueueUserWorkItem(
                        new System.Threading.WaitCallback(ServicioUtilidad.DescargarArchivo),
                        new Tupla<string, string> { item1 = ServicioUtilidad.CrearArchivoCSV(r2), item2 = "ReporteDetallado_" + DateTime.Now.ToString("dd/MM/yyyy") + ".csv" }
                    );
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " " + ex.StackTrace, ex.InnerException);
            }
        }
    }
}