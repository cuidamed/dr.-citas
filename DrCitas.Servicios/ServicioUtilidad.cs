﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.XPath;
using System.Text;
using System.IO;
using System.Web;
using System.Configuration;
using System.Net.Mail;
using DrCitas.Datos;
using System.Data.Objects;

namespace DrCitas.Servicios
{
    public class ServicioUtilidad {
        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Formatear mensaje de Error.</summary>
        /// <param name="pMensaje">Mensaje de Error</param>
        /// <returns>Mensaje de error para mostrar al usuario</returns>
        ///---------------------------------------------------------------------------------------------------------------------
        public static string GenerarMensaje(Exception pException) {
            String vMensaje = pException.Message + "(" + pException.TargetSite.Name + ")";
            if (pException.InnerException != null) vMensaje += " - " + pException.InnerException;

            if (vMensaje.Split(new char[] { ':' }).Count() == 1) return vMensaje;
            return vMensaje.Substring(vMensaje.IndexOf(":") + 1);
        }

        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para enviar un Email al supervisor</summary
        /// <param name="EmailSupervisor"></param>
        ///---------------------------------------------------------------------------------------------------------------------
        public static void NotificarCancelacionCita(int pIdCliente, int pIdUsuarioX, int idUsuariSeguimiento) {

            string Mensaje = "";
            ObjectParameter vResultado = new ObjectParameter("pResultado", "TRUE:");

            using (DrCitasEntidades vCnx = new DrCitasEntidades(FuncionesGlobales.Decry64Decry(System.Configuration.ConfigurationManager.ConnectionStrings["DrCitasEntidades"].ConnectionString))) {

                var Cliente = (
                                from cliente in vCnx.Tb_Cliente
                                join cita in vCnx.Tb_Cita on cliente.idCliente equals cita.idCliente
                                where cliente.idCliente == pIdCliente
                                select new {
                                    cliente.idCliente,
                                    cliente.Correo,
                                    cliente.Celular,
                                    cita.Numero,
                                    cita.HoraInicio
                                }
                        ).FirstOrDefault();

                var idUsuarioSeguimiento = vCnx.Tb_Cliente.First(w => w.idCliente == idUsuariSeguimiento);

                Mensaje = "Le informamos que en fecha: " + DateTime.Now + " fue cancelada la cita pautada para la fecha " + Cliente.HoraInicio + ", la misma fue efectuada por parte de: " + idUsuarioSeguimiento.Nombre + " " + idUsuarioSeguimiento.Apellido;

                NotificarAlCliente
                      (
                          "Cancelacion de la cita numero: " + Cliente.Numero,
                          Mensaje,
                          Cliente.Correo
                      );

                vCnx.Sp_Cita_P_Sms(Cliente.Numero, Mensaje, pIdUsuarioX, vResultado);
            }

        }

        public static bool NotificarAlCliente(string pAsunto, string pMensaje, string pRecibe) {
            /// string pServer, int pPuerto, string pFrom, string pPasswordFrom, string pTo, string pTitulo, string pMensaje
            /// string pAsunto, string pMensaje, string pSentBy, string pRecibe, string pServer, string pUsuario, string pPassword, bool tipo = true
            /// //pServer,25,pSentBy,pPassword,pRecibe,pAsunto,pMensaje
            return EnviarCorreo(pAsunto, pMensaje, "info@drcitas.com", pRecibe, "smtp.drcitas.com", "info@drcitas.com", "Abcd1234");
        }

        /// <summary>
        /// Metodo que envia correos
        /// </summary>
        /// <param name="pAsunto"></param>
        /// <param name="pMensaje"></param>
        /// <param name="pSentBy"></param>
        /// <param name="pRecibe"></param>
        /// <param name="pServer"></param>
        /// <param name="pUsuario"></param>
        /// <param name="pPassword"></param>
        /// string pServer, int pPuerto, string pFrom, string pPasswordFrom, string pTo, string pTitulo, string pMensaje
        /// string pAsunto, string pMensaje, string pSentBy, string pRecibe, string pServer, string pUsuario, string pPassword, bool tipo = true
        public static bool EnviarCorreo(string pAsunto, string pMensaje, string pSentBy, string pRecibe, string pServer, string pUsuario, string pPassword, bool tipo = true) {
            Servicios.Utilidades.Email email = new Utilidades.Email(pServer, 25, pSentBy, pPassword, pRecibe, pAsunto, pMensaje);
            return email.EnviarEmail();
        }

        ///---------------------------------------------------------------------------------------------------------------------
        /// <summary>Operación para enviar un Email al supervisor</summary
        ///---------------------------------------------------------------------------------------------------------------------
        public static void EnviarSMSAlCliente(int pIdAgenda, string pCelular, string pMensaje, int pIdUsuarioX) {
            ObjectParameter vResultado = new ObjectParameter("pResultado", "TRUE:");
            using (DrCitasEntidades vCnx = new DrCitasEntidades(FuncionesGlobales.Decry64Decry(System.Configuration.ConfigurationManager.ConnectionStrings["DrCitasEntidades"].ConnectionString))) {
                vCnx.Sp_Agenda_P_Sms(pIdAgenda, pCelular, pMensaje, pIdUsuarioX, vResultado);
                if (vResultado.Value.ToString() != "TRUE:") throw new Exception(vResultado.Value.ToString());
            }
        }
        ///---------------------------------------------------------------------------------------------------------------------

        ///<summary>Operación para calcular las coordenadas de la dirección.</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public static string CalcularCoordenadas(string pDireccion, string pArchivo) {
            string vLatitud;
            string vLongitud;

            // string pDireccion = this.txtaddress.Text;
            //Geocoding geocodign = new Geocoding();
            //Point point = new Point();

            GetXml(pDireccion, pArchivo);
            Hashtable vPosicion = GetData(pArchivo);

            vLongitud = vPosicion["Longitud"].ToString();
            vLatitud = vPosicion["Latitud"].ToString();

            //this.txtdata.Clear();
            //this.txtdata.Text = String.Format("{0}Dirección= {1} \r\n", this.txtdata.Text, point.Address);
            //this.txtdata.Text = String.Format("{0}Longitud= {1} \r\n", this.txtdata.Text, point.Longitude);
            //this.txtdata.Text = String.Format("{0}Latitud= {1} \r\n", this.txtdata.Text, point.Latitude);
            return string.Format("{0},{1}", vLatitud, vLongitud);
        }

        ///---------------------------------------------------------------------------------------------------------------------
        ///<summary>Operación para regresar los datos en formato XML y lo guarda en un archivo, el XML es lo que nos manda el googlemaps..</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public static void GetXml(string pDireccion, string pArchivo) {
            string vUrl = String.Format("{0}", pDireccion).Replace("#", "").Replace(" ", "%20");

            //http://maps.googleapis.com/maps/api/geocode/xml?address=caracas+23%20de%20enero&sensor=false
            //  string lcUrl = String.Format("http://maps.google.com/maps/geo?q={0}&output=xml&sensor=false&key=AIzaSyBL0dYxqoWJu6RJ0leOPnSZBx54uXMok14", url);
            string lcUrl = String.Format("http://maps.googleapis.com/maps/api/geocode/xml?address={0}&output=xml&sensor=false", vUrl);

            HttpWebRequest loHttp = (HttpWebRequest)WebRequest.Create(lcUrl);
            loHttp.Timeout = 30000;     // 30 secs
            loHttp.UserAgent = "Code Sample Web Client";
            HttpWebResponse loWebResponse = (HttpWebResponse)loHttp.GetResponse();
            Encoding enc = Encoding.GetEncoding(1252);  // Windows default Code Page
            StreamReader loResponseStream = new StreamReader(loWebResponse.GetResponseStream(), enc);

            string lcHtml = loResponseStream.ReadToEnd();

            StreamWriter sw = new StreamWriter(pArchivo, false);
            sw.Write(lcHtml);
            sw.Close();

            loWebResponse.Close();
            loResponseStream.Close();
        }

        ///---------------------------------------------------------------------------------------------------------------------
        ///<summary>Operación para regresar un Hashtable con los datos dirección (address), longitud (longitude), latitud (latitude). Estos datos los extraemos del arhivo XML que se guarda con el GetXml..</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public static Hashtable GetData(string pArchivo) {
            Hashtable data = new Hashtable();
            XPathDocument doc = new XPathDocument(pArchivo);
            string namespaceUri = "http://earth.google.com/kml/2.0";
            XmlNamespaceManager ns = new XmlNamespaceManager(new NameTable());
            XPathNavigator nav = doc.CreateNavigator();
            ns.AddNamespace("bz", namespaceUri);
            string query = "bz:kml/bz:Response/bz:Placemark/bz:Point/bz:coordinates";
            //XPathNodeIterator iter = nav.Select(query, ns);

            XPathNodeIterator iterLat = nav.Select("//location/lat");
            //string lat = (iter.MoveNext()) ? iter.Current.Value : "";

            XPathNodeIterator iterLon = nav.Select("//location/lng");

            //query = "bz:kml/bz:Response/bz:Placemark/bz:address";
            //iter = nav.Select(query, ns);
            // string address = ""; // (iter.MoveNext()) ? iter.Current.Value : "";

            string lon = (iterLon.MoveNext()) ? iterLon.Current.Value : "";
            string lat = (iterLat.MoveNext()) ? iterLat.Current.Value : "";
            //if (coordinates.Split(',').Length == 3)
            //{
            //    lon = coordinates.Split(',')[0].ToString();
            //    lat = coordinates.Split(',')[1].ToString();

            //    data.Add("longitude", lon);
            //    data.Add("latitude", lat);
            //    data.Add("address", address);
            //}
            //else
            //{
            data.Add("Longitud", lon);
            data.Add("Latitud", lat);
            //data.Add("address", address);
            //}
            return data;
        }

        ///---------------------------------------------------------------------------------------------------------------------
        ///<summary>Operación para regresar los datos en formato XML y lo guarda en un archivo, el XML es lo que nos manda el googlemaps..</summary>
        ///---------------------------------------------------------------------------------------------------------------------
        public static void ResizeImage(string OriginalFile, string NewFile, int NewWidth, int MaxHeight, bool OnlyResizeIfWider) {
            //System.Drawing.Image FullsizeImage = System.Drawing.Image.FromFile(OriginalFile);

            //// Prevent using images internal thumbnail
            //FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            //FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

            //if (OnlyResizeIfWider)
            //{
            //    if (FullsizeImage.Width <= NewWidth)
            //    {
            //        NewWidth = FullsizeImage.Width;
            //    }
            //}

            //int NewHeight = FullsizeImage.Height * NewWidth / FullsizeImage.Width;
            //if (NewHeight > MaxHeight)
            //{
            //    // Resize with height instead
            //    NewWidth = FullsizeImage.Width * MaxHeight / FullsizeImage.Height;
            //    NewHeight = MaxHeight;
            //}

            //System.Drawing.Image NewImage = FullsizeImage.GetThumbnailImage(NewWidth, NewHeight, null, IntPtr.Zero);

            //// Clear handle to original file so that we can overwrite it if necessary
            //FullsizeImage.Dispose();

            //// Save resized picture
            //NewImage.Save(NewFile);
        }

        public static int buscarIdCliente(DrCitasEntidades ent, string miSesion) {
            try {
                return buscarIdTipoCliente(ent, miSesion).item1;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + ' ' + ex.StackTrace, ex.InnerException);
            }
        }

        public static string buscarTipoCliente(DrCitasEntidades ent, string miSesion) {
            try {
                return buscarIdTipoCliente(ent, miSesion).item2;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + ' ' + ex.StackTrace, ex.InnerException);
            }
        }

        public static string buscarTipoCliente(DrCitasEntidades ent, int idCliente) {
            try {
                return ent.Tb_Cliente.Where(tbe => tbe.idCliente == idCliente).Select(e => e.TipoCliente).FirstOrDefault();
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + ' ' + ex.StackTrace, ex.InnerException);
            }
        }

        public static Tupla<int, string> buscarIdTipoCliente(DrCitasEntidades ent, string miSesion) {
            Tupla<int, string> idTipo = null;

            try {
                idTipo =
                    (
                        from usuario in ent.T_ClienteUsuario
                        join sesion in ent.T_Sesion on usuario.id_usuario equals sesion.id_usuario
                        where sesion.sesion == miSesion && sesion.sesionActiva == 1
                        select new Tupla<int, string> { item1 = usuario.id_cliente, item2 = usuario.Tb_Cliente.TipoCliente }
                    ).FirstOrDefault();

                return idTipo;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message + ' ' + ex.StackTrace, ex.InnerException);
            }
        }

        public static string manageImageProfile(int idCliente, int tipo, int modo, string imagen = "", string extension = "") {
            string temp = "", response = "", file = "";
            string[] ext = null, files = null;
            byte[] img = null;

            switch (tipo) {
                case 3: temp = "Medico";
                    break;
                case 5: temp = "Paciente";
                    break;
                default: break;
            }

            if (modo == 0) {
                if (!Directory.Exists("C:\\Imagenes Aplicativos\\DrCitas\\" + temp)) {
                    Directory.CreateDirectory("C:\\Imagenes Aplicativos\\DrCitas\\" + temp);
                }

                if (!Directory.Exists("C:\\Imagenes Aplicativos\\DrCitas\\" + temp + "\\" + idCliente)) {
                    Directory.CreateDirectory("C:\\Imagenes Aplicativos\\DrCitas\\" + temp + "\\" + idCliente);
                }

                if (!Directory.Exists("C:\\Imagenes Aplicativos\\DrCitas\\" + temp + "\\" + idCliente + "\\Perfil")) {
                    Directory.CreateDirectory("C:\\Imagenes Aplicativos\\DrCitas\\" + temp + "\\" + idCliente + "\\Perfil");
                }
            }

            ext = new string[] { ".jpg", ".jpeg", ".gif", ".png" };

            if (Directory.Exists("C:\\Imagenes Aplicativos\\DrCitas\\" + temp + "\\" + idCliente + "\\Perfil")) {
                files =
                    Directory.GetFiles(
                        "C:\\Imagenes Aplicativos\\DrCitas\\" + temp + "\\" + idCliente + "\\Perfil",
                        "*.*"
                    ).Where(s => ext.Any(e => s.EndsWith(e))).ToArray();

                foreach (var f in files) {
                    if (System.Text.RegularExpressions.Regex.IsMatch(f, temp + idCliente.ToString())) {
                        if (modo == 0)
                            File.Delete(f);
                        else {
                            extension = System.Text.RegularExpressions.Regex.Match(f, ".jpg|.jpeg|.gif|.png").Value.Substring(1);
                            img = File.ReadAllBytes(f);
                        }
                        break;
                    }
                }
            }

            if (modo == 0) {
                if (Directory.Exists("C:\\Imagenes Aplicativos\\DrCitas\\" + temp + "\\" + idCliente + "\\Perfil")) {
                    file = temp + idCliente + DateTime.Now.ToString("_yyyyMMdd_HH_mm_ss") + "." + extension;
                    File.WriteAllBytes(
                        "C:\\Imagenes Aplicativos\\DrCitas\\" + temp + "\\" + idCliente + "\\Perfil\\" + file,
                        Convert.FromBase64String(imagen)
                    );
                }
                response = file;
            }
            else
                response = (img != null ? "data:image/" + extension + ";base64," + Convert.ToBase64String(img) : "");

            return response;
        }

        public static string CrearArchivoCSV(List<object[]> data) {
            StringBuilder csvExport = new StringBuilder();
            string formato = "";
            int elementos = data.FirstOrDefault().Length;

            for (int i = 0; i < elementos; i++)
                formato += "\"{" + i + "}\"" + (i != elementos - 1 ? "," : "");

            data.ForEach(e => {
                csvExport.AppendLine(string.Format(formato, e));
            });

            return csvExport.ToString();
        }

        public static void DescargarArchivo(object data) {
            Tupla<string, string> info = (Tupla<string, string>)data;
            byte[] datos = new ASCIIEncoding().GetBytes(info.item1);

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "APPLICATION/OCTET-STREAM";
            HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename=" + info.item2);
            HttpContext.Current.Response.OutputStream.Write(datos, 0, datos.Length);
            HttpContext.Current.Response.End();
        }
    }
        
    public class Tupla<T, S> {

        public T item1 { get; set; }

        public S item2 { get; set; }
    }
}