using System;
using System.Collections.Generic;
using System.Text;

namespace DrCitas.Servicios.Utilidades
{
    public class Email
    {
        public Email()
        {
        }

        public Email(string pServer, int pPuerto, string pFrom, string pPasswordFrom, string pTo, string pTitulo, string pMensaje)
        {
            
            _Server = pServer;
            _Puerto = pPuerto;
            _From = pFrom;
            _PasswordFrom = pPasswordFrom;
            _To = pTo;
            _Titulo = pTitulo;
            _Mensaje = pMensaje;

        }

        public bool EnviarEmail()
        {
            try
            {
                System.Net.Mail.MailMessage Mail; 
                Mail = new System.Net.Mail.MailMessage(_From, _To, _Titulo, _Mensaje);

                System.Net.Mail.SmtpClient smtpMail = new System.Net.Mail.SmtpClient(_Server);

                Mail.IsBodyHtml = true;
                smtpMail.EnableSsl = false;
                smtpMail.UseDefaultCredentials = false;
                smtpMail.Port = _Puerto;
                smtpMail.Host = _Server;
                smtpMail.Credentials = new System.Net.NetworkCredential(_From, _PasswordFrom);
                
                smtpMail.Send(Mail);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string EnviarEmail2()
        {
            try
            {
                System.Net.Mail.MailMessage Mail;
                Mail = new System.Net.Mail.MailMessage(_From, _To, _Titulo, _Mensaje);

                System.Net.Mail.SmtpClient smtpMail = new System.Net.Mail.SmtpClient(_Server);

                Mail.IsBodyHtml = true;
                smtpMail.EnableSsl = false;
                smtpMail.UseDefaultCredentials = false;
                smtpMail.Port = _Puerto;
                smtpMail.Host = _Server;
                smtpMail.Credentials = new System.Net.NetworkCredential(_From, _PasswordFrom);
                smtpMail.Send(Mail);
                return "Enviado";
            }
            catch (Exception)
            {
                throw;
            }

        }


        private string _Server;

        public string Server
        {
            get { return _Server; }
            set { _Server = value; }
        }
        private int _Puerto;

        public int Puerto
        {
            get { return _Puerto; }
            set { _Puerto = value; }
        }
        private string _From;

        public string From
        {
            get { return _From; }
            set { _From = value; }
        }
        private string _PasswordFrom;

        public string PasswordFrom
        {
            get { return _PasswordFrom; }
            set { _PasswordFrom = value; }
        }
        private string _To;

        public string To
        {
            get { return _To; }
            set { _To = value; }
        }
        private string _Mensaje;

        public string Mensaje
        {
            get { return _Mensaje; }
            set { _Mensaje = value; }
        }
        private string _Titulo;

        public string Titulo
        {
            get { return _Titulo; }
            set { _Titulo = value; }
        }
    }
}