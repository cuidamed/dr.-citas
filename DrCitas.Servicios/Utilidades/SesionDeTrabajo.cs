﻿using System;
using System.Linq;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data;
using System.Data.Objects.DataClasses;
using DrCitas.Datos;

namespace DrCitas.Servicios.Utilidades
{
    public sealed class SesionDeTrabajo : ISesionDeTrabajo, IDisposable
    {
		private ObjectContext vSesion;
        private bool estaEnTransaccion = false;

        public ObjectContext Sesion
		{
            get { return vSesion; }
		}

		public bool EstaEnTransaccion
		{
			get { return estaEnTransaccion; }
		}

		public SesionDeTrabajo()
		{
            vSesion = new DrCitasEntidades();
		}

		public SesionDeTrabajo(EntityConnection connection)
		{
            vSesion = new DrCitasEntidades(connection);
		}

        public SesionDeTrabajo(string connectionString)
		{
            vSesion = new DrCitasEntidades(connectionString);
		}

		public void IniciarTransaccion()
		{
            vSesion = new DrCitasEntidades();
			estaEnTransaccion = true;
		}

		public void Rollback()
		{
            vSesion = new DrCitasEntidades();
			estaEnTransaccion = false;
		}

		public void Commit()
		{
			try
			{
				if(estaEnTransaccion == false)
					throw new EntityException("Unidad de Trabajo no puede hacer commit ya que no está en sesión");
                vSesion.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
				estaEnTransaccion = false;
			}
			catch(System.Data.SqlClient.SqlException exception)
			{
                vSesion = new DrCitasEntidades();
				estaEnTransaccion = false;
				throw new EntitySqlException("Ocurrió un error durante el Commit.", exception);
			}
			catch(Exception exception)
			{
				this.Rollback();
				throw new EntityException("Ocurrió un error durante el Commit.", exception);
			}
		}

		public void MarcarNuevo<T>(T entity) where T : class
		{
            vSesion.AddObject(typeof(T).Name, entity);
		}

		public void MarcarModificado<T>(T entity) where T : class
		{
			var entidadModificada = entity as EntityObject;
			if(entidadModificada == null)
				throw new EntityException("la entidad no es de tipo EntityObject.");
            if (!vSesion.IsAttachedTo(entity))
                vSesion.AttachTo(typeof(T).Name, entity);
			if(entidadModificada.EntityState != EntityState.Modified)
                vSesion.ObjectStateManager.ChangeObjectState(entity, EntityState.Modified);
		}

		public void MarcarEliminado<T>(T entity) where T : class
		{
			var entidadModificada = entity as EntityObject;
			if(entidadModificada == null)
				throw new EntityException("la entidad no es de tipo EntityObject.");
            if (!vSesion.IsAttachedTo(entity))
                vSesion.AttachTo(typeof(T).Name, entity);
			if(entidadModificada.EntityState != EntityState.Deleted)
                vSesion.ObjectStateManager.ChangeObjectState(entity, EntityState.Deleted);
		}

		#region Implementation of IDisposable

		public void Dispose()
		{
            if (vSesion != null)
			{
                if (vSesion.Connection.State != ConnectionState.Closed)
                    vSesion.Connection.Close();
                vSesion.Dispose();
			}
			GC.SuppressFinalize(true);
		}

		#endregion
	}
}
